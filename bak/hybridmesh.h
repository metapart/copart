/**
 *   @file hybridmesh.h
 *
 *   @author Maria Predari
 *
 *   @defgroup Struct Common structures.
 *   @brief Hybrid Mesh.
 *
 */


#ifndef __HYBRIDMESH_H__
#define __HYBRIDMESH_H__

#include "struct.h"
#include "mesh.h"


//@{

/* *********************************************************** */

void HcellCenter(HybridMesh * msh,  int i, double G[3]);
void HcellCenters(HybridMesh * msh, double * centers);

/** Hybridmesh-to-graph conversion. */
void Hybridmesh2Graph(HybridMesh * hmesh,  	 /** < [in] input hybridmesh */
		      Graph * graph  	/** > [out] graph */
		      );
int saveVTKHybridMesh(const char * output, 		/** > [out] output VTKfile */
		      HybridMesh * hmesh);		        /** < [in] input hybridmesh */

/** Print hybridMesh */
void printHybridMesh(HybridMesh * hmesh		/** < [in] input hybridmesh */
		     );		
/** Free memory allocated within hybridMesh stucture */		
void freeHybridMesh (HybridMesh * hmesh		/** < [in] input hybridmesh */
		     ); 

int loadHybridMesh(HybridMesh * hmesh, int nb_files, ...);
int saveHybridMesh(const char * output, HybridMesh * mesh);
void addHybridMeshVariable(HybridMesh * hmesh, const char * id, MeshVariableType type,
			   int nb_components, int * data);

void addHybridMeshCellVariableI(HybridMesh * hmesh, 
				const char * id, 
				int nb_components, 
				int * data
				);

/* *********************************************************** */
//@}

#endif // HYBRIDMESH
