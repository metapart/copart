/* Mesh Intersection */
/* Author(s): aurelien.esnard@labri.fr */

/* 
   Samples:
   $ ./mesh-intersection -z 1.0 -Z 1.0 ../../data/unit-cube-hexa-10x10x10.txt ../../data/unit-cube-tetra-5638.txt 
   $ ./mesh-intersection  ../../data/more/cerfacs/cylinder/cylinder.avtp.txt ../../data/more/cerfacs/cylinder/cylinder.avbp.part2.txt 
*/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <getopt.h>

#include "metapart.h"

/* *********************************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0], "[-z shiftz] [-Z vtkshiftz] meshA.txt meshB.txt [cellA cellB]");
  exit (EXIT_FAILURE);  
}

/* *********************************************************** */

int main (int argc, char *argv[]) 
{  
  int intersect2cells = 0;
  
  /* 0) input arguments */
  
  int opt;
  double shiftz = 0.0;
  double vtkshiftz = 0.0;
  while ((opt = getopt (argc, argv, "z:Z:")) != -1) {
    switch (opt) {
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
    case 'Z':
      vtkshiftz = strtod(optarg, NULL);     
      break;
    default:
      usage(argc,argv);
    }
  }
      
  if(argc - optind != 2 && argc - optind != 4) usage(argc, argv);

  char* meshfileA = argv[optind];
  char* meshfileB = argv[optind+1];
  int cellA = -1;
  int cellB = -1;
  if(argc - optind == 4) {
    cellA = atoi(argv[optind+2]); 
    cellB = atoi(argv[optind+3]); 
    assert(cellA >= 0 && cellB >= 0);
    intersect2cells = 1;
  }
  
  /* 1) load mesh A & B */
  
  // Mesh _meshA, _meshB;
  Mesh meshA, meshB;
  
  printf("Loading mesh A: %s\n", meshfileA);  
  loadMesh(meshfileA, &meshA); 
  printf("Loading mesh B: %s\n", meshfileB);  
  loadMesh(meshfileB, &meshB); 

  // debug
  // simplifyMesh(&_meshA, &meshA); 
  // simplifyMesh(&_meshB, &meshB); 
  // for(int i = 0 ; i < 3*meshA.nb_nodes ; i++) meshA.nodes[i] *= 100.0;
  // for(int i = 0 ; i < 3*meshB.nb_nodes ; i++) meshB.nodes[i] *= 100.0;
  // subdivideMeshIntoTetrahedra(&_meshA, &meshA); 
  // subdivideMeshIntoTetrahedra(&_meshB, &meshB); 

  printf("Shifting mesh B on Z: %.2f\n", shiftz);  
  shiftMesh(&meshB, 0.0, 0.0, shiftz);

  // scaleMesh(&meshA, 1.0, 1.0, 0.1);       
  // scaleMesh(&meshB, 1.0001, 1.0001, 1.0); // for CERFACS cylinder test case

  printf("nb cells in mesh A: %d\n", meshA.nb_cells);  
  printf("nb cells in mesh B: %d\n", meshB.nb_cells);  
  
#ifdef DEBUG
  printf("\n********************* MESH A *********************\n");
  printMesh(&meshA);
  printf("\n********************* MESH B *********************\n");
  printMesh(&meshB);  
#endif
  
  /* 2) intersect two cells only */
  
  if(intersect2cells) {
    
    printf("*************** INTERSECT TWO CELLS ***************\n"); 
    int cellsizeA = ELEMENT_SIZE[meshA.elm_type];    
    int cellsizeB = ELEMENT_SIZE[meshB.elm_type];    
    double vol = 0.0;
    int intersect = intersectCells(&meshA, &meshB, cellA, cellB, &vol);
    printf("Intersection of cell A%d and B%d: intersect = %d, volume = %.6f\n", cellA, cellB, intersect, vol);    
    printf("cell A%d = {", cellA);
    for(int k = 0 ; k < cellsizeA ; k++) printf("%d ", meshA.cells[cellA*cellsizeA+k]);
    printf("}\n");      
    printf("cell B%d = {", cellB);
    for(int k = 0 ; k < cellsizeB ; k++) printf("%d ", meshB.cells[cellB*cellsizeB+k]);
    printf("}\n");          
    
  }
  
  /* 3) intersect two meshes */ 
  
  printf("*************** INTERSECT MESH A & B ***************\n"); 
  
  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;
  double * volumes = NULL;  
  
  if(!intersect2cells) {
    intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, &volumes);
    const int minwgt = 0, maxwgt = 10;
    computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, minwgt, maxwgt);   
    printf("nb interedges found = %d\n", nbinteredges);
    
    // save interedges
    FILE* finteredges = fopen("./interedges.txt", "w");
    for(int k = 0 ; k < nbinteredges ; k++)
      fprintf(finteredges,"%d %d\n",interedges[2*k], interedges[2*k+1]);
    fclose(finteredges);
    printf("Saving interedges: %s\n", "interedges.txt");
    printf("sort it: cat interedges.txt | sort -n -k1 -k2 | uniq\n");
    
  }
  
  /* 4) saving vtk files (warning: shifting modify meshB) */
  
  printf("*************** SAVING VTK FILES ***************\n"); 
  
  int * selectionA = malloc(meshA.nb_cells*sizeof(int));
  int * selectionB = malloc(meshB.nb_cells*sizeof(int));
  memset(selectionA, 0, meshA.nb_cells*sizeof(int));
  memset(selectionB, 0, meshB.nb_cells*sizeof(int));

  if(intersect2cells) {
    selectionA[cellA] = 1;
    selectionB[cellB] = 1;
    addMeshVariable(&meshA, "selection", CELL_INTEGER, 1, selectionA);
    addMeshVariable(&meshB, "selection", CELL_INTEGER, 1, selectionB);
  }
  else {
    for(int k = 0 ; k < nbinteredges ; k++)
      selectionA[interedges[2*k]] = selectionB[interedges[2*k+1]] = 1;
    
    addMeshVariable(&meshA, "couplingzone", CELL_INTEGER, 1, selectionA);
    addMeshVariable(&meshB, "couplingzone", CELL_INTEGER, 1, selectionB);
  }
  
  printf("Shifting VTK mesh B on Z: %.2f\n", vtkshiftz);  
  shiftMesh(&meshB, 0.0, 0.0, vtkshiftz);

  Mesh cpl;  
  if(!intersect2cells)  
    generateCouplingMesh(&meshA, &meshB, nbinteredges, interedges, 0.0, &cpl);
  
  char * outfileA = "meshA.vtk";
  printf("Saving VTK mesh A: %s\n", outfileA);
  saveVTKMesh(outfileA, &meshA);
  
  char * outfileB = "meshB.vtk";
  printf("Saving VTK mesh B: %s\n", outfileB);
  saveVTKMesh(outfileB, &meshB);
  
  if(!intersect2cells) {
    char * outfileCPL = "cpl.vtk";
    printf("Saving VTK mesh: %s\n", outfileCPL);
    saveVTKMesh(outfileCPL, &cpl);
    freeMesh(&cpl);    
  }
  
  /* free */
  // freeMesh(&_meshA);
  // freeMesh(&_meshB);
  freeMesh(&meshA);
  freeMesh(&meshB);
  free(selectionA);
  free(selectionB);
  free(interedges);
  if(!intersect2cells) { free(weights); free(volumes); }
  
  return EXIT_SUCCESS;
}
