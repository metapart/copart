/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>

#include "matrixcomm.h"
#include "hypergraph.h"



/* *********************************************************** */

void createIdentityMatrix (int M, const int *perm, Hypergraph *out) {
  out->nvtxs = out->nhedges = M;
  out->eptr = malloc ((M+1) * sizeof (int));
  out->eind = malloc (M * sizeof (int));
  for (int i = 0; i < M; i++) {
    out->eptr[i] = i;
    out->eind[i] = (perm ? perm[i] : i);
  }
  out->eptr[M] = M;
  out->vwgts = out->hewgts = NULL;
}

/* *********************************************************** */

void create1dRepartMatrix (int M, int N, const int *perm, Hypergraph *out) {
  int *p = NULL;
  if (perm == NULL) {
    p = malloc (M * sizeof (int));
    for (int i = 0; i < M; i++)
      p[i] = i;
    perm = p;
  }
  
  out->nvtxs = N;
  out->nhedges = M;
  out->vwgts = out->hewgts = NULL;
  
  out->eptr = malloc ((out->nhedges+1) * sizeof (int));
  out->eptr[0] = 0;
  for (int i = 0; i < M; i++) {
    out->eptr[i+1] = out->eptr[i] +
		      ceil ((perm[i]+1) * (double)N/M) -
		      floor (perm[i] * (double)N/M);
  }
  
  out->eind = malloc (out->eptr[M] * sizeof (int));
  int k = 0;
  for (int i = 0; i < M; i++) {
    for (int j = floor (perm[i] * (double)N/M);
	 j < ceil ((perm[i]+1) * (double)N/M);
	 j++) {
      out->eind[k++] = j;
    }
  }
  
  free (p);
}

/* *********************************************************** */

void createDiag1dMatrix (int M, int N, const int *perm, Hypergraph *out) {
  Hypergraph id, _1d;
  if (M < N) {
    createIdentityMatrix (M, perm, &id);
    create1dRepartMatrix (M, N-M, perm, &_1d);
    concatH (&id, &_1d, out);
  }
  else { /* M > N */
    createIdentityMatrix (N, NULL, &id);
    create1dRepartMatrix (M-N, N, NULL, &_1d);
    if (perm) {
      Hypergraph tmp;
      concatV (&id, &_1d, &tmp);
      permuteHyperedges (&tmp, perm, out);
      freeHypergraph (&tmp);
    }
    else
      concatV (&id, &_1d, out);
  }
  freeHypergraph (&id);
  freeHypergraph (&_1d);
}

/* *********************************************************** */

void concatV (const Hypergraph *a, const Hypergraph *b, Hypergraph *out) {
  assert (a->nvtxs == b->nvtxs);
  
  out->nvtxs = a->nvtxs;
  out->nhedges = a->nhedges + b->nhedges;
  
  out->eptr = malloc ((out->nhedges+1) * sizeof (int));
  memcpy (out->eptr, a->eptr, (a->nhedges + 1) * sizeof (int));
  for (int i = 1; i <= b->nhedges; i++)
    out->eptr[a->nhedges+i] = out->eptr[a->nhedges] + b->eptr[i];
    
  out->eind = malloc ((a->eptr[a->nhedges] + b->eptr[b->nhedges]) * sizeof (int));
  memcpy (out->eind, a->eind, a->eptr[a->nhedges] * sizeof (int));
  memcpy (out->eind + a->eptr[a->nhedges], b->eind, b->eptr[b->nhedges] * sizeof (int));
  
  out->vwgts = out->hewgts = NULL;
}

/* *********************************************************** */

void concatH (const Hypergraph *a, const Hypergraph *b, Hypergraph *out) {
  assert (a->nhedges == b->nhedges);
  
  out->nvtxs = a->nvtxs + b->nvtxs;
  out->nhedges = a->nhedges;
  
  out->eptr = malloc ((out->nhedges+1) * sizeof (int));
  for (int i = 0; i <= out->nhedges; i++)
    out->eptr[i] = a->eptr[i] + b->eptr[i];
  
  out->eind = malloc (out->eptr[out->nhedges] * sizeof (int));
  for (int e = 0; e < out->nhedges; e++) {
    for (int i = a->eptr[e]; i < a->eptr[e+1]; i++)
      out->eind[out->eptr[e] + i-a->eptr[e]] = a->eind[i];
    for (int i = b->eptr[e]; i < b->eptr[e+1]; i++)
      out->eind[out->eptr[e] + a->eptr[e+1]-a->eptr[e] + i-b->eptr[e]] = a->nvtxs + b->eind[i];
  }
  
  out->vwgts = out->hewgts = NULL;
}

/* *********************************************************** */

void permuteHyperedges (const Hypergraph *in, const int *perm, Hypergraph *out) {
  out->nvtxs = in->nvtxs;
  out->nhedges = in->nhedges;
  out->vwgts = out->hewgts = NULL;
  
  out->eptr = malloc ((out->nhedges+1) * sizeof (int));
  out->eind = malloc (in->eptr[in->nhedges] * sizeof (int));
  out->eptr[0] = 0;
  for (int i = 0; i < out->nhedges; i++) {
    out->eptr[i+1] = out->eptr[i] + in->eptr[perm[i]+1] - in->eptr[perm[i]];
    for (int j = 0; j < in->eptr[perm[i]+1] - in->eptr[perm[i]]; j++)
      out->eind[out->eptr[i]+j] = in->eind[in->eptr[perm[i]]+j];
  }
}

/* *********************************************************** */

void sparse2dense (const Hypergraph *h, int *out) {
  memset (out, 0, h->nvtxs * h->nhedges * sizeof (int));
  for (int e = 0; e < h->nhedges; e++)
    for (int i = h->eptr[e]; i < h->eptr[e+1]; i++)
      out [e*h->nvtxs + h->eind[i]] = 1;
}

/* *********************************************************** */

void dense2sparse (const int *m, int M, int N, Hypergraph *out) {
  out->nvtxs = N;
  out->nhedges = M;
  
  int nnz = 0;
  for (int i = 0; i < M; i++)
    for (int j = 0; j < N; j++)
      if (m[i*N + j] != 0)
	nnz++;
  
  out->eptr = malloc ((out->nhedges+1) * sizeof (int));
  out->eind = malloc (nnz * sizeof (int));
  
  out->eptr[0] = 0;
  int k = 0;
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++)
      if (m[i*N + j] != 0)
	out->eind[k++] = j;
    out->eptr[i+1] = k;
  }
  
  out->hewgts = out->vwgts = NULL;
}

struct element {
  int r, c;
  struct element *cprev, *cnext;
  struct element *rprev, *rnext;
};

struct line {
  int is_row;
  int n;	/* Line/column index */
  int r;	/* Number of remaining elements in this row/column */
  struct line *prev, *next;
  struct element *e;
};

/* *********************************************************** */

int computeComms (int *m, int M, int N, const int *w) {
  struct line *rows = malloc (M * sizeof (struct line));
  struct line *columns = malloc (N * sizeof (struct line));
  
  for (int i = 0; i < M; i++) {
    rows[i].is_row = 1;
    rows[i].n = i;
    rows[i].r = 0;
    rows[i].prev = rows[i].next = NULL;
    rows[i].e = 0;
  }
  for (int i = 0; i < N; i++) {
    columns[i].is_row = 0;
    columns[i].n = i;
    columns[i].r = 0;
    columns[i].prev = columns[i].next = NULL;
    columns[i].e = 0;
  }
  
  int nnz = 0;
  for (int i = 0; i < M; i++)
    for (int j = 0; j < N; j++)
      if (m[i*N + j] != 0)
	nnz++;
  
  /* Search elements to be computed */
  struct element *el = malloc (nnz * sizeof (struct element));
  int k = 0;
  for (int i = 0; i < M; i++)
    for (int j = 0; j < N; j++)
      if (m[i*N + j] != 0) {
	struct element *e = &el[k++];
	e->r = i;
	e->c = j;
	/* Add in row element list */
	if (rows[i].e)
	  rows[i].e->rprev = e;
	e->rnext = rows[i].e;
	e->rprev = NULL;
	rows[i].e = e;
	rows[i].r++;
	/* Add in column element list */
	if (columns[j].e)
	  columns[j].e->cprev = e;
	e->cnext = columns[j].e;
	e->cprev = NULL;
	columns[j].e = e;
	columns[j].r++;
      }
  assert (k == nnz);
  
  /* Compute total communcation/weight */
  int total_weight = 0;
  for (int i = 0; i < M; i++)
    total_weight += w[i];
  
  /* Search row/column with only one element */
  struct line *one_element_line = NULL;
  for (int i = 0; i < M; i++)
    if (rows[i].r == 1) {
      if (one_element_line)
	one_element_line->prev = &rows[i];
      rows[i].next = one_element_line;
      one_element_line = &rows[i];
    }
  for (int i = 0; i < N; i++)
    if (columns[i].r == 1) {
      if (one_element_line)
	one_element_line->prev = &columns[i];
      columns[i].next = one_element_line;
      one_element_line = &columns[i];
    }
  assert (one_element_line != NULL);
  
  /* Compute the value of the elements */
  while (one_element_line) {
    struct element *e = one_element_line->e;
    assert(e);
    struct line *other;
    if (one_element_line->is_row) {
      int s = 0;
      for (int i = 0; i < N; i++)
	if (i != e->c)
	  s += m[e->r*N + i];
      m[e->r*N + e->c] = w[e->r] - s;
      
      if (e->cnext)
	e->cnext->cprev = e->cprev;
      if (e->cprev)
	e->cprev->cnext = e->cnext;
      if (columns[e->c].e == e)
	columns[e->c].e = e->cnext;
      other = &columns[e->c];
    }
    else {
      int s = 0;
      for (int i = 0; i < M; i++)
	if (i != e->r)
	  s += m[i*N + e->c];
      m[e->r*N + e->c] = total_weight/N + (total_weight%N > e->c ? 1 : 0) - s;
      
      if (e->rnext)
	e->rnext->rprev = e->rprev;
      if (e->rprev)
	e->rprev->rnext = e->rnext;
      if (rows[e->r].e == e)
	rows[e->r].e = e->rnext;
      other = &rows[e->r];
    }
    /* Remove head from list */
    one_element_line = one_element_line->next;
    if (one_element_line)
      one_element_line->prev = NULL;
    
    other->r--;
    if (other->r == 1) { /* Add in one_element_line list */
      if (one_element_line)
	one_element_line->prev = other;
      other->next = one_element_line;
      one_element_line = other;
    }
    else if (other->r == 0) { /* Remove form one_element_line list */
      if (other->next)
	other->next->prev = other->prev;
      if (other->prev)
	other->prev->next = other->next;
      if (other == one_element_line)
	one_element_line = other->next;
    }
    k--;
  }
  
  free (el);
  free (rows);
  free (columns);
  
  return k == 0;
}

/* *********************************************************** */

void t1 (int *m, int stride, int i1, int j1, int i2, int j2, int d) {
  m[i1*stride + j1] += d;
  m[i1*stride + j2] -= d;
  m[i2*stride + j1] -= d;
  m[i2*stride + j2] += d;
}

/* *********************************************************** */
	 
void printCommMatrix (const int *m, int M, int N) {
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++)
      printf ("%8d", m[i*N + j]);
    printf ("\n");
  }
}

/* *********************************************************** */

void printCommMatrixPerm(int *intercomms, int M, int N, int *perm) {
  
  for (int i = 0; i < N; i++)
    printf ("%8d", (perm ? perm[i] : i));
  printf ("\n");
  
  printf ("\n");
  
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      if (i == (perm ? perm[j] : j))
	printf ("\x1b[31m");
      else if (intercomms[i*N + j] == 0)
	printf ("\x1b[34m");
      printf ("%8d\x1b[0m", intercomms[i*N + j]);
    }
    printf ("\n");
  }
  printf ("\n");
}

/* *********************************************************** */

int checkCommMatrix(const int *m, int M, int N)
{

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      if(m[i*N + j] < 0) return 0;
    }
  }
  
  return 1;
}

/* *********************************************************** */
