
/* BENCHMARK for COPARTITIONING */
/* ./tools/parco16-comp-copart.exe -i 1 -s -A ../../lbc2-data/cerfacs/cylinder/cylinder.avbp.part1.txt -B ../../lbc2-data/cerfacs/cylinder/cylinder.avtp.txt -c ../../lbc2-data/cerfacs/cylinder/cylinder.avbp.part2.txt -m AW_SCOTCH_HUN -m NV_KMETIS  -m AW_SCOTCHRB -k 22 -n 9 -b 9 */
// Here the hybridmesh is supposed to be meshB
// TODO: for the automatic option should define from the user which code is getting automatic generation of nbcpl AND the limits. 

// #define _XOPEN_SOURCE 700
#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <libgen.h>
#include <limits.h>
#include <signal.h>
#include <setjmp.h>
#include <omp.h>
#include "metapart.h"
//#include "hybridmesh.h"

//#define USECGAL

/* *********************************************************** */
/*                  ERROR MANAGEMENT                           */
/* *********************************************************** */

static sigjmp_buf buf;
struct sigaction sa; 

static void handler(int sig)
{
  fprintf(stderr, "Signal %s received!\n", strsignal(sig));
  // if(sig == SIGABRT) sigaction(SIGABRT, &sa, NULL); // special abort behaviour!!!
  if(sig == SIGALRM) siglongjmp(buf, 2);
  siglongjmp(buf, 1);
}

/* *********************************************************** */
/*                  EXP. PARAMETERS                            */
/* *********************************************************** */

#define OUTPUT_STYLE 0 // O is for mine 1 for orel
#define MAX 255
#define UBFACTOR 5
#define OFFSET 317292 /* the offset to add to interedges. 
		       * We load (thus index) the prisma file first
                       * and then the hexa, as they do in cerfacs. 
		       * However when searching for the interedges we use
		       * internal indexing of the hexa mesh (0..2199)
		       * which does the intersection with meshB. 
		       * Therefore the interedges results for meshA should
		       * be shift to match the whole hmeshA indexing.*/
#define MAXCHAR 1024
#define TIMEOUT 60*20   // in seconds
#define PROSCPLSCHEME GEOMETRIC_ALPHA //PROPOTIONAL_ALPHA 

enum { NO = 0, RANDOM , CUBEFACE , BFS , BUBBLE , BUBBLEH, BUBBLEX, REPART , REPARTMXN  };


int niter = 1; // 10;
char* methods[MAX] = {"METIS", "KMETIS", "SCOTCHFM", NULL}; 
int npartsA[MAX] = {8, 16, 32, 64, 128, 256, 512, 1024, -1};
int npartsB[MAX] = {8, 16, 32, 64, 128, 256, 512, 1024, -1};
int npartscplBuser[MAX] = {-1};
int npartscplAuser[MAX] = {-1};
int auto_length[MAX] = {0};
#define MAXNB_EXPS 3  
char experiment[MAXNB_EXPS][MAX] = { "cylinder", "volumic", "t72" };
 
int formats[MAX] = {MESH, MESH, -1};
char* labels[MAX]; 
char* collections[MAX]; 
int ubfactor = UBFACTOR;

/* *********************************************************** */

FILE * stdout2;

/* *********************************************************** */



struct my_stat {
  int NA, NB, NAcpl, NBcpl;
  double ubA, ubB, ubAcpl, ubBcpl; 
  int cutA, cutB, cutAcpl, cutBcpl;
  int totV, totZeps, totZ;
  int exectime;
};



void savePartitionCerfacs(int n, int edgecut, int * parts, const char* outfile)
{
  FILE * file = fopen(outfile, "w");
  assert(file != NULL);
  printf("edgecut is %d\n",edgecut);
  for(int i = 0 ; i < n ; i++)
    fprintf(file, "%d\n",parts[i] + 1); // fortran format 
  fprintf(file, "%d\n",edgecut);
  fclose(file);  
}



/* Convenction 1: one hybrid mesh A*/
/* Convenction 2: coupling part if existed is with mesh A*/
/* Convenction 3: if a test case doesnt have a distint coupling mesh, the -c no*/
void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0], "[options] meshA_rpart.txt meshA_cpart.txt meshB.txt");
  printf("options:\n");
  printf("\t -h: print this message\n");
  printf("\t -m method : NV_SCOTCHRB ,PROJR_KGGGP_L, AW_KGGGP, AW_SCOTCH_HUN ...\n");
  printf("\t -i niters : nb iterations\n");
  printf("\t -k nparts : nb partitions for code A\n");
  printf("\t -n nparts : nb partitions for code B\n");
  printf("\t\t -p: save output partition of meshes A and B, and save interedges,txt and save avbp file\n");
  printf("\t\t -l: load interedges.txt (instead of computing it)\n");
  printf("\t\t -z shiftz: shift meshB on z axis (after loading file)\n");
  printf("\t\t -v: produce VTK output files\n");
  printf("\t -U ub : unbalance factor (default 5%%)\n");  
  printf("\t -s : silent mode\n");
  printf("\t -S : short mode for output\n");  
  printf("\t -A : load provided meshA \n");  
  printf("\t -B : load provided meshB \n");
  printf("\t -c : load the part of meshA that is involved in the coupling (if hybrid) \n");
  /* printf("\t -c : overwrite collection name for all experiments\n");   */
  printf("\t -a : test multiple values for nbpartscplB \n");
  printf("\t -d : dry mode\n");  
  exit (EXIT_FAILURE);  
}


int sortinputfiles(char **list1 , char ** list2,char **list3,char **list4, int size) {

  char ** temp1, ** temp2,** temp3,** temp4; //[size][MAX],temp2[size][MAX],temp3[size][MAX],temp4[size][MAX] ;
  
  temp1 = malloc(size *sizeof(char *));
  temp2 = malloc(size * sizeof(char *));
  temp3 = malloc(size * sizeof(char *));
  temp4 = malloc(size * sizeof(char *));
  
  for (int f = 0; f < size; f++) {
  temp1[f] = malloc(MAX*sizeof(char ));
  temp2[f] = malloc(MAX*sizeof(char ));
  temp3[f] = malloc(MAX*sizeof(char ));
  temp4[f] = malloc(MAX*sizeof(char ));
}    
    
  for (int f = 0; f < size; f++) {
    printf("%d--> %s\n",f, list1[f]);
    if (list1[f]) strcpy(temp1[f],list1[f]);
    printf("%d--> %s\n",f, temp1[f]);
  }
  printf("---------------------------\n"); 
  for (int f = 0; f < size; f++) {
    printf("%d--> %s\n",f, list2[f]);
    if (list2[f]) strcpy(temp2[f],list2[f]);
    printf("%d--> %s\n",f, temp2[f]);
  }
  printf("---------------------------\n");
  for (int f = 0; f < size; f++) { 
    printf("%d--> %s\n",f, list3[f]);
    if (list3[f]) strcpy(temp3[f],list3[f]);
    printf("%d--> %s\n",f, temp3[f]);
    printf("---------------------------\n");
  }
  for (int f = 0; f < size; f++) {
    printf("%d--> %s\n",f, list4[f]);
    if (list4[f]) strcpy(temp4[f],list4[f]);
    printf("%d--> %s\n",f, temp4[f]);
  }
  printf("---------------------------\n");





  int order[MAXNB_EXPS][4]; // 4 lists
  int idx = 0;
  for (int j = 0; j < MAXNB_EXPS; j++)
    for (int i = 0; i < 4; i++)
      order[j][i] = -1;


  for (int i = 0; i < MAXNB_EXPS; i++) {
    for (int f = 0; f < size; f++) {
      if(list1[f]) {
	if(strstr(list1[f],experiment[i]) != NULL) {
	  order[i][0] = f;
	  break;
	}
      }
    }
    for (int f = 0; f < size; f++) {
      if(list2[f]) {
	if(strstr(list2[f],experiment[i]) != NULL) {
	  order[i][1] = f; break;
	}	
      }
    }
    for (int f = 0; f < size; f++) {
      if(list3[f]) {
	if(strstr(list3[f],experiment[i]) != NULL) {
	  order[i][2] = f; break;
	}
      }
    }
    for (int f = 0; f < size; f++) {
      if(list4[f]) {
	if(strstr(list4[f],experiment[i]) != NULL) {
	  order[i][3] = f; break;
	}
      }
    }
  }
  for (int j = 0; j < MAXNB_EXPS; j++) {
    for (int i = 0; i < 4; i++) {
      printf("%d ",order[j][i]);
    }
    printf("\n");
  }
  for (int j = 0; j < 4; j++) {
    for (int i = 0; i <  MAXNB_EXPS; i++) {
      if(j == 0) {
	if(order[i][j]==-1) list1[i] = NULL;
	else //strcpy(list1[i], temp1[order[i][j]]);
	list1[i] = temp1[order[i][j]];
      } 
      else if (j == 1)	{
	if(order[i][j]==-1) list2[i] = NULL;
	else //strcpy(list2[i], temp2[order[i][j]]);
	list2[i] = temp2[order[i][j]]; 
}
      else if (j == 2){
      	if(order[i][j]==-1)
      	  list3[i] = NULL;
      	else {
      	  printf("Replace list3 indx=%d to order[%d][%d]=%d\n",i,i,j,order[i][j]);
      	  printf("%s \n",temp3[order[i][j]]);
      	  list3[i] = temp3[order[i][j]];
	  //      	  strcpy(list3[i], temp3[order[i][j]]);
      	  //strcpy(list3[i], temp3[order[i][j]]);
      	}
      }
      else if (j == 3) {
	if(order[i][j]==-1) list4[i] = NULL;
	else { 
	  printf("Replace list4 indx=%d to order[%d][%d]=%d\n",i,i,j,order[i][j]);
	  //strcpy(list4[i], temp4[order[i][j]]);
	  list4[i] = temp4[order[i][j]];
	}
      }
    }
  }

  for (int f = 0; f < size; f++)
    printf("%d--> %s\n",f, list1[f]);
  printf("---------------------------\n");
  for (int f = 0; f < size; f++)
    printf("%d--> %s\n",f, list2[f]);
  printf("---------------------------\n");
  for (int f = 0; f < size; f++)
    printf("%d--> %s\n",f, list3[f]);
  printf("---------------------------\n");
  for (int f = 0; f < size; f++)
    printf("%d--> %s\n",f, list4[f]);
  printf("---------------------------\n");

  //permute:



  return 0;
}


/* *********************************************************** */

int main(int argc, char * argv[]) 
{   

  char* filesA[MAX] = {NULL}; 
  char* filesB[MAX];
  char * cplfiles[MAX]; // = { NULL };  
  /* ************* INPUT ARGS ************* */
  
  int opt;
  int kidx = 0; // nbA
  int nidx = 0;  // nbB
  int ncplidx = 0; // nbcplAuser
  int kcplidx = 0; // nbcplBuser
  int midx = 0;
  int fAidx = 0;  
  int fBidx = 0;
  int fcidx = 0;
  /* default */
  int silent = 0;
  /* default values for the generation of npartscplB. if automatic is set, generation of automatic_size nb of parts cplB are created.*/
  int automatic = 0;
  int automatic_size = 1;
  int dry = 0;  
  int format = -1;
  int useropt = 0;
  int specialopt = 0;
  int savegraph = 0;
  int readgraph = 0;
  int relative = 0;
  char * collectionname = NULL;
  int shortmode = 0; // corresponds to mine 1 to orel's output
  int vtkopt = 0;
  int saveopt = 0;
  int loadopt[MAX] = {0};
  char * loadinter[MAX] = { NULL };
  int lidx = 0;
  int autoidx = 0;

  int offset = 0; //same of OFFSET (for cylinder is the size of the file loaded first:prism  and for T72 is the size of the file load first: tetra )

  double shiftz = 0.0;
  
  while ((opt = getopt (argc, argv, "rhsovgpl:m:i:n:k:a:b:u:z:U:A:B:dc:S")) != -1) {
    switch (opt) {
      
    case 'm':
      methods[midx] = optarg; methods[midx+1] = NULL; 
      midx++;
      break;
      
    case 'i':
      niter = atoi(optarg); 
      break;
      
    case 'U':
      ubfactor = atoi(optarg); 
      break;
    case 'k':
      npartsA[kidx] = atoi(optarg); npartsA[kidx+1] = -1;
      kidx++;
      break;
    case 'n':
      npartsB[nidx] = atoi(optarg); npartsB[nidx+1] = -1;
      nidx++;
      break;
    case 'a':
      useropt = 1;
      npartscplAuser[kcplidx] = atoi(optarg); npartscplAuser[kcplidx+1] = -1;
      kcplidx++;
      break;
    case 'b':
      useropt = 1;
      npartscplBuser[ncplidx] = atoi(optarg); npartscplBuser[ncplidx+1] = -1;
      ncplidx++;
      break;

    case 's':
      silent = 1;
      break;
    case 'g':
      savegraph = 1;
      break;
    case 'r':
      readgraph = 1;
      break;
    case 'S':
      shortmode = 1;
      break;
    case 'o':
      automatic = 1;
      break;      
    case 'd':
      dry = 1;
      break;
    case 'v':
      vtkopt = 1;
      break;
    case 'p':
      saveopt = 1;
      break;
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
    case 'l':
      loadopt[lidx] = 1;
      loadinter[lidx] =  optarg; loadinter[lidx + 1] = NULL;
      lidx++;
      break;
    case 'A':
      filesA[fAidx] = optarg; filesA[fAidx+1] = NULL; 
      fAidx++;
      break;
    case 'B':
      filesB[fBidx] = optarg; filesB[fBidx+1] = NULL; 
      fBidx++;
      break;
    case 'c':
      cplfiles[fcidx] = optarg; cplfiles[fcidx+1] = NULL;
      fcidx++;
      break;            
    case 'h':
    default:
      usage(argc,argv);
    }
  }
  // kidx = kidx = nb of experiments for each method.

  if(argc - optind != 0) usage(argc,argv);  
  // else if(argc == 1) usage(argc,argv);     
  
  int nbfilesA = 0;   
  int nbfilesB = 0;   
  int nbcplfiles = 0;
  int nbloadinter = 0;
  int nbmethods = 0; 
  int nbnpartsA = 0;
  int nbnpartscplB = 0; 
  int nbnpartscplA = 0;
  int nbnpartsB = 0;
  int nb_exp = 0; // it's the number of expes -> Na*Nb if Na!=Nb and Na if Na = Nb
  int flag = 0;
  int idx_a = 0;
  int idx_b = 0;

  while(filesA[nbfilesA] != NULL) nbfilesA++;
  while(filesB[nbfilesB] != NULL) nbfilesB++;
  while(cplfiles[nbcplfiles] != NULL) nbcplfiles++;
  while(loadinter[nbloadinter] != NULL)  nbloadinter++;

 while(methods[nbmethods] != NULL) nbmethods++;
  while(npartsA[nbnpartsA] != -1) nbnpartsA++;
  while(npartsB[nbnpartsB] != -1) nbnpartsB++;
  if(useropt) {
    while(npartscplBuser[nbnpartscplB] != -1) nbnpartscplB++;
    while(npartscplAuser[nbnpartscplA] != -1) nbnpartscplA++;

  }
  if (nbnpartsA == nbnpartsB) { 
    nb_exp = nbnpartsA;
    flag = 1; // type of expe -k 30 -n 9
  }
  else nb_exp = nbnpartsA * nbnpartsB; // type of expe -k 30 -n 9 -n 12 -n 18
  // check if user's input on nbpartscpl is correct
  assert(nbnpartscplB <= nbnpartsB);
  assert(nbnpartscplA <= nbnpartsA);
  for(int i = 0; i < nbnpartscplA; i++)
   assert(npartscplAuser[i] <= npartsA[i]);
  for(int i = 0; i < nbnpartscplB; i++)
    assert(npartscplBuser[i] <= npartsB[i]);
  
  assert(nbfilesA == nbfilesB);
  
  int nbfiles = nbfilesA;
  char testcase[nbfiles][MAX];

  printf("# ATTENTION: this code is in inverse.\n");
  
  for(int i = 0; i < nbloadinter; i++)
    if(loadinter[i])
	printf("ATTENTION: make sure that interedges are \"meshB meshA\" and not \"meshA meshB\"");

  printf("# Test Parameters\n");
  printf("# - number of simulations = %d\n",nbfilesA);
  printf("# - list of methods = [");
  for(int i = 0; i < nbmethods; i++)
    printf("%s ",methods[i]);
  printf("] Size: %d\n",nbmethods);
  printf("# - number of experiments %d ",nb_exp);
  if(!flag) printf("(%d x %d)\n",nbnpartsA, nbnpartsB);
  else printf("\n");
  printf("# - list of partsA = [");
  for(int i = 0; i < nbnpartsA; i++)
    printf("%d ",npartsA[i]);
  printf("] Size: %d\n",nbnpartsA);
  printf("# - list of partsB = [");
  for(int i = 0; i < nbnpartsB; i++)
    printf("%d ",npartsB[i]);
  printf("] Size: %d\n",nbnpartsB);
  if(useropt) {
    printf("# - USER: list of partsB cpl = [");
    for(int i = 0; i < nbnpartscplB; i++)
      printf("%d ",npartscplBuser[i]);
    printf("] Size: %d\n",nbnpartscplB);
    printf("# - USER: list of partsA cpl = [");
    for(int i = 0; i < nbnpartscplA; i++)
      printf("%d ",npartscplAuser[i]);
    printf("] Size: %d\n",nbnpartscplA);
  }
  printf("# - number of iterations %d\n",niter);
  if(automatic)
    printf("# - automatic generation of partscplB coupling\n");
  if(vtkopt)
    printf("# - vtk output\n");
  if(saveopt)
    printf("# - partition output\n");

  for (int i = 0; i < nbfilesA; i++) {
     printf("# FILES: A: %s c: %s B: %s \n",basename(filesA[i]), basename(cplfiles[i]), basename(filesB[i]));
     if(!strcmp(cplfiles[i], "no"))
       cplfiles[i] = NULL;
   }
   int copartscheme[nbmethods];
   char * schemestr[nbmethods];
   // "-m SCOTCHML1_SCOTCHRB -m SCOTCHML1_KGGGP_L 
   // -m SCOTCHML1_KGGGP -m SCOTCHML1_HUN"
   char partition_env[nbmethods][MAX];
   for (int i = 0; i < nbmethods; i++) {
     if(strstr(methods[i], "NV") != NULL) {
       copartscheme[i] = COPART_NAIVE;
       schemestr[i] = "NAIVE";
     }
     else if(strstr(methods[i], "AW") != NULL) {
       copartscheme[i] = COPART_AWARE_NAIVE;
       schemestr[i] = "AWARE";
     }
     else if(strstr(methods[i], "PROJR") != NULL) {
       copartscheme[i] = COPART_AWARE_PROJREPART;
       schemestr[i] = "PROJREPART";
     }
     else if(strstr(methods[i], "PROJS") != NULL) {
       copartscheme[i] = COPART_AWARE_PROJSUBPART;
       schemestr[i] = "PROJSUBPART";
     }
     else if(strstr(methods[i], "MLT") != NULL) {
       copartscheme[i] = COPART_MULTICONST;
       schemestr[i] = "MULTICONST";
     }
     else { 
       printf("%s did not got matched!",methods[i]);
       usage(argc,argv); 
       exit(EXIT_FAILURE); 
     }

     if(strstr(methods[i], "SCOTCHRB") != NULL)
       strcpy(partition_env[i],"SCOTCHML1_SCOTCHRB");
     else if(strstr(methods[i], "KGGGP_L") != NULL)
       strcpy(partition_env[i],"SCOTCHML1_KGGGP_L");
     else if(strstr(methods[i], "KGGGP") != NULL)
       strcpy(partition_env[i],"SCOTCHML1_KGGGP");
     else if(strstr(methods[i], "HUN") != NULL)
       strcpy(partition_env[i],"SCOTCHML1_HUN");
     else if(strstr(methods[i], "KMETIS") != NULL)
       strcpy(partition_env[i],"KMETIS");
     else { 
       printf("%s did not got matched!",methods[i]);
       usage(argc,argv); 
       exit(EXIT_FAILURE); 
     }


   }

   for (int i = 0; i < nbmethods; i++) { 
     if(!strcmp(partition_env[i],"KMETIS") && (strcmp(schemestr[i],"NAIVE")
	&& strcmp(schemestr[i],"MULTICONST")) ) {
       printf("WARNING: KMETIS can only be used with NAIVE scheme!\n");
       exit(EXIT_FAILURE);
     }
   }
   for (int f = 0; f < nbfiles; f++) {
     for (int i = 0; i < 3; i++) {
       if(strstr(filesA[f], experiment[i]) != NULL) {
	 sprintf(testcase[f],"%s",experiment[i]);
	 break;
       }
       else sprintf(testcase[f],"other");
     }
   }

  /* ************* SILENT MODE ************* */
  
  stdout2 = stdout;
  if(silent) {
    int out2 = dup(1);
    // int redirect = open("/dev/null", O_WRONLY);
    int redirect = open("/tmp/comp.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    dup2(redirect, 1);
    dup2(redirect, 2);
    close(redirect);
    stdout2 = fdopen(out2, "w");
    fprintf(stdout2, "# Silent mode: stdout & stderr redirection in /tmp/comp.txt\n");
  }
  

  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;
  double * volumes = NULL;   

  /* 1) load mesh A & B */  

  Mesh meshA[nbfiles];
  Mesh meshBcpl[nbfiles], meshBncpl[nbfiles];
  HybridMesh meshB[nbfiles];

  Graph gA[nbfiles], gB[nbfiles], gBcpl[nbfiles],gBncpl[nbfiles]; //TODO: maybe I don't need the last one
  /* Here starts the experiments */ 
  for (int i = 0; i < nbfiles; i++) {
    // attention: load fileB to meshA inorder to keep input the same as parco-comp-copart16.c
    loadMesh(filesB[i], &meshA[i]);   
    printf("Mesh A[%d]: nb cells = %d, nb nodes = %d\n", i,
	   meshA[i].nb_cells, meshA[i].nb_nodes);  
    // FOR HYBRID MESHES DO THE ORDER OF part1,part2 AS IN CERFACS
    // for cylinder case: 1->prism 2->hexa
    // for t72 case: 1->tetra 2->prism
    // attention: load fileA to meshB in order to keep input the same as parco-comp-copart16.c
    loadHybridMesh(&meshB[i], 2, filesA[i], cplfiles[i]);
    loadMesh(cplfiles[i], &meshBcpl[i]);
    loadMesh(filesA[i],&meshBncpl[i]);
    printf("Nb of cells for meshBncpl is %d\n",meshBncpl[i].nb_cells);
    printf("Mesh B[%d]: nb cells = %d, nb nodes = %d\n", i,
	   meshB[i].nb_cells, meshB[i].nb_nodes);   


  /* 2) generate graphs from meshes */
    mesh2Graph(&meshA[i], &gA[i]);
    mesh2Graph(&meshBcpl[i], &gBcpl[i]);
    Hybridmesh2Graph(&meshB[i], &gB[i]);

    if(!strcmp(testcase[i],"cylinder") && readgraph) { 
      printf("Load the graph for AVBP from input files.\n");
      FILE * file; 
      if (NULL == (file = fopen("in_graph_xadj.txt", "r"))) {
	fprintf(stderr, "ERROR: opening file in_graph_xadj.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < gA[i].nvtxs + 1; k++)
	fscanf(file, "%d", &gA[i].xadj[k]);
      fclose(file);
      file = NULL;
      if (NULL == (file = fopen("in_graph_adjncy.txt", "r"))) {
	fprintf(stderr, "ERROR: opening file in_graph_adjncy.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < 2 * gA[i].nedges; k++)
	   fscanf(file, "%d", &gA[i].adjncy[k]);
      fclose(file);
    for (int k = 0 ; k < gA[i].nvtxs + 1; k++)
      gA[i].xadj[k]--;
    for (int k = 0 ; k < 2 * gA[i].nedges; k++)
      gA[i].adjncy[k]--;
    }
    printf("Graph A[%d]: nb vertices = %d, nb edges = %d\n", i, gA[i].nvtxs, gA[i].nedges);  
    printf("Graph B[%d]: nb vertices = %d, nb edges = %d\n", i, gB[i].nvtxs, gB[i].nedges);  
    
    if(savegraph) {
      printf("Save the graph for debug reasons.\n");
      FILE * file; 
      if (NULL == (file = fopen("graph_xadj.txt", "w"))) {
	fprintf(stderr, "ERROR: opening file graph_xadj.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < gA[i].nvtxs + 1; k++)
	   fprintf(file, "%d \n", gA[i].xadj[k]);
      fclose(file);

      file = NULL;
      if (NULL == (file = fopen("graph_adjncy.txt", "w"))) {
	fprintf(stderr, "ERROR: opening file graph_adjncy.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < 2* gA[i].nedges ; k++)
	   fprintf(file, "%d \n", gA[i].adjncy[k]);
      fclose(file);     
      file = NULL;

      if (NULL == (file = fopen("graphB_xadj.txt", "w"))) {
	fprintf(stderr, "ERROR: opening file graphB_xadj.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < gB[i].nvtxs + 1; k++)
	   fprintf(file, "%d \n", gB[i].xadj[k]);
      fclose(file);

      file = NULL;
      if (NULL == (file = fopen("graphB_adjncy.txt", "w"))) {
	fprintf(stderr, "ERROR: opening file graphB_adjncy.txt failed.\n");
	return 0;
      }
      for (int k = 0 ; k < 2* gB[i].nedges ; k++)
	   fprintf(file, "%d \n", gB[i].adjncy[k]);
      fclose(file);     
      file = NULL;



      if (NULL == (file = fopen("graph.txt", "w"))) {
	fprintf(stderr, "ERROR: opening file graph.txt failed.\n");
	return 0;
      } 
      for (int k = 0 ; k < gA[i].nvtxs ; k++) {        
	for (int j = gA[i].xadj[k] ; j < gA[i].xadj[k+1] ; j++) {            
	  fprintf(file, "%d ", gA[i].adjncy[j]);  
	}
	fprintf(file, "\n");
      }
      fclose(file);
      
    /* Check if the graph of meshAcpl 
       is consistent with the new graph 
       adjacency*/
      if(cplfiles[i]){
	if (NULL == (file = fopen("graphcpl.txt", "w"))) {
	  fprintf(stderr, "ERROR: opening file graph_xadj.txt failed.\n");
	  return 0;
	}
	for(int k = 0 ; k < gBcpl[i].nvtxs ; k++) {
	  fprintf(file,"  %d -> [ ", k);
	  for(int j = gBcpl[i].xadj[k] ; j < gBcpl[i].xadj[k+1] ; j++) {
	    fprintf(file,"%d ", gBcpl[i].adjncy[j]);
	  }
	  fprintf(file,"]\n");
	}
	fclose(file);
      }
    }
    /* 3) compute/load/save inter-edges */
 
  nbinteredges = 0;
  interedges = NULL;
  
  if(loadinter[i]) {
    printf("\n*************** LOAD INTER-EDGES ***************\n\n");
    
    printf("Loading interedges: %s\n",loadinter[i]);
    FILE* finteredges = fopen(loadinter[i], "r");
    if (finteredges == NULL) {
        printf ("Could not open file!\n");
        return 1;
    }
    int a, b, w;
    while(fscanf(finteredges,"%d %d %d\n", &a, &b, &w) > 0) nbinteredges++;
    interedges = malloc(2*nbinteredges*sizeof(int)); assert(interedges);
    weights = malloc(nbinteredges*sizeof(int)); assert(weights);   
    rewind(finteredges);
    // reading interedges in the inverse order because meshA and MESHB are inversed anyway interedges + 2*k + 1 first and then interedges + 2*k 
    for(int k = 0 ; k < nbinteredges ; k++)
      fscanf(finteredges,"%d %d %d\n", interedges + 2*k + 1, interedges + 2*k, weights + k);
    fclose(finteredges);
    //    if(!strcmp(testcase[i],"cylinder")) offset = 317292;
    //    else if(!strcmp(testcase[i],"t72")) offset = 52668427;
    // we add the offset to numbering of interedges related to meshA
    // in order to use them with gA (the graph of hybrid meshA)
    /* offset = meshAfirst[i].nb_cells; */
    /* for(int k = 0 ; k < nbinteredges ; k++) { */
    /*   interedges[2*k + 1]=interedges[2*k + 1] +  offset; // + offset; // */
    /*   //printf("%d ",interedges[k]); */
    /* } */
  }
  else {
    
    printf("\n*************** COMPUTE INTER-EDGES ***************\n\n");
    /* Find intersections with meshA and the coupled part of meshB */
    intersectMesh( &meshA[i], &meshBcpl[i], 
		   &nbinteredges, 
		   &interedges, 
		   &volumes);
    //    if(!strcmp(testcase[i],"cylinder")) offset = 317292;
    //    else if(!strcmp(testcase[i],"t72")) offset = 52668427;
    // we add the offset to numbering of interedges related to part1 ofmeshB
    // (since its coupling part is loaded second)
    offset = meshBncpl[i].nb_cells;
    for(int k = 0 ; k < nbinteredges ; k++) {
      interedges[2*k+1]=interedges[2*k+1] +  offset; // + offset to meshB (2*k+1)
    }
    printf("OFFSET is %d\n",offset);
    /* TODO: decide if I will save all that are not  
     * provided as a file or do sth more complicated */
    const int minwgt = 0, maxwgt = 10;
    computeInteregdeWeights(&nbinteredges, &interedges, 
			    &volumes, &weights, minwgt, maxwgt);       
    if(weights) 
      printf("weights for the interedges have been computed!\n");  
    // save interedges
    // interedges have the form: vertex_meshA vertex_meshB weight
    FILE* finteredges = fopen("./interedges.txt", "w");
    for(int k = 0 ; k < nbinteredges ; k++)
      fprintf(finteredges,"%d %d %d\n",interedges[2*k], interedges[2*k+1], 
	      weights[k]);
    fclose(finteredges);
    printf("Saving interedges: %s\n", "interedges.txt");  
  }

  printf("nb interedges found = %d\n", nbinteredges);

  /* ************* ERROR MANAGEMENT ************* */

  sa.sa_handler = handler;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGSEGV, &sa, NULL);
  sigaction(SIGFPE, &sa, NULL);
  sigaction(SIGABRT, &sa, NULL);
  sigaction(SIGBUS, &sa, NULL);
  sigaction(SIGQUIT, &sa, NULL);
  sigaction(SIGALRM, &sa, NULL);
  sigprocmask(SIG_SETMASK, &sa.sa_mask, NULL);
  
  char procscplstr[256] = "GEOMETRIC_ALPHA";
  int procscplscheme = GEOMETRIC_ALPHA;

  /* 4) generate the number of processors in coupling phase */
  // generate the number of processors based on the PROSCPLSCHEME
  int * npartscplA, * npartscplB;
  npartscplA = malloc(nbnpartsA * sizeof(int));
  npartscplB = malloc(nbnpartsB * sizeof(int));

  /* ************* BENCHMARK MAIN LOOP ************* */
  int exectime = 0;
  int ubfactor = UBFACTOR;
  int ** partA = calloc(nbnpartsA,sizeof(int *));
  for (int k = 0; k < nbnpartsA; k++)
    partA[k]  = calloc(gA[i].nvtxs,sizeof(int));
  int ** partB = calloc(nbnpartsB,sizeof(int *));
    for (int n = 0; n < nbnpartsB; n++)
      partB[n] = calloc(gB[i].nvtxs,sizeof(int));
  int edgecutA, edgecutB;
  int nbcomms = 0;
  int * intercomm = NULL;
  CopartResults copartinfo[nb_exp][nbmethods]; // nbexps = the number of different comb (nbnpartsA,nbnpartsB)
  int npartscplB_geom = 0;
    

  fprintf(stdout2, "# TESTCASE CO-SCHEME PART METHOD A-NPARTS A-NPARTSCPL B-NPARTS B-NPARTSCPL GEOM-B-NPARTSCPL SEED A-UB B-UB A-UBCPL B-UBCPL A-EDGECUT B-EDGECUT NBMSGS TVOL MINORS TIME CHECK\n");
	     fprintf(stdout2, "# EXP MESHA MESHB OVERLAP METHOD SCHEME SEED NPARTSA NPARTSB NPARTSACPL NPARTSBCPL NPARTSACPLR NPARTSBCPLR UBA UBB UBACPL UBBCPL CUTA CUTB CUTACPL CUTBCPL TOTZ TOTZM TOTZOPT TIME ERROR\n");
  for(int k = 0; k < nb_exp; k++) {

    if(flag) idx_a = idx_b = k;
    else {
      idx_a = k / nbnpartsB; 
      idx_b = k % nbnpartsB;
    }
    printf("STARTING EXPERIMENT #%d (nbpartA = %d  nbpartB = %d)\n",k, npartsA[idx_a],npartsB[idx_b]);
    printf("-----------------------------------\n");
     
    /* Generation the processors in the coupling*/
      generateProcsCpl(&gA[i], &gB[i], npartsA[idx_a], npartsB[idx_b], 
		       nbinteredges, interedges, weights, procscplscheme, 
		       &npartscplA[idx_a], &npartscplB[idx_b]);
      
      npartscplB_geom = npartscplB[idx_b];
      if(idx_a < nbnpartscplA)
	npartscplA[idx_a] = npartscplAuser[idx_a];
      if(idx_b < nbnpartscplB)
	npartscplB[idx_b] = npartscplBuser[idx_b];
      printf("- procscplscheme = %s\n", procscplstr);
      printf("- npartscplA = %d\n", npartscplA[idx_a]);
      printf("- npartscplB = %d\n", npartscplB[idx_b]);

    /* if automatic is active we automatically run the 
     * experiments for all values of npartscplB between 
     * npartscplA and npartsA 
    */
      if(automatic) {
	npartscplB[idx_b] = npartscplA[idx_a]; 
	automatic_size = npartsB[idx_b] - npartscplB[idx_b] + 1;
      }

      intercomm = calloc(npartsA[idx_a] * npartsB[idx_b] , sizeof(int));

      for (int m = 0; m < nbmethods; m++) {
      
	struct my_stat stats[niter];
      
	for (int l = 0; l < niter; l++) {
	  //generate seeds
	  int seed = l*10;
	  CopartResults copartinfo;
	  
	  /* cleaning previous results */
	  for (int j = 0; j < npartsA[idx_a] * npartsB[idx_b]; j++)
	    intercomm[j] = 0;
	  edgecutA = 0;
	  edgecutB = 0;
	  for (int f = 0; f < gA[i].nvtxs; f++)
	    partA[idx_a][f]  = 0;
	  for (int f = 0; f < gB[i].nvtxs; f++)
	    partB[idx_b][f]  = 0;
	  /* end of cleaning */
	  
	  int error = 0;
	  error = sigsetjmp(buf, 1); // return 0 if OK, 1 if signal and 2 if timeout

	 for(int nbcplB_idx = 0; nbcplB_idx < automatic_size; nbcplB_idx++) {
	   printf("---------------------------------------------\n");
	   printf("Calling copart(A,B) with:\n");
	   printf("Graph A[%d]: nb vertices = %d, nb edges = %d\n",i, gA[i].nvtxs, gA[i].nedges);  
	   printf("Graph B[%d]: nb vertices = %d, nb edges = %d\n",i, gB[i].nvtxs, gB[i].nedges);
	   printf("method = %s\n",methods[m]);
	   printf("nbinteredges = %d\n", nbinteredges);
	   printf("interedges [ ");
	   for(int i=0; i< 10; i++)
	     printf("%d ", interedges[i]);
	   printf("]\n");
	   printf("weights [ ");
	   if(weights) {
	     for(int i=0; i< 10; i++) {
	       if(weights[i]) printf("%d ", weights[i]);
	     }
	   }
	   printf("]\n");
	   printf("npartsA = %d, npartsB = %d\n", npartsA[idx_a],npartsB[idx_b]);
	   printf("npartscplA = %d, npartscplB = %d\n",npartscplA[idx_a], 
		  npartscplB[idx_b] + nbcplB_idx);
	   printf("seeds = %d\n",seed);
	   printf("---------------------------------------------\n");
	   
	   // here interedges should be indexed based on the internal numbering of gA and gB
	   exectime = copart(&gA[i], &gB[i],
			     npartsA[idx_a], npartsB[idx_b],
			     ubfactor,
			     nbinteredges,
			     interedges,
			     weights,
			     copartscheme[m],
			     npartscplA[idx_a], npartscplB[idx_b] + nbcplB_idx,
			     seed,
			     partA[idx_a], partB[idx_b],
			     &edgecutA, &edgecutB,
			     NULL, &meshA[i],&meshB[i],
			     &meshBcpl[i], 
			     partition_env[m]);

	   int intercomm2[npartsA[idx_a]][npartsB[idx_b]];
	   computeCopartDiagnostic(&gA[i], &gB[i],
				   npartsA[idx_a], 
				   npartsB[idx_b],
				   npartscplA[idx_a], npartscplB[idx_b] + nbcplB_idx,
				   partA[idx_a], partB[idx_b],
				   nbinteredges, interedges,
				   weights, &copartinfo, 
				   (int*)intercomm2);
	   if(!shortmode) {
	     fprintf(stdout2, " %s %s %s %s %d %d  %d %d  %d %d %.2f %.2f %.2f %.2f %d %d %d %d %d %d",
       	 	 testcase[i],
       	 	 schemestr[m],
       	 	 partition_env[m],
       	 	 methods[m],
       	 	 copartinfo.kA, copartinfo.kcplA,
	 	 copartinfo.kB, copartinfo.kcplB,
       	 	 npartscplB_geom,
       	 	 seed,
	 	 copartinfo.maxubA, copartinfo.maxubB,
	 	 copartinfo.maxubcplA, copartinfo.maxubcplB,
	 	 copartinfo.edgecutA, copartinfo.edgecutB,
	 	 //copartinfo.edgecutcplA, copartinfo.edgecutcplB,
	 	 // copartinfo.totV,
		 copartinfo.totZ,
		 copartinfo.totZM, 
		 copartinfo.totZopt, 
	 	 exectime
       	 	 );
	     if(error == 1) fprintf(stdout2, " ERROR\n");
	     else if(error == 2) fprintf(stdout2, " TIMEOUT\n");
	     else if(copartinfo.maxubA > (ubfactor+0.01) || copartinfo.maxubB > (ubfactor+0.01))
	       fprintf(stdout2, " WARNING\n");
	     else fprintf(stdout2, " OK\n");
	   }
	   else {
	     fprintf(stdout2, "%s %s %s %d %s %s %d %d %d %d %d %d %d %.2f %.2f %.2f %.2f %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
      		 testcase[i],
		     basename(filesB[i]),basename(filesA[i]),0,
       		 partition_env[m],
       		 schemestr[m],
		 seed,		 
		 copartinfo.kA, copartinfo.kB,
		 copartinfo.kcplA, copartinfo.kcplB,
		 copartinfo.kcplAreal, copartinfo.kcplBreal,	     		 
		 copartinfo.maxubA, copartinfo.maxubB,
		 copartinfo.maxubcplA, copartinfo.maxubcplB,	     
		 copartinfo.edgecutA, copartinfo.edgecutB,
		 copartinfo.edgecutcplA, copartinfo.edgecutcplB,
		   copartinfo.maxV, copartinfo.maxVopt,
		   copartinfo.totV, copartinfo.totVopt,		   		   
		   copartinfo.totZ, copartinfo.totZM, copartinfo.totZopt, 
		   exectime,
		   error);

	   }


	   printf("\n*************** DIAGNOSTIC ***************\n\n");
  
	   printCopartDiagnostic(&gA[i], &gB[i],
				   npartsA[idx_a], 
				   npartsB[idx_b],
				   npartscplA[idx_a], npartscplB[idx_b] + nbcplB_idx,
				   partA[idx_a], partB[idx_b],
				   nbinteredges, interedges,
				   weights);


	   if(vtkopt && !l)  {  // for one seed value  
	   printf("\n*************** VTK OUTPUT FILES ***************\n\n");
	   char vtkmeshB[100];
	   sprintf(vtkmeshB,"%c-meshA-%s-%d-%d-%d.vtk",testcase[i][0],methods[m],npartsA[idx_a],npartsB[idx_b],nbcplB_idx);
	   char vtkmeshBcpl[100];
	   sprintf(vtkmeshBcpl,"%c-meshAcpl-%s-%d-%d-%d.vtk",testcase[i][0],methods[m],npartsA[idx_a],npartsB[idx_b],nbcplB_idx);
	   char vtkmeshA[100];
	   sprintf(vtkmeshA,"%c-meshB-%s-%d-%d-%d.vtk",testcase[i][0],methods[m],npartsA[idx_a],npartsB[idx_b],nbcplB_idx);
	   char vtkcpl[100];
	   sprintf(vtkcpl,"%c-cpl-%s-%d-%d-%d.vtk",testcase[i][0],methods[m],npartsA[idx_a],npartsB[idx_b],nbcplB_idx);
	   Mesh cpl;

	   addMeshVariable(&meshA[i],"partB", CELL_INTEGER, 1, partA[idx_a]);
	   addHybridMeshVariable(&meshB[i], "partA", CELL_INTEGER, 1, partB[idx_b]);

	   // here interedges should be indexed based on internal numbering
	   // inside gAcpl (or meshAcpl) and gB.

	   int tmp_interedges[2*nbinteredges];
	   for(int k = 0 ; k < nbinteredges ; k++) {
	     tmp_interedges[2*k]=interedges[2*k];
	     tmp_interedges[2*k + 1]=interedges[2*k + 1] - offset;
	   }
	   // WARNING: the cpl may be inverse in the vtk output but it is not big deal
	   generateCouplingMesh(&meshBcpl[i], &meshA[i], nbinteredges, tmp_interedges, 0.0, &cpl);

	   printf("Saving VTK mesh: %s\n", vtkmeshA);
	   saveVTKMesh(vtkmeshA, &meshA[i]);
	   printf("Saving VTK mesh: %s\n", vtkmeshB);
	   saveVTKHybridMesh(vtkmeshB, &meshB[i]);
	   printf("Saving VTK mesh: %s\n", vtkmeshBcpl);
	   saveVTKMesh(vtkmeshBcpl, &meshBcpl[i]);
	   printf("Saving VTK mesh: %s\n", vtkcpl);
	   saveVTKMesh(vtkcpl, &cpl);
	   freeMesh(&cpl);


	 }// vtkopt

	 if(saveopt && !l) {
	   printf("\n*************** SAVE PARTITION FOR CERFACS ***************\n\n");	   
	   /* mesh A */
	   char partAfile[255];
	   sprintf(partAfile,"%sA.%s.part%d", testcase[i],methods[m], npartsA[idx_a]);
	   printf("Saving mesh partition file: %s\n", partAfile);
	   savePartitionCerfacs(meshA[i].nb_cells, copartinfo.edgecutA, partA[idx_a], partAfile);  
	   /* mesh B */
	   char partBfile[255];
	   sprintf(partBfile,"%sB.%s.part%d", testcase[i],methods[m], npartsB[idx_b]);
	   printf("Saving mesh partition file: %s\n", partBfile);
	   savePartitionCerfacs(meshB[i].nb_cells, copartinfo.edgecutB, partB[idx_b], partBfile);  

	   /* fprintf(stdout2,"# Checking variables AVBP:\n"); */
	   /* for(int s = 2 ; s < meshA[i].nb_cells ; s+= 31600) */
	   /*   fprintf(stdout2, "# %d %d\n", s + 1, partA[idx_a][s] + 1); */
	   /* fprintf(stdout2,"# Checking variables AVTP:\n"); */
	   /* for(int s = 1 ; s < meshB[i].nb_cells ; s += 5100) */
	   /*   fprintf(stdout2, "# %d %d\n", s + 1,partB[idx_b][s] + 1); */
	 }
       } // nbcplB_idx-loop-> number of nbpartscplB or 1 when automatic is 0
      } // l-loop-> number of seeds
    } // m-loop-> number of methods
    free(intercomm);


  } // k-loop->number of different experiments nbpartsA x nbpartsB
    
  for (int i = 0; i < nbnpartsB; i++) 
    free(partB[i]);
  for (int i = 0; i < nbnpartsA; i++) 
    free(partA[i]);
  free(partA);
  free(partB);
  free(interedges);
  printf("OK\n");
  }// i-loop->number of different testcases, cylinder, volumic etc (up to 3)

  
  if(silent) fclose(stdout2);

  free(weights);    
  /* free(partcplA); free(partcplB); */
  /* free(usedA); free(usedB); */
  /* free(mappingA); free(mappingB); */
  /* freeGraph(&gcplA); freeGraph(&gcplB); */

  return EXIT_SUCCESS;
  
}
