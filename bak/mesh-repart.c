/* Mesh Partitioning in M and then repartitioning in N */
/* Author(s): aurelien.esnard@labri.fr */

/* Example: */
/* $ ./mesh-repart -c -d ../data/grid-200x200.txt ../data/grid-200x200.txt.kgggp5 5 5 SR DIFF */
/* $ ./mesh-repart -w 10 -a 0 -d ../data/grid-200x200.txt ../data/grid-200x200.txt.kgggp5 5 7 SR HB2 DIFF */
/* $ ./mesh-repart -w 10 -c -d  -o output.vtk -a 0  ../data/grid-200x200.txt ../data/grid-200x200.txt.kgggp8 8 13 SR HB2 DIFF */
/* $ ./mesh-repart -w 10 -c -d  -o output.vtk -a 0 -f mm ../data/uf/brack2 ../data/uf/brack2.kgggp8 8 12 SR HB2 DIFF */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <sys/time.h>

#include <metapart.h>
// #include <svg.h>

// #define SVG              // SVG output
#define VTKDEBUG

#define UBFACTOR 5
#define MIGCOST 1
#define REPARTMULT 1

#define XSTR(s) STR(s)
#define STR(s) #s
extern const char *usage;

/* *********************************************************** */

void saveSVG2(int M, int N, int* matrix, Graph * quotient, Mesh * mesh, int* oldpart, char* basename)
{  
  assert(mesh && mesh->nodes);

  /* hypergraph model */
  Hypergraph dualhg, hg;
  dense2sparse((int*)matrix, M, N, &dualhg);
  dualHypergraph (&dualhg, &hg);
  
  // model svg output
  char filename[255];
  // int use_cells = (addMeshPartition == addMeshCellVariableI);
  int use_cells = 1;
  double coords[M][2];
  initCoordsFromMesh(M, mesh, use_cells, oldpart, coords);
  
  snprintf (filename, 255, "%s.svg", basename);
  FILE *file = fopen (filename, "w");      
  
  beginSVGFile (file);
  // drawQuotientAndHypergraph (file, &quotient, &modelhg, mesh, (addMeshPartition == addMeshCellVariableI), oldpart);
  drawEdges(file, quotient, coords, "black", 1, 0); // tag=1 & oriented=0
  drawHyperedges(file, &hg, coords);
  drawVertices (file, M, coords, hg.vwgts);  
  endSVGFile (file);      
  fclose (file);
  printf("Saving SVG: %s\n", filename);
  
  freeHypergraph(&dualhg);
  freeHypergraph(&hg);
}


/* *********************************************************** */

/* algebraic sending matrix of dimension MxN (i send matrix[i][j] data to j; if matrix[i][j] is negative, it means i receive from j) */
void saveSVG(int M, int N, int* matrix, Graph * quotient, Mesh * mesh, int* part, char* basename)
{
  assert(mesh && mesh->nodes);

  /* hypergraph model */
  Hypergraph dualhg, hg;
  dense2sparse((int*)matrix, M, N, &dualhg);
  dualHypergraph (&dualhg, &hg);
  
  // compute extended matrix (dimension NxN)
  int * ggmat = calloc(N*N, sizeof(int));
  for(int i = 0 ; i < M ; i++)
    for(int j = 0 ; j < N ; j++) 
      if(i != j && matrix[i*N+j] != 0) ggmat[i*N+j] = ggmat[j*N+i] = 1;
  
  /* printf("prout2\n"); */
  /* printCommMatrix(ggmat, N, N); */
  
  /* printf("prout1\n");   */
  /* printCommMatrix(matrix, M, N); */
  
  /* compute extended graph */
  Graph gg;
  mat2Graph(N, ggmat, &gg);  
  /* fill weight for vertices & edges */
  gg.vwgts = malloc(gg.nvtxs*sizeof(int));
  memset(gg.vwgts, 0, gg.nvtxs*sizeof(int)); // N vertices
  // memcpy(gg.vwgts, g->vwgts, g->nvtxs*sizeof(int)); // copy M first weight
  
  // apply migration (remove data sent and add data receive) !  
  for(int j = 0 ; j < N ; j++) {
    if(j < M)
      gg.vwgts[j] = matrix[j*N+j]; // in-place 
    else 
      gg.vwgts[j] = 0;
    for(int i = 0 ; i < M ; i++) {
      if(i != j) gg.vwgts[j] += matrix[i*N+j]; 
    }
  }
  
  gg.ewgts = malloc(2*gg.nedges*sizeof(int));
  memset(gg.ewgts, 0, 2*gg.nedges*sizeof(int)); // reset edge weight  
  for(int i = 0 ; i < gg.nvtxs ; i++) 
    for(int k = gg.xadj[i] ; k < gg.xadj[i+1] ; k++) {
      int j = gg.adjncy[k];  /* edge (i,j) */
      if(i < M && j < N && matrix[i*N+j] != 0) {      
	gg.ewgts[k] = matrix[i*N+j]; 
      }
      else if(j < M && i < N && matrix[j*N+i] != 0) { // ???
	gg.ewgts[k] = -matrix[j*N+i]; 
      }
      
    }    
  
  
  /* compute coords for quotient graph layout */
  double * coords = calloc(N*2,sizeof(double));
  initCoordsFromMesh(M, mesh, 1, part, (double(*)[2])coords);
  
  double center[2];
  center[0] = center[1] = 0;
  for(int i = 0; i < M ; i++) {
    center[0] += coords[i*2+0];
    center[1] += coords[i*2+1];
  }
  center[0] /= M;
  center[1] /= M;
  
  for(int j = M; j < N ; j++) { /* new part */
    coords[j*2+0] = 0;
    coords[j*2+1] = 0;    
    int k =0;
    for(int i = 0; i < M ; i++) {
      if(matrix[i*N+j] > 0) {
	coords[j*2+0] += coords[i*2+0];
	coords[j*2+1] += coords[i*2+1];
	k++;
      }
    }
    coords[j*2+0] /= k;
    coords[j*2+1] /= k;
  }
  
  int rmax = 0, r = 0;
  for(int i = 0; i < M ; i++) {
    r = (coords[i*2+0]-center[0])*(coords[i*2+0]-center[0]);
    r += (coords[i*2+1]-center[1])*(coords[i*2+1]-center[1]);
    r = sqrt(r);
    if(r > rmax) rmax = r;
  }
  // printf("rmax = %d \n", rmax);
  
  for(int i = M; i < N ; i++) {
    r = (coords[i*2+0]-center[0])*(coords[i*2+0]-center[0]);
    r += (coords[i*2+1]-center[1])*(coords[i*2+1]-center[1]);
    r = sqrt(r);
    coords[i*2+0] = center[0] + ((coords[i*2+0]-center[0])*1.5*rmax/r);
    coords[i*2+1] = center[1] + ((coords[i*2+1]-center[1])*1.5*rmax/r);    
  } 
  
  // resized coords
  double * rcoords = calloc(N*2,sizeof(double));
  memcpy(rcoords, coords, N*2*sizeof(double));
  const int boxsize = 600;
  resizeCoords(N, (double(*)[2])rcoords, boxsize, 0); 
  
  /* ************ SVG ************ */
  
  char svgfilename[256];
  FILE * svg;
  
  /* saving quotient graph as SVG (initial state) */
  snprintf (svgfilename, sizeof(svgfilename),"%s_0.svg", basename);  
  svg = fopen(svgfilename, "w+");
  beginSVGFile(svg);
  drawEdges(svg, quotient, (double(*)[2])rcoords, "black", 1, 0);  // draw quotient edges
  // drawHyperedges (svg, &modelhg, rcoords);
  drawVertices (svg, M, (double(*)[2])rcoords, quotient->vwgts);
  endSVGFile(svg);
  fclose(svg);
  printf("SVG output written in %s\n", svgfilename); 
  
  /* saving quotient graph as SVG (migration) */
  snprintf (svgfilename, sizeof(svgfilename),"%s_1.svg", basename);
  svg = fopen(svgfilename, "w+");
  beginSVGFile(svg);
  // drawGraph(svg, &migg, rcoords, 1, 1, 1);     
  drawEdges(svg, quotient, (double(*)[2])rcoords, "black", 0, 0);  // draw quotient edges
  drawEdges(svg, &gg, (double(*)[2])rcoords, "blue", 1, 1);  // draw migration edges
  // drawHyperedges (svg, &modelhg, rcoords);
  drawVertices (svg, N, (double(*)[2])rcoords, gg.vwgts);
  endSVGFile(svg);
  fclose(svg);
  printf("SVG output written in %s\n", svgfilename); 
  
  /* saving hyperedges */
  snprintf (svgfilename, sizeof(svgfilename),"%s_2.svg", basename);
  svg = fopen(svgfilename, "w+");
  beginSVGFile (svg);
  // drawQuotientAndHypergraph (svg, &quotient, &dual, &m, 1, part);
  drawEdges (svg, quotient, (double(*)[2])coords, "black", 1, 0); // tag=1 & oriented=0
  drawHyperedges (svg, &hg, (double(*)[2])coords);
  // drawVertices (svg, M, (double(*)[2])coords, gg.vwgts); 
  drawVertices (svg, M, (double(*)[2])coords, NULL); 
  endSVGFile (svg);
  fclose(svg);
  printf("SVG output written in %s\n", svgfilename); 
  
  // free
  free(coords);
  free(rcoords);
  free(ggmat);
  freeGraph(&gg);
}

/* *********************************************************** */
/* *********************************************************** */

static const struct scheme {
  enum RepartScheme rscheme;
  enum CommScheme cscheme;
  const void *options;
  const char *name;
} scheme_map[] = {
  { SCRATCHREMAP, UNUSED, NULL, "SR" },
  { FIXEDVTXS, B1D, NULL, "1D" },
  { FIXEDVTXS, ZOLTAN, NULL, "ZOLTAN" },
  { FIXEDVTXS, UB1D, NULL, "UB1D" },
  { FIXEDVTXS, HM, NULL, "HM" },
  { FIXEDVTXS, UBHM, NULL, "UBHM" },
  { FIXEDVTXS, GREEDYHB, NULL, "HB" },
  { FIXEDVTXS, GREEDYHB2, &GHB_NO_DIAG, "HB2" },
  { FIXEDVTXS, GREEDYHB2, &GHB_OPT_DIAG, "HB2D" },
  { FIXEDVTXS, BRUTEFORCE, NULL, "BF" },
  { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_TOTALV, "DIFF" },
  { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_MAXV, "DIFFMV" },
  { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_TOTALZ, "DIFFTZ" },
  { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_MAXZ, "DIFFMZ" },
  { DIFFUSION, GREEDYHB2, NULL, "DIFF2" },
  { DIFFUSION_MM, OPTIMIZE, &GREEDYDEGREE_TOTALV, "DIFFMM" },
  { COUPLING_HYPEREDGES, UNUSED, NULL, "CH" },
  { COUPLING_HYPEREDGES_FIXEDVTXS, UNUSED, NULL, "CHF" },
  { COUPLING_HYPEREDGES_PERFECT, UNUSED, NULL, "CHP" },
  { -1, -1, NULL, NULL }
};

/* *********************************************************** */

int main(int argc, char * argv[]) 
{  
  int print_diag = 0, print_comm_mat = 0;
  char outfile[255] = "";
  int migcost = MIGCOST;
  int repartmult = REPARTMULT;
  int ubfactor = UBFACTOR;
  int use_graph = 1;
  //int change_weight = 0;
  int unbalance = -1;
  int seed = time(NULL);
  double *part_ub = NULL;
  int vtk_output = 0;
  int svg_output = 0;
  
  enum InputFormat {
    MESH,
    SCOTCH,
    RB,
    MM
  } input_format = MESH;  
  
  /* *********************************************************** */
  /*                   INPUT PARAMETERS                          */
  /* *********************************************************** */
  
  int opt;
  while ((opt = getopt (argc, argv, "f:hdcr:m:o:u:w:a:s")) != -1) {
    switch (opt) {
    case 'h':
      use_graph = 0;
      break;
      
    case 'd':
      print_diag = 1;
      break;
      
    case 'c':
      print_comm_mat = 1;
      break;
      
    case 'r':
      repartmult = atoi (optarg);
      break;
      
    case 'm':
      migcost = atoi (optarg);
      break;
      
    case 'o':
      vtk_output = 1;
      strncpy (outfile, optarg, sizeof(outfile));
      break;
      
    case 's':
      svg_output = 1;
      break;
      
    case 'u':
      ubfactor = atoi (optarg);
      break;
      
    case 'f':
      if (strcmp (optarg, "mesh") == 0)
	input_format = MESH;
      else if (strcmp (optarg, "scotch") == 0)
	input_format = SCOTCH;
      else if (strcmp (optarg, "mm") == 0)
	input_format = MM;
      else {
	fprintf (stderr, "Unknown input format: %s\n", optarg);
	exit (EXIT_FAILURE);
      }
      break;
      
      /*case 'w':
	change_weight = 1;
	if (optarg) {
	FILE *f = fopen (optarg, "r");
	int c = 0;
	double w;
	while (fscanf (f, "%lf", &w) == 1)
	c++;
	rewind (f);
	part_ub = malloc (c * sizeof (double));
	for (int i = 0; i < c; i++) {
	fscanf (f, "%lf", &part_ub[i]);
	}
	fclose (f);
	}
	break;*/
      
    case 'w':
      unbalance = strtol (optarg, NULL, 10);
      break;
      
    case 'a':
      seed = atoi (optarg);
      break;
      
    default:
      fprintf (stderr, usage, argv[0]);
      exit (EXIT_FAILURE);
    }
  }
  
  if(argc - optind < 4) {
    fprintf (stderr, "Not enough arguments\n");
    fprintf (stderr, usage, argv[0]);
    exit (EXIT_FAILURE);
  }  
  
  /* initialize random seed */
  srand(seed);
  printf("Random Seed = %d\n", seed);
  
  
  char *meshfile = argv[optind];
  char *partitionfile = argv[optind+1];
  int M = atoi (argv[optind+2]);
  int N = atoi (argv[optind+3]);
  
  assert(M > 0 && N > 0);
  
  int nb_schemes = argc - optind - 4;
  int schemes[nb_schemes];
  for (int i = 0; i < nb_schemes; i++) {
    int j = 0;
    while (scheme_map[j].name) {
      if (strcmp (argv[optind+4+i], scheme_map[j].name) == 0) {
	schemes[i] = j;
	break;
      }
      j++;
    }
    if (!scheme_map[j].name) {
      fprintf (stderr, "Unknown scheme \"%s\"\n", argv[optind+4+i]);
      exit (EXIT_FAILURE);
    }
  }
  
  if (outfile[0] == '\0') {
    char basename[255];
    int length = strlen(meshfile);
    int i;
    for (i = length-1; i >= 0; i--)
      if (meshfile[i] == '.')
	break;
    if (i < 0)
      i = length;
    strncpy (basename, meshfile, i);
    basename[i] = '\0';
    // char m[255] = "";
    // for (int i = 0; i < nb_schemes; i++) strcat (m, getSchemeName(schemes[i]));
    // snprintf(outfile, 255, "%s_%d_%d_%s.vtk", basename, M, N, m);
    // snprintf(outfile, 255, "%s_%d_%d.vtk", basename, M, N);
    snprintf(outfile, 255, "repart_%d_%d.vtk", M, N);
  }
  
  /* *********************************************************** */
  /*                     LOAD MESH                               */
  /* *********************************************************** */
  
  Mesh mesh;
  Hypergraph hg;
  Graph g;
  MeshVariableType meshvartype = CELL_INTEGER;
  
  if (input_format == MESH) {
    printf("Loading mesh: %s\n", meshfile);  
    loadMesh(meshfile, &mesh); 
    mesh2Graph(&mesh,&g);
    if(!use_graph)
      mesh2Hypergraph(&mesh, &hg);
    meshvartype = CELL_INTEGER;
  }
  else if (input_format == SCOTCH) {
    printf("Loading Scotch graph.[grf,xyz]: %s\n", meshfile);  
    char filename[256];
    snprintf (filename, sizeof(filename), "%s.grf", meshfile);
    loadScotchGraph (filename, &g);
    double *coords = malloc (3*g.nvtxs * sizeof(double));
    snprintf (filename, sizeof(filename), "%s.xyz", meshfile);
    loadScotchCoordinates (filename, g.nvtxs, coords);
    graph2LineMesh(&g, coords, &mesh);
    if(!use_graph)
      graph2HypergraphV(&g, &hg);
    meshvartype = NODE_INTEGER;
    free(coords);
  }
  else if (input_format == MM) {
    char filename[256];
    snprintf (filename, sizeof(filename), "%s.mtx", meshfile);
    loadMMGraph (filename, &g);
    double *coords = malloc (3*g.nvtxs * sizeof(double));
    snprintf (filename, sizeof(filename), "%s_coord.mtx", meshfile);
    if (loadMMCoordinates (filename, g.nvtxs, coords) == EXIT_SUCCESS) {
      graph2LineMesh(&g, coords, &mesh);
      if(!use_graph)
	graph2HypergraphV(&g, &hg);
      meshvartype = NODE_INTEGER;
    }
    free (coords);
  }
  
  /* *********************************************************** */
  /*                     LOAD PARTITION                          */
  /* *********************************************************** */
  
  int nvtxs = g.nvtxs;
  
  /* Loading (hyper)graph partition in M */
  
  printf("********************* %sGRAPH LOAD PARTITION in M *********************\n", use_graph?"":"HYPER");
  
  int *oldpart = malloc (nvtxs * sizeof (int));
  loadPartition (partitionfile, nvtxs, oldpart);
  
  if (print_diag) {
    if (use_graph)
      printGraphDiagnostic (&g, M, oldpart);
    else
      printHypergraphDiagnostic(&hg, M, oldpart);
  }
  
  if(vtk_output)
    addMeshVariable(&mesh, "oldpart", meshvartype, 1, oldpart);
  
  
  //if (change_weight) {
  if (unbalance >= 0) {
    printf("********************* UNBALANCED PARTITION in M *********************\n");
    printf ("Using seed %d\n", seed);
    srand (seed);
    
    if (!g.vwgts)
      g.vwgts = malloc (g.nvtxs * sizeof (int));
    /*unbalancePartition(g.vwgts, g.nvtxs, M, oldpart, part_ub);*/
    unbalancePartitionLinear (g.vwgts, g.nvtxs, M, oldpart, unbalance);
    if (!use_graph) {
      if (!hg.vwgts)
	hg.vwgts = malloc (hg.nvtxs * sizeof (int));
      memcpy (hg.vwgts, g.vwgts, g.nvtxs * sizeof (int));
    }
    
    
    if (print_diag) {
      if (use_graph)
	printGraphDiagnostic (&g, M, oldpart);
      else
	printHypergraphDiagnostic(&hg, M, oldpart);
    }
    
    if(vtk_output) 
      addMeshVariable(&mesh, "weights", meshvartype, 1, g.vwgts);
  }
  
  Graph quotient;
  quotientGraph (&g, M, oldpart, &quotient);
  
  /* *********************************************************** */
  /*                     REPARTITIONING                          */
  /* *********************************************************** */
  
  int *repart = malloc (nvtxs * sizeof (int));
  
  for (int k = 0; k < nb_schemes; k++) {
    const struct scheme *scheme = &scheme_map[schemes[k]];
    srand (seed);
    
    printf("********************* %sGRAPH RE-PARTITIONING in N (%s) *********************\n", use_graph?"":"HYPER", scheme->name);
    
    int edgecut = 0;
    
    int modelmat[M][N]; /* the communication matrix used as model */
    int realmat[M][N];  /* the real communication matrix obtained */   
    
    struct timeval start, end;
    gettimeofday(&start, NULL);
    
    int doremap = 0;
    
#ifdef VTKDEBUG
    if (use_graph)
      repartitionGraph(&g, M, oldpart, ubfactor, repartmult, migcost, N, scheme->rscheme, scheme->cscheme, scheme->options, repart, &edgecut, (int*)modelmat, (int*)realmat, doremap, &mesh, NULL); // "SCOTCHK"
    else
      repartitionHypergraph(&hg, M, oldpart, ubfactor, repartmult, migcost, N, scheme->rscheme, scheme->cscheme, scheme->options, repart, &edgecut, (int*)modelmat, (int*)realmat, &mesh);  
#else
    if (use_graph)
      repartitionGraph(&g, M, oldpart, ubfactor, repartmult, migcost, N, scheme->rscheme, scheme->cscheme, scheme->options, repart, &edgecut, (int*)modelmat, (int*)realmat, doremap, NULL, NULL); // "SCOTCHK"
    else
      repartitionHypergraph(&hg, M, oldpart, ubfactor, repartmult, migcost, N, scheme->rscheme, scheme->cscheme, scheme->options, repart, &edgecut, (int*)modelmat, (int*)realmat, NULL);  
#endif
    
    gettimeofday(&end, NULL);
    
    /* NOTA BENE: no model used for SCRATCHREMAP and COUPLING_HYPEREDGES schemes !!! */
    Hypergraph dualmodelhg, modelhg;
    dense2sparse((int*)modelmat, M, N, &dualmodelhg); 
    dualHypergraph (&dualmodelhg, &modelhg);      
    Hypergraph dualrealhg, realhg;
    dense2sparse((int*)realmat, M, N, &dualrealhg); 
    dualHypergraph (&dualrealhg, &realhg);          
    
    /* // compute real communication matrix */
    /* int comms[M][N]; */
    /* memset (comms, 0, sizeof(comms)); */
    /* if(use_graph) */
    /*   for (int i = 0; i < g.nvtxs; i++) */
    /* 	comms[oldpart[i]][repart[i]] += g.vwgts?g.vwgts[i]:1; */
    /* else */
    /*   for (int i = 0; i < hg.nvtxs; i++) */
    /* 	comms[oldpart[i]][repart[i]] += hg.vwgts?hg.vwgts[i]:1; */
    
    /* // compute remapping */
    /* int * perm = malloc (N * sizeof (int)); */
    /* remap(M, N, (int*)comms, perm); */
    
    // print repart diagnostic    
    if (print_diag) {
      if (use_graph)
	printGraphDiagnostic(&g, N, repart);
      else
	printHypergraphDiagnostic(&hg, N, repart);      
      
      // print comm diagnostic 
      // diagComms (M, N, comms);
      
      // migration
      int tot = 0;
      int nbcomms = 0;
      for(int i = 0 ; i < M ; i++) 
	for(int j = 0 ; j < N ; j++) {
	  tot += realmat[i][j];
	  if(realmat[i][j] > 0) nbcomms++;
	}
      
      int inplace = 0;
      for(int i = 0 ; i < M && i < N ; i++) {
	if(i >= 0) inplace += realmat[i][i];
      }
      
      int mig = tot - inplace;
      
      printf("  - total comm = %d\n", tot);
      printf("  - in-place comm = %d (%.2f%%)\n", inplace, inplace*100.0/tot);
      printf("  - migration comm = %d (%.2f%%)\n", mig, mig*100.0/tot);
      if (scheme->rscheme != SCRATCHREMAP) printf("  - nb model comms = %d\n", modelhg.eptr[modelhg.nhedges]);
      printf("  - nb real comms = %d\n", nbcomms);     
      
      // compute matching score !
      int score = matchingScore (&dualmodelhg, &quotient, NULL);
      printf("  - matching score = %d\n", score);      
      
      // compute old edgecut !!!
      int oldedgecut = 0;
      for(int i = 0 ; i < M ; i++) 
	for(int k = quotient.xadj[i] ; k < quotient.xadj[i+1] ; k++)
	  oldedgecut += quotient.ewgts[k];
      oldedgecut /= 2;
      printf("  - old edge cut = %d\n", oldedgecut);
      
      // compute new edgecut !!!
      int newedgecut = 0;
      Graph newquotient;
      quotientGraph (&g, N, repart, &newquotient);
      for(int j = 0 ; j < N ; j++) 
	for(int k = newquotient.xadj[j] ; k < newquotient.xadj[j+1] ; k++)
	  newedgecut += newquotient.ewgts[k];
      newedgecut /= 2;
      printf("  - new edge cut = %d\n", newedgecut);
      
      printf("  - time = %.2f ms\n", ((end.tv_sec*1000.0*1000.0+end.tv_usec)-(start.tv_sec*1000.0*1000.0+start.tv_usec))/1000.0 ); // in ms
    }
    
    // communication matrix        
    if (print_comm_mat && (scheme->rscheme != SCRATCHREMAP)) {      
      // print model
      // printf("Model "); 
      // printHypergraph(&model);
      
      printf("********************* MxN COMM MODEL for %s *********************\n", scheme->name);         
      
      /* int * modelcomms = malloc(M * N * sizeof (int)); */
      /* assert(modelcomms); */
      /* sparse2dense(&model, modelcomms); */
      /* computeComms(modelcomms, M, N, quotient.vwgts);  */
      
      // int * modelperm = malloc (N * sizeof (int));
      // remap(M, N, (int*)modelmat, modelperm);
      // printCommMatrixPerm(M, N, (int*)modelmat, modelperm);    
      // free(modelcomms);
      // free(modelperm);
      
      printCommMatrixPerm((int*)modelmat, M, N, NULL);    
    }
    
    if (print_comm_mat) {            
      printf("********************* MxN COMM MATRIX for %s *********************\n", scheme->name);   
      // printCommMatrixPerm(M,N, (int*)comms, perm);
      printCommMatrixPerm((int*)realmat, M, N, NULL);
    }
    
    // free(perm);  
    
    // SVG
    if(svg_output) {
      char basename[255];
      snprintf (basename, 255, "repart_%d_%d_model_%s", M, N, scheme->name);
      saveSVG(M, N, (int*)realmat, &quotient, &mesh, oldpart, basename);
      saveSVG2(M, N, (int*)modelmat, &quotient, &mesh, oldpart, basename);
      snprintf (basename, 255, "repart_%d_%d_real_%s", M, N, scheme->name);
      saveSVG2(M, N, (int*)realmat, &quotient, &mesh, oldpart, basename);
    }
    
    if(vtk_output) {
      char varname[32];
      snprintf (varname, 32, "repart_%s", scheme->name);
      addMeshVariable(&mesh, varname, meshvartype, 1, repart);
    }
    
    freeHypergraph(&modelhg);    
    freeHypergraph(&dualmodelhg);        
  }
  
  free (repart);
  freeGraph (&quotient);
  
  if(vtk_output) {
    printf("Saving VTK mesh: %s\n", outfile);
    saveVTKMesh(outfile, &mesh);
  }
  
  free (oldpart);
  if(!use_graph) freeHypergraph (&hg);
  freeGraph (&g);
  freeMesh (&mesh);
  if (part_ub) free (part_ub);
  
  return EXIT_SUCCESS;
}

/* *********************************************************** */

const char *usage = "Usage: %s [-d] [-h] [-c] [-s] [-a seed] [-r repartmult] [-m migcost] [-o outfile] mesh partition M N [SR|ZOLTAN|1D|UB1D|HM|UBHM|CH|CHP|HB ...]\n"
  " -h\t\tUse hypergraph partitioner instead of graph\n"
  " -d\t\tPrint diagnostic\n"
  " -c\t\tPrint communication matrix\n"
  " -r repartmult\tSet repartition multiplier (default is " XSTR(REPARTMULT) ")\n"
  " -m migcost\tSet migration cost (default is " XSTR(MIGCOST) ")\n"
  " -o outfile\tChoose vtk mesh out file\n"
  " -s\tSVG output\n"
  " -u ubfactor\tSet the unbalance factor (default is " XSTR(UBFACTOR) ")\n"
  " -w[file]\tchange vertex weights from file or randomly after initial partition\n"
  " -a seed\tSet random generator seed\n"
  " -f format\tChoose input format (mesh, scotch, mm). Just give basename for scotch and mm.\n";
								       
