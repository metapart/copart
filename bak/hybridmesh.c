
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <stdarg.h>
#include <errno.h>
#include <assert.h>
#include <limits.h>

#include "hybridmesh.h"
#include "copart.h"
#include "struct.h"
#include "mesh.h"
#include "graph.h"
#include "geom.h"
#include "tools.h"
#include "repart.h"
#include "partition.h"
#include "debug.h"
#include "diag.h"
#include "io.h"
#include "sort.h"

/* *********************************************************** */

#define UBFACTOR 5

#define NBPARTAVBPCPL 14

#define MAX_LINE_LENGTH 300 

#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))

/* connectivity parameters for polyhedron only  */
#define EDGE_CONNECTIVITY 0 
#define FACE_CONNECTIVITY 1 

/* *********************************************************** */

void HcellCenter(HybridMesh * msh,    /* [in] input mesh */
		int i,         /* [in] cell index */
		double G[3])   /* [out] gravity center */
{
  int * cell = &msh->cells[i*(msh->max_elm_size+1)];

  int cellsize = ELEMENT_SIZE[msh->cells[i*(msh->max_elm_size+1)]];
  
  double * coords[cellsize];
  for(int k = 1; k < cellsize+1; k++)
    coords[k-1] = &msh->nodes[cell[k]*3];
  
  polygonCenter(cellsize, coords, G);
}

/* *********************************************************** */

void HcellCenters(HybridMesh * msh,
		 double * centers)
{
  for(int i = 0 ; i < msh->nb_cells ; i++)
    HcellCenter(msh, i, centers+3*i);
}


/* *********************************************************** */

void HboundingBox(HybridMesh * msh,        /* [in] input mesh */
		  double bb[3][2])   /* [out] 3D bounding box */
{
  if(msh->nb_nodes == 0) return;
  
  /* initialization */
  for(int j = 0 ; j < 3 ; j++) {
    bb[j][0] = +INFINITY; // min
    bb[j][1] = -INFINITY; // max
  }
  
  for(int i = 0 ; i < msh->nb_nodes ; i++) 
    for(int j = 0 ; j < 3 ; j++) {
      /* update lower bound */
      if(msh->nodes[i*3+j] <  bb[j][0]) bb[j][0] = msh->nodes[i*3+j]; // min
      /* update upper bound */
      if(msh->nodes[i*3+j] >  bb[j][1]) bb[j][1] = msh->nodes[i*3+j]; // max
    }  
  
}

/* *********************************************************** */

void HcellLength(HybridMesh * msh,    /* [in] input mesh */
		 int i,         /* [in] cell index */
		 double length[3]) /* [out] cell length on x,y,z */  
{
  int cellsize =  ELEMENT_SIZE[msh->cells[i*(msh->max_elm_size+1)]];
  int * cell = msh->cells + i*(msh->max_elm_size+1);   
  
  length[0] = length[1] = length[2] = -1.0;
  
  /* for each pair (k0,k1) of nodes in cell */
  for(int k0 = 1; k0 < cellsize + 1; k0++) {
    for(int k1 = k0+1; k1 < cellsize + 1; k1++) {
      
      double * A0 = msh->nodes + cell[k0]*3;
      double * A1 = msh->nodes + cell[k1]*3;
      
      /* update the max length */ 
      // for(int i = 0 ; i < 3 ; i++) {
      double lx = fabs(A0[0] - A1[0]);
      double ly = fabs(A0[1] - A1[1]);
      double lz = fabs(A0[2] - A1[2]);
      if(lx > length[0]) length[0] = lx; 
      if(ly > length[1]) length[1] = ly; 
      if(lz > length[2]) length[2] = lz; 
    }
  }  
}

/* *********************************************************** */

void HcellsLength(HybridMesh * msh, double length[3])   /* [in] input mesh */
  
{
  /* for all cells, compute max cell length... */
  
  length[0] = length[1] = length[2] = -1.0;
  int index = -1;
  
  for (int i = 0 ; i < msh->nb_cells ; i++) {    
    double l[3];
    HcellLength(msh, i, l);
    if(l[0] > length[0]) length[0] = l[0]; 
    if(l[1] > length[1]) length[1] = l[1]; 
    if(l[2] > length[2]) length[2] = l[2]; 
  }
  
  // PRINT("index of the longest cell = %d, length = %.2f\n", index, l);
  
}

/* *********************************************************** */


void addHybridMeshVariable(HybridMesh * hmesh, 
			   const char * id,
			   MeshVariableType type,
			   int nb_components, 
			   int * data)
{
  assert(type == CELL_INTEGER);

  int k = hmesh->nb_cell_vars++;
  hmesh->cell_vars = realloc(hmesh->cell_vars, hmesh->nb_cell_vars*sizeof(Variable));
  assert(hmesh->cell_vars != NULL);
  strcpy(hmesh->cell_vars[k].id, id);
  hmesh->cell_vars[k].nb_components = nb_components;
  hmesh->cell_vars[k].nb_tuples = hmesh->nb_cells * nb_components;
  hmesh->cell_vars[k].type = VAR_INT;
  hmesh->cell_vars[k].data = malloc (hmesh->nb_cells * nb_components * sizeof(int));
  assert(hmesh->cell_vars[k].data);
  memcpy (hmesh->cell_vars[k].data, data, hmesh->nb_cells * nb_components * sizeof(int));
}


/* *********************************************************** */

#define MAX_NB_COMPONENTS 8

/* *********************************************************** */ 

int intersectHybridCells(HybridMesh * mshA, /* [in] input mesh A */
			 Mesh * mshB,   /* [in] input mesh B */
			 int a,         /* [in] cell index in mesh A */
			 int b)         /* [in] cell index in mesh B */
{
  int i;
  //  for ( j = 1; j < ELEMENT_SIZE[hmesh->cells[i*(elm_size+1)]]+1; j++) {
  int elm_size = mshA->max_elm_size;
  
  int cellsizeA = ELEMENT_SIZE[mshA->cells[a*(elm_size+1)]];
  //debug
  printf("Size of element indexed: %d is %d\n",a,cellsizeA);
  //ELEMENT_SIZE[mshA->elm_type]; // nb of nodes
  int cellsizeB = ELEMENT_SIZE[mshB->elm_type]; // nb of nodes
  int * cellA = mshA->cells + a*(elm_size+1);   
  int * cellB = mshB->cells + b*cellsizeB;   
  double * coordsA[cellsizeA];
  double * coordsB[cellsizeB];
  
  for (int i = 1 ; i < cellsizeA + 1; i++)
    coordsA[i] = mshA->nodes + cellA[i]*3;
  for (int i = 0 ; i < cellsizeB ; i++)
    coordsB[i] = mshB->nodes + cellB[i]*3;

  double _w;
  
  /* 2D polygon-polygon intersection (surface) */
  if( (mshA->cells[a*(elm_size+1)] == TRIANGLE || mshA->cells[a*(elm_size+1)] == QUADRANGLE) && 
      (mshB->elm_type == TRIANGLE || mshB->elm_type == QUADRANGLE) )
    printf("change the static inline...\n");
  // return intersectPolygon2D(cellsizeA, cellsizeB, coordsA, coordsB); // z not used !!!  
  /* 3D polyhedron-polyhedron intersection (volume) */
  else if( (mshA->cells[a*(elm_size+1)] == TETRAHEDRON || mshA->cells[a*(elm_size+1)] == PRISM || mshA->cells[a*(elm_size+1)] == HEXAHEDRON) &&  (mshB->elm_type == TETRAHEDRON || mshB->elm_type == PRISM || mshB->elm_type == HEXAHEDRON) ) 
    return intersectPolyhedron(cellsizeA, coordsA, cellsizeB, coordsB, &_w);  
  else {
    fprintf(stderr,"ERROR: intersectCells() failure!\n");
    assert(0);
  }
  return 0;
}

/* *********************************************************** */

void intersectHybridMesh(HybridMesh * mshA,        /* [in] input mesh A */
			 Mesh * mshB,        /* [in] input mesh B */
			 int * nbinteredges, /* [out] nb inter-edges */
			 int ** interedges)  /* [out] allocated array of inter-edges */
{
  *nbinteredges = 0;
  *interedges = NULL;
  
  /* 
     There is an interedge a-b between cell a and cell b if there is a
     geometric intersection between this two cells.
  */
  int i;
  for(i= 0; i< mshA->nb_components; i++) {
    assert( /* surface (3D) */
	   ((mshA->elm_types[i] == TRIANGLE || mshA->elm_types[i] == QUADRANGLE) &&
	    (mshB->elm_type == TRIANGLE || mshB->elm_type == QUADRANGLE)) ||
	   /* volume (3D) */
	   ((mshA->elm_types[i] == TETRAHEDRON || mshA->elm_types[i] == PRISM || mshA->elm_types[i] == HEXAHEDRON) &&
	    (mshB->elm_type == TETRAHEDRON || mshB->elm_type == PRISM || mshB->elm_type == HEXAHEDRON))
	    );
  }
  /* 1) compute cell centers */
  
  double * centersA = malloc(mshA->nb_cells*3*sizeof(double));
  double * centersB = malloc(mshB->nb_cells*3*sizeof(double));
  assert(centersA != NULL && centersB != NULL);
  
  for(int i = 0 ; i < mshA->nb_cells ; i++)
    HcellCenter(mshA, i, centersA + i*3); // ???
  
  for(int i = 0 ; i < mshB->nb_cells ; i++)
    cellCenter(mshB, i, centersB + i*3);
  
  /* 2) pre-compute coarse boxes for fast cell intersection algorithm */
  
  /* compute max cell length  */
  double hA[3]; 
  HcellsLength(mshA, hA);
  double hB[3]; 
  cellsLength(mshB, hB);
  double H[3];
  H[0] = 2.0*MAX(hA[0],hB[0]);
  H[1] = 2.0*MAX(hA[1],hB[1]);
  H[2] = 2.0*MAX(hA[2],hB[2]);
  printf("max cell length = (%.5f,%.5f,%.5f)\n", H[0], H[1], H[2]);       
  
  /* compute bounding box for A & B */
  
  double bbA[3][2];
  double bbB[3][2];
  HboundingBox(mshA, bbA);
  boundingBox(mshB, bbB);
  
  printf("bounding box A {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	 bbA[0][0], bbA[0][1], bbA[1][0], bbA[1][1], bbA[2][0], bbA[2][1]); /* debug */
  printf("bounding box B {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	 bbB[0][0], bbB[0][1], bbB[1][0], bbB[1][1], bbB[2][0], bbB[2][1]); /* debug */
  
  /* compute bounding box (bbA U bbB)*/
  double bb[3][2];
  for(int j = 0 ; j < 3 ; j++) {
    bb[j][0] = (bbA[j][0]<bbB[j][0])?bbA[j][0]:bbB[j][0]; /* lower bound */
    bb[j][1] = (bbA[j][1]>bbB[j][1])?bbA[j][1]:bbB[j][1]; /* upper bound */
  }
  
  fprintf(stdout, "bounding box {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	  bb[0][0], bb[0][1], bb[1][0], bb[1][1], bb[2][0], bb[2][1]); /* debug */
  
  /* compute box dim */  
  int boxdim[3];
  for(int j = 0 ; j < 3 ; j++)  {
    boxdim[j] = ceil((bb[j][1]-bb[j][0]) / H[j]);
    if(boxdim[j] == 0) boxdim[j] = 1;
  }
  
  fprintf(stdout, "box dim {%d %d %d}\n", boxdim[0], boxdim[1], boxdim[2]); 
  assert(boxdim[0] > 0 && boxdim[1] > 0 && boxdim[2] > 0); 
  
  /* compute mapping between cells and boxes */
  
  int nbboxes = boxdim[2]*boxdim[1]*boxdim[0];
  assert(nbboxes > 0);
  int **boxesA = calloc (nbboxes, sizeof(int *));
  int **boxesB = calloc (nbboxes, sizeof(int *));
  int *nbcellsinboxesA = calloc (nbboxes, sizeof(int));
  int *nbcellsinboxesB = calloc (nbboxes, sizeof(int));
  int * boxmappingA = malloc(mshA->nb_cells*sizeof(int));
  assert(boxmappingA != NULL);
  int * boxmappingB = malloc(mshB->nb_cells*sizeof(int));
  assert(boxmappingB != NULL);
  
  /* pre-compute nb cells per boxes for A and mapping */
  for(int i = 0 ; i < mshA->nb_cells ; i++) {
    double * G = centersA + i*3;    
    int boxcoordX = floor((G[0]-bb[0][0]) / H[0]);
    int boxcoordY = floor((G[1]-bb[1][0]) / H[1]);
    int boxcoordZ = floor((G[2]-bb[2][0]) / H[2]);
    // int k = boxcoordY*boxdim[0] + boxcoordX;
    int k = boxcoordZ*boxdim[0]*boxdim[1] + boxcoordY*boxdim[0] + boxcoordX;
    assert(boxcoordX >= 0 && boxcoordX < boxdim[0]);
    assert(boxcoordY >= 0 && boxcoordY < boxdim[1]);
    assert(boxcoordZ >= 0 && boxcoordZ < boxdim[2]);
    assert(k >= 0 && k < nbboxes);
    nbcellsinboxesA[k]++;
    boxmappingA[i] = k;
  }
  
  /* pre-compute nb cells per boxes for B */
  for(int i = 0 ; i < mshB->nb_cells ; i++) {
    double * G = centersB + i*3;    
    int boxcoordX = floor((G[0]-bb[0][0]) / H[0]);
    int boxcoordY = floor((G[1]-bb[1][0]) / H[1]);
    int boxcoordZ = floor((G[2]-bb[2][0]) / H[2]);
    int k = boxcoordZ*boxdim[0]*boxdim[1] + boxcoordY*boxdim[0] + boxcoordX;
    // int k = boxcoordY*boxdim[0] + boxcoordX;
    assert(boxcoordX >= 0 && boxcoordX < boxdim[0]);
    assert(boxcoordY >= 0 && boxcoordY < boxdim[1]);
    assert(boxcoordZ >= 0 && boxcoordZ < boxdim[2]);
    assert(k >= 0 && k < nbboxes);
    nbcellsinboxesB[k]++;
    boxmappingB[i] = k;
  }  
  
  /* debug */
  Mesh bbmsh;
  generateGrid3D(boxdim[0], boxdim[1], boxdim[2], H[0], H[1], H[2], &bbmsh); 
  shiftMesh(&bbmsh, bb[0][0], bb[1][0], bb[2][0]);
  addMeshVariable(&bbmsh, "nbcellsA", CELL_INTEGER, 1, nbcellsinboxesA);
  addMeshVariable(&bbmsh, "nbcellsB", CELL_INTEGER, 1, nbcellsinboxesB);
  printf("Saving bbmsh.vtk\n");
  saveVTKMesh("bbmsh.vtk", &bbmsh);    
  freeMesh(&bbmsh);
  
  /* allocate all boxes */
  int checkcellsumA = 0, checkcellsumB = 0;
  for(int k = 0 ; k < nbboxes ; k++) {
    checkcellsumA += nbcellsinboxesA[k];
    checkcellsumB += nbcellsinboxesB[k];
    boxesA[k] = boxesB[k] = NULL;
    if(nbcellsinboxesA[k] > 0) {
      boxesA[k] = malloc(nbcellsinboxesA[k]*sizeof(int));
      assert(boxesA[k] != NULL);
      
    }
    if(nbcellsinboxesB[k] > 0) {
      boxesB[k] = malloc(nbcellsinboxesB[k]*sizeof(int));    
      assert(boxesB[k] != NULL);
    }
  }
  /* check sum */
  assert(checkcellsumA == mshA->nb_cells &&  checkcellsumB == mshB->nb_cells);
  
  /* sort cells of mesh A in boxes */
  
  int *currentcellinboxesA = calloc (nbboxes, sizeof(int));
  for(int i = 0 ; i < mshA->nb_cells ; i++) {
    int k = boxmappingA[i];
    assert(currentcellinboxesA[k]  < nbcellsinboxesA[k]); 
    boxesA[k][currentcellinboxesA[k]] = i;
    currentcellinboxesA[k]++;
  }
  free (currentcellinboxesA);
  
  int *currentcellinboxesB = calloc (nbboxes, sizeof(int));
  for(int i = 0 ; i < mshB->nb_cells ; i++) {
    int k = boxmappingB[i];
    assert(currentcellinboxesB[k]  < nbcellsinboxesB[k]); 
    boxesB[k][currentcellinboxesB[k]] = i;
    currentcellinboxesB[k]++;
  }
  free (currentcellinboxesB);
  /* #define DEBUG   */
  /* #ifdef DEBUG   */
  /*   for(int k = 0 ; k < nbboxes ; k++) { */
  /*     PRINT("nb cells in box A[%d] = %d\n", k, nbcellsinboxesA[k]); */
  /*     PRINT("nb cells in box B[%d] = %d\n", k, nbcellsinboxesB[k]); */
  /*   } */
  /* #endif */
  
#define DEBUG  
#ifdef DEBUG  
  for(int k = 0 ; k < nbboxes ; k++) {
    if(nbcellsinboxesA[k])
      printf("nb cells in box A[%d] = %d\n", k, nbcellsinboxesA[k]);
    if(nbcellsinboxesB[k])
      printf("nb cells in box B[%d] = %d\n", k, nbcellsinboxesB[k]);
  }
#endif
  
  /* 3) compute inter edges */
  
  *nbinteredges = 0; /* out */
  double mi = round(sqrt(mshA->nb_cells)*sqrt(mshB->nb_cells)); 
  assert(mi < INT_MAX);
  int maxinteredges = (int)mi; /* can be more... *2 MARIA*/
  *interedges = malloc(maxinteredges*2*sizeof(int));  /* out */
  assert(*interedges);
  
  long nbtest = 0;
  long nbboxtest = 0;
  long maxboxtest = 0;
  
  /* for boxesA[k] and neighbor boxesB[kk], test intersection... */
  // #pragma omp parallel for
  for(int k = 0 ; k < nbboxes ; k++) {
    
    // debug
    if((k+1) % (int)(ceil(nbboxes/10.0)) == 0) fprintf(stdout,"%d%% : %d/%d boxes, %d interedges found\n", (int)((k+1)*100.0/nbboxes), k+1, nbboxes, *nbinteredges); 
    
    int z = k / (boxdim[0]*boxdim[1]);
    int r = k % (boxdim[0]*boxdim[1]);
    int y = r / boxdim[0];
    int x = r % boxdim[0];
    assert(x >= 0 && x < boxdim[0]);
    assert(y >= 0 && y < boxdim[1]);
    assert(z >= 0 && z < boxdim[2]);
    assert((z*boxdim[1]*boxdim[0] + y*boxdim[0] + x) == k);
    
    
    for(int dx = -1 ; dx <= 1  ; dx++) {
      int xx = x + dx;
      if(xx < 0 || xx >= boxdim[0]) continue;
      for(int dy = -1 ; dy <= 1  ; dy++) {
	int yy = y + dy;
	if(yy < 0 || yy >= boxdim[1]) continue;
	for(int dz = -1 ; dz <= 1  ; dz++) {
	  int zz = z + dz;
	  if(zz < 0 || zz >= boxdim[2]) continue;
	  
	  int kk = zz*boxdim[1]*boxdim[0] + yy*boxdim[0] + xx;
	  assert(kk >= 0 && kk < nbboxes);
	  
	  maxboxtest++;
	  
	  if(nbcellsinboxesA[k] == 0 || nbcellsinboxesB[kk] == 0) {
	    PRINT("skip box intersection of A%d and B%d\n", k, kk);
	    continue; // skip if box is empty
	  }    
	  
	  nbboxtest++;
	  
	  /* for all cells in boxesA[k] */
	  for(int a = 0 ; a < nbcellsinboxesA[k] ; a++)
	    for(int b = 0 ; b < nbcellsinboxesB[kk] ; b++)
	      {
		nbtest++;
		int aa = boxesA[k][a];   /* cell index in mesh A */
		int bb = boxesB[kk][b];  /* cell index in mesh B */
		PRINT("\n===== TEST INTEREDGE (%d,%d) from boxA %d and boxB %d =====\n\n", aa,bb, k, kk); 
		/* test inter-edge (aa,bb) */
		printf("aa is %d bb %d\n", aa,bb);
		if(intersectHybridCells(mshA,mshB,aa,bb)) {
		  printf("=> ADD INTEREDGE %d  = (%d,%d)\n", *nbinteredges,aa,bb);       
		  // #pragma omp critical
		  //{
		  (*interedges)[2 * *nbinteredges] = aa;
		  (*interedges)[2 * *nbinteredges + 1] = bb;
		  (*nbinteredges)++;
		  //}
		  
		  /* realloc if needed... */
		  if(*nbinteredges == maxinteredges) { 
		    mi *= 2; 
		    assert(mi < INT_MAX);
		    maxinteredges = (int)mi;
		    printf("=> REALLOC INTEREDGES WITH MAX = %d\n", maxinteredges);       
		    *interedges = realloc(*interedges, maxinteredges*2*sizeof(int));
		    printf("BEFORE THE SEG FAULT\n");
		    assert(*interedges);
		    
		  }
		  
		  assert(*nbinteredges < maxinteredges); // check
		  
		} /* for b */
	      } /* for a */
	  
	} /* for dz */	
      } /* for dy */
    } /* for dx */ 
    
  } /* for k */
  
  
  long maxtest = mshA->nb_cells*mshB->nb_cells;
  printf("=> mesh intersection: nb box/box tests = %ld/%ld, nb elm/elm tests = %ld/%ld (%.5f%%)\n", 
	 nbboxtest, maxboxtest, nbtest, maxtest, nbtest*100.0/maxtest);
  
  /* free unused memory */
  for(int k = 0 ; k < nbboxes ; k++) { 
    if(nbcellsinboxesA[k] > 0) free(boxesA[k]);
    if(nbcellsinboxesB[k] > 0) free(boxesB[k]);
  }
  free (nbcellsinboxesA);
  free (nbcellsinboxesB);
  free (boxesA);
  free (boxesB);
  
  free(boxmappingA);
  free(boxmappingB);
  
  *interedges = realloc(*interedges, 2 * (*nbinteredges) * sizeof(int)); 
  free(centersA);
  free(centersB);
  
  /* sort left interegde endpoint */
  /*int * left = malloc(_nbinteredges*sizeof(int));
    int * leftperm = malloc(_nbinteredges*sizeof(int));
    assert(left != NULL && leftperm != NULL);
    for(int i = 0 ; i < _nbinteredges ; i++) 
    left[i] = _interedges[2*i];
    sortI(_nbinteredges, left, leftperm);
    *interedges = malloc(_nbinteredges*2*sizeof(int));
    for(int i = 0 ; i < _nbinteredges ; i++) {
    (*interedges)[2*i] = _interedges[2*leftperm[i]];
    (*interedges)[2*i+1] = _interedges[2*leftperm[i]+1];
    }
    free(left); 
    free(leftperm);*/
}


/********************************************************/
/*** Begining of hybrid mesh fuctions definition *******/
/* Load hybridmesh from one file as command line argument*/
int loadFullHybridMesh(const char * input,  HybridMesh * mesh)
{
  int i,k;
  /* open input file */
  FILE * file;  
  if (NULL == (file = fopen(input, "r"))) {
    fprintf(stderr, "ERROR: opening file %s failed.\n",input);
    exit(EXIT_FAILURE);
  }
  
  int elm_types[MAX_NB_COMPONENTS];
  int nb_nodes = -1;
  int nb_cells = -1;
  int nb_components = -1;
  int nb_node_vars = -1;
  int nb_cell_vars = -1;
  int max_elm_size = -1;
  
  char line[MAX_LINE_LENGTH];
  
  int node_idx = 0;
  int cell_idx = 0;
  
  while (NULL !=  fgets(line, MAX_LINE_LENGTH, file)) {
    
    if(strlen(line) >= MAX_LINE_LENGTH -1)
      fprintf(stderr, "WARNING: line truncated when reading (too long)!\n"); 
    
    /* skip commentary or void line */
    if ((0 == strncmp("\n", line, 1)) || 
	(0 == strncmp("#", line, 1)) ||
	(0 == strncmp("%", line, 1)))
      continue; /* skip */
    
    /* read mesh header */
    
    if(nb_components == -1) {
      sscanf(line, "%d",&nb_components);
      for(i= 0; i< nb_components; i++) {
	sscanf(line, "%d", &elm_types[i]);
	if(ELEMENT_SIZE[elm_types[i]] > max_elm_size)  
	  max_elm_size =  ELEMENT_SIZE[elm_types[i]];
      }
      sscanf(line, "%d %d %d %d\n", &nb_nodes, &nb_cells, &nb_node_vars, &nb_cell_vars);
      /* check */
      assert(nb_nodes != -1 && nb_cells != -1);
      for(i= 0; i< nb_components; i++)       
	assert(elm_types[i] == LINE || elm_types[i] == TRIANGLE || elm_types[i] == QUADRANGLE || elm_types[i] == TETRAHEDRON || elm_types[i] == HEXAHEDRON || elm_types[i] == PRISM);
      
      /* initialize mesh */
      for(i= 0; i< nb_components; i++)
	mesh->elm_types[i] = elm_types[i];
      mesh->nb_nodes = nb_nodes;
      mesh->nb_cells = nb_cells;
      mesh->nodes = malloc(nb_nodes*3*sizeof(double));
      mesh->max_elm_size = max_elm_size; 
      mesh->cells = malloc(nb_cells*(max_elm_size+1)*sizeof(int));
      
      /* not yet supported */
      mesh->nb_node_vars = 0;
      mesh->nb_cell_vars = 0;
      mesh->node_vars = NULL;
      mesh->cell_vars = NULL;      
      
      if(nb_node_vars > 0 || nb_cell_vars > 0) {
	printf("WARNING: Variables are ignored while loading %s!\n", input);
      }
      
      continue;
    }
    
    /* /\* cells *\/     */
    /* if(cell_idx < nb_cells) { */
    /*   char * str = line; */
    /*   char * endptr; */
    /*   int elm_size = ELEMENT_SIZE[elm_type]; */
    /*   for(int k = 0 ; k < elm_size ; k++) { */
    /* 	int kk = strtol(str, &endptr, 10); */
    /* 	assert(kk >= 0 && errno != ERANGE); */
    /* 	mesh->cells[cell_idx*elm_size+k] = kk; */
    /* 	str = endptr;       */
    /*   } */
    /*   cell_idx++; */
    /*   continue; */
    /* } */
    
    /* nodes */
    
    if(node_idx < nb_nodes) {
      char * str = line;
      char * endptr;
      for(int k = 0 ; k < 3 ; k++) {
	double kk = strtod(str, &endptr);
	assert(errno != ERANGE);
	mesh->nodes[node_idx*3+k] = kk;
	assert(str != endptr);
	str = endptr;      
      }
      node_idx++;
      continue;
    }
    
  }  
  
  assert(cell_idx == nb_cells);
  assert(node_idx == nb_nodes);
  
  fclose(file);
  return EXIT_SUCCESS;
  
}  

/* *********************************************************** */

int saveHybridMesh(const char * output, HybridMesh * mesh)
{ 
  /* only line, triangle or quadrangle */
  int i, j;
  
  FILE * file;
  
  /* open output file */
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "Opening file %s failed.\n", output);
    return EXIT_FAILURE;
  }
  /* save header */
  fprintf(file, "# Mesh Header (nb_files, elm_types, nb_nodes, nb_cells, nb_node_vars, nb_cells_vars)\n");
  fprintf(file, "%d ", mesh->nb_components);
  for(i= 0; i< mesh->nb_components; i++) {
    fprintf(file, "%d ", mesh->elm_types[i]);
  }
  fprintf(file, "%d ", mesh->nb_nodes);
  fprintf(file, "%d ", mesh->nb_cells);
  fprintf(file, "%d ", mesh->nb_node_vars);
  fprintf(file, "%d\n", mesh->nb_cell_vars);
  
  fprintf(file, "#Cells\n");
  for (i = 0; i < mesh->nb_cells ; i++) {
    for ( j = 1; j < ELEMENT_SIZE[mesh->cells[i*(mesh->max_elm_size+1)]]+1; j++) {
      fprintf(file,"%d ",mesh->cells[(i*(mesh->max_elm_size+1))+j]);
    }
    fprintf(file,"\n");
  }
  
  fprintf(file, "#Nodes\n"); 
  for(i= 0; i< mesh->nb_nodes; i++) {
    for(j=0; j<3; j++) {  
      fprintf(file,"%.10lf ", mesh->nodes[i*3+j]);
    }	
    fprintf(file,"\n");
  }
  
  fclose(file);  
  return EXIT_SUCCESS;
}
/****** End of defining the hybridmesh functions****/  
/***************************************************/





/* Load a hybridmesh that constist of multiple number of files (parts). */

int loadHybridMesh(HybridMesh * hmesh, int nb_files, ...)
{
  /* variables for the variadic form of the function*/
  va_list list_files;
  int j = 0;
  int i = 0;
  int k = 0;
  /* Initializations for HybridMesh */
  hmesh-> elm_types = malloc(nb_files*sizeof(int));
  hmesh -> nb_components = nb_files;
  hmesh->nb_nodes = 0;
  hmesh->nb_cells = 0;
  
  int * each_mesh_nb_cells = malloc((nb_files)*sizeof(int));
  int * elm_types = malloc(nb_files*sizeof(int));
  for(i=0;i<nb_files; i++) elm_types[i] = -1;
  int elm_size = -1;
  int nb_nodes = -1;
  int nb_cells = -1;
  int nb_node_vars = -1;
  int nb_cell_vars = -1;
  int max_elm_size = 0;
  
  char line[MAX_LINE_LENGTH];
  
  int node_idx = 0;
  int cell_idx = 0;
  va_start(list_files, nb_files);
  FILE ** files = malloc(sizeof(FILE*)*(nb_files));
  const char * current_file;
  
  /* open input files for reading the headline only*/
  
  for(j=0; j < nb_files; j++) {
    /* you need to set it to zero each time you have a new file to manipulate. */  
    cell_idx = 0;  
    current_file = va_arg(list_files, char *);
    printf("%s\n",current_file);    
    if (NULL == (files[j] = fopen(current_file, "r"))) {
      fprintf(stderr, "ERROR: opening file %s failed.\n",current_file);
      exit(EXIT_FAILURE);
    }
    while (NULL !=  fgets(line, MAX_LINE_LENGTH, files[j])) {
      if(strlen(line) >= MAX_LINE_LENGTH -1)
	fprintf(stderr, "WARNING: line truncated when reading file %s (too long)!\n",current_file); 	    
      
      /* skip commentary or void line */
      if ((0 == strncmp("\n", line, 1)) || 
	  (0 == strncmp("#", line, 1)) ||
	  (0 == strncmp("%", line, 1)))
	continue; /* skip */
      
      /* read mesh header */
      if(elm_types[j] == -1) {
	sscanf(line, "%d %d %d %d %d\n", &elm_types[j], &nb_nodes, &nb_cells, &nb_node_vars, &nb_cell_vars);
	/* check */
	assert(nb_nodes != -1 && nb_cells != -1);
	assert((elm_types[j] == LINE) || (elm_types[j] == TRIANGLE) || 
	       (elm_types[j] == QUADRANGLE) || (elm_types[j] == TETRAHEDRON) || 
	       (elm_types[j] == HEXAHEDRON) || (elm_types[j] == PRISM));
	if(nb_nodes != hmesh->nb_nodes && hmesh->nb_nodes!= 0) {
	  fprintf(stderr, "ERROR: file %s doesn't contain the right number of nodes!\n",current_file);
	  exit(EXIT_FAILURE);
	}
	
	/* initialize hybridmesh */
	hmesh->elm_types[j] = elm_types[j];
	hmesh->nb_nodes = nb_nodes;
	hmesh->nb_cells = hmesh->nb_cells + nb_cells;			
	each_mesh_nb_cells[j] = nb_cells;
	hmesh->nb_node_vars = 0;
	hmesh->nb_cell_vars = 0;
	hmesh->node_vars = NULL;
	hmesh->cell_vars = NULL;      
	
	if(nb_node_vars > 0 || nb_cell_vars > 0) {
	  printf("WARNING: Variables are ignored while loading %s!\n", current_file);
	}
	if(ELEMENT_SIZE[elm_types[j]] > max_elm_size)  max_elm_size =  ELEMENT_SIZE[elm_types[j]]; 
	continue;
      }
      else break;
    }
    fclose(files[j]);
  }
  va_end(list_files);
  
  hmesh->max_elm_size = max_elm_size;
  hmesh-> cells = malloc(hmesh->nb_cells*(max_elm_size+1)*sizeof(int));
  hmesh->nodes = malloc(hmesh->nb_nodes*3*sizeof(double));      
  
  int cell_end = 0;
  va_start(list_files, nb_files);
  int headerline;
  for(j=0; j < nb_files; j++) {
    headerline = 1; 
    current_file = va_arg(list_files, char *);    
    
    if (NULL == (files[j] = fopen(current_file, "r"))) {
      fprintf(stderr, "ERROR: opening file %s failed.\n",current_file);
      exit(EXIT_FAILURE);
    }
    while (NULL !=  fgets(line, MAX_LINE_LENGTH, files[j])) {
      if(strlen(line) >= MAX_LINE_LENGTH -1)
	fprintf(stderr, "WARNING: line truncated when reading file %s (too long)!\n",current_file); 	    
      
      
      /* skip commentary or void line */
      if ((0 == strncmp("\n", line, 1)) || 
	  (0 == strncmp("#", line, 1)) ||
	  (0 == strncmp("%", line, 1)))
	continue; /* skip */
      else if(headerline == 1) { 
	/*things that will happen one time for each file.*/ 
	headerline = 0; 
	cell_end = cell_end + each_mesh_nb_cells[j];
	continue; 
      }	
      
      
      /* cells */
      
      elm_size = ELEMENT_SIZE[elm_types[j]];
      if(cell_idx < cell_end) {
	char * str = line;
	char * endptr;			
	for(i = 0 ; i < elm_size+1; i++) {
	  if(i == 0) hmesh->cells[(cell_idx*(max_elm_size+1))] = elm_types[j];
	  else {
	    int kk = strtol(str, &endptr, 10);
	    assert(kk >= 0 && errno != ERANGE);
	    hmesh->cells[(cell_idx*(max_elm_size+1))+i] = kk;
	    str = endptr;      
	  }
	}
	for(i = elm_size + 1 ; i < max_elm_size + 1 ; i++) 
	  hmesh->cells[(cell_idx*(max_elm_size+1))+i] = -1; 
	cell_idx++;
	continue;
      } 
      
      /* nodes */
      if(node_idx < hmesh->nb_nodes) {
	char * str = line;
	char * endptr;
	for(i = 0 ; i < 3 ; i++) {
	  double kk = strtod(str, &endptr);
	  assert(errno != ERANGE);
	  hmesh->nodes[(node_idx*3)+i] = kk;
	  assert(str != endptr);
	  str = endptr;      
	}
	
	node_idx++;
	continue;
      }  
    }
    fclose(files[j]);	
  }	 
  
  
  
  free(elm_types);
  free(files);
  return EXIT_SUCCESS;
  
}

/* *************************************************************************************/




/* Print diagnostic information about a hybridmesh.*/  
void printHybridMesh(HybridMesh * hmesh) {
  
  int i, j;
  printf("HYBRIDMESH\n");
  printf("- element_types:\n");
  for(i= 0; i< hmesh->nb_components; i++) {
    printf("= %d\n", hmesh->elm_types[i]);
  }
  printf("- maximum element size: %d\n", hmesh->max_elm_size);
  printf("- nb components: %d\n", hmesh->nb_components);
  printf("- nb nodes: %d\n", hmesh->nb_nodes);
  printf("- nb cells: %d\n", hmesh->nb_cells);
  
  /* print connectivity */
  int max_cells = 100; 
  int nb_cells = (hmesh->nb_cells <= max_cells) ? hmesh->nb_cells : max_cells;
  
  printf("- connectivity list\n");
  for (i = 0; i < nb_cells ; i++) {
    printf("%d -> [ ",i);
    for(j = 0 ; j < hmesh->max_elm_size+1 ; j++) {
      printf("%d ",hmesh->cells[(i*(hmesh->max_elm_size+1))+j]);
    }
    printf("]\n");
  }
  if (hmesh->nb_cells > max_cells)   printf("  ...\n");
  
  /* print node coordinates */
  int max_nodes = 100; 
  int nb_nodes = (hmesh->nb_nodes <= max_nodes) ? hmesh->nb_nodes : max_nodes;
  printf("- node coordinates\n");
  for(i= 0; i< nb_nodes; i++) {
    printf("%d -> [ ",i);
    for(j=0;j<3;j++) {  
      printf("%.10lf ", hmesh->nodes[i*3+j]);
    }	
    printf("]\n");
  }
  if (hmesh->nb_nodes > max_nodes)   printf("  ...\n");
  
  
  /* print variables */
  printf("- nb node variables = %d\n", hmesh->nb_node_vars);
  printf("- nb cell variables = %d\n", hmesh->nb_cell_vars);
  
  /* cell variables */
  for(int i = 0 ; i < hmesh->nb_cell_vars ; i++) {
    printf("- cell variable \"%s\" [", hmesh->cell_vars[i].id);
    for(int j = 0 ; j < nb_cells ; j++)
      if (hmesh->cell_vars[i].type == VAR_INT)
	printf("%d ", ((int *)hmesh->cell_vars[i].data)[j]);      
      else if (hmesh->cell_vars[i].type == VAR_DOUBLE)
	printf("%f ", ((double *)hmesh->cell_vars[i].data)[j]);      
    printf("]\n"); 
    if (hmesh->nb_cells > max_cells)   printf("  ...\n");    
  }
  
  /* node variables */
  for(int i = 0 ; i < hmesh->nb_node_vars ; i++) {
    printf("- node variable \"%s\" [", hmesh->node_vars[i].id);
    for(int j = 0 ; j < nb_nodes ; j++) 
      if (hmesh->node_vars[i].type == VAR_INT)
	printf("%d ", ((int *)hmesh->node_vars[i].data)[j]);      
      else if (hmesh->node_vars[i].type == VAR_DOUBLE)
	printf("%f ", ((double *)hmesh->node_vars[i].data)[j]);      
    printf("]\n");
    if (hmesh->nb_nodes > max_nodes)   printf("  ...\n");
  }
  
}


/* *********************************************************** */

/* void graph2HybridMesh(Graph * graph, */
/* 		double * coords, */
/* 		HybridMesh * hmesh) */
/* { */
/*   mesh->elm_type = LINE; /\* lines *\/ */
/*   mesh->nb_nodes = graph->nvtxs;            */
/*   mesh->nb_cells = graph->nedges; */
/*   mesh->nodes = malloc(3*mesh->nb_nodes*sizeof(double)); */
/*   mesh->cells = malloc(2*mesh->nb_cells*sizeof(int)); */
/*   mesh->nb_node_vars = 0; */
/*   mesh->nb_cell_vars = 0; */
/*   mesh->node_vars = NULL;   */
/*   mesh->cell_vars = NULL;   */

/*   int k = 0; */
/*   for(int i = 0; i < graph->nvtxs; i++) { */
/*     for(int j = graph->xadj[i]; j < graph->xadj[i+1]; j++) { */
/*       int ii = graph->adjncy[j]; /\* edge (i,ii) *\/ */
/*       if(i <= ii) { */
/* 	mesh->cells[k*2] = i; */
/* 	mesh->cells[k*2+1] = ii; */
/* 	k++; */
/*       } */
/*     } */
/*   } */

/*   /\* coords *\/ */
/*   memcpy(mesh->nodes, coords, 3*mesh->nb_nodes*sizeof(double)); */
/* } */

/* *********************************************************** */


void freeHybridMesh(HybridMesh * hmesh)
{
  int i;	
  if (hmesh != NULL){
    if (hmesh->nodes != NULL)
      free(hmesh->nodes);
    if (hmesh->cells != NULL)
      free(hmesh->cells);
    if (hmesh->elm_types!= NULL)
      free(hmesh->elm_types);
    if (hmesh->node_vars != NULL) {
      for ( i = 0; i < hmesh->nb_node_vars; i++)
	free (hmesh->node_vars[i].data);
      free(hmesh->node_vars);
    }
    if (hmesh->cell_vars != NULL) {
      for (i = 0; i < hmesh->nb_cell_vars; i++)
	free (hmesh->cell_vars[i].data);
      free(hmesh->cell_vars);
    }
  }
}




/* *********************************************************** */
void Hybridmesh2Graph(HybridMesh * hmesh,
		      Graph * graph)
{
  assert(hmesh);
  
  /*
    The graph vertices are represented by mesh elements.
    
    If the mesh uses surfacic elements (triangle or quad), there is one
    edge in the graph if two elements are connected by a mesh edge.
    
    If the mesh uses volumic elements (tetrahedron, hexahedron or
    prism), there is one edge in the graph if two elements are
    connected by a mesh face (FACEg_CONNECTIVITY) and/or by a mesh
    edge (EDGE_CONNECTIVITY).
  */   
  int i, j, k1, k2, f, x, e, k;
  
  for (i = 0; i < hmesh->nb_components; i++) {
    assert(hmesh->elm_types[i] == LINE || 
	   hmesh->elm_types[i] == TRIANGLE ||
	   hmesh->elm_types[i] == QUADRANGLE ||
	   hmesh->elm_types[i] == TETRAHEDRON ||
	   hmesh->elm_types[i] == HEXAHEDRON ||
	   hmesh->elm_types[i] == PRISM);
  } 
  
  int elm_size = hmesh->max_elm_size;
  
  
  /* 1) create the dual structure */
  /*
    For the k-th node, dual[k] is the list of all cells sharing this
    node.
  */
  
  const int MAX = 60; /* max nb of cells shared by a node */
  
  List * dual;  /* array of nb_nodes List */
  dual = malloc(hmesh->nb_nodes*sizeof(List));
  for(i = 0 ; i < hmesh->nb_nodes ; i++) {
    dual[i].size = 0;
    dual[i].max = MAX;
    dual[i].array = malloc(dual[i].max*sizeof(int));
  }
  
  
  
  /* for all nodes of each cell */
  for (int i = 0; i < hmesh->nb_cells; i++) {
    //for (int j = 1; j < elm_size+1; j++) { 
    for ( j = 1; j < ELEMENT_SIZE[hmesh->cells[i*(elm_size+1)]]+1; j++) { 
      int node = hmesh->cells[i * (elm_size+1) + j];
      assert (node < hmesh->nb_nodes);
      if (dual[node].size+1 > dual[node].max) {
	dual[node].max *= 2;
	dual[node].array = realloc (dual[node].array, dual[node].max * sizeof (int));
	assert (dual[node].array != NULL);
      }
      dual[node].array[dual[node].size++] = i;
    }
  }
  
  
  
  /* sort all lists */
  for( i = 0 ; i < hmesh->nb_nodes ; i++) {
    assert(dual[i].size >= 0);
    sortList(&dual[i]);
  }
  
  /* printf("DUAL\n"); */
  /* for(int i = 0 ; i < hmesh->nb_nodes ; i++) { */
  /*   printf("  %d -> [ ", i); */
  /*   for(int j = 0 ; j < dual[i].size ; j++) { */
  /*     printf("%d ", dual[i].array[j]); */
  /*   } */
  /*   printf("]\n"); */
  /* } */
  
  
  /* List of found elements, connected to a given cell */
  /* useful to remove duplicated edges */
  List found;
  found.size = 0;
  found.max = MAX;
  found.array = malloc(found.max*sizeof(int));
  
  /* 2) create the graph */
  graph->nvtxs = hmesh->nb_cells;
  int maxedges = 0;
  for ( i = 0; i < hmesh->nb_cells; i++)
    maxedges = maxedges + ELEMENT_SIZE[hmesh->cells[i*(elm_size+1)]];
  graph->nedges = 0;
  graph->xadj = malloc((graph->nvtxs+1) * sizeof(int)); 
  assert(graph->xadj);
  graph->adjncy = malloc(2*maxedges*sizeof(int)); /* max */
  assert(graph->adjncy);
  graph->vwgts = NULL; /* not used */
  graph->ewgts = NULL; /* not used */
  
  
  int adjncyIndex = 0;
  
  
  int one_time_tetra = 1;
  int one_time_hexa = 1;	
  int one_time_prism= 1;
  /* for all cells */
  for( i = 0 ; i < hmesh->nb_cells ; i++) {
    /* ****** LINE (node connectivity) ****** */
    if (hmesh->cells[i*(elm_size+1)] == LINE) { /* just two nodes (n1,n2)*/
      
      graph->xadj[i] = adjncyIndex;
      int n1 =  hmesh->cells[i*2];
      int n2 =  hmesh->cells[i*2+1];
      /* test if another line j element is connected to the i-th line element */
      for( k1 = 0 ; k1 < dual[n1].size ; k1++) {
    	int j = dual[n1].array[k1];
    	if(j != i) {
    	  graph->adjncy[adjncyIndex++] = j;
    	  assert(adjncyIndex <= 2*maxedges);
    	}
      }
      for( k2 = 0 ; k2 < dual[n2].size ; k2++) {
    	int j = dual[n2].array[k2];
    	if(j != i) {
    	  graph->adjncy[adjncyIndex++] = j;
    	  assert(adjncyIndex <= 2*maxedges);
    	}
      }
      
      /* check memory */
      if((adjncyIndex+2) >= 2*maxedges) {
    	maxedges *= 2;
    	graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int));
    	assert(graph->adjncy);
      }
    }
    
    /* ****** TRIANGLE & QUANDRANGLE (edge connectivity) ****** */
    else if (hmesh->cells[i*(elm_size+1)] == TRIANGLE || hmesh->cells[i*(elm_size+1)] == QUADRANGLE) {
      if( hmesh->cells[i*(elm_size+1)] == QUADRANGLE) printf("Edge connectivity QUADRANGLE\n");
      
      graph->xadj[i] = adjncyIndex;
      /* for all edges of the i-th cell */
      // for(int j = 0 ; j < elm_size ; j++) {
      int cell_end = ELEMENT_SIZE[hmesh->cells[i*(elm_size+1)]];
      for(int j = 0 ; j < cell_end ; j++) {
	int n1 =  hmesh->cells[i*(elm_size+1)+(j%elm_size + 1)];
	int n2 =  hmesh->cells[i*(elm_size+1)+((j+1)%cell_end + 1)];
	/* test if another cell is connected to the i-th cell */
	int k;
	for(k = 0 ; k < dual[n1].size ; k++) {
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1) {
	      graph->adjncy[adjncyIndex++] = kk;
	      assert(adjncyIndex <= 2*maxedges);
	    }
	  }
	}
      }
      
      /* check memory */
      if((adjncyIndex + elm_size) >= 2*maxedges) {
	maxedges *= 2;
	graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int));
	assert(graph->adjncy);
      }
    }
    
    
    
    /*    graph->xadj[i] = adjncyIndex; */
    /*       /\* for all edges of the i-th cell *\/ */
    /*       for( j = 0 ; j < elm_size + 1 ; j++) { */
    /*       //for ( j = 1; j < ELEMENT_SIZE[hmesh->cells[i*(elm_size+1)]]+1; j++){ */
    /* 	int n1 =  hmesh->cells[i*(elm_size+1)+(j%(elm_size */
    /* +1))]; */
    /* 	int n2 =  hmesh->cells[i*(elm_size+1)+((j+1)%(elm_size+1))]; */
    /* 	/\* test if another cell is connected to the i-th cell *\/ */
    /* 	int k; */
    /* 	for(k = 0 ; k < dual[n1].size ; k++) { */
    /* 	  int kk = dual[n1].array[k]; */
    /* 	  if(kk != i) { */
    /* 	    if(findSortedList(&dual[n2], kk) != -1) { */
    /* 	      graph->adjncy[adjncyIndex++] = kk; */
    /* 	      assert(adjncyIndex <= 2*maxedges); */
    /* 	    } */
    /* 	  } */
    /* 	} */
    /*       } */
    
    /*       /\* check memory *\/ */
    /*       if((adjncyIndex + elm_size+1) >= 2*maxedges) { */
    /* 	maxedges *= 2; */
    /* 	graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int)); */
    /* 	assert(graph->adjncy); */
    /*       }  */ 
    /*     } */
    
    /* ****** TETRAHEDRON (face/edge connectivity) ****** */
    else if (hmesh->cells[i*(elm_size+1)] == TETRAHEDRON) {
      const int nb_faces = 4;
      int faces[nb_faces][3];
      while (one_time_tetra == 1) {
	faces[0][0] = 0;  faces[0][1] = 1; faces[0][2] = 2;
	faces[1][0] = 0;  faces[1][1] = 1; faces[1][2] = 3;
	faces[2][0] = 1;  faces[2][1] = 3; faces[2][2] = 2;
	faces[3][0] = 0;  faces[3][1] = 2; faces[3][2] = 3;
	
	const int nb_edges = 6;
	int edges[nb_edges][2];
	edges[0][0] = 0;  edges[0][1] = 1;
	edges[1][0] = 1;  edges[1][1] = 2;
	edges[2][0] = 2;  edges[2][1] = 0;
	edges[3][0] = 0;  edges[3][1] = 3;
	edges[4][0] = 1;  edges[4][1] = 3;
	edges[5][0] = 2;  edges[5][1] = 3;
	one_time_tetra = 0;
      }
      graph->xadj[i] = adjncyIndex;
      found.size = 0; // reset
      
#if(FACE_CONNECTIVITY == 1)
      
      /* for all faces of the i-th cell */
      for (f = 0; f < nb_faces; f++) {
	/* consider face f with 3 nodes {n1,n2,n3} */
	int v1 = faces[f][0];
	int v2 = faces[f][1];
	int v3 = faces[f][2];
	/* consider triangle face with nodes {n1,n2,n3} */
	int n1 = hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 = hmesh->cells[i*(elm_size+1)+v2+1];
	int n3 = hmesh->cells[i*(elm_size+1)+v3+1];
	/* search a cell connected to i by face {n1,n2,n3} */
	for( k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1 && findSortedList(&dual[n3], kk) != -1)
	      insertList(&found, kk);
	  }
	}
      }
#endif
      
#if(EDGE_CONNECTIVITY == 1)
      
      /* for all edges of the i-th cell */
      for ( e = 0; e < nb_edges; e++) {
	/* consider edge e with 2 nodes {n1,n2} */
	int v1 = edges[e][0];
	int v2 = edges[e][1];
	int n1 = hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 = hmesh->cells[i*(elm_size+1)+v2+1];
	/* search a cell connected to i by edge {n1,n2} */
	for(k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1)
	      insertList(&found, kk);
	  }
	}
      }
      
#endif
      
      /* insert graph edges */
      sortList(&found);
      for( x = 0 ; x < found.size ; x++) {
	if(x > 0 && found.array[x] == found.array[x-1]) continue; // skip duplicated edges
	graph->adjncy[adjncyIndex++] = found.array[x];
	if(adjncyIndex >= 2*maxedges) {
	  maxedges *= 2;
	  graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int));
	  assert(graph->adjncy);
	}
      }
    }
    
    /* ****** HEXAHEDRON (face/edge connectivity) ****** */
    else if (hmesh->cells[i*(elm_size+1)] == HEXAHEDRON) {
      const int nb_faces = 6;
      int faces[nb_faces][4];
      while (one_time_hexa) {
	faces[0][0] = 0;  faces[0][1] = 1; faces[0][2] = 2;  faces[0][3] = 3;
	faces[1][0] = 4;  faces[1][1] = 5; faces[1][2] = 6;  faces[1][3] = 7;
	faces[2][0] = 0;  faces[2][1] = 1; faces[2][2] = 5;  faces[2][3] = 4;
	faces[3][0] = 2;  faces[3][1] = 6; faces[3][2] = 7;  faces[3][3] = 3;
	faces[4][0] = 1;  faces[4][1] = 2; faces[4][2] = 6;  faces[4][3] = 5;
	faces[5][0] = 0;  faces[5][1] = 3; faces[5][2] = 7;  faces[5][3] = 4;
	
	const int nb_edges = 12;
	int edges[nb_edges][2];
	edges[0][0] = 0;  edges[0][1] = 1;
	edges[1][0] = 1;  edges[1][1] = 2;
	edges[2][0] = 2;  edges[2][1] = 3;
	edges[3][0] = 3;  edges[3][1] = 0;
	edges[4][0] = 4;  edges[4][1] = 5;
	edges[5][0] = 5;  edges[5][1] = 6;
	edges[6][0] = 6;  edges[6][1] = 7;
	edges[7][0] = 7;  edges[7][1] = 4;
	edges[8][0] = 0;  edges[8][1] = 4;
	edges[9][0] = 1;  edges[9][1] = 5;
	edges[10][0] = 2;  edges[10][1] = 6;
	edges[11][0] = 3;  edges[11][1] = 7;
	one_time_hexa = 0;
      }
      graph->xadj[i] = adjncyIndex;
      found.size = 0; // reset
      
#if(FACE_CONNECTIVITY == 1)
      
      /* for all faces of the i-th cell */
      for (f = 0; f < nb_faces; f++) {
	/* consider face f with 4 nodes {n1,n2,n3,n4} */
	int v1 = faces[f][0];
	int v2 = faces[f][1];
	int v3 = faces[f][2];
	int v4 = faces[f][3];
	int n1 = hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 = hmesh->cells[i*(elm_size+1)+v2+1];
	int n3 = hmesh->cells[i*(elm_size+1)+v3+1];
	int n4 = hmesh->cells[i*(elm_size+1)+v4+1];
	
	/* search a cell connected to i by face {n1,n2,n3,n4} */
	for(k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1 && findSortedList(&dual[n3], kk) != -1 && findSortedList(&dual[n4], kk) != -1)
	      insertList(&found, kk);
	  }
	}
      }
#endif
      
#if(EDGE_CONNECTIVITY == 1)
      /* for all edges of the i-th cell */
      for ( e = 0; e < nb_edges; e++) {
	/* consider edge e with 2 nodes {n1,n2} */
	int v1 = edges[e][0];
	int v2 = edges[e][1];
	int n1 =hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 =hmesh->cells[i*(elm_size+1)+v2+1];
	/* search a cell connected to i by edge {n1,n2} */
	for(k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1)
	      insertList(&found, kk);
	  }
	}
      }
#endif
      
      /* insert graph edges */
      sortList(&found);
      for(x = 0 ; x < found.size ; x++) {
	if(x > 0 && found.array[x] == found.array[x-1]) continue; // skip duplicated edges
	graph->adjncy[adjncyIndex++] = found.array[x];
	if(adjncyIndex >= 2*maxedges) {
	  maxedges *= 2;
	  graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int));
	  assert(graph->adjncy);
	}
      }
    }
    
    /* ****** PRISM (face/edge connectivity) ****** */
    else if (hmesh->cells[i*(elm_size+1)] == PRISM) {
      const int nb_faces = 5;
      int faces[nb_faces][4];
      while(one_time_prism == 1) {
	faces[0][0] = 0;  faces[0][1] = 1; faces[0][2] = 2;  faces[0][3] = -1; /* triangle */
	faces[1][0] = 3;  faces[1][1] = 4; faces[1][2] = 5;  faces[1][3] = -1; /* triangle */
	faces[2][0] = 0;  faces[2][1] = 2; faces[2][2] = 5;  faces[2][3] = 3;
	faces[3][0] = 2;  faces[3][1] = 1; faces[3][2] = 4;  faces[3][3] = 5;
	faces[4][0] = 0;  faces[4][1] = 1; faces[4][2] = 4;  faces[4][3] = 3;
	
	const int nb_edges = 9;
	int edges[nb_edges][2];
	edges[0][0] = 0;  edges[0][1] = 1;
	edges[1][0] = 1;  edges[1][1] = 2;
	edges[2][0] = 2;  edges[2][1] = 0;
	edges[3][0] = 3;  edges[3][1] = 4;
	edges[4][0] = 4;  edges[4][1] = 5;
	edges[5][0] = 5;  edges[5][1] = 3;
	edges[6][0] = 0;  edges[6][1] = 3;
	edges[7][0] = 1;  edges[7][1] = 4;
	edges[8][0] = 2;  edges[8][1] = 5;
	one_time_prism = 0;
      }
      
      graph->xadj[i] = adjncyIndex;
      found.size = 0; // reset
      
#if(FACE_CONNECTIVITY == 1)
      
      /* for all faces of the i-th cell */
      for (f = 0; f < nb_faces; f++) {
	/* consider face f with 3 or 4 nodes {n1,n2,n3,n4} */
	int v1 = faces[f][0];
	int v2 = faces[f][1];
	int v3 = faces[f][2];
	int v4 = faces[f][3];
	int n1 = hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 = hmesh->cells[i*(elm_size+1)+v2+1];
	int n3 = hmesh->cells[i*(elm_size+1)+v3+1];
	int n4 = (v4 != -1) ? hmesh->cells[i*(elm_size+1)+v4+1] : -1; /* not always used */
	
	/* search a cell connected to i by face {n1,n2,n3} or {n1,n2,n3,n4} */
	for( k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1 && findSortedList(&dual[n3], kk) != -1) {
	      if(n4 != -1 && findSortedList(&dual[n4], kk) != -1) /* quad face */
		insertList(&found, kk);
	      else if (n4 == -1) /* triangular face */
		insertList(&found, kk);
	    }
	  }
	}
      }
      
#endif
      
#if(EDGE_CONNECTIVITY == 1)
      /* for all edges of the i-th cell */
      for ( e = 0; e < nb_edges; e++) {
	/* consider edge e with 2 nodes {n1,n2} */
	int v1 = edges[e][0];
	int v2 = edges[e][1];
	int n1 = hmesh->cells[i*(elm_size+1)+v1+1];
	int n2 = hmesh->cells[i*(elm_size+1)+v2+1];
	/* search a cell connected to i by edge {n1,n2} */
	for( k = 0; k < dual[n1].size; k++) {
	  /* check if cell kk is connected to cell i */
	  int kk = dual[n1].array[k];
	  if(kk != i) {
	    if(findSortedList(&dual[n2], kk) != -1)
	      insertList(&found, kk);
	  }
	}
      }
      
#endif
      
      /* insert graph edges */
      sortList(&found);
      for( x = 0 ; x < found.size ; x++) {
	if(x > 0 && found.array[x] == found.array[x-1]) continue; // skip duplicated edges
	graph->adjncy[adjncyIndex++] = found.array[x];
	if(adjncyIndex >= 2*maxedges) {
	  maxedges *= 2;
	  graph->adjncy = realloc(graph->adjncy, 2*maxedges*sizeof(int));
	  assert(graph->adjncy);
	}
      }
    }
  }
  
  /* end */
  
  graph->xadj[graph->nvtxs] = adjncyIndex;
  graph->nedges = adjncyIndex/2;
  
  /* free unused memory */
  graph->adjncy = realloc(graph->adjncy, graph->nedges*2*sizeof(int));      
  free(found.array);
  
  
  /* free dual */
  for(i = 0 ; i < hmesh->nb_nodes ; i++) 
    free(dual[i].array);
  free(dual);
  
}



/* *********************************************************** */

int saveVTKHybridMesh(const char * output, HybridMesh * hmesh)
{
  
  /* only line, triangle or quadrangle */
  int i, j, k1, k2, f, x, e, k;
  
  for (i = 0; i < hmesh->nb_components; i++) {
    assert(hmesh->elm_types[i] == LINE || 
	   hmesh->elm_types[i] == TRIANGLE ||
	   hmesh->elm_types[i] == QUADRANGLE ||
	   hmesh->elm_types[i] == TETRAHEDRON ||
	   hmesh->elm_types[i] == HEXAHEDRON ||
	   hmesh->elm_types[i] == PRISM);
  }
  
  int elm_size = hmesh->max_elm_size;
  FILE * file;
  
  /* open output file */
  if (NULL == (file = fopen(output, "w"))) {
    fprintf(stderr, "Opening file %s failed.\n", output);
    return EXIT_FAILURE;
  }
  
  /* save header */
  fprintf(file, "# vtk DataFile Version 2.0\n");
  fprintf(file, "Saved %s\n",output);
  fprintf(file, "ASCII\n");
  fprintf(file, "DATASET UNSTRUCTURED_GRID\n"); /* UNSTRUCTURED GRID, POLYDATA ??? */
  
  /* save section POINTS (3D coordinates) */
  fprintf(file, "POINTS %d double\n", hmesh->nb_nodes);
  for (i=0;i<hmesh->nb_nodes;i++){
    fprintf(file, "%f %f %f\n", hmesh->nodes[3*i], hmesh->nodes[3*i+1], hmesh->nodes[3*i+2]);
  }
  
  fprintf(file, "\n");
  int nb_nodes_cell = 0;
  for (i=0;i<hmesh->nb_cells;i++)
    nb_nodes_cell = nb_nodes_cell + (ELEMENT_SIZE[hmesh->cells[(elm_size+1)*i]]+1);
  /* save section CELLS*/
  fprintf(file, "CELLS %d %d\n", hmesh->nb_cells, nb_nodes_cell); 
  /* the size of data is (first argument of next ligne + 1) * hmesh->nb_cells (in case of triangle 2D = 4) */
  
  for (i=0;i<hmesh->nb_cells;i++){
    fprintf(file, "%d ", ELEMENT_SIZE[hmesh->cells[(elm_size+1)*i]]); /* element size */
    for (j = 1;j < ELEMENT_SIZE [hmesh->cells[(elm_size+1)*i]]+1; j++){ /* cell connectivity */
      fprintf(file, "%d ", hmesh->cells[(elm_size+1)*i+j]);
    }
    fprintf(file, "\n");
  }
  fprintf(file, "\n") ;
  
  /* save section CELL_TYPES*/
  fprintf(file, "CELL_TYPES %d\n", hmesh->nb_cells);
  
  for ( i=0; i<hmesh->nb_cells; i++){
    if(hmesh->cells[i*(elm_size+1)] == LINE) { /* line */  
      fprintf(file, "3\n"); /* 3 refers to VTK_LINE type  */
    }
    else if(hmesh->cells[i*(elm_size+1)] == TRIANGLE) { /* triangle */
      fprintf(file, "5\n"); /* 5 refers to VTK_TRIANGLE type  */
    }
    else if(hmesh->cells[i*(elm_size+1)] == QUADRANGLE) { /* quadrangle */
      fprintf(file, "9\n"); /* 9 refers to VTK_QUAD type  */
    }
    else if(hmesh->cells[i*(elm_size+1)] == TETRAHEDRON) { /* tetrahedron */
      fprintf(file, "10\n"); /* 10 refers to VTK_TETRA type  */
    }
    else if(hmesh->cells[i*(elm_size+1)] == HEXAHEDRON) { /* hexahedron */
      fprintf(file, "12\n"); /* 12 refers to VTK_HEXAHEDRON type  */         
    }  
    else if(hmesh->cells[i*(elm_size+1)] == PRISM) { /* prism */
      fprintf(file, "13\n"); /* 13 refers to VTK_WEDGE type  */
    }    
  }
  
  /* save section POINT_DATA */
  if(0 != hmesh->nb_node_vars){
    fprintf(file, "\n") ;
    fprintf(file, "POINT_DATA %d \n", hmesh->nb_nodes);
    fprintf(file, "FIELD FieldData %d \n", hmesh->nb_node_vars);
    for ( i = 0 ; i < hmesh->nb_node_vars ; i++){
      const char *type;
      if (hmesh->node_vars[i].type == VAR_INT)
	type = "int";
      else if (hmesh->node_vars[i].type == VAR_DOUBLE)
	type = "double";
      else
	assert (0 && "Unknown variable type.");
      fprintf(file, "%s %d %d %s\n", hmesh->node_vars[i].id, hmesh->node_vars->nb_components, hmesh->node_vars->nb_tuples, type);	
      if(NULL != hmesh->node_vars[i].data){
	int data_size = hmesh->node_vars->nb_components * hmesh->node_vars->nb_tuples ;
	for ( j = 0 ; j < data_size ; j++){
	  if (hmesh->node_vars[i].type == VAR_INT)
	    fprintf(file, "%d ", ((int *)hmesh->node_vars[i].data)[j]);
	  else if (hmesh->node_vars[i].type == VAR_DOUBLE)
	    fprintf(file, "%lf ", ((double *)hmesh->node_vars[i].data)[j]);
	  if ((i+1)%(3 * hmesh->node_vars->nb_components)==0) /* le modulo est juste  un choix pour l'affichage */
	    fprintf(file, "\n") ;
	}
      }
      fprintf(file, "\n") ;
    }
  }
  
  /* save section CELL_DATA*/
  if(0 != hmesh->nb_cell_vars){
    fprintf(file, "\n") ;
    fprintf(file, "CELL_DATA %d \n", hmesh->nb_cells);
    fprintf(file, "FIELD FieldData %d \n", hmesh->nb_cell_vars);
    for ( i = 0 ; i < hmesh->nb_cell_vars ; i++){
      const char *type;
      if (hmesh->cell_vars[i].type == VAR_INT)
	type = "int";
      else if (hmesh->cell_vars[i].type == VAR_DOUBLE)
	type = "double";
      else
	assert (0 && "Unknown variable type.");
      fprintf(file, "%s %d %d %s\n", hmesh->cell_vars[i].id, hmesh->cell_vars->nb_components, 
	      hmesh->cell_vars->nb_tuples, type);	
      if(NULL != hmesh->cell_vars[i].data){
	int data_size = hmesh->cell_vars->nb_components * hmesh->cell_vars->nb_tuples ;
	for (j = 0 ; j < data_size ; j++){
	  if (hmesh->cell_vars[i].type == VAR_INT)
	    fprintf(file, "%d ", ((int *)hmesh->cell_vars[i].data)[j]);
	  else if (hmesh->cell_vars[i].type == VAR_DOUBLE)
	    fprintf(file, "%lf ", ((double *)hmesh->cell_vars[i].data)[j]);
	  if ((i+1)%(3 * hmesh->cell_vars->nb_components)==0) /* le modulo est juste  un choix pour l'affichage */
	    fprintf(file, "\n") ;
	}
      }
      fprintf(file, "\n") ;
    }
  }
  
  fclose(file);  
  return EXIT_SUCCESS;
}

/* *********************************************************** */
