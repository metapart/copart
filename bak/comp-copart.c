/* Benchmark for CO-PARTITIONING */
/* Author(s): aurelien.esnard@labri.fr */
/* Examples: 
   $ ./comp-copart.exe -z 1.0 ../data/unit-cube-hexa-25x25x25.txt  ../data/unit-cube-hexa-100x100x100.txt -s NAIVE -s AWARE -s PROJREPART -m SCOTCHML1_HUN -k 10x20 -n 5 
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>
#include <assert.h>
#include <libgen.h>
#include <signal.h>
#include <setjmp.h>

#include "metapart.h"

/* *********************************************************** */
/*                  ERROR MANAGEMENT                           */
/* *********************************************************** */

static sigjmp_buf buf;
struct sigaction sa; 

static void handler(int sig)
{
  fprintf(stderr, "Signal %s received!\n", strsignal(sig));
  // if(sig == SIGABRT) sigaction(SIGABRT, &sa, NULL); // special abort behaviour!!!
  // if(sig == SIGALRM) siglongjmp(buf, 2);
  siglongjmp(buf, 1);
}


/* *********************************************************** */
/*                  EXP. PARAMETERS                            */
/* *********************************************************** */

#define MAX 255
#define UBFACTOR 5
#define MAXCHAR 1024

/* *********************************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0], "meshA.txt meshB.txt [options]");
  printf("options:\n");
  printf("\t -h: print this message\n");
  printf("\t -d: debug\n");  
  printf("\t -k kAxkB : nb procs for meshA and meshB\n");
  printf("\t -K kAxkB-kcplAxkcplB : nb procs for meshA and meshB (kA x kB) and its coupling interface (kcplA x kcplB)\n");  
  printf("\t -c kcplscheme : numbering scheme for cpl (LINEAR, GEOMETRIC, MULTIPLE, ...)\n");  
  printf("\t -m method : partitioning method (KMETIS, SCOTCH, KGGGP...)\n");
  printf("\t -s copartscheme : copartitioning scheme (NAIVE, AWARE, PROJREPART, REPARTPROJ, PROJSUBPART, SUBPARTPROJ, MULTICONST, ...)\n");  
  printf("\t -n niters : nb iterations\n");
  printf("\t -u ub : unbalance factor (default 5%%)\n");  
  printf("\t -z shift : shift meshB on Z axis\n");
  printf("\t -x exp : experiment label\n");  
  printf("\t -r interedges.txt : read interedge file\n");
  printf("\t -w interedges.txt : write interedge file\n");  
  exit (EXIT_FAILURE);  
}

/* *********************************************************** */

int main(int argc, char * argv[]) 
{   
  
  /* 0) parameters */  
  
  int niter = 1;
  char* methods[MAX];
  char* strschemes[MAX];
  int schemes[MAX];    
  int npartsA[MAX];
  int npartsB[MAX];   
  int npartscplA[MAX];
  int npartscplB[MAX];   
  char* files[MAX]; 
  char* labels[MAX]; 
  int ubfactor = UBFACTOR;  
  char* meshfileA;
  char* meshfileB;
  double shiftz = 0.0;
  char * writeinteredgefile = NULL;
  char * readinteredgefile = NULL;
  char * exp = NULL;
  int numcplscheme = GEOMETRIC_ALPHA;
  int debug = 0;
  
  /* 0) input args */  
  
  int opt;
  int kidx = 0;
  int midx = 0;
  int sidx = 0;  
  
  while ((opt = getopt (argc, argv, "z:hm:s:n:k:K:w:r:u:x:c:d")) != -1) {
    switch (opt) {
      
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
      
    case 'm':
      methods[midx] = optarg; methods[midx+1] = NULL; 
      midx++;
      break;
      
    case 'k':
      {
	int kA = -1; int kB = -1;
	int r = sscanf(optarg, "%dx%d", &kA, &kB);
	assert(kA > 0 && kB > 0);
	if(r != 2) usage(argc,argv);
	npartsA[kidx] = kA; npartsA[kidx+1] = -1;
	npartsB[kidx] = kB; npartsB[kidx+1] = -1;
	npartscplA[kidx] = 0; npartscplA[kidx+1] = -1;
	npartscplB[kidx] = 0; npartscplB[kidx+1] = -1;      		
	kidx++;
      }
      break;

    case 'K':
      {
	int kA = -1; int kB = -1; int kcplA = -1; int kcplB = -1;
	int r = sscanf(optarg, "%dx%d-%dx%d", &kA, &kB, &kcplA, &kcplB);
	assert(kA > 0 && kB > 0);
	assert(kcplA > 0 && kcplB > 0);
	if(r != 4) usage(argc,argv);	
	npartsA[kidx] = kA; npartsA[kidx+1] = -1;
	npartsB[kidx] = kB; npartsB[kidx+1] = -1;
	npartscplA[kidx] = kcplA; npartscplA[kidx+1] = -1;
	npartscplB[kidx] = kcplB; npartscplB[kidx+1] = -1;      	
	kidx++;
      }
      break;
      
    case 's':
      strschemes[sidx] = optarg; strschemes[sidx+1] = NULL;
      if(strcmp(optarg, "NAIVE") == 0) schemes[sidx] = COPART_NAIVE;
      else if(strcmp(optarg, "AWARE") == 0) schemes[sidx] = COPART_AWARE_NAIVE;
      else if(strcmp(optarg, "PROJREPART") == 0) schemes[sidx] = COPART_AWARE_PROJREPART;            
      else if(strcmp(optarg, "REPARTPROJ") == 0) schemes[sidx] = COPART_AWARE_REPARTPROJ;
      else if(strcmp(optarg, "PROJSUBPART") == 0) schemes[sidx] = COPART_AWARE_PROJSUBPART;
      else if(strcmp(optarg, "SUBPARTPROJ") == 0) schemes[sidx] = COPART_AWARE_SUBPARTPROJ;      
      else if(strcmp(optarg, "MULTICONST") == 0) schemes[sidx] = COPART_MULTICONST;            
      else usage(argc,argv);
      schemes[sidx+1] = -1;      
      sidx++;
      break;      
      
    case 'n':
      niter = atoi(optarg); 
      break;
      
    case 'u':
      ubfactor = atoi(optarg); 
      break;
      
    case 'w':
      writeinteredgefile  = optarg;
      break;
      
    case 'r':
      readinteredgefile  = optarg;
      break;
      
    case 'x':
      exp = optarg;
      break;      
      
    case 'c':
      if(strcmp(optarg, "LINEAR") == 0) numcplscheme = LINEAR_ALPHA;
      else if(strcmp(optarg, "GEOMETRIC") == 0) numcplscheme = GEOMETRIC_ALPHA;
      else if(strcmp(optarg, "MULTIPLE") == 0) numcplscheme = MULTIPLE_ALPHA;
      else if(strcmp(optarg, "USER") == 0) numcplscheme = USER_ALPHA;            
      else usage(argc,argv);
      break;      

    case 'd':
      debug = 1;
      break;
      
    case 'h':
    default:
      usage(argc,argv);
    }
  }
  
  // if(argc - optind != 0) usage(argc,argv); 
  if(argc - optind != 2) usage(argc,argv);
  
  meshfileA = argv[optind]; 
  meshfileB = argv[optind+1];  
  
  int nbschemes = sidx;
  int nbmethods = midx;
  int nbnparts = kidx;  
  
  /* compute labels */
  char labelA[8096];
  char labelB[8096];      
  char tmp[8096]; // basename & dirname requires to work on a temp copy...
  strcpy(tmp, meshfileA); strcpy(labelA, basename(tmp)); char * end = strrchr(labelA,'.'); if(end) *end = '\0';
  strcpy(tmp, meshfileB); strcpy(labelB, basename(tmp)); end = strrchr(labelB,'.'); if(end) *end = '\0';      
  
  /* compute exp */
  char defaultexp[MAX];
  strcat(defaultexp,labelA);
  strcat(defaultexp,"_");	  
  strcat(defaultexp,labelB);	    
  
  printf("# exp = %s, meshA = %s, meshB = %s, nbmethods = %d, nbschemes = %d, nbnparts = %d, niter = %d, ub = %d, shiftz = %.2f\n",
	 exp?exp:defaultexp, meshfileA, meshfileB, nbmethods, nbschemes, nbnparts, niter, ubfactor, shiftz);    
  
  /* 1) load mesh A & B */ 
  
  Mesh meshA, meshB;
  loadMesh(meshfileA, &meshA); 
  loadMesh(meshfileB, &meshB); 
  shiftMesh(&meshB, 0.0, 0.0, shiftz);
  
  /* 2) generate graphs from meshes */
  
  Graph gA, gB;
  mesh2Graph(&meshA, &gA);
  mesh2Graph(&meshB, &gB);  
  
  /* 3) compute/load/save inter-edges */ 
  
  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;
  double * volumes = NULL;       
  
  /* load interedges */
  if(readinteredgefile) {    
    FILE* finteredges = fopen(readinteredgefile, "r");
    assert(finteredges);
    int a, b, w;    
    while(fscanf(finteredges,"%d %d %d\n", &a, &b, &w) > 0) nbinteredges++;
    interedges = malloc(2*nbinteredges*sizeof(int)); assert(interedges);
    weights = malloc(nbinteredges*sizeof(int)); assert(weights);   
    rewind(finteredges);
    for(int k = 0 ; k < nbinteredges ; k++)
      fscanf(finteredges,"%d %d %d\n", interedges + 2*k, interedges + 2*k+1, weights + k);
    fclose(finteredges);
  }
  /* compute interedges */
  else {
    intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, &volumes);
    const int minwgt = 0, maxwgt = 10;
    computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, minwgt, maxwgt);       
  }
  
  /* save interedges */
  if(writeinteredgefile) {    
    FILE* finteredges = fopen(writeinteredgefile, "w");
    assert(finteredges);    
    for(int k = 0 ; k < nbinteredges ; k++)
      fprintf(finteredges,"%d %d %d\n",interedges[2*k], interedges[2*k+1], weights[k]);
    fclose(finteredges);
  }

  /* error management */

  sa.sa_handler = handler;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);
  sigaction(SIGSEGV, &sa, NULL);
  sigaction(SIGFPE, &sa, NULL);
  sigaction(SIGABRT, &sa, NULL);
  sigaction(SIGBUS, &sa, NULL);
  sigaction(SIGQUIT, &sa, NULL);
  // sigaction(SIGALRM, &sa, NULL);    
  sigprocmask(SIG_SETMASK, &sa.sa_mask, NULL);
  
  /* 5) copart */
  
  int* partA = malloc(gA.nvtxs*sizeof(int));
  int* partB = malloc(gB.nvtxs*sizeof(int));  
  int edgecutA = 0, edgecutB = 0;
  
  printf("# EXP MESHA MESHB OVERLAP METHOD SCHEME SEED NPARTSA NPARTSB NPARTSACPL NPARTSBCPL NPARTSACPLR NPARTSBCPLR UBA UBB UBACPL UBBCPL CUTA CUTB CUTACPL CUTBCPL MAXV MAXVOPT TOTV TOTVOPT TOTZ TOTZM TOTZOPT TIME ERROR\n");
  
  for(int n = 0 ; n < nbnparts ; n++) {
    
    int nA = npartsA[n]; 
    int nB = npartsB[n]; 
    int ncplA = npartscplA[n];
    int ncplB = npartscplB[n];

    if(ncplA <= 0 || ncplB <= 0)
      generateProcsCpl(&gA, &gB, nA, nB, nbinteredges, interedges, weights, numcplscheme, &ncplA, &ncplB);
    
    assert(ncplA > 0 && ncplA <= nA);
    assert(ncplB > 0 && ncplB <= nB);    
    
    for(int s = 0 ; s < nbschemes ; s++) {
      
      for(int m = 0 ; m < nbmethods ; m++) {
	
	for(int k = 0 ; k < niter ; k++) {
	  
	  int error = 0;
	  int seed = k*10;	
	  int exectime = 0;	  
	  CopartResults copartinfo;
	  int overlap = 100-round(shiftz*100); // in %
	  
	  error = sigsetjmp(buf, 1); // return 0 if OK, 1 if signal 

	  if(!error) {
	    exectime = copart(&gA, &gB, 
			      nA, nB, 
			      ubfactor,
			      nbinteredges, 
			      interedges,
			      weights,
			      schemes[s],
			      ncplA, ncplB,
			      seed,
			      partA, partB,
			      &edgecutA, &edgecutB, 
			      NULL, &meshA,
			      NULL, &meshB,
			      methods[m]); 
	    
	    int intercomm[nA][nB];
	    computeCopartDiagnostic(&gA, &gB, nA, nB, ncplA, ncplB, partA, partB, nbinteredges, interedges, weights, &copartinfo, (int*)intercomm);
	  }
	  
	  if(copartinfo.maxubA > (ubfactor+0.01) || copartinfo.maxubB > (ubfactor+0.01)) {
	    // fprintf(stderr,"ERROR: imbalance factor not respected!\n");
	    error = 2;
	  }	    
	  
	  if(!debug)
	    printf("%s %s %s %d %s %s %d %d %d %d %d %d %d %.2f %.2f %.2f %.2f %d %d %d %d %d %d %d %d %d %d %d %d %d\n",
		   exp?exp:defaultexp,
		   labelA, labelB,
		   overlap,
		   methods[m],
		   strschemes[s],
		   seed,		 
		   copartinfo.kA, copartinfo.kB,
		   copartinfo.kcplA, copartinfo.kcplB,
		   copartinfo.kcplAreal, copartinfo.kcplBreal,	     		 
		   copartinfo.maxubA, copartinfo.maxubB,
		   copartinfo.maxubcplA, copartinfo.maxubcplB,	     
		   copartinfo.edgecutA, copartinfo.edgecutB,
		   copartinfo.edgecutcplA, copartinfo.edgecutcplB,
		   copartinfo.maxV, copartinfo.maxVopt,
		   copartinfo.totV, copartinfo.totVopt,		   		   
		   copartinfo.totZ, copartinfo.totZM, copartinfo.totZopt, 
		   exectime,
		   error);
	  
	  if(debug)
	    printCopartDiagnostic(&gA, &gB, nA, nB, ncplA, ncplB, partA, partB, nbinteredges, interedges, weights);	  
	  
	}
	
      }
      
    }
    
  }
  
  /* 8) free */
  freeGraph(&gA);
  freeGraph(&gB);
  freeMesh(&meshA);
  freeMesh(&meshB);
  free(partA);
  free(partB);
  free(interedges);
  free(weights);  
  
  return EXIT_SUCCESS;
  
}
