
/**
 *   @file tetratetra.h
 *
 *   @author Aur�lien Esnard & others.
 *
 *   @defgroup Geom Geometry routines.
 *   @brief Geometry routines for mesh.
 *
 */

#ifndef __TETRATETRA_H__
#define __TETRATETRA_H__

/* *********************************************************** */

//@{

#ifdef __cplusplus
extern "C" {
#endif
  
  /** Fast Tetrahedron-Tetrahedron Overlap Algorithm, by Fabio Ganovelli, Frederico Ponchio, Claudio Rocchini. ACM 2002. */
  int intersectTetrahedronGanovelli(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
				    double * coordsB[4]);  /* [in] pointers on 3D coord of tetrahedron B */  
  
  /** Naive tetrahedron-tetrahedron overlap test, based on triangle-triangle overlap test. */
  int intersectTetrahedronOrel(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			       double * coordsB[4]);  /* [in] pointers on 3D coord of tetrahedron B */  
  
  /**  Fast, robust polyhedral intersections, analytic integration, and conservative voxelization. Devon Powell, 2015. 
       Compute intersection volume...
       https://github.com/devonmpowell/r3d 
  */
  int intersectTetrahedronPowell(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
				 double * coordsB[4],  /* [in] pointers on 3D coord of tetrahedron B */
				 double * vol);        /* [out] intersection volume */  
  
  int isInsideTetrahedron (double *coords[4],    /* [in] 4 3D points of a tetrahedron */
			   double X[3]) ;
  
#ifdef __cplusplus
}
#endif

//@}

/* *********************************************************** */

#endif
