/* Tetrahedron-Tetrahedron intersection test routine */

#include <math.h>
#include <assert.h>

#include "debug.h"
#include "tetratetra.h"
#include "geom.h"

/* *********************************************************** */

#define CROSS(v1,v2,dest){			\
    dest[0]=v1[1]*v2[2]-v1[2]*v2[1];		\
    dest[1]=v1[2]*v2[0]-v1[0]*v2[2];		\
    dest[2]=v1[0]*v2[1]-v1[1]*v2[0];}

/* *********************************************************** */

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

/* *********************************************************** */

#define VECTOR(A,B,AB){			\
    AB[0]=B[0]-A[0];			\
    AB[1]=B[1]-A[1];			\
    AB[2]=B[2]-A[2];}

/* *********************************************************** */

#define VECNORM(V,VN){							\
    double N = sqrt(V[0]*V[0] + V[1]*V[1] + V[2]*V[2]);			\
    VN[0] = V[0] / N; VN[1] = V[1] / N; VN[2] = V[2] / N; }		

/* *********************************************************** */

#define DET2D(A,B) ((A[0])*(B[1]) - (A[1])*(B[0]))

/* *********************************************************** */

#define DET3D(A,B,C) ((A[0])*((B[1])*(C[2]) - (B[2])*(C[1])) + (B[0])*((C[1])*(A[2]) - (C[2])*(A[1])) + (C[0])*((A[1])*(B[2]) - (A[2])*(B[1])))

/* *********************************************************** */

int isInsideTetrahedron (double *coords[4],    /* [in] 4 3D points of a tetrahedron */
			 double X[3])          /* [in] Point coords */
{
  double u[3], v[3];
  double a[3], b[3];
  
  /* X is on same side as 3 of 0 1 2 face */
  VECTOR (coords[0], coords[1], u);
  VECTOR (coords[0], coords[2], v);
  VECTOR (coords[0], coords[3], a);
  VECTOR (coords[0], X, b);
  if (DET3D(u, v, a) * DET3D(u, v, b) <= 0)
    return 0;
  /* X is on same side as 2 of 0 1 3 face */
  VECTOR (coords[0], coords[3], v);
  VECTOR (coords[0], coords[2], a);
  if (DET3D(u, v, a) * DET3D(u, v, b) <= 0)
    return 0;
  /* X is on same side as 1 of 0 2 3 face */
  VECTOR (coords[0], coords[2], u);
  VECTOR (coords[0], coords[1], a);
  if (DET3D(u, v, a) * DET3D(u, v, b) <= 0)
    return 0;
  /* X is on same side as 0 of 1 2 3 face */
  VECTOR (coords[1], coords[2], u);
  VECTOR (coords[1], coords[3], v);
  VECTOR (coords[1], coords[0], a);
  VECTOR (coords[1], X, b);
  if (DET3D(u, v, a) * DET3D(u, v, b) <= 0)
    return 0;
  
  return 1;
}

/* *********************************************************** */

int intersectTetrahedronOrel(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			     double * coordsB[4])  /* [in] pointers on 3D coord of tetrahedron B */  
{
  
  /* 1) A intersect B if any triangle face of A intersect any triangle face of B */
  
  double * coordsTA[3];
  double * coordsTB[3];    
  
  /* for any triangle TA {i, ii, iii} */
  for (int i = 0 ; i < 4 ; i++) {
    int ii = (i + 1) % 4;
    int iii = (i + 2) % 4;
    coordsTA[0] = coordsA[i];
    coordsTA[1] = coordsA[ii];
    coordsTA[2] = coordsA[iii];
    
    /* for any triangle TB {j, jj, jjj} */    
    for (int j = 0 ; j < 4 ; j++)  {      
      int jj = (j + 1) % 4;
      int jjj = (j + 2) % 4;
      coordsTB[0] = coordsB[j];
      coordsTB[1] = coordsB[jj];
      coordsTB[2] = coordsB[jjj];
      
      PRINT("test triangle intersection between TA(%d,%d,%d) and TB(%d,%d,%d)\n",
	    i,ii,iii,j,jj,jjj);
      
      if(intersectTriangle(coordsTA, coordsTB)) return 1;
    }
  }
  
  /* 2) TA is inside TB if the gravity center of A is inside B (or inversely) */
  
  double A[3];
  polygonCenter(4,coordsA,A);
  double B[3];
  polygonCenter(4,coordsB,B);
  
  if(isInsideTetrahedron(coordsA, B) || isInsideTetrahedron(coordsB, A)) {
    PRINT(" => tetrahedron A is inside B or reciprocally!\n");
    return 1;
  }
    
  return 0;
}

/* *********************************************************** */
