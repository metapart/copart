/* test triangle-triangle intersection with CGAL by orel */
/* compilation: g++ -frounding-math  -std=c++11 test-cgal.c++ -lCGAL -o test-cgal */

#include "tritri.h"

#ifdef USECGAL 
#define EPSILON 1E-5

#include <CGAL/Triangle_3.h>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/intersections.h>
#include <CGAL/result_of.h>
#include <vector>

typedef CGAL::Simple_cartesian<double> K; // kernel
typedef CGAL::Point_3<K> P3;
typedef CGAL::Segment_3<K> S3;
typedef CGAL::Triangle_3<K> T3; // or typedef K::Triangle_3 T3;
typedef std::vector<P3>  VP3;

#else

#include <cstdio>
#include <cstdlib>

#endif

/* *********************************************************** */


int intersectTriangleCGAL(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
			    double * T2[3])  /* [in] vertex 3D coords of triangle T2 */
{
#ifndef USECGAL
  fprintf(stderr, "ERROR: The CGAL library is not available (USECGAL not defined).\n");
  exit(EXIT_FAILURE);
#else

  
  T3 tri1(P3(T1[0][0], T1[0][1], T1[0][2]), 
	  P3(T1[1][0], T1[1][1], T1[1][2]), 
	  P3(T1[2][0], T1[2][1], T1[2][2]));
  T3 tri2(P3(T2[0][0], T2[0][1], T2[0][2]), 
	  P3(T2[1][0], T2[1][1], T2[1][2]), 
	  P3(T2[2][0], T2[2][1], T2[2][2]));
  
  bool intersect = CGAL::do_intersect(tri1, tri2);   
  if(intersect) return 1;

#endif

  return 0;  
}

/* *********************************************************** */

// int intersectTriangleCGALv1(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
// 			    double * T2[3])  /* [in] vertex 3D coords of triangle T2 */
// {
  
//   T3 tri1(P3(T1[0][0], T1[0][1], T1[0][2]), 
// 	  P3(T1[1][0], T1[1][1], T1[1][2]), 
// 	  P3(T1[2][0], T1[2][1], T1[2][2]));
//   T3 tri2(P3(T2[0][0], T2[0][1], T2[0][2]), 
// 	  P3(T2[1][0], T2[1][1], T2[1][2]), 
// 	  P3(T2[2][0], T2[2][1], T2[2][2]));
  
//   try {
//     auto result = intersection(tri1, tri2); // with C++11 support 
//     // CGAL::cpp11::result_of<typename K::Intersect_3(T3, T3)>::type result = intersection(tri1, tri2); // without C++11 support 
    
//     const T3* tri = CGAL::object_cast<T3>(&result);
//     const S3* seg = CGAL::object_cast<S3>(&result);
//     const P3* point = CGAL::object_cast<P3>(&result);  
//     const VP3* points = CGAL::object_cast<VP3>(&result);      

//     // simple test
//     if(tri || seg || point || points) return 1;
//     return 0;
//     /*orel*/   
//     /* debug */
//     // if(point) printf("Warning: intersection on a point.\n");
//     // if(points) printf("Warning: intersection on %ld points.\n", points->size());
//     // if(seg) printf("Warning: intersection on a segment.\n");
//     // if(tri) printf("Warning: intersection on a triangle.\n");
    
//     // if(tri && sqrt(tri->squared_area()) < EPSILON) { 
//     //   printf("Warning: intersection on triangle ignored (area lower than threshold).\n"); 
//     //   return 0;    
//     // }
//     // else if(tri) return 1;
//     // else if(seg && sqrt(seg->squared_length()) < EPSILON) {
//     //   printf("Warning: intersection on segment ignored (length lower than threshold).\n"); 
//     //   return 0;
//     // }
//     // else if(seg) return 1;
//     // else if(points || point) return 0; // intersection on point ignored!
//     // else return 0; // no intersection
//     /*orel*/
//   } catch(...) { 
//     // printf("Warning: intersection exception because of degenerated triangle.\n"); 
//     return 0;
//   }  
  
//   return 0;
// }

/* *********************************************************** */


