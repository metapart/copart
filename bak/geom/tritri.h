
/**
 *   @file tritri.h
 *
 *   @author  Aur�lien Esnard & others.
 *
 *   @defgroup Geom Geometry routines.
 *   @brief Geometry routines for mesh.
 *
 */

#ifndef __TRITRI_H__
#define __TRITRI_H__

/* *********************************************************** */

//@{

#ifdef __cplusplus
extern "C" {
#endif   

/** A Fast Triangle-Triangle Intersection Test, by Tomas Moller, 1997. */
int intersectTriangleMoller(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
			    double * T2[3]);  /* [in] vertex 3D coords of triangle T2 */

/** Triangle-Triangle Intersection Test, by Aur�lien Esnard, inspired by Tomas Moller, 1997. */
int intersectTriangleOrel(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
			  double * T2[3]);  /* [in] vertex 3D coords of triangle T2 */



/**  Fast and Robust Triangle-Triangle Overlap Test Using Orientation Predicates, by P. Guigue and O. Devillers, 2003.  */
int intersectTriangleGuigueDevillers(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
				     double * T2[3]);  /* [in] vertex 3D coords of triangle T2 */


/**  Triangle-Triangle Intersection Test implemented in CGAL library (http://www.cgal.org/). */
int intersectTriangleCGAL(double * T1[3],   /* [in] vertex 3D coords of triangle T1 */
			  double * T2[3]);  /* [in] vertex 3D coords of triangle T2 */

#ifdef __cplusplus
}
#endif

//@}

/* *********************************************************** */

#endif
