/*  
    Fast, robust polyhedral intersections, analytic integration, and conservative voxelization. 
    
    Devon Powell, 2015. 
    
    https://github.com/devonmpowell/r3d 
*/

/* *********************************************************** */

#include "r3d.h"
#include <stdio.h>
#include <assert.h>

#define POLY_ORDER 2

/* *********************************************************** */

int intersectTetrahedronPowell(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			       double * coordsB[4],  /* [in] pointers on 3D coord of tetrahedron B */
			       double * vol)           /* [out] intersection volume */  
			       
{

  assert(sizeof(r3d_real) == sizeof(double)); // check precision
  *vol = 0.0;
  
  r3d_rvec3 verts1[4];
  r3d_rvec3 verts2[4];  
  r3d_poly poly1;
  r3d_real moments[R3D_NUM_MOMENTS(POLY_ORDER)];
  r3d_plane faces[4];    

  for(int i = 0 ; i < 4 ; i++) {
    verts1[i].x = coordsA[i][0]; verts1[i].y = coordsA[i][1]; verts1[i].z = coordsA[i][2];
    verts2[i].x = coordsB[i][0]; verts2[i].y = coordsB[i][1]; verts2[i].z = coordsB[i][2];
  }

  // compute volumes
  r3d_real _vol1 = r3d_orient(verts1); // signed volume of tet1
  r3d_real _vol2 = r3d_orient(verts2); // signed volume of tet2
  
  // check orientation of tet1
  if(_vol1 < 0.0) {
    r3d_rvec3 swp;
    swp = verts1[2];
    verts1[2] = verts1[3];
    verts1[3] = swp;
    _vol1 = -_vol1;
  }
  
  // check orientation of tet2  
  if(_vol2 < 0.0) {
    r3d_rvec3 swp;
    swp = verts2[2];
    verts2[2] = verts2[3];
    verts2[3] = swp;
    _vol2 = -_vol2;
  }
  
  r3d_init_tet(&poly1, verts1);
  r3d_tet_faces_from_verts(faces, verts2);			       
  r3d_clip(&poly1, faces, 4);
  r3d_reduce(&poly1, moments, POLY_ORDER);
  
  r3d_real _vol = moments[0]; // volume of intersection (first moment... I cannot explain this!)
  
  // printf("intersection volume %.6f (tet1 = %.6f, tet2 = %.6f)\n", _vol, _vol1, _vol2);

  // result
  *vol = _vol; 
  if(_vol <= 0.0) return 0;  
  return 1;
  
}

/* *********************************************************** */
