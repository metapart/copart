/* Geometry Routines */
/* Author(s): esnard@labri.fr */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <omp.h>

// #define DEBUG
// #define DEBUGADV

#include "geom.h"
#include "struct.h"
#include "tetratetra.h"
#include "tritri.h"
#include "io.h"
#include "debug.h"
#include "mesh.h"

#define EPSILON 1E-8
#define SCALEFACTOR 1.001 // scale by +0.1%
// #define SCALEFACTOR 0.98    // scale by -0.1%
#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))


/* *********************************************************** */
/*                      SOME MACROS                            */
/* *********************************************************** */

#define INDEX2D(I,J,DX,DY) ((J)*(DX)+(I))

/* *********************************************************** */

#define INDEX3D(I,J,K,DX,DY,DZ) ((K)*(DX)*(DY)+(J)*(DX)+(I))

/* *********************************************************** */

#define CROSS(v1,v2,dest){			\
    dest[0]=v1[1]*v2[2]-v1[2]*v2[1];		\
    dest[1]=v1[2]*v2[0]-v1[0]*v2[2];		\
    dest[2]=v1[0]*v2[1]-v1[1]*v2[0];}

/* *********************************************************** */

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

/* *********************************************************** */

#define VECTOR(A,B,AB){			\
    AB[0]=B[0]-A[0];			\
    AB[1]=B[1]-A[1];			\
    AB[2]=B[2]-A[2];}
  
/* *********************************************************** */

#define VECNORM(V,VN){							\
    double N = sqrt(V[0]*V[0] + V[1]*V[1] + V[2]*V[2]);			\
    VN[0] = V[0] / N; VN[1] = V[1] / N; VN[2] = V[2] / N; }		

/* *********************************************************** */

#define DET2D(A,B) ((A[0])*(B[1]) - (A[1])*(B[0]))

/* *********************************************************** */

#define DET3D(A,B,C) ((A[0])*((B[1])*(C[2]) - (B[2])*(C[1])) + (B[0])*((C[1])*(A[2]) - (C[2])*(A[1])) + (C[0])*((A[1])*(B[2]) - (A[2])*(B[1])))

/* *********************************************************** */

void polygonCenter(int n,               /* [in] nb of vertices of polygon */
		   double * coords[3],  /* [in] array of size n of pointer on 3D coords */
		   double G[3])         /* [out] gravity center */
{
  /* gravity center of polygon */
  for(int k = 0; k < 3; k++) {
    G[k] = 0.0;      
    for(int i = 0; i < n; i++) 
      G[k] += coords[i][k];
    G[k] /= n;
  }  
}

/* *********************************************************** */

void cellCenter(Mesh * msh,    /* [in] input mesh */
		int i,         /* [in] cell index */
		double G[3])   /* [out] gravity center */
{
  int cellsize = ELEMENT_SIZE[msh->elm_type];
  int * cell = &msh->cells[i*cellsize];
  
  double * coords[cellsize];
  for(int k = 0; k < cellsize; k++) 
    coords[k] = &msh->nodes[cell[k]*3];
  
  polygonCenter(cellsize, coords, G);
}

/* *********************************************************** */

void cellCenters(Mesh * msh,        
		 double * centers)
{
  for(int i = 0 ; i < msh->nb_cells ; i++)
    cellCenter(msh, i, centers+3*i); 
}

/* *********************************************************** */

static inline 
int isInsidePolygon2D(int n,             /* [in] nb of vertices of polygon */
		      double ** coords,  /* [in] array of size n of pointer on 2D coords */
		      double X[3])       /* [in] X coords */
{
  int inside = 0; /* -1 or +1 */
  
  /* for all vertices of polygon */
  for(int k = 0; k < n ; k++) {     
    double * B0 = coords[k];
    double * B1 = coords[(k+1)%n];
    double V0[3]; /* V0 = [X,B0] */
    double V1[3]; /* V1 = [X,B1] */
    for(int i = 0; i < 3; i++) V0[i] = B0[i] - X[i]; 
    for(int i = 0; i < 3; i++) V1[i] = B1[i] - X[i]; 	  
    double w = V0[0]*V1[1]-V0[1]*V1[0]; /* V0^V1 on Z axis */	
    /* we check all w are of the same sign! */
    if(w > 0) inside++; else inside--;
    if(abs(inside) != k+1) break; /* outside */
  }
  
  if(abs(inside) == n) 
    return 1;
  else
    return 0;  
}

/* *********************************************************** */

/* int isInsideCell(Mesh * msh,    /\* [in] input mesh *\/ */
/* 		 int i,         /\* [in] cell index  *\/ */
/* 		 double X[3])   /\* [in] X coords *\/ */
/* { */
/*   assert (msh->elm_type == TRIANGLE || msh->elm_type == QUADRANGLE); */

/*   int cellsize = ELEMENT_SIZE[msh->elm_type]; */
/*   int * cell = msh->cells + i*cellsize; */

/*   double * coords[cellsize]; */
/*   for(int i = 0; i < cellsize ; i++)  */
/*     coords[i] = msh->nodes + cell[i]*3; */

/*   return isInsidePolygon2D(cellsize, coords, X); */
/* } */

/* *********************************************************** */

void cellLength(Mesh * msh,    /* [in] input mesh */
		int i,         /* [in] cell index */
		double length[3]) /* [out] cell length on x,y,z */  
{
  int cellsize = ELEMENT_SIZE[msh->elm_type];
  int * cell = msh->cells + i*cellsize;   
  
  length[0] = length[1] = length[2] = -1.0;
  
  /* for each pair (k0,k1) of nodes in cell */
  for(int k0 = 0; k0 < cellsize; k0++) {
    for(int k1 = k0+1; k1 < cellsize; k1++) {
      
      double * A0 = msh->nodes + cell[k0]*3;
      double * A1 = msh->nodes + cell[k1]*3;
      
      /* update the max length */ 
      // for(int i = 0 ; i < 3 ; i++) {
      double lx = fabs(A0[0] - A1[0]);
      double ly = fabs(A0[1] - A1[1]);
      double lz = fabs(A0[2] - A1[2]);
      if(lx > length[0]) length[0] = lx; 
      if(ly > length[1]) length[1] = ly; 
      if(lz > length[2]) length[2] = lz; 
      
    }
    
  }  
  
}


/* *********************************************************** */

void cellsLength(Mesh * msh, double length[3])   /* [in] input mesh */
  
{
  /* for all cells, compute max cell length... */
  
  length[0] = length[1] = length[2] = -1.0;
  int index = -1;
  
  for (int i = 0 ; i < msh->nb_cells ; i++) {    
    double l[3];
    cellLength(msh, i, l);
    if(l[0] > length[0]) length[0] = l[0]; 
    if(l[1] > length[1]) length[1] = l[1]; 
    if(l[2] > length[2]) length[2] = l[2]; 
  }
  
  // PRINT("index of the longest cell = %d, length = %.2f\n", index, l);
  
}

/* *********************************************************** */

static inline
int intersectSegment2D(double A[2], double B[2], double C[2], double D[2]) 
{
  double d = (B[0]-A[0])*(D[1]-C[1])-(B[1]-A[1])*(D[0]-C[0]); /* det(AB, CD) */
  
  if(fabs(d) < EPSILON)  /* AB parallel with CD (or coincident) */ 
    return 0;
  
  double r = (A[1]-C[1])*(D[0]-C[0])-(A[0]-C[0])*(D[1]-C[1]); /* det(AC, CD) */
  double s = (A[1]-C[1])*(B[0]-A[0])-(A[0]-C[0])*(B[1]-A[1]); /* det(AC, AB) */
  r /= d;
  s /= d;
  
  /* if A = C => r = 0 and s = 0 */
  /* if A = D => r = 0 and s = 1 */
  /* if B = C => r = 1 and s = 0 */
  /* if B = D => r = 1 and s = 1 */
  
  if(r > EPSILON && r < 1-EPSILON && s > EPSILON && s < 1-EPSILON) return 1;
  
  return 0;
}

/* *********************************************************** */

void boundingBox(Mesh * msh,        /* [in] input mesh */
		 double bb[3][2])   /* [out] 3D bounding box */
{
  if(msh->nb_nodes == 0) return;
  
  /* initialization */
  for(int j = 0 ; j < 3 ; j++) {
    bb[j][0] = +INFINITY; // min
    bb[j][1] = -INFINITY; // max
  }
  
  for(int i = 0 ; i < msh->nb_nodes ; i++) 
    for(int j = 0 ; j < 3 ; j++) {
      /* update lower bound */
      if(msh->nodes[i*3+j] <  bb[j][0]) bb[j][0] = msh->nodes[i*3+j]; // min
      /* update upper bound */
      if(msh->nodes[i*3+j] >  bb[j][1]) bb[j][1] = msh->nodes[i*3+j]; // max
    }  
  
}

/* *********************************************************** */

void boundingBoxMesh(Mesh * msh,        /* [in] input mesh */
		     Mesh * bbmsh)      /* [out] hexahedron output mesh with 3D bounding box for each cell */
{
  assert(msh && bbmsh);
  
  /* allocate bbmsh */
  bbmsh->elm_type = HEXAHEDRON;
  bbmsh->nb_cells = msh->nb_cells;
  bbmsh->nb_nodes = msh->nb_cells * ELEMENT_SIZE[HEXAHEDRON];
  bbmsh->nodes = malloc(bbmsh->nb_nodes*3*sizeof(double));       
  bbmsh->cells = malloc(bbmsh->nb_cells*ELEMENT_SIZE[HEXAHEDRON]*sizeof(int));           
  bbmsh->nb_node_vars = 0;
  bbmsh->nb_cell_vars = 0;
  bbmsh->node_vars = NULL;
  bbmsh->cell_vars = NULL;
  
  /* compute bounding box for each cell */
  for(int i = 0 ; i < bbmsh->nb_cells*ELEMENT_SIZE[HEXAHEDRON] ; i++) 
    bbmsh->cells[i] = i;
  
  
  for(int c = 0 ; c < msh->nb_cells ; c++) { // for each input cell
    double xmin = +INFINITY, ymin = +INFINITY, zmin = +INFINITY;
    double xmax = -INFINITY, ymax = -INFINITY, zmax = -INFINITY;
    for(int n = 0 ; n < ELEMENT_SIZE[msh->elm_type] ; n++) { // for each node per cell
      int nn = msh->cells[c*ELEMENT_SIZE[msh->elm_type]+n]; // node index
      double x = msh->nodes[nn*3+0];
      double y = msh->nodes[nn*3+1];
      double z = msh->nodes[nn*3+2];
      if(x < xmin) xmin = x;  if(y < ymin) ymin = y;  if(z < zmin) zmin = z;
      if(x > xmax) xmax = x;  if(y > ymax) ymax = y;  if(z > zmax) zmax = z;
    }
    
    // 8-nodes boxes {a,b,c,d,e,f,g,h} 
    /*
     *    e---f
     *  a---b
     *  | h | g
     *  d---c
     *
     *  lower corner : a
     *  upper corner : g
     *
     *    -y
     *    ^ 
     *    |
     *    ---> +x
     *   /
     *  -z
     */
    
    bbmsh->nodes[(c*8+0)*3+0] = xmin; // xa
    bbmsh->nodes[(c*8+0)*3+1] = ymin; // ya
    bbmsh->nodes[(c*8+0)*3+2] = zmax; // za
    bbmsh->nodes[(c*8+1)*3+0] = xmax; // xb
    bbmsh->nodes[(c*8+1)*3+1] = ymin; // yb
    bbmsh->nodes[(c*8+1)*3+2] = zmax; // zb
    bbmsh->nodes[(c*8+5)*3+0] = xmax; // xf
    bbmsh->nodes[(c*8+5)*3+1] = ymin; // yf
    bbmsh->nodes[(c*8+5)*3+2] = zmin; // zf
    bbmsh->nodes[(c*8+4)*3+0] = xmin; // xe
    bbmsh->nodes[(c*8+4)*3+1] = ymin; // ye
    bbmsh->nodes[(c*8+4)*3+2] = zmin; // ze
    
    bbmsh->nodes[(c*8+3)*3+0] = xmin; // xd
    bbmsh->nodes[(c*8+3)*3+1] = ymax; // yd
    bbmsh->nodes[(c*8+3)*3+2] = zmax; // zd
    bbmsh->nodes[(c*8+2)*3+0] = xmax; // xc
    bbmsh->nodes[(c*8+2)*3+1] = ymax; // yc
    bbmsh->nodes[(c*8+2)*3+2] = zmax; // zc    
    bbmsh->nodes[(c*8+6)*3+0] = xmax; // xg
    bbmsh->nodes[(c*8+6)*3+1] = ymax; // yg
    bbmsh->nodes[(c*8+6)*3+2] = zmin; // zg    
    bbmsh->nodes[(c*8+7)*3+0] = xmin; // xh
    bbmsh->nodes[(c*8+7)*3+1] = ymax; // yh
    bbmsh->nodes[(c*8+7)*3+2] = zmin; // zh
  }
  
}

/* *********************************************************** */

static inline 
int intersectPolygon2D(int nA,             /* [in] nb of vertices of A */
		       int nB,             /* [in] nb of vertices of B */
		       double ** coordsA,  /* [in] array of size nA of pointers on 2D coord of A */
		       double ** coordsB)  /* [in] array of size nB of pointers on 2D coord of B */  
{
  /* Remark: Z coordinate is not used. 
     \TODO: check z == 0
  */
  
  /* 1) A intersect B if any segment of A intersect any segment of B */
  
  for (int i = 0 ; i < nA ; i++)
    for (int j = 0 ; j < nB ; j++)
      {
  	int ii = (i + 1) % nA;
  	int jj = (j + 1) % nB;
	int intersect = intersectSegment2D(coordsA[i], coordsA[ii], coordsB[j], coordsB[jj]);
	// PRINT("intersect segment of cell a (%d,%d) and cell b (%d,%d) : %d\n", 
	//        i, ii, j, jj, intersect);
  	if(intersect) return 1;
      }
  
  /* 2) A is inside B if the gravity center of A is inside B (or inversely) */
  
  double A[3]; 
  polygonCenter(nA,coordsA,A); 
  double B[3]; 
  polygonCenter(nB,coordsB,B);  
  int insideA = isInsidePolygon2D(nA, coordsA, B);
  int insideB = isInsidePolygon2D(nB, coordsB, A);
  // PRINT("is cell center of a inside A : %d\n", insideA);
  // PRINT("is cell center of b inside B : %d\n", insideB);
  return insideA || insideB;
}

/* *********************************************************** */

int intersectTriangle(double * T1[3],  /* [in] vertex 3D coords of triangle T1 */
		      double * T2[3])  /* [in] vertex 3D coords of triangle T2 */
{ 
  // if(intersectTriangleOrel(T1, T2)) return 1;
  // if(intersectTriangleMoller(T1, T2)) return 1;
  // if(intersectTriangleGuigueDevillers(T1, T2)) return 1;  
  // if(intersectTriangleCGAL(T1, T2)) return 1;  
  assert(0);
  return 0;
}

/* *********************************************************** */

// scaling tetra by a scale factor of alpha
void scaleTetrahedron(double * coords[4], double alpha, double * scoords[4])
{     
  /* compute gravity center of tetra */
  double center[3];
  for (int n = 0 ; n < 3 ; n++) 
    center[n] = (coords[0][n] + coords[1][n] + coords[2][n] + coords[3][n]) / 4.0;
  
  /* (O,A') = (O,G) + (G,A') with (G,A') = ALPHA * (G,A) */  
  double vec[3];        
  for (int k = 0 ; k < 4 ; k++) {
    for (int n = 0 ; n < 3 ; n++) {     
      scoords[k][n] = center[n] + alpha * (coords[k][n] - center[n]);
    }    
  }
  
#ifdef DEBUG
  // check center invariant
  double scenter[3];
  for (int n = 0 ; n < 3 ; n++) 
    scenter[n] = (scoords[0][n] + scoords[1][n] + scoords[2][n] + scoords[3][n]) / 4.0;
  for (int n = 0 ; n < 3 ; n++) assert(fabs(center[n]-scenter[n]) < EPSILON);
  
  // check if tetra is inside
  for (int k = 0 ; k < 4 ; k++) {
    if(alpha < 1.0) assert(isInsideTetrahedron(coords, scoords[k])); // newer inside former
    else assert(isInsideTetrahedron(scoords, coords[k])); // former inside newer
  }
#endif
  
}

/* *********************************************************** */

int intersectTetrahedron(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			 double * coordsB[4],  /* [in] pointers on 3D coord of tetrahedron B */
			 double * vol)         /* [out] intersection volume */  			 
{
  *vol = 0.0;
  // return intersectTetrahedronGanovelli(coordsA,coordsB);
  // return intersectTetrahedronOrel(coordsA,coordsB);
  return intersectTetrahedronPowell(coordsA,coordsB, vol);
}

/* *********************************************************** */

int intersectScaledTetrahedron(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			       double * coordsB[4],  /* [in] pointers on 3D coord of tetrahedron B */
			       double alpha,         /* [in] scale up a tetra by factor alpha */
			       double * vol)           /* [out] intersection volume */  			 			       
{  
  
  double _scoordsA[4][3];
  double _scoordsB[4][3];
  double * scoordsA[4];
  double * scoordsB[4];
  for(int k = 0 ; k < 4 ; k++) {
    scoordsA[k] = _scoordsA[k];
    scoordsB[k] = _scoordsB[k];
    assert(_scoordsB[k] == &_scoordsB[k][0]);
    assert(_scoordsA[k] == &_scoordsA[k][0]);    
  }
  
  scaleTetrahedron(coordsA, alpha, scoordsA);
  scaleTetrahedron(coordsB, alpha, scoordsB);  
  *vol = 0.0;
  if(intersectTetrahedron(scoordsA, scoordsB, vol)) return 1;  
  return 0;
}

/* *********************************************************** */

// convert an hexahedron in 6 tetrahedra
void hexahedron2tetrahedron(double * hexa[8],      /* [in] array of 8 pointers on 3D coord of an hexahedron */
			    double* tetras[6][4])  /* [in] 6 arrays of 4 pointers on 3D coord of each tetra  */
{
  
  /*
   * The k-th cell, that connects nodes {a,b,c,d,e,f,g,h}   
   *
   *    h---g
   *  e---f
   *  | d | c
   *  a---b
   * 
   *    +y
   *    ^ 
   *    |
   *    0---> +x
   *
   * with a the lower corner assumed on 'a'
   */
  
  // [Tetrahedral Decompositions of Hexahedral Meshes. DEREK HACON AND CARLOS TOMEI]
  
  /* "There is one well known way of decomposing a hexahedral mesh
   * into non-overlapping tetrahedra which always works. Start by
   * labelling the vertices 1,2,3, .... Then divide each face into two
   * triangles by the diagonal containing the first vertex of the
   * face. Next consider a hexahedron H with first vertex V. The three
   * faces of H not containing V have already been divided up into a
   * total of six triangles, so take each of these to be the base of a
   * tetrahedron with apex V."
   */
  
  /*
   * Consider H={a,b,c,d,e,f,g,h} 
   * a) Select the first vertex V=a of H.
   * b) Select the 3 faces of H not containing a: F0={b,c,g,f}, F1={c,d,h,g}, F2={e,f,g,h}
   * c) Divide each face into two triangles by the diagonal containing the first vertex of the face:
   *   - F0 = T00 u T01, with T00={b,g,c} and T01={b,g,f}
   *   - F1 = T10 u T11, with T10={c,h,d} and T11={c,h,g}
   *   - F2 = T20 u T21, with T20={e,g,f} and T21={e,g,h}  
   * d) Build the 6 tetrahedrons, such that each of these triangles T* is the base of a tetrahedron with apex V:
   *   - {a,b,g,c}, {a,b,g,f}, {a,c,h,d}, {a,c,h,g}, {a,e,g,f}, {a,e,g,h}.
   */
  
  /* Decompose hexahedron {a,b,c,d,e,f,g,h} in 6 tetrahedra: {a,b,g,c}, {a,b,g,f}, {a,c,h,d}, {a,c,h,g}, {a,e,g,f}, {a,e,g,h}. */
  
  double* a = hexa[0];
  double* b = hexa[1];
  double* c = hexa[2];
  double* d = hexa[3];
  double* e = hexa[4];
  double* f = hexa[5];
  double* g = hexa[6];
  double* h = hexa[7];
  
  tetras[0][0] = a; tetras[0][1] = b; tetras[0][2] = g; tetras[0][3] = c; /* face T00 */
  tetras[1][0] = a; tetras[1][1] = b; tetras[1][2] = g; tetras[1][3] = f; /* face T01 */
  tetras[2][0] = a; tetras[2][1] = c; tetras[2][2] = h; tetras[2][3] = d; /* face T10 */
  tetras[3][0] = a; tetras[3][1] = c; tetras[3][2] = h; tetras[3][3] = g; /* face T11 */
  tetras[4][0] = a; tetras[4][1] = e; tetras[4][2] = g; tetras[4][3] = f; /* face T20 */
  tetras[5][0] = a; tetras[5][1] = e; tetras[5][2] = g; tetras[5][3] = h; /* face T21 */
  
}

/* *********************************************************** */

// convert a prism in 3 tetrahedra

/* Paper: How to Subdivide Pyramids, Prisms and Hexahedra into Tetrahedra. Julien Dompierre, Paul Labb�, Marie-Gabrielle Vallet, Ricardo Camarero. */

void prism2tetrahedron(double * prism[6],      /* [in] array of 6 pointers on 3D coord of a prism */
		       double* tetras[3][4])   /* [in] 3 arrays of 4 pointers on 3D coord of each tetra  */
{
  
  /* prism {0,1,2,3,4,5} based on 2 triangles {0,1,2} and {3,4,5} */
  
  /* tetra0: {0,3,4,5} */
  tetras[0][0] = prism[0]; tetras[0][1] = prism[3]; tetras[0][2] = prism[4]; tetras[0][3] = prism[5]; 
  /* tetra1: {0,1,4,5} */
  tetras[1][0] = prism[0]; tetras[1][1] = prism[1]; tetras[1][2] = prism[4]; tetras[1][3] = prism[5]; 
  /* tetra2: {0,1,2,5} */
  tetras[2][0] = prism[0]; tetras[2][1] = prism[1]; tetras[2][2] = prism[2]; tetras[2][3] = prism[5]; 
  
}

/* *********************************************************** */

// convert an hexahedron in 6 tetrahedra
void hexahedron2tetrahedron2(Mesh * msh,            /* [in] input mesh */
			     int k,                 /* [in] input hexa element */
			     int tetras[6][4])      /* [in] 6 tetra connectivity lists */
{
  /*
   * The k-th cell, that connects nodes {a,b,c,d,e,f,g,h}   
   *
   *    h---g
   *  e---f
   *  | d | c
   *  a---b
   * 
   *    +y
   *    ^ 
   *    |
   *    0---> +x
   *
   * with a the lower corner assumed on 'a'
   */
  
  assert(msh);
  assert(msh->elm_type == HEXAHEDRON);
  int elm_size = ELEMENT_SIZE[HEXAHEDRON];
  
  // convert an hexahedron {a,b,c,d,e,f,g,h} in 6 tetrahedra: {a,b,g,c}, {a,b,g,f}, {a,c,h,d}, {a,c,h,g}, {a,e,g,f}, {a,e,g,h}.
  int a = msh->cells[k*elm_size+0];
  int b = msh->cells[k*elm_size+1];
  int c = msh->cells[k*elm_size+2];
  int d = msh->cells[k*elm_size+3];
  int e = msh->cells[k*elm_size+4];
  int f = msh->cells[k*elm_size+5];
  int g = msh->cells[k*elm_size+6];
  int h = msh->cells[k*elm_size+7];
  
  // {a,b,g,c}
  tetras[0][0] = a;
  tetras[0][1] = b;
  tetras[0][2] = g;
  tetras[0][3] = c;
  // {a,b,g,f}
  tetras[1][0] = a;
  tetras[1][1] = b;
  tetras[1][2] = g;
  tetras[1][3] = f;
  // {a,c,h,d}
  tetras[2][0] = a;
  tetras[2][1] = c;
  tetras[2][2] = h;
  tetras[2][3] = d;
  // {a,c,h,g}
  tetras[3][0] = a;
  tetras[3][1] = c;
  tetras[3][2] = h;
  tetras[3][3] = g;
  // {a,e,g,f}
  tetras[4][0] = a;
  tetras[4][1] = e;
  tetras[4][2] = g;
  tetras[4][3] = f;
  // {a,e,g,h}
  tetras[5][0] = a;
  tetras[5][1] = e;
  tetras[5][2] = g;
  tetras[5][3] = h;  
}

/* *********************************************************** */

// convert a prism in 3 tetrahedra
void prism2tetrahedron2(Mesh * msh,            /* [in] input mesh */
			int k,                 /* [in] input hexa element */
			int tetras[3][4])      /* [in] 3 tetra connectivity lists */  
{
  assert(msh);
  assert(msh->elm_type == PRISM);
  int elm_size = ELEMENT_SIZE[PRISM];
  
  // convert a prism {a,b,c,d,e,f} in 3 tetrahedra: {a,d,e,f}, {a,b,e,f}, {a,b,c,f}.  
  int a = msh->cells[k*elm_size+0];
  int b = msh->cells[k*elm_size+1];
  int c = msh->cells[k*elm_size+2];
  int d = msh->cells[k*elm_size+3];
  int e = msh->cells[k*elm_size+4];
  int f = msh->cells[k*elm_size+5];
  
  // {a,d,e,f}
  tetras[0][0] = a;
  tetras[0][1] = d;
  tetras[0][2] = e;
  tetras[0][3] = f;
  // {a,b,e,f}
  tetras[1][0] = a;
  tetras[1][1] = b;
  tetras[1][2] = e;
  tetras[1][3] = f;
  // {a,b,c,f}
  tetras[2][0] = a;
  tetras[2][1] = b;
  tetras[2][2] = c;
  tetras[2][3] = f;      
}

/* *********************************************************** */

static inline 
int intersectHexahedron(double * coordsA[8],  /* [in] pointers on 3D coord of hexahedron A */
			double * coordsB[8],  /* [in] pointers on 3D coord of hexahedron B */
			double * vol)         /* [out] intersection volume */  			 			
{  
  /* Decompose hexahedron A into 6 tetrahedra: {0,1,7,2}, {0,1,7,5}, {0,2,6,3}, {0,2,6,7}, {0,4,7,5}, {0,4,7,6}. */ 
  double * TetrasA[6][4];
  TetrasA[0][0] = TetrasA[1][0] = TetrasA[2][0] = TetrasA[3][0] = TetrasA[4][0] = TetrasA[5][0] = coordsA[0]; /* apex V */
  TetrasA[0][1] = coordsA[1]; TetrasA[0][2] = coordsA[7]; TetrasA[0][3] = coordsA[2]; /* face T00 */
  TetrasA[1][1] = coordsA[1]; TetrasA[1][2] = coordsA[7]; TetrasA[1][3] = coordsA[5]; /* face T01 */
  TetrasA[2][1] = coordsA[2]; TetrasA[2][2] = coordsA[6]; TetrasA[2][3] = coordsA[3]; /* face T10 */
  TetrasA[3][1] = coordsA[2]; TetrasA[3][2] = coordsA[6]; TetrasA[3][3] = coordsA[7]; /* face T11 */
  TetrasA[4][1] = coordsA[4]; TetrasA[4][2] = coordsA[7]; TetrasA[4][3] = coordsA[5]; /* face T20 */
  TetrasA[5][1] = coordsA[4]; TetrasA[5][2] = coordsA[7]; TetrasA[5][3] = coordsA[6]; /* face T21 */
  
  /* Decompose hexahedron B */
  double * TetrasB[6][4];
  TetrasB[0][0] = TetrasB[1][0] = TetrasB[2][0] = TetrasB[3][0] = TetrasB[4][0] = TetrasB[5][0] = coordsB[0]; /* apex V */
  TetrasB[0][1] = coordsB[1]; TetrasB[0][2] = coordsB[7]; TetrasB[0][3] = coordsB[2]; /* face T00 */
  TetrasB[1][1] = coordsB[1]; TetrasB[1][2] = coordsB[7]; TetrasB[1][3] = coordsB[5]; /* face T01 */
  TetrasB[2][1] = coordsB[2]; TetrasB[2][2] = coordsB[6]; TetrasB[2][3] = coordsB[3]; /* face T10 */
  TetrasB[3][1] = coordsB[2]; TetrasB[3][2] = coordsB[6]; TetrasB[3][3] = coordsB[7]; /* face T11 */
  TetrasB[4][1] = coordsB[4]; TetrasB[4][2] = coordsB[7]; TetrasB[4][3] = coordsB[5]; /* face T20 */
  TetrasB[5][1] = coordsB[4]; TetrasB[5][2] = coordsB[7]; TetrasB[5][3] = coordsB[6]; /* face T21 */
  
  // 2) intersect all tetrahedrons
  *vol = 0.0;
  double _vol = 0.0;
  int ret = 0;
  for(int i = 0 ; i < 6 ; i++)
    for(int j = 0 ; j < 6 ; j++)
      {
	ret += intersectTetrahedron(TetrasA[i],TetrasB[j], &_vol);
	*vol += _vol;
      }
  
  return ret;
}

/* *********************************************************** */

int intersectCube(double * coordsA[8], 
		  double * coordsB[8])
{
  enum {a = 0, b, c, d, e, f, g, h};
  enum {x = 0, y, z};
  
  /* fast intersection algorithm assuming that hexahedron = cube aligned on x,y,z axis */
  
  /*
   *
   *    e---f
   *  a---b
   *  | h | g
   *  d---c
   *
   *  lower corner : a
   *  upper corner : g
   *
   *    -y
   *    ^ 
   *    |
   *    ---> +x
   *   /
   *  -z
   */
  
  double coordsC[8][3]; // cube C = A n B
  
  // check cube coords for A
  
  assert(coordsA[a][x] < coordsA[b][x]); 
  assert(coordsA[d][x] < coordsA[c][x]);
  assert(coordsA[e][x] < coordsA[f][x]); 
  assert(coordsA[h][x] < coordsA[g][x]);
  
  assert(coordsA[a][y] < coordsA[d][y]); 
  assert(coordsA[b][y] < coordsA[c][y]);
  assert(coordsA[e][y] < coordsA[h][y]); 
  assert(coordsA[f][y] < coordsA[g][y]);
  
  assert(coordsA[a][z] < coordsA[e][z]); 
  assert(coordsA[b][z] < coordsA[f][z]);
  assert(coordsA[c][z] < coordsA[g][z]); 
  assert(coordsA[d][z] < coordsA[h][z]);
  
  // check cube coords for B
  
  assert(coordsB[a][x] < coordsB[b][x]); 
  assert(coordsB[d][x] < coordsB[c][x]);
  assert(coordsB[e][x] < coordsB[f][x]); 
  assert(coordsB[h][x] < coordsB[g][x]);
  
  assert(coordsB[a][y] < coordsB[d][y]); 
  assert(coordsB[b][y] < coordsB[c][y]);
  assert(coordsB[e][y] < coordsB[h][y]); 
  assert(coordsB[f][y] < coordsB[g][y]);
  
  assert(coordsB[a][z] < coordsB[e][z]); 
  assert(coordsB[b][z] < coordsB[f][z]);
  assert(coordsB[c][z] < coordsB[g][z]); 
  assert(coordsB[d][z] < coordsB[h][z]);
  
  // compute intersection C = A n B
  
  /* axis X: MAX for {a,b,e,f} and MIN for {b,c,f,g} */
  coordsC[a][x] = MAX(coordsA[a][x],coordsB[a][x]);
  coordsC[d][x] = MAX(coordsA[d][x],coordsB[d][x]);
  coordsC[e][x] = MAX(coordsA[e][x],coordsB[e][x]);
  coordsC[h][x] = MAX(coordsA[h][x],coordsB[h][x]);
  coordsC[b][x] = MIN(coordsA[b][x],coordsB[b][x]);
  coordsC[c][x] = MIN(coordsA[c][x],coordsB[c][x]);
  coordsC[f][x] = MIN(coordsA[f][x],coordsB[f][x]);
  coordsC[g][x] = MIN(coordsA[g][x],coordsB[g][x]);
  
  /* axis Y: MAX for {a,b,e,f} and MIN for {d,c,h,g} */
  coordsC[a][y] = MAX(coordsA[a][y],coordsB[a][y]);
  coordsC[b][y] = MAX(coordsA[b][y],coordsB[b][y]);
  coordsC[e][y] = MAX(coordsA[e][y],coordsB[e][y]);
  coordsC[f][y] = MAX(coordsA[f][y],coordsB[f][y]);
  coordsC[d][y] = MIN(coordsA[d][y],coordsB[d][y]);
  coordsC[c][y] = MIN(coordsA[c][y],coordsB[c][y]);
  coordsC[h][y] = MIN(coordsA[h][y],coordsB[h][y]);
  coordsC[g][y] = MIN(coordsA[g][y],coordsB[g][y]);
  
  /* axis Z: MAX for {a,b,c,d} and MIN for {e,f,g,h} */
  coordsC[a][z] = MAX(coordsA[a][z],coordsB[a][z]);
  coordsC[b][z] = MAX(coordsA[b][z],coordsB[b][z]);
  coordsC[c][z] = MAX(coordsA[c][z],coordsB[c][z]);
  coordsC[d][z] = MAX(coordsA[d][z],coordsB[d][z]);
  coordsC[e][z] = MIN(coordsA[e][z],coordsB[e][z]);
  coordsC[f][z] = MIN(coordsA[f][z],coordsB[f][z]);
  coordsC[g][z] = MIN(coordsA[g][z],coordsB[g][z]);
  coordsC[h][z] = MIN(coordsA[h][z],coordsB[h][z]);
  
  // check if intersection is valid !!! 
  
  if( (coordsC[a][x] < coordsC[b][x]) &&  
      (coordsC[d][x] < coordsC[c][x]) && 
      (coordsC[e][x] < coordsC[f][x]) &&  
      (coordsC[h][x] < coordsC[g][x]) && 
      (coordsC[a][y] < coordsC[d][y]) &&  
      (coordsC[b][y] < coordsC[c][y]) && 
      (coordsC[e][y] < coordsC[h][y]) &&  
      (coordsC[f][y] < coordsC[g][y]) && 
      (coordsC[a][z] < coordsC[e][z]) &&  
      (coordsC[b][z] < coordsC[f][z]) && 
      (coordsC[c][z] < coordsC[g][z]) &&  
      (coordsC[d][z] < coordsC[h][z]) ) 
    return 1;
  
  return 0;
  
  // WARNING: not optimal at all, see bounding box intersection algorithm in intersectPolyhedron() routine...
  
}

/* *********************************************************** */

int intersectPolyhedron(int cellsizeA, double * coordsA[cellsizeA], 
			int cellsizeB, double * coordsB[cellsizeB],
			double * w)
{
  assert(cellsizeA == 4 || cellsizeA == 6 || cellsizeA == 8); // tetra or prism or hexa
  assert(cellsizeB == 4 || cellsizeB == 6 || cellsizeB == 8); // tetra or prism or hexa  
  
  // 0) compute bounding box for each cell and test overlap of boxes
  
  double xminA = +INFINITY, yminA = +INFINITY, zminA = +INFINITY;
  double xmaxA = -INFINITY, ymaxA = -INFINITY, zmaxA = -INFINITY;
  for(int k = 0 ; k < cellsizeA ; k++) { 
    double x = coordsA[k][0];
    double y = coordsA[k][1];
    double z = coordsA[k][2];
    if(x < xminA) xminA = x;  if(y < yminA) yminA = y;  if(z < zminA) zminA = z;
    if(x > xmaxA) xmaxA = x;  if(y > ymaxA) ymaxA = y;  if(z > zmaxA) zmaxA = z;
  }
  
  double xminB = +INFINITY, yminB = +INFINITY, zminB = +INFINITY;
  double xmaxB = -INFINITY, ymaxB = -INFINITY, zmaxB = -INFINITY;
  for(int k = 0 ; k < cellsizeB ; k++) { 
    double x = coordsB[k][0];
    double y = coordsB[k][1];
    double z = coordsB[k][2];
    if(x < xminB) xminB = x;  if(y < yminB) yminB = y;  if(z < zminB) zminB = z;
    if(x > xmaxB) xmaxB = x;  if(y > ymaxB) ymaxB = y;  if(z > zmaxB) zmaxB = z;
  }
  
  /* no intersection if (xminA < xmaxA < xminB < xmaxB) or (xminB < xmaxB < xminA < xmaxA) */  
  if( (xmaxA < xminB || xmaxB < xminA) || 
      (ymaxA < yminB || ymaxB < yminA) ||
      (zmaxA < zminB || zmaxB < zminA)) 
    // skip polyhedron intersect because respective cell bounding boxes don't overlap...
    return 0;   
  
  // 1) decompose polyhedron in several tetrahedrons 
  
  /* 1 single hexahedron */
  int nbTetrasA = 1;
  int nbTetrasB = 1;
  /* 3 tetrahedra required for 1 prism */
  if(cellsizeA == 6) nbTetrasA = 3; 
  if(cellsizeB == 6) nbTetrasB = 3;
  /* 6 tetrahedra required for 1 hexahedron */
  if(cellsizeA == 8) nbTetrasA = 6; 
  if(cellsizeB == 8) nbTetrasB = 6;
  
  double * tetrasA[nbTetrasA][4];
  double * tetrasB[nbTetrasB][4];
  
  if(cellsizeA == 4) memcpy(tetrasA[0], coordsA, 4*sizeof(double*));   // 1 single tetra
  else if(cellsizeA == 6) prism2tetrahedron(coordsA, tetrasA);         // prism decomposed into 3 tetras
  else if(cellsizeA == 8) hexahedron2tetrahedron(coordsA, tetrasA);    // hexa decomposed into 6 tetras
  
  if(cellsizeB == 4) memcpy(tetrasB[0], coordsB, 4*sizeof(double*));   // 1 single tetra
  else if(cellsizeB == 6) prism2tetrahedron(coordsB, tetrasB);         // prism decomposed into 3 tetras
  else if(cellsizeB == 8) hexahedron2tetrahedron(coordsB, tetrasB);    // hexa decomposed into 6 tetras
  
  // 2) intersect tetrahedrons  
  *w = 0.0;
  double _w = 0.0;
  int ret = 0;
  for(int i = 0 ; i < nbTetrasA ; i++)
    for(int j = 0 ; j < nbTetrasB ; j++)
      {
#ifndef SCALEFACTOR
	ret += intersectTetrahedron(tetrasA[i],tetrasB[j], &_w);
#else
	ret += intersectScaledTetrahedron(tetrasA[i],tetrasB[j], SCALEFACTOR, &_w);
#endif
	*w += _w;	
      }
  
  return ret;
}

/* *********************************************************** */

int intersectCells(Mesh * mshA,   /* [in] input mesh A */
		   Mesh * mshB,   /* [in] input mesh B */
		   int a,         /* [in] cell index in mesh A */
		   int b,         /* [in] cell index in mesh B */
		   double * vol)  /* [out] intesection volume */     
{
  int cellsizeA = ELEMENT_SIZE[mshA->elm_type]; // nb of nodes
  int cellsizeB = ELEMENT_SIZE[mshB->elm_type]; // nb of nodes
  int * cellA = mshA->cells + a*cellsizeA;   
  int * cellB = mshB->cells + b*cellsizeB;   
  double * coordsA[cellsizeA];
  double * coordsB[cellsizeB];
  *vol = 0.0;
  
  for (int i = 0 ; i < cellsizeA ; i++)
    coordsA[i] = mshA->nodes + cellA[i]*3;
  for (int i = 0 ; i < cellsizeB ; i++)
    coordsB[i] = mshB->nodes + cellB[i]*3;
  
  /* 2D polygon-polygon intersection (surface) */
  if( (mshA->elm_type == TRIANGLE || mshA->elm_type == QUADRANGLE) && 
      (mshB->elm_type == TRIANGLE || mshB->elm_type == QUADRANGLE) )     
    return intersectPolygon2D(cellsizeA, cellsizeB, coordsA, coordsB); // z not used !!!  
  
  /* 3D polyhedron-polyhedron intersection (volume) */
  // else if( mshA->elm_type == HEXAHEDRON &&  mshB->elm_type == HEXAHEDRON ) 
  // return intersectCube(coordsA, coordsB); // WARNING: faster, but not generic (assumed cube are aligned on cartesian grid) !!!
  // return intersectHexahedron(coordsA, coordsB);
  
  else if( (mshA->elm_type == TETRAHEDRON || mshA->elm_type == PRISM || mshA->elm_type == HEXAHEDRON) &&  
	   (mshB->elm_type == TETRAHEDRON || mshB->elm_type == PRISM || mshB->elm_type == HEXAHEDRON) ) 
    return intersectPolyhedron(cellsizeA, coordsA, cellsizeB, coordsB, vol);
  
  else {
    fprintf(stderr,"ERROR: intersectCells() failure!\n");
    assert(0);
  }
  
  return 0;
}

/* *********************************************************** */

void intersectMesh(Mesh * mshA,        /* [in] input mesh A */
		   Mesh * mshB,        /* [in] input mesh B */
		   int * nbinteredges, /* [out] nb inter-edges */
		   int ** interedges,  /* [out] allocated array of inter-edges */
		   double ** volumes)  /* [out] allocated array of interedge volumes (or NULL if not used) */		   
{
  assert(nbinteredges && interedges);
  
  /* reset output */
  *nbinteredges = 0;
  *interedges = NULL;
  if(volumes) *volumes = NULL;
  
  /* 
     There is an interedge a-b between cell a and cell b if there is a
     geometric intersection between this two cells.
  */
  
  assert( /* surface (3D) */
	 ((mshA->elm_type == TRIANGLE || mshA->elm_type == QUADRANGLE) &&
	  (mshB->elm_type == TRIANGLE || mshB->elm_type == QUADRANGLE)) ||
	 /* volume (3D) */
	 ((mshA->elm_type == TETRAHEDRON || mshA->elm_type == PRISM || mshA->elm_type == HEXAHEDRON) &&
	  (mshB->elm_type == TETRAHEDRON || mshB->elm_type == PRISM || mshB->elm_type == HEXAHEDRON))
	  );
  
  /* 1) compute cell centers */
  
  double * centersA = malloc(mshA->nb_cells*3*sizeof(double));
  double * centersB = malloc(mshB->nb_cells*3*sizeof(double));
  assert(centersA && centersB);  
  for(int i = 0 ; i < mshA->nb_cells ; i++) cellCenter(mshA, i, centersA + i*3);  
  for(int i = 0 ; i < mshB->nb_cells ; i++) cellCenter(mshB, i, centersB + i*3);
  
  /* 2) pre-compute coarse boxes for fast cell intersection algorithm */
  
  /* compute max cell length  */
  double hA[3]; 
  cellsLength(mshA, hA);
  double hB[3]; 
  cellsLength(mshB, hB);
  double H[3];
  H[0] = 2.0*MAX(hA[0],hB[0]);
  H[1] = 2.0*MAX(hA[1],hB[1]);
  H[2] = 2.0*MAX(hA[2],hB[2]);
  PRINT("max cell length = (%.5f,%.5f,%.5f)\n", H[0], H[1], H[2]);       
  
  if(H[0] <= EPSILON) PRINT("WARNING: dimension X missing!\n");
  if(H[1] <= EPSILON) PRINT("WARNING: dimension Y missing!\n");
  if(H[2] <= EPSILON) PRINT("WARNING: dimension Z missing!\n");
  
  /* compute bounding box for A & B */
  
  double bbA[3][2];
  double bbB[3][2];
  boundingBox(mshA, bbA);
  boundingBox(mshB, bbB);
  
  PRINT("bounding box A {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	 bbA[0][0], bbA[0][1], bbA[1][0], bbA[1][1], bbA[2][0], bbA[2][1]); /* debug */
  PRINT("bounding box B {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	 bbB[0][0], bbB[0][1], bbB[1][0], bbB[1][1], bbB[2][0], bbB[2][1]); /* debug */
  
  /* compute bounding box (bbA U bbB)*/
  double bb[3][2];
  for(int j = 0 ; j < 3 ; j++) {
    bb[j][0] = (bbA[j][0]<bbB[j][0])?bbA[j][0]:bbB[j][0]; /* lower bound */
    bb[j][1] = (bbA[j][1]>bbB[j][1])?bbA[j][1]:bbB[j][1]; /* upper bound */
  }
  
  PRINT("bounding box {(%.5f,%.5f) (%.5f,%.5f) (%.5f,%.5f)}\n", 
	  bb[0][0], bb[0][1], bb[1][0], bb[1][1], bb[2][0], bb[2][1]); /* debug */
  
  /* compute box dim */  
  int boxdim[3];
  for(int j = 0 ; j < 3 ; j++)  {
    if(H[j] >= EPSILON) boxdim[j] = ceil((bb[j][1]-bb[j][0]) / H[j]); else boxdim[j] = 1;
    if(boxdim[j] == 0) boxdim[j] = 1;
  }
  
  PRINT("box dim {%d %d %d}\n", boxdim[0], boxdim[1], boxdim[2]); 
  assert(boxdim[0] > 0 && boxdim[1] > 0 && boxdim[2] > 0); 
  
  /* compute mapping between cells and boxes */
  
  int nbboxes = boxdim[2]*boxdim[1]*boxdim[0];
  assert(nbboxes > 0);
  int **boxesA = calloc (nbboxes, sizeof(int *));
  int **boxesB = calloc (nbboxes, sizeof(int *));
  int *nbcellsinboxesA = calloc (nbboxes, sizeof(int));
  int *nbcellsinboxesB = calloc (nbboxes, sizeof(int));
  int * boxmappingA = malloc(mshA->nb_cells*sizeof(int));
  assert(boxmappingA != NULL);
  int * boxmappingB = malloc(mshB->nb_cells*sizeof(int));
  assert(boxmappingB != NULL);
  
  /* pre-compute nb cells per boxes for A and mapping */
  for(int i = 0 ; i < mshA->nb_cells ; i++) {
    double * G = centersA + i*3;    
    int boxcoordX = 0; if(H[0] >= EPSILON) boxcoordX = floor((G[0]-bb[0][0]) / H[0]);
    int boxcoordY = 0; if(H[1] >= EPSILON) boxcoordY = floor((G[1]-bb[1][0]) / H[1]);
    int boxcoordZ = 0; if(H[2] >= EPSILON) boxcoordZ = floor((G[2]-bb[2][0]) / H[2]);
    // int k = boxcoordY*boxdim[0] + boxcoordX;
    int k = boxcoordZ*boxdim[0]*boxdim[1] + boxcoordY*boxdim[0] + boxcoordX;
    assert(boxcoordX >= 0 && boxcoordX < boxdim[0]);
    assert(boxcoordY >= 0 && boxcoordY < boxdim[1]);
    assert(boxcoordZ >= 0 && boxcoordZ < boxdim[2]);
    assert(k >= 0 && k < nbboxes);
    nbcellsinboxesA[k]++;
    boxmappingA[i] = k;
  }
  
  /* pre-compute nb cells per boxes for B */
  for(int i = 0 ; i < mshB->nb_cells ; i++) {
    double * G = centersB + i*3;    
    int boxcoordX = 0; if(H[0] >= EPSILON) boxcoordX = floor((G[0]-bb[0][0]) / H[0]);
    int boxcoordY = 0; if(H[1] >= EPSILON) boxcoordY = floor((G[1]-bb[1][0]) / H[1]);
    int boxcoordZ = 0; if(H[2] >= EPSILON) boxcoordZ = floor((G[2]-bb[2][0]) / H[2]);
    int k = boxcoordZ*boxdim[0]*boxdim[1] + boxcoordY*boxdim[0] + boxcoordX;
    // int k = boxcoordY*boxdim[0] + boxcoordX;
    assert(boxcoordX >= 0 && boxcoordX < boxdim[0]);
    assert(boxcoordY >= 0 && boxcoordY < boxdim[1]);
    assert(boxcoordZ >= 0 && boxcoordZ < boxdim[2]);
    assert(k >= 0 && k < nbboxes);
    nbcellsinboxesB[k]++;
    boxmappingB[i] = k;
  }  
  
  /* debug */
#ifdef DEBUG
  Mesh bbmsh;
  generateGrid3D(boxdim[0], boxdim[1], boxdim[2], H[0], H[1], H[2], &bbmsh); 
  shiftMesh(&bbmsh, bb[0][0], bb[1][0], bb[2][0]);
  addMeshVariable(&bbmsh, "nbcellsA", CELL_INTEGER, 1, nbcellsinboxesA);
  addMeshVariable(&bbmsh, "nbcellsB", CELL_INTEGER, 1, nbcellsinboxesB);
  printf("Saving bbmsh.vtk\n");
  saveVTKMesh("bbmsh.vtk", &bbmsh);    
  freeMesh(&bbmsh);
#endif  
  
  /* allocate all boxes */
  int checkcellsumA = 0, checkcellsumB = 0;
  for(int k = 0 ; k < nbboxes ; k++) {
    checkcellsumA += nbcellsinboxesA[k];
    checkcellsumB += nbcellsinboxesB[k];
    boxesA[k] = boxesB[k] = NULL;
    if(nbcellsinboxesA[k] > 0) {
      boxesA[k] = malloc(nbcellsinboxesA[k]*sizeof(int));
      assert(boxesA[k] != NULL);
      
    }
    if(nbcellsinboxesB[k] > 0) {
      boxesB[k] = malloc(nbcellsinboxesB[k]*sizeof(int));    
      assert(boxesB[k] != NULL);
    }
  }
  /* check sum */
  assert(checkcellsumA == mshA->nb_cells &&  checkcellsumB == mshB->nb_cells);
  
  /* sort cells of mesh A in boxes */
  
  int *currentcellinboxesA = calloc (nbboxes, sizeof(int));
  for(int i = 0 ; i < mshA->nb_cells ; i++) {
    int k = boxmappingA[i];
    assert(currentcellinboxesA[k]  < nbcellsinboxesA[k]); 
    boxesA[k][currentcellinboxesA[k]] = i;
    currentcellinboxesA[k]++;
  }
  free (currentcellinboxesA);
  
  int *currentcellinboxesB = calloc (nbboxes, sizeof(int));
  for(int i = 0 ; i < mshB->nb_cells ; i++) {
    int k = boxmappingB[i];
    assert(currentcellinboxesB[k]  < nbcellsinboxesB[k]); 
    boxesB[k][currentcellinboxesB[k]] = i;
    currentcellinboxesB[k]++;
  }
  free (currentcellinboxesB);
  
#ifdef DEBUG  
  for(int k = 0 ; k < nbboxes ; k++) {
    PRINT("nb cells in box A[%d] = %d\n", k, nbcellsinboxesA[k]);
    PRINT("nb cells in box B[%d] = %d\n", k, nbcellsinboxesB[k]);
  }
#endif
  
  /* 3) compute inter edges */
  
#ifdef SCALEFACTOR
  PRINT("tetra scale factor: %.3f\n", SCALEFACTOR);
#endif  
  
  /* long nbtest = 0; */
  /* long nbboxtest = 0; */
  /* long maxboxtest = 0; */
  // int K = 0;
  
  int nthreads = omp_get_max_threads();
  assert(nthreads >= 1);
  int _nbinteredges[nthreads];
  int _maxinteredges[nthreads];
  int * _interedges[nthreads];
  double * _volumes[nthreads];  
  
  /* for boxesA[k] and neighbor boxesB[kk], test intersection... */
#pragma omp parallel default(none) firstprivate(nthreads,nbboxes,boxdim,mshA,mshB,boxesA,boxesB,nbcellsinboxesA,nbcellsinboxesB,stderr) shared(_nbinteredges,_maxinteredges,_interedges, _volumes) 
  {
    // alloc local buffer for each thread
    int tid = omp_get_thread_num();
    assert(tid >= 0 && tid < nthreads);
    _nbinteredges[tid] = 0;
    _maxinteredges[tid] = 10000;
    _interedges[tid] = malloc(_maxinteredges[tid]*2*sizeof(int));
    _volumes[tid] = malloc(_maxinteredges[tid]*sizeof(double));      
    assert(_interedges[tid]);
    assert(_volumes[tid]);    
    
    // #pragma omp for schedule(static,1) 
#pragma omp for schedule(dynamic,1)     
    for(int k = 0 ; k < nbboxes ; k++) {
      
      // debug
      // if((k+1) % (int)(ceil(nbboxes/10.0)) == 0)
      // printf("%d%% : %d/%d boxes, %d interedges found\n", (int)((k+1)*100.0/nbboxes), k+1, nbboxes, _nbinteredges);
      
      int z = k / (boxdim[0]*boxdim[1]);
      int r = k % (boxdim[0]*boxdim[1]);
      int y = r / boxdim[0];
      int x = r % boxdim[0];
      assert(x >= 0 && x < boxdim[0]);
      assert(y >= 0 && y < boxdim[1]);
      assert(z >= 0 && z < boxdim[2]);
      assert((z*boxdim[1]*boxdim[0] + y*boxdim[0] + x) == k);    
      
      for(int dx = -1 ; dx <= 1  ; dx++) {
	int xx = x + dx;
	if(xx < 0 || xx >= boxdim[0]) continue;
	for(int dy = -1 ; dy <= 1  ; dy++) {
	  int yy = y + dy;
	  if(yy < 0 || yy >= boxdim[1]) continue;
	  for(int dz = -1 ; dz <= 1  ; dz++) {
	    int zz = z + dz;
	    if(zz < 0 || zz >= boxdim[2]) continue;
	    
	    int kk = zz*boxdim[1]*boxdim[0] + yy*boxdim[0] + xx;
	    assert(kk >= 0 && kk < nbboxes);
	    
	    // maxboxtest++;
	    
	    if(nbcellsinboxesA[k] == 0 || nbcellsinboxesB[kk] == 0) {
	      PRINT("skip box intersection of A%d and B%d\n", k, kk);
	      continue; // skip if box is empty
	    }    
	    
	    // nbboxtest++;
	    
	    /* for all cells in boxesA[k] */
	    for(int a = 0 ; a < nbcellsinboxesA[k] ; a++) {
	      for(int b = 0 ; b < nbcellsinboxesB[kk] ; b++) {
		// nbtest++;
		int aa = boxesA[k][a];   /* cell index in mesh A */
		int bb = boxesB[kk][b];  /* cell index in mesh B */
		PRINT("\n===== TEST INTEREDGE (%d,%d) from boxA %d and boxB %d =====\n\n", aa,bb, k, kk); 
		
		/* test inter-edge (aa,bb) */
		double w = 0.0;
		if(intersectCells(mshA,mshB,aa,bb, &w)) {
		  _interedges[tid][2*_nbinteredges[tid]+0] = aa;
		  _interedges[tid][2*_nbinteredges[tid]+1] = bb;
		  _volumes[tid][_nbinteredges[tid]] = w; // interedge weigth is the intersection volume
		  _nbinteredges[tid]++;
		  
		  // if(_nbinteredges[tid] > 0 && _nbinteredges[tid] % 10000 == 0)
		  // printf("[thread %d] %d interedges found\n", tid,_nbinteredges[tid]); 
		  
		  /* realloc if needed... */
		  if(_nbinteredges[tid] == _maxinteredges[tid]) {
		    double newmax = 2.0*_maxinteredges[tid];
		    assert(newmax < INT_MAX);
		    _maxinteredges[tid] = (int)newmax;
		    PRINT("=> REALLOC INTEREDGES WITH MAX = %d\n", _maxinteredges[tid]);
		    _interedges[tid] = realloc(_interedges[tid], _maxinteredges[tid]*2*sizeof(int));
		    _volumes[tid] = realloc(_volumes[tid], _maxinteredges[tid]*sizeof(double));		    
		    assert(_interedges[tid]);
		    assert(_volumes[tid]);		    
		  }		  
		  assert(_nbinteredges[tid] < _maxinteredges[tid]); // check
		} /* fi */
		
	      } /* for b */
	    } /* for a */	    
	  } /* for dz */	
	} /* for dy */
      } /* for dx */ 
      
#ifdef DEBUG
#pragma omp critical
      PRINT("[thread %d] box %d process, %d interedges found\n", tid, k, _nbinteredges[tid]);
#endif
      
    } /* omp for k */    
  } /* omp parallel */
  
  /* free unused memory */
  for(int k = 0 ; k < nbboxes ; k++) if(nbcellsinboxesA[k] > 0) free(boxesA[k]);
  for(int k = 0 ; k < nbboxes ; k++) if(nbcellsinboxesB[k] > 0) free(boxesB[k]);
  free (nbcellsinboxesA); free (nbcellsinboxesB);
  free (boxesA); free (boxesB);  
  free(boxmappingA); free(boxmappingB);
  free(centersA); free(centersB);
  
  /* merge thread arrays as output */
  double max = 0.0;
  int offset[nthreads];
  *nbinteredges = 0;
  for(int t = 0 ; t < nthreads ; t++) {
    offset[t] = *nbinteredges;
    PRINT("[thread %d] %d interedges found\n", t, _nbinteredges[t]);
    max += _nbinteredges[t];
    assert(max < INT_MAX);
    *nbinteredges += _nbinteredges[t];    
  }
  *interedges = malloc(2 * *nbinteredges * sizeof(int));
  assert(*interedges);
  assert(*nbinteredges > 0);

  if(volumes) { *volumes = malloc(*nbinteredges * sizeof(double));  assert(*volumes); }

  for(int t = 0 ; t < nthreads ; t++) {
    memcpy(*interedges + 2*offset[t], _interedges[t], _nbinteredges[t]*2*sizeof(int));
    free(_interedges[t]);
    if(volumes) memcpy(*volumes + offset[t], _volumes[t], _nbinteredges[t]*sizeof(double));    
    free(_volumes[t]);    
  }
  
}

/* *********************************************************** */

void computeInteregdeWeights(int * nbinteredges, /* [inout] nb inter-edges */
			     int ** interedges,  /* [inout] allocated array of interedges */
			     double ** volumes,  /* [inout] allocated array of interedge volumes */
			     int ** weights,     /* [out] allocated array of interedge weights (or NULL if not used) */
			     int minwgt,         /* [in] min interedge weight */
			     int maxwgt          /* [in] max interedge weight */
			     )			
{
  assert(nbinteredges);
  assert(interedges && *interedges);
  assert(volumes && *volumes);
  assert(weights);
  
  /* compute integer interedge weights from intersection volume */
  
  *weights = malloc(*nbinteredges * sizeof(int));
  assert(*weights);
  
  double minvol = +INFINITY;
  double maxvol = -INFINITY;    
  for(int k = 0 ; k < *nbinteredges ; k++) {
    if((*volumes)[k] > maxvol) maxvol = (*volumes)[k];
    if((*volumes)[k] < minvol) minvol = (*volumes)[k];      
  }
  
  PRINT("=> intersection volume in range [%e,%e]\n", minvol, maxvol);
  
  // linear scaling from [min,max] to [MINWGT,MAXWGT]
  int _minwgt = INT_MAX;
  int _maxwgt = INT_MIN;  
  for(int k = 0 ; k < *nbinteredges ; k++) { 
    double w = ((*volumes)[k] - minvol) / (maxvol - minvol); // in range [0.0, 1.0]
    int _w = minwgt + (int)round((maxwgt-minwgt) * w) ; // in range [minwgt, maxwgt]
    (*weights)[k] = _w;
    if(_w < _minwgt) _minwgt = _w;
    if(_w > _maxwgt) _maxwgt = _w;
    assert(_w >= minwgt && _w <= maxwgt); // check
  }
  PRINT("=> scaling of interedge weights in range [%d,%d]\n", _minwgt, _maxwgt);    
  
  /* remove interedges with null weights! */
  int kk = 0;
  for(int k = 0 ; k < *nbinteredges ; k++) {
    if((*weights)[k] > 0) {
      (*interedges)[2*kk] = (*interedges)[2*k];
      (*interedges)[2*kk+1] = (*interedges)[2*k+1];      
      (*weights)[kk] = (*weights)[k];
      kk++;
    }    
  }
  
  assert(*nbinteredges >= kk);
  if(*nbinteredges != kk) PRINT("=> %d interedges of null weight removed!\n", *nbinteredges - kk);
  *nbinteredges = kk;
  
  /* free mem */
  *interedges = realloc(*interedges, 2 * *nbinteredges * sizeof(int));
  *weights = realloc(*weights, *nbinteredges * sizeof(int));  
  
}

/* *********************************************************** */

void generateCouplingMesh(Mesh * mshA,         /* [in]  */
			  Mesh * mshB,         /* [in]  */
			  int nbinteredges,    /* [in] nb inter-edges */
			  int * interedges,    /* [in] array of inter-edges */
			  double zshift,       /* [in] shift z coordinate of B */
			  Mesh * cpl)          /* [out] */
{
  
  /* initialization of mesh cpl */
  cpl->elm_type = LINE;           /* lines */
  cpl->nb_nodes = mshA->nb_cells + mshB->nb_cells;
  cpl->nb_cells = nbinteredges;
  cpl->cells = malloc(cpl->nb_cells*2*sizeof(int));
  cpl->nodes = malloc(cpl->nb_nodes*3*sizeof(double)); /* xyz */
  cpl->nb_node_vars = 0;
  cpl->nb_cell_vars = 0;
  cpl->node_vars = NULL;
  cpl->cell_vars = NULL;
  
  int offset = mshA->nb_cells;
  
  /* add line cells in mesh */
  for(int i = 0 ;i < nbinteredges ; i++) {
    cpl->cells[2*i] = interedges[2*i]; /* from mesh A */
    cpl->cells[2*i+1] = interedges[2*i+1] + offset; /* from mesh B */
  }
  
  /* add node coordinates in mesh */
  
  /* for all cells of code A */
  for(int i = 0 ; i < mshA->nb_cells ; i++) {
    double G[3];
    cellCenter(mshA,i,G);
    cpl->nodes[3*i+0] = G[0]; /* x */
    cpl->nodes[3*i+1] = G[1]; /* y */
    cpl->nodes[3*i+2] = G[2]; /* z */
  }  
  
  /* for all cells of code B */
  for(int i = 0 ; i < mshB->nb_cells ; i++) {
    double G[3];    
    cellCenter(mshB,i,G);
    cpl->nodes[3*(i+offset)+0] = G[0]; /* x */
    cpl->nodes[3*(i+offset)+1] = G[1]; /* y */
    cpl->nodes[3*(i+offset)+2] = G[2] + zshift; /* z */
  }
  
}

/* *********************************************************** */


void generateGrid2D(int width, 
		    int height, 
		    double z0,
		    double cellsizeX,
		    double cellsizeY,
		    Mesh * grid)       /* [out] */
{
  int elmsize = ELEMENT_SIZE[QUADRANGLE];
  
  /* initialization */
  grid->elm_type = QUADRANGLE;
  grid->nb_nodes = (width+1)*(height+1);
  grid->nb_cells = width*height;
  grid->cells = malloc(grid->nb_cells*elmsize*sizeof(int));
  grid->nodes = malloc(grid->nb_nodes*3*sizeof(double));
  grid->nb_node_vars = 0;
  grid->nb_cell_vars = 0;
  grid->node_vars = NULL;
  grid->cell_vars = NULL;
  
  /*
    The k-th cell, that connects nodes {a,b,c,d}
    
    a---b
    | k |
    d---c
    
  */
  
  int idx = 0;
  
  /* for all cells */
  for(int j = 0 ; j < height ; j++) 
    for(int i = 0 ; i < width ; i++) 
      {	
	/* cell index */
	int k = INDEX2D(i,j,width,height); 
	assert(k == idx);
	/* node indices */
	int a = INDEX2D(i,j,width+1,height+1);
	int b = INDEX2D(i+1,j,width+1,height+1); 
	int c = INDEX2D(i+1,j+1,width+1,height+1); 
	int d = INDEX2D(i,j+1,width+1,height+1); 
	grid->cells[elmsize*k+0] = a;
	grid->cells[elmsize*k+1] = b;
	grid->cells[elmsize*k+2] = c;
	grid->cells[elmsize*k+3] = d;
	idx++;
      }
  
  /* for all nodes of code  */
  for(int j = 0 ; j < height+1 ; j++) 
    for(int i = 0 ; i < width+1 ; i++) 
      {
	int k = j*(width+1) + i;  
	grid->nodes[3*k+0] = i*cellsizeX; /* x */
	grid->nodes[3*k+1] = j*cellsizeY; /* y */
	grid->nodes[3*k+2] = z0;          /* z */
      }    
}


/* *********************************************************** */

void generateGrid3D(int dimX, 
		    int dimY, 
		    int dimZ,
		    double cellsizeX,
		    double cellsizeY,
		    double cellsizeZ,
		    Mesh * grid)
{
  
  int elmsize = ELEMENT_SIZE[HEXAHEDRON];
  
  /* initialization */
  grid->elm_type = HEXAHEDRON;
  grid->nb_nodes = (dimX+1)*(dimY+1)*(dimZ+1);
  grid->nb_cells = dimX*dimY*dimZ;
  grid->cells = malloc(grid->nb_cells*elmsize*sizeof(int));
  grid->nodes = malloc(grid->nb_nodes*3*sizeof(double));
  grid->nb_node_vars = 0;
  grid->nb_cell_vars = 0;
  grid->node_vars = NULL;
  grid->cell_vars = NULL;
  
  /*
   * The k-th cell, that connects nodes {a,b,c,d,e,f,g,h}   
   *
   *    e---f
   *  a---b
   *  | h | g
   *  d---c
   *
   *  lower corner : a
   *  upper corner : g
   *
   *    -y
   *    ^ 
   *    |
   *    ---> +x
   *   /
   *  -z
   */
  
  int idx = 0;
  
  /* for all cells (x,y,z) */
  for(int z = 0 ; z < dimZ ; z++) 
    for(int y = 0 ; y < dimY ; y++) 
      for(int x = 0 ; x < dimX ; x++) 
	{	
	  /* cell index */
	  int k = INDEX3D(x,y,z,dimX,dimY,dimZ); 
	  assert(k == idx);
	  /* node indices */
	  int a = INDEX3D(x,y,z,dimX+1,dimY+1,dimZ+1);
	  int b = INDEX3D(x+1,y,z,dimX+1,dimY+1,dimZ+1); 
	  int c = INDEX3D(x+1,y+1,z,dimX+1,dimY+1,dimZ+1); 
	  int d = INDEX3D(x,y+1,z,dimX+1,dimY+1,dimZ+1); 
	  int e = INDEX3D(x,y,z+1,dimX+1,dimY+1,dimZ+1);
	  int f = INDEX3D(x+1,y,z+1,dimX+1,dimY+1,dimZ+1); 
	  int g = INDEX3D(x+1,y+1,z+1,dimX+1,dimY+1,dimZ+1); 
	  int h = INDEX3D(x,y+1,z+1,dimX+1,dimY+1,dimZ+1); 
	  grid->cells[elmsize*k+0] = a;
	  grid->cells[elmsize*k+1] = b;
	  grid->cells[elmsize*k+2] = c;
	  grid->cells[elmsize*k+3] = d;
	  grid->cells[elmsize*k+4] = e;
	  grid->cells[elmsize*k+5] = f;
	  grid->cells[elmsize*k+6] = g;
	  grid->cells[elmsize*k+7] = h;
	  idx++;
	}
  
  /* for all nodes of code  */
  for(int z = 0 ; z < dimZ+1 ; z++) 
    for(int y = 0 ; y < dimY+1 ; y++) 
      for(int x = 0 ; x < dimX+1 ; x++) {
	int k = INDEX3D(x,y,z,dimX+1,dimY+1,dimZ+1); 
	grid->nodes[3*k+0] = x*cellsizeX; /* x */
	grid->nodes[3*k+1] = y*cellsizeY; /* y */
	grid->nodes[3*k+2] = z*cellsizeZ; /* z */
      }    
  
}

/* *********************************************************** */

void subdivideMeshIntoTetrahedra(Mesh * msh,         /* [in] input mesh */
				 Mesh * tetramsh)    /* [out] output mesh subdivided into tetrahedra */
{  
  assert(msh && tetramsh);
  assert(msh->elm_type == HEXAHEDRON || msh->elm_type == PRISM || msh->elm_type == TETRAHEDRON);
  
  // trivial case
  if(msh->elm_type == TETRAHEDRON) {
    dupMesh(msh, tetramsh);
    return;
  }
  
  /* allocate tetramsh */
  tetramsh->elm_type = TETRAHEDRON;
  tetramsh->nb_cells = 0;
  if(msh->elm_type == HEXAHEDRON) tetramsh->nb_cells = 6*msh->nb_cells;
  if(msh->elm_type == PRISM) tetramsh->nb_cells = 3*msh->nb_cells;
  tetramsh->cells = malloc(tetramsh->nb_cells*ELEMENT_SIZE[TETRAHEDRON]*sizeof(int));           
  tetramsh->nb_nodes = msh->nb_nodes; // we keep exactly the same nodes !
  tetramsh->nodes = malloc(tetramsh->nb_nodes*3*sizeof(double));       
  memcpy(tetramsh->nodes, msh->nodes, tetramsh->nb_nodes*3*sizeof(double));
  tetramsh->nb_node_vars = 0;
  tetramsh->nb_cell_vars = 0;
  tetramsh->node_vars = NULL;
  tetramsh->cell_vars = NULL;
  
  // compute cell connectivity
  if(msh->elm_type == HEXAHEDRON) {
    for(int k = 0 ; k < msh->nb_cells ; k++) {
      int tetras[6][4];
      hexahedron2tetrahedron2(msh, k, tetras);      
      for(int i = 0 ; i < 6 ; i++) // 6 tetras
	for(int j = 0 ; j < 4 ; j++)
	  tetramsh->cells[(k*6+i)*4+j] = tetras[i][j];
    }
  }
  
  if(msh->elm_type == PRISM) {
    for(int k = 0 ; k < msh->nb_cells ; k++) {
      int tetras[3][4];
      prism2tetrahedron2(msh, k, tetras);      
      for(int i = 0 ; i < 3 ; i++) // 3 tetras
	for(int j = 0 ; j < 4 ; j++)
	  tetramsh->cells[(k*3+i)*4+j] = tetras[i][j];
    }
    
  }
  
}

/* *********************************************************** */

void quad2tri(Mesh * quad, Mesh * tri)
{
  assert(quad && tri);
  
  dupMesh(quad, tri);
  tri->elm_type = TRIANGLE;
  tri->nb_cells *= 2;
  tri->cells = realloc(tri->cells, tri->nb_cells*3*sizeof(int));
  assert(tri->cells);
  
  /*
    a -- b
    |    | --> 2 triangles (a,b,c) and (a,c,d)
    d -- c
    
  */
  
  int k = 0;
  for(int i = 0 ; i < quad->nb_cells ; i++) {
    int a = quad->cells[i*4];
    int b = quad->cells[i*4+1];
    int c = quad->cells[i*4+2];
    int d = quad->cells[i*4+3];
    tri->cells[k*3] = a;
    tri->cells[k*3+1] = b;
    tri->cells[k*3+2] = c;
    k++;
    tri->cells[k*3] = a;
    tri->cells[k*3+1] = c;
    tri->cells[k*3+2] = d;
    k++;
  }
  assert(k == tri->nb_cells);
  
}

/* *********************************************************** */
