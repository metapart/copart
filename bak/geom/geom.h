/**
 *   @file geom.h
 *
 *   @author     Aurelien Esnard
 *
 *   @defgroup Geom Geometry routines.
 *   @brief Geometry routines for mesh.
 *
 */

#ifndef __GEOM_H__
#define __GEOM_H__

#include "struct.h"

/* *********************************************************** */

//@{

void generateGrid2D(int width, 
		    int height, 
		    double z0,
		    double cellsizeX,
		    double cellsizeY,
		    Mesh * grid);       /* [out] */

void generateGrid3D(int dimX, 
		    int dimY, 
		    int dimZ,
		    double cellsizeX,
		    double cellsizeY,
		    double cellsizeZ,
		    Mesh * grid);       /* [out] */

/** Convert a quad-mesh into a triangle-mesh */
void quad2tri(Mesh * quad, Mesh * tri);

void cellCenter(Mesh * msh,        /* [in] input mesh */
		int i,             /* [in] cell index */
		double G[3]);      /* [out] gravity center */

void cellCenters(Mesh * msh,        /* [in] input mesh */
		 double * centers); /* [out] gravity centers (array of size msh->nb_cells*3) */

void boundingBox(Mesh * msh,        /* [in] input mesh */
		 double bb[3][2]);  /* [out] 3D bounding box */

void boundingBoxMesh(Mesh * msh,        /* [in] input mesh */
		     Mesh * bbmsh);     /* [out] hexahedron output mesh with 3D bounding box for each cell */

void cellLength(Mesh * msh,      /* [in] input mesh */
		int i,          /* [in] cell index */
		double length[3]);

void cellsLength(Mesh * msh,    /* [in] input mesh */
		 double length[3]);

int intersectCells(Mesh * mshA,    /* [in] input mesh A */
		   Mesh * mshB,    /* [in] input mesh B */
		   int a,          /* [in] cell index in mesh A */
		   int b,          /* [in] cell index in mesh B */
		   double * vol);  /* [out] intersection volume */     		   

void intersectMesh(Mesh * mshA,        /* [in] input mesh A */
		   Mesh * mshB,        /* [in] input mesh B */
		   int * nbinteredges, /* [out] nb inter-edges */
		   int ** interedges,  /* [out] allocated array of interedges */
		   double ** volumes  /* [out] allocated array of interedge volumes (or NULL if not used) */
		   );

void computeInteregdeWeights(int * nbinteredges, /* [inout] nb inter-edges */
			     int ** interedges,  /* [inout] allocated array of interedges */
			     double ** volumes,  /* [inout] allocated array of interedge volumes */
			     int ** weights,     /* [out] allocated array of interedge weights (or NULL if not used) */
			     int minwgt,         /* [in] min interedge weight */
			     int maxwgt          /* [in] max interedge weight */
			     );


void generateCouplingMesh(Mesh * mshA,         /* [in] input mesh A */
			  Mesh * mshB,         /* [in] input mesh B */
			  int nbinteredges,    /* [in] nb inter-edges */
			  int * interedges,    /* [in] array of inter-edges */
			  double zshift,       /* [in] shift z coordinate of B */
			  Mesh * cpl);         /* [out] coupling mesh (line elements) */

void subdivideMeshIntoTetrahedra(Mesh * msh,         /* [in] input mesh */
				 Mesh * tetramsh);   /* [out] output mesh subdivided into tetrahedra */

void polygonCenter(int n,             /* [in] nb of vertices of polygon */
		   double * coords[3],  /* [in] array of size n of pointer on 3D coords */
		   double G[3]);      /* [out] gravity center */

int intersectTriangle(double * T1[3],   /* [in] vertex coords of triangle T1 */
		      double * T2[3]);  /* [in] vertex coords of triangle T2 */

int intersectTetrahedron(double * tetraA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			 double * tetraB[4],  /* [in] pointers on 3D coord of tetrahedron B */
			 double * vol);         /* [out] intersection volume */

int intersectScaledTetrahedron(double * coordsA[4],  /* [in] pointers on 3D coord of tetrahedron A */
			       double * coordsB[4],  /* [in] pointers on 3D coord of tetrahedron B */
			       double alpha,         /* [in] scale up a tetra by factor alpha */
			       double * w);         /* [out] intersection volume */

int intersectPolyhedron(int cellsizeA,
			double * coordsA[cellsizeA], 
			int cellsizeB,
			double * coordsB[cellsizeB],
			double * vol);           /* [out] intersection volume */			

//@}

/* *********************************************************** */

#endif
