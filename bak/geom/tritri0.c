/* Triangle/triangle intersection test routine */

#include <math.h>
#include <assert.h>

#include "debug.h"
#include "tritri.h"
#include "stdio.h"

#define EPSILON 1E-8

/* *********************************************************** */

#define CROSS(v1,v2,dest){			\
    dest[0]=v1[1]*v2[2]-v1[2]*v2[1];		\
    dest[1]=v1[2]*v2[0]-v1[0]*v2[2];		\
    dest[2]=v1[0]*v2[1]-v1[1]*v2[0];}

/* *********************************************************** */

#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])

/* *********************************************************** */

#define VECTOR(A,B,AB){			\
    AB[0]=B[0]-A[0];			\
    AB[1]=B[1]-A[1];			\
    AB[2]=B[2]-A[2];}

/* *********************************************************** */

#define VECNORM(V,VN){							\
    double N = sqrt(V[0]*V[0] + V[1]*V[1] + V[2]*V[2]);			\
    VN[0] = V[0] / N; VN[1] = V[1] / N; VN[2] = V[2] / N; }		

/* *********************************************************** */

void computeInterval(double * p, /* [in] projection on L */
		     double * d, /* [in] distance from other plane */
		     double * t) /* [out] 2D interval */
{   
  double d0d1 = d[0]*d[1];
  double d0d2 = d[0]*d[2];
  double d1d2 = d[1]*d[2];
  
  if(fabs(d0d1) < EPSILON) d0d1 = 0.0;
  if(fabs(d0d2) < EPSILON) d0d2 = 0.0;
  if(fabs(d1d2) < EPSILON) d1d2 = 0.0;

  double A, B, C, D, E;
  
  if(d0d1 > 0.0) 
    { 
      /* here we know that d0d2 <= 0.0 */ 
      /* that is 0 and 1 are on the same side, 2 on the other or on the plane */ 
      A=p[2]; B=(p[0]-p[2])*d[2]; C=(p[1]-p[2])*d[2]; D=d[2]-d[0]; E=d[2]-d[1]; 
    } 
  else if(d0d2 > 0.0)
    { 
      /* here we know that d0d1 <= 0.0 */ 
      /* that is 0 and 2 are on the same side, 1 on the other or on the plane */ 
      A=p[1]; B=(p[0]-p[1])*d[1]; C=(p[2]-p[1])*d[1]; D=d[1]-d[0]; E=d[1]-d[2]; 
    } 
  else if(d1d2 > 0.0 || d[0] != 0.0) 
    { 
      /* here we know that d0d1 <= 0.0 or that d[0] != 0.0 */ 
      /* that is 1, 2 are on the same side, d0 on the other or on the plane */
      /* OR that is 1, 2 are on the plane, but not 0 */ 
      A=p[0]; B=(p[1]-p[0])*d[0]; C=(p[2]-p[0])*d[0]; D=d[0]-d[1]; E=d[0]-d[2]; 
    } 
  else if(d[1] != 0.0) 
    { 
      /* 0, 2 are on the plane, but not 1 */ 
      A=p[1]; B=(p[0]-p[1])*d[1]; C=(p[2]-p[1])*d[1]; D=d[1]-d[0]; E=d[1]-d[2]; 
    } 
  else if(d[2] != 0.0) 
    { 
      /* 0, 1 are on the plane, but not 2 */ 
      A=p[2]; B=(p[0]-p[2])*d[2]; C=(p[1]-p[2])*d[2]; D=d[2]-d[0]; E=d[2]-d[1]; 
    } 
  else 
    { 
      /* triangles are coplanar */ 
      assert(0);
    } 
  
  /* If points 0 and 1 are not on the same side, the segment [0,1]
     intersects L with parameter t.  And so, Thales says: 
     d0 / d1 = (t - p0) / (t - p1)
  */
  
  t[0] = A+B/D;
  t[1] = A+C/E;
}

/* *********************************************************** */

int intersectTriangleOrel(double * T1[3],  /* [in] vertex 3D coords of triangle T1 */
			  double * T2[3])  /* [in] vertex 3D coords of triangle T2 */
{  
  /* 
   * From Triangle/triangle intersection test routine, by Tomas Moller, 1997.
   * "A Fast Triangle-Triangle Intersection Test", Journal of Graphics Tools, 2(2), 1997
   */
  
  /* Plane Equation (Hessian Normal Form): N.X = -r, where r is the
     distance of the plane from the origin and N the unit normal
     vector. */  
  /*  - plane1: N1.X = -r1  */
  /*  - plane2: N2.X = -r2  */  
  
  /* T1 = {A1, B1, C1}, T2 = {A2, B2, C2} */
  /* U1 = A1B1, V1=A1C1, U2 = A2B2, V2=A2C2 */
  
  double U1[3], V1[3], U2[3], V2[3];
  VECTOR(T1[0],T1[1],U1);
  VECTOR(T1[0],T1[2],V1);
  VECTOR(T2[0],T2[1],U2);
  VECTOR(T2[0],T2[2],V2);
  
  /* N1 = U1^V1 (normalized) */
  /* N2 = U2^V2 (normalized) */
  double N1[3], N2[3];
  CROSS(U1,V1,N1); VECNORM(N1,N1);
  CROSS(U2,V2,N2); VECNORM(N1,N1);
  
  /* r1 = -(N1.A1), r2 = -(N2.A2) */
  double r1 = -DOT(N1,T1[0]);
  double r2 = -DOT(N2,T2[0]);
  
  PRINTADV(" - N1 = (%.2f,%.2f,%.2f) and r1 = %.2f\n", N1[0], N1[1], N1[2], r1);
  PRINTADV(" - N2 = (%.2f,%.2f,%.2f) and r2 = %.2f\n", N2[0], N2[1], N2[2], r2);
  
  /* compute distance from A1, B1, C1 to plane2 */
  double d1[3];
  d1[0] = DOT(T1[0],N2) + r2; /* for A1 */
  d1[1] = DOT(T1[1],N2) + r2; /* for B1 */
  d1[2] = DOT(T1[2],N2) + r2; /* for C1 */
  
  /* compute distance from A2, B2, C2 to plane1 */
  double d2[3];
  d2[0] = DOT(T2[0],N1) + r1; /* for A2 */
  d2[1] = DOT(T2[1],N1) + r1; /* for B2 */
  d2[2] = DOT(T2[2],N1) + r1; /* for C2 */
  
  PRINTADV(" - distance of T1 from Plane2 = (%.5f,%.5f,%.5f)\n", d1[0], d1[1], d1[2]);
  PRINTADV(" - distance of T2 from Plane1 = (%.5f,%.5f,%.5f)\n", d2[0], d2[1], d2[2]);
  
  /* robustness epsilon check */
  if(fabs(d1[0]) < EPSILON) d1[0]=0.0;
  if(fabs(d1[1]) < EPSILON) d1[1]=0.0;
  if(fabs(d1[2]) < EPSILON) d1[2]=0.0;
  if(fabs(d2[0]) < EPSILON) d2[0]=0.0;
  if(fabs(d2[1]) < EPSILON) d2[1]=0.0;
  if(fabs(d2[2]) < EPSILON) d2[2]=0.0;
  
  /* 1) no intersection occurs if same sign on all and not equal 0 */
  
  double d10d11 = d1[0]*d1[1];
  double d10d12 = d1[0]*d1[2];

  if(fabs(d10d11) < EPSILON) d10d11 = 0.0;
  if(fabs(d10d12) < EPSILON) d10d12 = 0.0;

  if(d10d11 > 0.0 && d10d12 > 0.0) {
    PRINT(" => no intersection occurs (T1 doesnt intersect L)\n");
    return 0;
  }
  
  double d20d21 = d2[0]*d2[1];
  double d20d22 = d2[0]*d2[2];  
  
  if(fabs(d20d21) < EPSILON) d20d21 = 0.0;
  if(fabs(d20d22) < EPSILON) d20d22 = 0.0;

  if(d20d21 > 0.0 && d20d22 > 0.0) {
    PRINT(" => no intersection occurs (T2 doesnt intersect L)\n");
    return 0;  
  }  
  
  if( (fabs(d1[0]) <= EPSILON && fabs(d1[1]) <= EPSILON) ||
      (fabs(d1[1]) <= EPSILON && fabs(d1[2]) <= EPSILON) ||
      (fabs(d1[2]) <= EPSILON && fabs(d1[0]) <= EPSILON) ) {
    printf(" => no intersection occurs (T1 intersects L on a point)\n");
    return 0;  
  }
  
  if( (fabs(d2[0]) <= EPSILON && fabs(d2[1]) <= EPSILON) ||
      (fabs(d2[1]) <= EPSILON && fabs(d2[2]) <= EPSILON) ||
      (fabs(d2[2]) <= EPSILON && fabs(d2[0]) <= EPSILON) ) {
    printf(" => no intersection occurs (T2 intersects L on a point)\n");
    return 0;  
  }
  
  /* 2) T1 and T2 are coplanar */
  // if(d1[0] == 0.0 && d1[1] == 0.0 && d1[2] == 0.0) {
  if( (fabs(d1[0]) < EPSILON && fabs(d1[1]) < EPSILON && fabs(d1[2]) < EPSILON) ||
      (fabs(d2[0]) < EPSILON && fabs(d2[1]) < EPSILON && fabs(d2[2]) < EPSILON))
    {
      
      /* we dont consider coplanar intersection... */
      // PRINT(" => T1 and T2 are coplanar, and so dont intersect.\n");
      printf(" => T1 and T2 are coplanar...\n");
      return 0; 

#ifdef COPLANAR_INTERSECTION

      double * projcoordsT1[3];
      double * projcoordsT2[3];
      
      /* first project onto an axis-aligned plane, that maximizes the area */
      /* of the triangles, compute indices: i0,i1. */
      
      double N[3];
      N[0]=fabs(N1[0]);
      N[1]=fabs(N1[1]);
      N[2]=fabs(N1[2]);
      
      short i0 = -1, i1 = -1;
      if(N[0] >= N[1] && N[0] >= N[2]) i0=1, i1=2;  /* N[0] is greatest */
      else if(N[1] >= N[0] && N[1] >= N[2]) i0=2, i1=0;  /* N[1] is greatest */
      else if(N[2] >= N[0] && N[2] >= N[1]) i0=0, i1=1;  /* N[2] is greatest */
      assert(i0 != -1 && i1 != -1);
      
      double T1p[3][2];
      double T2p[3][2];
      
      for(int i = 0 ; i < 3 ; i++) {
	T1p[i][0] = T1[i][i0];  /* on X */
	T1p[i][1] = T1[i][i1];  /* on Y */
	projcoordsT1[i] = T1p[i];
	T2p[i][0] = T2[i][i0];  /* on X */
	T2p[i][1] = T2[i][i1];  /* on Y */
	projcoordsT2[i] = T2p[i];
      }
      
      if(intersectPolygon2D(3,3, projcoordsT1, projcoordsT2)) {
	// PRINT(" => T1 and T2 are coplanar and intersect!\n");
	printf(" => T1 and T2 are coplanar and intersect!\n");
	return 1;
      }
      
      // PRINT(" => T1 and T2 are coplanar, but dont intersect.\n");
      return 0;

#endif

    }
  
  /* 
     3) Else, the intersection of plane1 and plane2 is a line (L): X =
     O + t.D with direction D = N1^N2. So T1 and T2 intersect this
     line.  In fact, the point O does not need to be computed (see
     Moller).
  */
  
  double D[3];
  CROSS(N1,N2,D);
  PRINTADV(" - D = (%.5f,%.5f,%.5f)\n", D[0], D[1], D[2]);  
  
  /* projection onto line L of A1, B1, C1 */
  double p1[3];
  p1[0] = DOT(D,T1[0]);
  p1[1] = DOT(D,T1[1]);
  p1[2] = DOT(D,T1[2]);
  
  /* projection onto line L of A2, B2, C2 */
  double p2[3];
  p2[0] = DOT(D,T2[0]);
  p2[1] = DOT(D,T2[1]);
  p2[2] = DOT(D,T2[2]);  
  
  /* compute interval related to T1 on L */
  double t1[2];
  computeInterval(p1, d1, t1);
  
  /* compute interval related to T2 on L */
  double t2[2];
  computeInterval(p2, d2, t2);
  
  /* sort intervals */
  double tmp;
  if(t1[0] > t1[1]) { tmp = t1[0]; t1[0] = t1[1]; t1[1] = tmp; }
  if(t2[0] > t2[1]) { tmp = t2[0]; t2[0] = t2[1]; t2[1] = tmp; }
  
  PRINTADV(" - T1 intersects L on interval [%g,%g]\n",t1[0],t1[1]);
  PRINTADV(" - T2 intersects L on interval [%g,%g]\n",t2[0],t2[1]);  
  
  /* intersection of T1 and L on a single point */
  if(fabs(t1[0]-t1[1]) <= EPSILON) {
    printf(" => no intersection occurs (T1 intersect L on a point)\n");
    return 0;
  }
  
  /* intersection of T2 and L on a single point */
  if(fabs(t2[0] - t2[1]) <= EPSILON) {
    printf(" => no intersection occurs (T2 intersect L on a point)\n");
    return 0;
  }
  
  /* test if intervals overlap or not */
  if( ((t1[0] <= t2[0]) && (t1[1]-t2[0]) <= EPSILON) ||
      ((t1[0] > t2[0]) && (t2[1]-t1[0]) <= EPSILON) ) {
    PRINT( " => no intersection occurs (intervals dont overlap)\n");
    return 0;
  }
  
  PRINT( " => intersection occurs (intervals overlap) !!!\n");
  return 1;
}

/* *********************************************************** */
