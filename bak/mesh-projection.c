/* Mesh projection. */
/* Author(s): aurelien.esnard@labri.fr */

/* Examples: 
 ./mesh-projection.exe -a 0 -v ../data/unit-square-quad-10x10.txt ../data/unit-square-quad-100x100.txt  4
 ./mesh-projection.exe ../data/unit-cube-hexa-10x10x10.txt ../data/unit-cube-tetra-5638.txt 10
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>

#include "metapart.h"

/* *********************************************************** */

#define UBFACTOR 5      // imbalance factor

/* *********************************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0],  
	 "[-d] [-p] [-v] [-b] [-l interedgefile] [-z shiftz] [-Z vtkshiftz] [-a seed] [-m method] meshA.txt meshB.txt nparts");
  printf("\t\t -a seed: set the random generator seed\n");
  printf("\t\t -m method: partitioning method (require fixed vertices)\n");
  printf("\t\t -v: produce VTK output files\n");
  printf("\t\t -w/-W: use interedge weights (default no weight used)\n");  
  printf("\t\t -d: debug mode\n");
  printf("\t\t -s interedgefile: save interedge file\n");
  printf("\t\t -l interedgefile: load interedge file (instead of computing it)\n");
  printf("\t\t -z shiftz: shift meshB on z axis (after loading file)\n");
  printf("\t\t -Z vtkshiftz: shift meshB on z axis (for VTK output)\n");
  
  exit (EXIT_FAILURE);  
}

/* *********************************************************** */


int main(int argc, char * argv[]) 
{   
  
  /* 0) input args */
  
  int opt;
  int usew = 0;  
  int vtkopt = 0;
  int debugopt = 0;
  int saveopt = 0;
  int loadopt = 0;
  double shiftz = 0.0;  
  double vtkshiftz = 0.0;    
  char interedgestr[256];
  int randseed = time (NULL);
  char * env = NULL;
  
  while ((opt = getopt (argc, argv, "vds:l:a:wWz:Z:m:")) != -1) {
    switch (opt) {
      
    case 'a':
      randseed = atoi (optarg);     
      break; 
      
    case 'w':
      usew = 1;
      break; 
      
    case 'W':
      usew = 2;
      break; 
      
    case 'd':
      debugopt = 1;
      break;
      
    case 'v':
      vtkopt = 1;
      break;
      
    case 's':
      saveopt = 1;
      strncpy(interedgestr,optarg,256);
      break;
      
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
      
    case 'Z':
      vtkshiftz = strtod(optarg, NULL);     
      break;  
      
    case 'l':
      loadopt = 1;
      strncpy(interedgestr,optarg,256);
      break;

    case 'm':
      env = optarg;
      break;
      
    default:
      usage(argc,argv);
    }
  }  

  // check
  assert(saveopt + loadopt < 2);
  if(loadopt) assert(!usew);
    
  char* meshfileA;
  char* meshfileB;
  int nparts; 
  
  if(argc - optind != 3) usage(argc,argv);
  
  meshfileA = argv[optind];
  meshfileB = argv[optind+1];
  nparts = atoi(argv[optind+2]); 
  
  /* initialize random seed */
  srand(randseed);
  printf("Random Seed = %d\n", randseed);
  
  printf("nparts = %d\n", nparts);
  assert(nparts > 0); 
  
  /* 1) load mesh A & B */ 
  
  printf("\n*************** LOADING MESHES ***************\n\n");
  
  Mesh meshA, meshB;
  
  printf("Loading mesh A: %s\n", meshfileA);  
  loadMesh(meshfileA, &meshA); 
  
  printf("Loading mesh B: %s\n", meshfileB);  
  loadMesh(meshfileB, &meshB);

  if(shiftz != 0.0) {
    printf("Shifting mesh B on Z: %.2f\n", shiftz);  
    shiftMesh(&meshB, 0.0, 0.0, shiftz);
  }
    
  if(debugopt) {
    printf("********************* MESH A *********************\n");
    printMesh(&meshA);
    printf("\n********************* MESH B *********************\n");
    printMesh(&meshB);
  }
  
  printf("Mesh A: nb cells = %d, nb nodes = %d\n", meshA.nb_cells, meshA.nb_nodes);  
  printf("Mesh B: nb cells = %d, nb nodes = %d\n", meshB.nb_cells, meshB.nb_nodes);  
  
  /* 2) generate graphs from meshes */
  
  Graph gA, gB;
  mesh2Graph(&meshA, &gA);
  mesh2Graph(&meshB, &gB);
  
  if(debugopt) {
    printf("\n********************* GRAPH A *********************\n");
    printGraph(&gA);
    printf("\n********************* GRAPH B *********************\n");
    printGraph(&gB);
  }
  
  printf("Graph A: nb vertices = %d, nb edges = %d\n", gA.nvtxs, gA.nedges);  
  printf("Graph B: nb vertices = %d, nb edges = %d\n", gB.nvtxs, gB.nedges);  
  
  /* 3) compute/load/save inter-edges */ 
  
  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;
  double * volumes = NULL;       
  
  if(loadopt) {
    printf("\n*************** LOAD INTER-EDGES ***************\n\n");
    
    printf("Loading interedges: %s\n", interedgestr);
    FILE* finteredges = fopen(interedgestr, "r");
    assert(finteredges);
    int a, b, w;
    
    while(fscanf(finteredges,"%d %d %d\n", &a, &b, &w) > 0) nbinteredges++;
    interedges = malloc(2*nbinteredges*sizeof(int));
    weights = malloc(nbinteredges*sizeof(int));    
    rewind(finteredges);
    for(int k = 0 ; k < nbinteredges ; k++) {
      int r = fscanf(finteredges,"%d %d %d\n", interedges + 2*k, interedges + 2*k+1, weights + k);
      assert(r == 3);
    }
    fclose(finteredges);
    
  }
  else {
    
    printf("\n*************** COMPUTE INTER-EDGES ***************\n\n");

    if(usew) {
      intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, &volumes); 
      if(usew == 1) computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, 1, 10); // keep all interedges
      if(usew == 2) computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, 0, 10); // remove minor interedges (almost null)      
    }
    else
      intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, NULL); // default
    
    // save interedges
    if(saveopt) {
      FILE* finteredges = fopen(interedgestr, "w");
      for(int k = 0 ; k < nbinteredges ; k++)
	fprintf(finteredges,"%d %d %d\n",interedges[2*k], interedges[2*k+1], weights?weights[k]:1);
      fclose(finteredges);
      printf("Saving interedges: %s\n", interedgestr);
    }
    
  }
  
  printf("nb interedges found = %d\n", nbinteredges);  
  
  if(debugopt) {
    for(int i = 0 ; i < nbinteredges ; i++)
      printf("interedge %d: A%d <---> B%d (%d)\n", i, interedges[2*i], interedges[2*i+1], weights[i]);
  }
  
  assert(nbinteredges > 0);
  
  /* 4) partition of A in nparts */  
  
  printf("\n*************** PARTITION of A in %d ***************\n\n", nparts);
  
  int * partA = malloc(gA.nvtxs*sizeof(int));
  assert(partA);
  for(int i = 0 ; i < gA.nvtxs ; i++) partA[i] = -1;
  int edgecutA = 0;
  partitionGraph(&gA, nparts, UBFACTOR, 0, partA, &edgecutA, NULL, env);           
  // diagnostic
  printGraphDiagnostic(&gA, nparts, partA); 
  
  /* 5) projection from A to B */
  
  printf("\n*************** PROJECTION A->B ***************\n\n");
  
  int* partB = malloc(gB.nvtxs*sizeof(int));  
  assert(partB);
  for(int i = 0 ; i < gB.nvtxs ; i++) partB[i] = -1;
  int edgecutB = 0;
  
  projection(&gA, &gB, nparts, UBFACTOR, nbinteredges, interedges, weights, partA, partB, &edgecutB, env);      
  
  // diagnostic
  printGraphDiagnostic(&gB, nparts, partB); 
  // printf("  - exectime = %.2f ms\n", info.exectime);      
  
  printf("\n*************** COPART DIAGNOSTIC ***************\n\n");
  
  printCopartDiagnostic(&gA, &gB, nparts, nparts, nparts, nparts, partA, partB, nbinteredges, interedges, weights);
  
  /* 6) saving partition */
  
  if(debugopt) {
    
    printf("\n*************** SAVE PARTITION ***************\n\n");
    
    /* mesh A */
    char partfileA[255];
    sprintf(partfileA,"%s.part%d", meshfileA, nparts);
    printf("Saving mesh partition file: %s\n", partfileA);
    savePartition(meshA.nb_cells, partA, partfileA);
    
    /* mesh B */
    char partfileB[255];
    sprintf(partfileB,"%s.part%d", meshfileB, nparts);
    printf("Saving mesh partition file: %s\n", partfileB);
    savePartition(meshB.nb_cells, partB, partfileB);
  }
  
  /* 7) VTK output*/
  
  if(vtkopt) {
    
    printf("\n*************** VTK OUTPUT FILES ***************\n\n");
    
    addMeshVariable(&meshA, "partA", CELL_INTEGER, 1, partA);
    addMeshVariable(&meshB, "partB", CELL_INTEGER, 1, partB);

    if(vtkshiftz != 0.0) {
      printf("Shifting mesh B on Z for VTK output: %.2f\n", vtkshiftz);  
      shiftMesh(&meshB, 0.0, 0.0, vtkshiftz);
    }
    
    // interedges
    char * outfileCPL = "cpl.vtk";
    Mesh cpl;
    generateCouplingMesh(&meshA, &meshB, nbinteredges, interedges, 0.0, &cpl);
    printf("Saving VTK mesh: %s\n", outfileCPL);
    saveVTKMesh(outfileCPL, &cpl);
    freeMesh(&cpl);
    
    // mesh
    char * outfileA = "meshA.vtk";
    printf("Saving VTK mesh: %s\n", outfileA);
    saveVTKMesh(outfileA, &meshA);
    
    char * outfileB = "meshB.vtk";
    printf("Saving VTK mesh: %s\n", outfileB);
    saveVTKMesh(outfileB, &meshB);


    /* if(vtkopt > 1) { */
    /*   // graph */
    /*   char * outfileGA = "graphA.vtk"; */
    /*   printf("Saving VTK mesh: %s\n", outfileGA); */
    /*   double * centersA = malloc(meshA.nb_cells*3*sizeof(double)); */
    /*   cellCenters(&meshA, centersA); */
    /*   Mesh GA;  */
    /*   graph2LineMesh(&gA, centersA, &GA); */
    /*   saveVTKMesh(outfileGA, &GA); */
      
    /*   char * outfileGB = "graphB.vtk"; */
    /*   printf("Saving VTK mesh: %s\n", outfileGB); */
    /*   double * centersB = malloc(meshB.nb_cells*3*sizeof(double)); */
    /*   cellCenters(&meshB, centersB); */
    /*   Mesh GB;  */
    /*   graph2LineMesh(&gB, centersB, &GB); */
    /*   saveVTKMesh(outfileGB, &GB); */
      
    /*   free(centersA); */
    /*   free(centersB);           */
    /* } */

    
  }
  
  
  /* 8) free */
  freeGraph(&gA);
  freeGraph(&gB);
  freeMesh(&meshA);
  freeMesh(&meshB);
  free(partA);
  free(partB);
  free(interedges);
  if(weights) free(weights);
  if(volumes) free(volumes);    
  
  return EXIT_SUCCESS;
  
}
