/* Co-Partitioning of 2 Coupled Meshes. */
/* Author(s): aurelien.esnard@labri.fr */


/* Examples: ./hybridmesh-copart.exe -s PROJREPART -n USER -v -p ../data/cerfacs/cylinder/cylinder.avbp.part2.txt ../data/cerfacs/cylinder/cylinder.avbp.part1.txt ../data/cerfacs/cylinder/cylinder.avtp.txt 30 9 9 9 */
/*  ./hybridmesh-copart -s PROJREPART -n USER -v -p ../data/cylinder.avbp.part2.txt ../data/cylinder.avbp.part1.txt ../data/cylinder.avtp.txt 12 12 7 5
 * ./hybridmesh-copart -s PROJREPART -n USER -v -p /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/AVBPperso.outfile.txt.part1 /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/AVBPperso.outfile.txt.part2 /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/AVTPperso.outfile.txt 12 12 7 5
 * ./hybridmesh-copart -s PROJREPART -n USER -v -p /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/T72.fluid.outfile.txt.part1 /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/T72.fluid.outfile.txt.part2 /home/maria/Documents/Phd/Tmp/hdfreader/hdf5output/T72_solid.outfile.txt 12 12 7 5
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>

#include "metapart.h"


#define UBFACTOR 5

#define OFFSET 317292 /* the offset to add to interedges. 
		       * We load (thus index) the prisma file first
                       * and then the hexa, as they do in cerfacs. 
		       * However when searching for the interedges we use
		       * internal indexing of the hexa mesh (0..2199)
		       * which does the intersection with meshB. 
		       * Therefore the interedges results for meshA should
		       * be shift to match the whole hmeshA indexing.*/

/* *********************************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0],  
	 "[-d] [-p] [-v] [-z shiftz] [-Z vtkshiftz] [-n nprocscheme] [-s copartscheme] [-a seed] meshA1cpl.txt meshA2.txt meshB.txt npartsA npartsB [procsnumbcplA procsnumbcplB] ");
  printf("\t\t -s copartscheme: select the co-partitioning scheme (NAIVE, AWARE, PROJREPART, MULTI)\n");
  printf("\t\t -n nprocscheme: select the number of processors in cpl scheme (USER, GEOMET, PROP,)\n");
  printf("\t\t -a seed: set the random generator seed\n");
  printf("\t\t -v: produce VTK output files\n");
  printf("\t\t -V: produce more VTK output files\n");
  printf("\t\t -d: debug mode\n");
  printf("\t\t -p: save output partition of meshes A and B, and save interedges,txt and save avbp file\n");
  printf("\t\t -l: load interedges.txt (instead of computing it)\n");
  printf("\t\t -z shiftz: shift meshB on z axis (after loading file)\n");
  printf("\t\t -Z vtkshiftz: shift meshB on z axis (for VTK output)\n");
  
  exit (EXIT_FAILURE);  
}

/* *********************************************************** */


int main(int argc, char * argv[]) 
{   
  
  /* 0) input args */
  int opt;
  int vtkopt = 0;
  int debugopt = 0;
  int saveopt = 0;
  int loadopt = 0;
  int inverse = 0;
  double shiftz = 0.0;
  char interedgefile[256]= "./interedges.txt";
  char schemestr[256] = "PROJREPART";
  int procscplscheme = -1;
  char procscplstr[256] = "GEOMET";
  int randseed = time (NULL);
  
  while ((opt = getopt (argc, argv, "viVdps:z:l:n:a:")) != -1) {
    switch (opt) {
      
    case 'a':
      randseed = atoi (optarg);     
      break; 
      
    case 'd':
      debugopt = 1;
      break;
      
    case 'v':
      vtkopt = 1;
      break;
      
    case 'V':
      vtkopt = 2;
      break;
      
    case 'p':
      saveopt = 1;
      break;

    case 'i':
      inverse = 1;
      break;
      
    case 's':
      strncpy(schemestr,optarg,256);
      break;
      
    case 'l':
      loadopt = 1;
      strncpy(interedgefile,optarg,256);
      break;
      
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
    case 'n':
      strncpy(procscplstr,optarg,256);
      break;	
    default:
      usage(argc,argv);
    }
  }  
  
  char* meshfileApart1;
  char* meshfileApart2;
  char* meshfileB;
  int npartsA; 
  int npartsB;
  int procsnumbcplA = 0;
  int procsnumbcplB = 0;
  
  if (!strcmp(procscplstr,"USER")) {
    if(argc - optind != 7) usage(argc,argv);
    
    meshfileApart1 = argv[optind];
    meshfileApart2 = argv[optind+1];
    meshfileB = argv[optind+2];
    npartsA = atoi(argv[optind+3]); 
    npartsB = atoi(argv[optind+4]);
    procsnumbcplA =  atoi(argv[optind+5]);
    procsnumbcplB =  atoi(argv[optind+6]);
    procscplscheme = USER_ALPHA;
    assert(procsnumbcplA > 0 && procsnumbcplB > 0);
  }
  else
    {
      if(argc - optind != 5) usage(argc,argv);
      
      meshfileApart1 = argv[optind];
      meshfileApart2 = argv[optind+1];
      meshfileB = argv[optind+2];
      npartsA = atoi(argv[optind+3]); 
      npartsB = atoi(argv[optind+4]);
    }
  
  /* initialize random seed */
  srand(randseed);
  printf("Random Seed = %d\n", randseed);
  
  printf("npartsA = %d npartsB = %d\n", npartsA,npartsB);
  assert(npartsA > 0 && npartsB > 0);
  
  
  // check scheme
  int scheme = -1; 
  printf("COPARTITIONING SCHEME: %s\n", schemestr);
  if(strcmp(schemestr,"NAIVE") == 0) scheme = COPART_NAIVE;
  else if(strcmp(schemestr,"AWARE") == 0) scheme = COPART_AWARE_NAIVE;
  // else if(strcmp(schemestr,"BIASED") == 0) scheme = COPART_AWARE_BIASED;
  else if(strcmp(schemestr,"PROJREPART") == 0) scheme = COPART_AWARE_PROJREPART;
  else if(strcmp(schemestr,"MULTI") == 0) scheme = COPART_MULTICONST;
  else { usage(argc,argv); exit(EXIT_FAILURE); }
  
  // check the scheme for generation of processor for cpl
  // TODO change like above in copart.h
  printf("GENERATING PROCESSOR CPL SCHEME: %s \n",procscplstr);
  if ( !strcmp(procscplstr,"PROP")) procscplscheme = LINEAR_ALPHA;
  if ( !strcmp(procscplstr,"GEOMET")) procscplscheme = GEOMETRIC_ALPHA;
  
  /* 1) load mesh A & B */ 
  
  /* 1) load mesh A & B */ 
  
  printf("\n*************** LOADING MESHES ***************\n\n");
  
  Mesh meshAcpl, meshB;
  HybridMesh meshA;
  
 // first prism and then hexa as they do at cerfacs  
  int r = loadHybridMesh(&meshA, 2, meshfileApart2, meshfileApart1); 
  printf("Mesh A[0]: nb cells = %d, nb nodes = %d\n", meshA.nb_cells, meshA.nb_nodes);    
  loadMesh(meshfileApart1, &meshAcpl);
  

  loadMesh(meshfileB, &meshB); 
  printf("Mesh B[0]: nb cells = %d, nb nodes = %d\n", meshB.nb_cells, meshB.nb_nodes);  
  
  if(debugopt) {
    printf("********************* MESH A *********************\n");
    printHybridMesh(&meshA);
    printf("\n********************* MESH B *********************\n");
    printMesh(&meshB);
  }
  

  
  /* 2) generate graphs from meshes */
  
  Graph gA, gB, gAcpl;
  Hybridmesh2Graph(&meshA, &gA);
  mesh2Graph(&meshB, &gB);
  mesh2Graph(&meshAcpl, &gAcpl); // debug
  saveGraph("gA.txt",&gA); // debug
  
  if(debugopt) {
    printf("\n********************* GRAPH A *********************\n");
    printGraph(&gA);
    printf("\n********************* GRAPH B *********************\n");
    printGraph(&gB);
  }
  
  printf("Graph A: nb vertices = %d, nb edges = %d\n", gA.nvtxs, gA.nedges);  
  printf("Graph B: nb vertices = %d, nb edges = %d\n", gB.nvtxs, gB.nedges);  
  
  /* 3) compute/load/save inter-edges */ 
  
  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;
  double * volumes = NULL;       
  
  if(loadopt) {
    printf("\n*************** LOAD INTER-EDGES ***************\n\n");
    
    printf("Loading interedges: %s\n",interedgefile);
    FILE* finteredges = fopen(interedgefile, "r");
    int a, b;
    
    while(fscanf(finteredges,"%d %d\n", &a, &b) > 0) nbinteredges++;
    interedges = malloc(2*nbinteredges*sizeof(int));
    rewind(finteredges);
    for(int k = 0 ; k < nbinteredges ; k++)
      fscanf(finteredges,"%d %d\n", interedges + 2*k, interedges + 2*k+1);
    fclose(finteredges);
    
  }
  else {
    
    printf("\n*************** COMPUTE INTER-EDGES ***************\n\n");
    /* Find the intersections with the part of hybrid meshA that we know that interacts with meshB.*/
    
    // scaleMesh(&meshB, 1.0001, 1.0001, 1.0); // ???
    intersectMesh(&meshAcpl, &meshB, &nbinteredges, &interedges, &volumes);
    const int minwgt = 0, maxwgt = 10;
    computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, minwgt, maxwgt);   
    if(weights) printf("Weights for the interedges have been computed!\n");
    //intersectHybridMesh(&meshA, &meshB, &nbinteredges, &interedges);
    
    // save interedges
    if(saveopt) {
      FILE* finteredges = fopen("./interedges.txt", "w");
      for(int k = 0 ; k < nbinteredges ; k++)
	fprintf(finteredges,"%d %d\n",interedges[2*k], interedges[2*k+1]);
      fclose(finteredges);
      printf("Saving interedges: %s\n", "interedges.txt");
    }
    
    for(int k = 0 ; k < nbinteredges ; k++)
      interedges[2*k]=interedges[2*k] + OFFSET;

  }
  
  printf("nb interedges found = %d\n", nbinteredges);  
  
  if(debugopt) {
    for(int i = 0 ; i < nbinteredges ; i++)
      printf("interedge %d: A%d <---> B%d\n", i, interedges[2*i], interedges[2*i+1]);
  }
  
  /* 4) generate the number of processors in coupling phase */ 
  printf("STARTING EXPERIMENT #0 (nbpartA = %d  nbpartB = %d)\n", npartsA,npartsB);
  printf("-----------------------------------\n");
  
  int npartscplA, npartscplB;
  if (procscplscheme == GEOMETRIC_ALPHA || procscplscheme == LINEAR_ALPHA)
    generateProcsCpl(&gA, &gB, npartsA, npartsB, nbinteredges, interedges, weights, procscplscheme, &npartscplA, &npartscplB);
  else if (procscplscheme == USER_ALPHA) {
    npartscplA = procsnumbcplA;
    npartscplB = procsnumbcplB;
  }  
  printf("- procscplscheme = %s\n", procscplstr);
  printf("- npartscplA = %d\n", npartscplA);
  printf("- npartscplB = %d\n", npartscplB);
  
  /* 5) copart */
  
  printf("\n*************** CO-PARTITIONING SCHEME: %s ***************\n\n", schemestr);
  
  int* partA = calloc(gA.nvtxs,sizeof(int));
  int* partB = calloc(gB.nvtxs,sizeof(int));  
  int edgecutA = 0, edgecutB = 0;
  int nbcomms = 0;
  int intercomm[npartsA][npartsB]; /* A --> B */
  int ubfactor = UBFACTOR;
  
 

  if(!inverse) {
    printf("---------------------------------------------\n");
    printf("Calling copart(A,B) with:\n");
    printf("Graph A: nb vertices = %d, nb edges = %d\n", gA.nvtxs, gA.nedges);  
    printf("Graph B: nb vertices = %d, nb edges = %d\n", gB.nvtxs, gB.nedges);
    printf("copartscheme = %d\n",scheme);
    printf("nbinteredges = %d\n", nbinteredges);
    printf("interedges [ ");
    for(int i=0; i< 20; i++)
      printf("%d ", interedges[i]);
    printf("]\n");
    printf("weights [ ");
    for(int i=0; i< 20; i++) {
      if(weights[i]) printf("%d ", weights[i]);
    }
    printf("]\n");
    printf("npartsA = %d, npartsB = %d\n", npartsA,npartsB);
    printf("npartscplA = %d, npartscplB = %d\n",npartscplA, npartscplB);
    printf("seeds = %d\n",randseed);
    printf("---------------------------------------------\n");
    copart(&gA, &gB,
	     npartsA, npartsB,
	     ubfactor,
	     nbinteredges,
	     interedges,
	     weights,
	     scheme,
	     npartscplA, npartscplB,
	     randseed,
	     partA, partB,
	     &edgecutA, &edgecutB,
	     &meshA, &meshAcpl,NULL,
	     &meshB,
	     NULL);
  
    printf("\n*************** DIAGNOSTIC ***************\n\n");
    
    printCopartDiagnostic(&gA, &gB, npartsA, npartsB, npartscplA, npartscplB, partA, partB, nbinteredges, interedges, weights);
  }
  else {
    printf("---------------------------------------------\n");
    printf("Calling copart(B,A) with:\n");
    printf("Graph A: nb vertices = %d, nb edges = %d\n", gA.nvtxs, gA.nedges);  
    printf("Graph B: nb vertices = %d, nb edges = %d\n", gB.nvtxs, gB.nedges);
    printf("copartscheme = %s\n",schemestr);
    printf("nbinteredges = %d\n", nbinteredges);
        printf("interedges [ ");
    for(int i=0; i< 20; i++)
      printf("%d ", interedges[i]);
    printf("]\n");
    printf("npartsA = %d, npartsB = %d\n", npartsA,npartsB);
    printf("npartscplA = %d, npartscplB = %d\n",npartscplA, npartscplB);
    printf("seeds = %d\n",randseed);
    printf("---------------------------------------------\n");

    copart(&gB, &gA, 
	     npartsB, npartsA, 
	     ubfactor,
	     nbinteredges, 
	     interedges,
	     NULL,
	     scheme,
	     npartscplB, npartscplA,
	     randseed,
	     partB, partA,
	     &edgecutB, &edgecutA, 
	     NULL, &meshB,
	     &meshA, &meshAcpl,
	     NULL);  
  
    printf("\n*************** DIAGNOSTIC ***************\n\n");
  
    printCopartDiagnostic(&gB, &gA, npartsB, npartsA, npartscplA, npartscplB, partB, partA, nbinteredges, interedges, weights);
  }
  printf("\n");
  printf(" %s %s %d %d  %d %d  %d %d\n",
	  schemestr,
	  procscplstr,
	  npartsA,npartscplA,
	  npartsB,npartscplB,
	  edgecutA, edgecutB
	  );
  printf("\n");
  
  /* 6) saving partition */
  
  if(saveopt) {
    
    printf("\n*************** SAVE PARTITION ***************\n\n");
    
    saveHybridMesh("cylinder.avbp.txt", &meshA);    
    printf("Saving avbp cylinder mesh file\n");
    /* mesh A */
    char partfileA[255];
    sprintf(partfileA,"%s.part%d", meshfileApart1, npartsA);
    printf("Saving mesh partition file: %s\n", partfileA);
    savePartition(meshA.nb_cells, partA, partfileA);
    
    /* mesh B */
    char partfileB[255];
    sprintf(partfileB,"%s.part%d", meshfileB, npartsB);
    printf("Saving mesh partition file: %s\n", partfileB);
    savePartition(meshB.nb_cells, partB, partfileB);
    
  }
  
  /* 7) VTK output*/
  
  if(vtkopt) {
    
    printf("\n*************** VTK OUTPUT FILES ***************\n\n");

    addHybridMeshVariable(&meshA, "partA", CELL_INTEGER, 1, partA);
    addMeshVariable(&meshB, "partB", CELL_INTEGER, 1, partB);
    
    /* if(vtkshiftz != 0.0) { */
    /*   printf("Shifting mesh B on Z for VTK output: %.2f\n", vtkshiftz);   */
    /*   shiftMesh(&meshB, 0.0, 0.0, vtkshiftz); */
    /* } */
    
    // interedges
    char * outfileCPL = "hcpl.vtk";    
    Mesh cpl;  

    // here interedges should be indexed based on internal numbering
    // inside gAcpl (or meshAcpl) and gB.
    for(int k = 0 ; k < nbinteredges ; k++)
      interedges[2*k]=interedges[2*k] - OFFSET;
    generateCouplingMesh(&meshAcpl, &meshB, nbinteredges, interedges, 0.0, &cpl); 
    printf("Saving VTK mesh: %s\n", outfileCPL);
    saveVTKMesh(outfileCPL, &cpl);
    freeMesh(&cpl);
    
    char * houtfileA = "hmeshA.vtk";
    printf("Saving VTK mesh: %s\n", houtfileA);
    saveVTKHybridMesh(houtfileA, &meshA);
    
    // mesh
    char * outfileA = "hmeshAcpl.vtk";
    printf("Saving VTK mesh: %s\n", outfileA);
    saveVTKMesh(outfileA, &meshAcpl);
    
    char * outfileB = "hmeshB.vtk";
    printf("Saving VTK mesh: %s\n", outfileB);
    saveVTKMesh(outfileB, &meshB);
    
    if(vtkopt > 1) {
      // graph
      
      char * outfileGAcpl = "graphAcpl.vtk";
      printf("Saving VTK mesh: %s\n", outfileGAcpl);
      double * centersAcpl = malloc(meshAcpl.nb_cells*3*sizeof(double));
      cellCenters(&meshAcpl, centersAcpl);
      Mesh GAcpl;
      graph2LineMesh(&gAcpl, centersAcpl, &GAcpl);
      saveVTKMesh(outfileGAcpl, &GAcpl);
      
      char * outfileGA = "graphA.vtk";
      printf("Saving VTK mesh: %s\n", outfileGA);
      double * centersA = malloc(meshA.nb_cells*3*sizeof(double));
      HcellCenters(&meshA, centersA);
      Mesh GA;
      //printGraph(&gA);
      graph2LineMesh(&gA, centersA, &GA);
      saveVTKMesh(outfileGA, &GA);
      
      char * outfileGB = "graphB.vtk";
      printf("Saving VTK mesh: %s\n", outfileGB);
      double * centersB = malloc(meshB.nb_cells*3*sizeof(double));
      cellCenters(&meshB, centersB);
      Mesh GB;
      graph2LineMesh(&gB, centersB, &GB);
      saveVTKMesh(outfileGB, &GB);
      
      free(centersA);
      free(centersB);        
    }
    
    // quotient
    /* char * outfileQA = "quotientA.vtk"; */
    /* printf("Saving VTK mesh: %s\n", outfileQA); */
    /* Graph qA; */
    /* quotientGraph(&gA, npartsA, partA, &qA); */
    /* double * centersQA = calloc(npartsA*3, sizeof(double)); */
    /* for(int i = 0 ; i < gA.ntvxs ; i++) { */
    /*   int pa = partA[i]; */
    /*   centersQA[pa*3] += centersA[i*3]; */
    /*   centersQA[pa*3+1] += 0; */
    /*   centersQA[pa*3+2] += 0; */
    /* } */
    /* Mesh QA; */
    /* graph2Mesh(&qA, centersQA, &QA); */
    /* saveVTKMesh(outfileGA, &QA); */    
    // free(centersQA);
  }
  
  
  /* 8) free */
  freeGraph(&gA);
  freeGraph(&gB);
  freeGraph(&gAcpl);
  freeMesh(&meshAcpl);
  freeMesh(&meshB);
  freeHybridMesh(&meshA);
  free(partA);
  free(partB);
  free(interedges);
  free(weights);  
  
  return EXIT_SUCCESS;
  
}
