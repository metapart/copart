/**
 *   @file matrixcomm.h
 *
 *   @author     Aurelien Esnard
 *   @author     Clement Vuchener
 *
 */

#ifndef MATRIXCOMM_H
#define MATRIXCOMM_H

#include "struct.h"

/** Create an identity matrix of size \p M */
void createIdentityMatrix (int M,		/**< [in] Size of the matrix */
			   const int *perm,	/**< [in] Permutation of the lines (may be NULL for identity) */
			   Hypergraph *out	/**< [out] Result sparse matrix in an hypergraph structure */
			   );

/** Create a 1d repartitioning matrix of size MxN */
void create1dRepartMatrix (int M,		/**< [in] Number of lines/hyperedges/old parts */
			   int N,		/**< [in] Number of columns/vertices/new parts */
			   const int *perm,	/**< [in] Permutation of the lines (may be NULL for identity) */
			   Hypergraph *out	/**< [out] Result sparse matrix in an hypergraph structure */
			   );

/** Create a repartitioning matrix with a diagonal block and the remaining filled with a 1d repartitioning matrix */
void createDiag1dMatrix (int M,			/**< [in] Number of lines/hyperedges/old parts */
			 int N,			/**< [in] Number of columns/vertices/new parts */
			 const int *perm,	/**< [in] Permutation of the lines (may be NULL for identity) */
			 Hypergraph *out	/**< [out] Result sparse matrix in an hypergraph structure */
			 );

/** Cancatenate verticaly two matrices with the same number of columns */
void concatV (const Hypergraph *a,	/**< [in] First matrix, will be placed above */
	      const Hypergraph *b,	/**< [in] Second matrix, will be placed below */
	      Hypergraph *out		/**< [out] Result sparse matrix in an hypergraph structure */
	      );

/** Cancatenate horizontaly two matrices with the same number of lines */
void concatH (const Hypergraph *a,	/**< [in] First matrix, will be placed on the left */
	      const Hypergraph *b,	/**< [in] Second matrix, will be placed on the right */
	      Hypergraph *out		/**< [out] Result sparse matrix in an hypergraph structure */
	      );

/** Permute the hyperedges of the hypergraph */
void permuteHyperedges (const Hypergraph *in,	/** [in] Input hypergraph */
			const int *perm,	/** [in] Permutation of the hyperedges (new edge i is old edge p[i]) */
			Hypergraph *out		/** [out] Permuted hypergraph */
			);

/** Convert a sparse matrix into a dense one. */
void sparse2dense (const Hypergraph *h, int *out);

/** Convert a dense matrix of size MxN into a sparse matrix */
void dense2sparse (const int *m, int M, int N, Hypergraph *out);

/** Replace non-zeros with communication volume in matrix \p m.
 * The sum of communications on line i is w[i].
 * The sum on each column is balanced (\f$\frac{\sum_i w_i}{N}\f$)
 * \return 1 if success, 0 otherwise */
int computeComms (int *m,		/**< [in,out] Communication matrix */
		  int M,		/**< [in] Number of lines/old parts */
		  int N,		/**< [in] Number of ciolumns/new parts */
		  const int *w		/**< [in] Weights of old parts */
		  );

/** Apply a simple transformation on \p m preserving row and column sums.
 * \p d is added to (\p i1, \p j1) and (\p i2, \p j2), and subtracted from (\p i1, \p j2), (\p i2, \p j1). */
void t1 (int *m,	/**< [in, out] Communication matrix */
	 int stride,	/**< [in] Matrix stride */
	 int i1,	/**< [in] First row */
	 int j1,	/**< [in] First column */
	 int i2,	/**< [in] Second row */
	 int j2,	/**< [in] Second column */
	 int d		/**< [in] Value of the transformation */
	 );

/** Print the communication matrix */
void printCommMatrix(const int *m, int M, int N);

/** Check the communication  matrix */
int checkCommMatrix(const int *m, int M, int N);

/** Print the communication matrix (with permutation) */
void printCommMatrixPerm(int *intercomms, int M, int N, int *perm);

#endif
