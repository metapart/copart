/**
 *   @file misc.h
 *
 *   @author     Aurelien Esnard
 *
 *   @defgroup Struct Common structures.
 *   @brief Common structure definitions for mesh, graph and
 *   hypergraph.
 *
 */

#ifndef __MISC_H__
#define __MISC_H__

/* *********************************************************** */
/*                       HYBRID MESH                           */
/* *********************************************************** */

/** Hybrid Mesh structure. */
typedef struct HybridMesh_ {
  int nb_components;
  int max_elm_size;
  int nb_nodes;              /**< nb nodes */
  int nb_cells;              /**< nb cells */
  ElementType * elm_types;   /**< element types */
  double * nodes ;           /**< array of node coordinates (of size nb_nodes x 3D) */
  int * cells;               /**< array of node numbers (of size nb_cells x MAX_ELM_SIZE)  */
  int nb_node_vars;          /**< nb of variables associated to nodes */
  int nb_cell_vars;          /**< nb of variables associated to cells */
  Variable * node_vars ;     /**< array of point variable (of size nb_node_vars) */
  Variable * cell_vars;      /**< array of cell variable (of size nb_cell_vars) */
} HybridMesh;


/* /\* *********************************************************** *\/ */
/* /\*                          QUEUE                              *\/ */
/* /\* *********************************************************** *\/ */

/* /\** Queue structure. *\/ */
/* typedef struct Queue_ { */
/*   int *tab; */
/*   int size; */
/*   int start; */
/*   int end; */
/* } Queue; */

/* void queue_init (Queue *self, int size); */
/* void queue_push_back (Queue* self, int v); */
/* int queue_pop_front (Queue* self); */
/* int queue_is_empty (const Queue *self); */
/* void queue_free (Queue *self); */

/* /\* *********************************************************** *\/ */
/* /\*                           LIST                              *\/ */
/* /\* *********************************************************** *\/ */

/* /\** List structure. *\/ */
/* typedef struct List_ {   */
/*   int size;         /\**< nb elements *\/ */
/*   int max;          /\**< max elements *\/ */
/*   int * array;      /\**< array of elements (of size max) *\/ */
/* } List; */

/* /\** Init list. *\/ */
/* void initList(List * l, int max); */

/* /\** Free list. *\/ */
/* void freeList(List * l); */

/* /\** Print list. *\/ */
/* void printList(List * l); */

/* /\** Reset list (without freeing memory). *\/ */
/* void resetList(List * l); */

/* /\** Insert an element in an integer list (realloc memory if required). *\/ */
/* void insertList(List * l, int element); */

/* /\** Quicksort routine for integer list. *\/ */
/* void sortList(List * l); */

/* /\** Find an element in a sorted integer list. Log complexity. Return -1 if not found. *\/ */
/* int findSortedList(List * l, int element); */

/* /\** Insert an element in a sorted integer list (realloc memory if required). Linear complexity. *\/ */
/* /\* Return false if it tries to insert a duplicated element in unique list. *\/ */
/* int insertSortedList(List * l, int element, int duplicate); */

/* /\** Merge two sorted lists. Linear complexity. *\/ */
/* void mergeSortedList(List * la, List * lb, List * lc, int duplicate); */

/* *********************************************************** */
/*                           MISC                              */
/* *********************************************************** */

/** Partition tree structure. */
/** The level 0 is made up of only one part. At level l, there is
    exactly k merged parts, with k=2^l < nparts. If the level is
    greater or equal to h=ceil(ln(nparts)), it contains all the
    parts.
*/
typedef struct PartitionTree_ {  
  int nparts;       /**< nb of parts in the tree */
  int height;       /**< nb of levels of the underlying perfect binary tree */
  int realheight;   /**< real height of the tree could be height+1 if the tree is not binary perfect */
  int length;       /**< array length  */
  int * array;      /**< array that stores the size of each merged part at each level */
} PartitionTree;


typedef struct CoarsenInfo_ {
  int clevel; // not complete
  
}CoarsenInfo;

/** Partition extra information */
typedef struct PartitionInfo_ {
  double exectime;       /**< execution time (in ms) */
  double mlexectime[3];  /**< execution time (in ms) for multilevel phases (coarsening=0, initial partitioning and uncoarsening) */
  double hexectime[2];   /**< execution time (in ms) for hybrid partitioning phases (top=0 and bot=1) */  
  Mesh* mesh;            /**< mesh */
  int * partorder;       /**< order in which each vertex is assigned to its part (array of size nvtxs) */
  CoarsenInfo * cinfo;
} PartitionInfo;

typedef struct CopartResults_ {
  int kA;
  int kB;  
  int kcplA;
  int kcplB;  

  int nvtxsA;
  int nvtxsB;
  int nvtxscplA;
  int nvtxscplB;    
  
  int nbinteredges;

  int kcplAreal;
  int kcplBreal;  
  
  double maxubA;
  double maxubB;
  double maxubcplA;
  double maxubcplB;

  int edgecutA;
  int edgecutB;
  int edgecutcplA;
  int edgecutcplB;  
  
  int threshold; // thresold for minor/major messages
  int totZ; // total nb of messages
  int totZopt; // total nb of messages (optimal)
  int totZm; // nb of minor messages
  int totZM; // nb of major messages

  int totV; // total volume of comm
  int totVopt; // total volume of comm  
  int maxM; // max message volume
  // int maxS; // max send volume / proc
  // int maxR; // max recv volume / proc
  int maxV; // max send/recv volume / proc
  int maxVopt; // max send/recv volume / proc    

  int * intercomm; 
  
} CopartResults;



//@}

#endif
