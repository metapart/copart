/**
 *   @file modelcomm.h
 *
 *   @author     Aurelien Esnard
 *   @author     Clement Vuchener
 *
 *   @addtogroup Repart Repartitioning & migration routines.
 *
 */

#ifndef __MODELCOMM_H__
#define __MODELCOMM_H__

#include "struct.h"
#include "migration.h"

//@{

/** MxN communication schemes */
enum CommScheme {
  UNUSED,               /**< communication scheme unused for repartitioning */
  ZOLTAN,		/**< scheme for Zoltan repartitioning with fixed vertices (one for each old part) */
  B1D,	       	        /**< 1D scheme for balanced initial partition */
  UB1D,	                /**< 1D scheme for unbalanced initial partition */
  HM,		        /**< scheme based on hypergraph matching */
  UBHM,	                /**< scheme based on hypergraph matching and tweaked comunication matrix */
  GREEDYHB,	        /**< greedy algorithm based on quotient graph (orel) */
  GREEDYHBP,	        /**< same scheme as \ref GREEDYHB, but without unbalance factor (orel) */
  GREEDYHB2,	        /**< same scheme as \ref GREEDYHB but with different heuristic (clement) */
  GREEDYDEGREE,         /**< greedy heuritic, that control the max size (or degree) of hyperedges built (orel). */
  OPTIMIZE, 		/**< optimize another scheme with linear-programming optimization. */
  BRUTEFORCE	        /**< brute-force and slow algorithm */
};

struct ghb_options {
  int opt_diag;
};

extern const struct ghb_options
  GHB_NO_DIAG,
  GHB_OPT_DIAG;

struct opt_options {
  int objective;
  enum CommScheme base_scheme;
  void *base_options;
};

extern const struct opt_options
  GREEDYDEGREE_TOTALV,
  GREEDYDEGREE_MAXV,
  GREEDYDEGREE_TOTALZ,
  GREEDYDEGREE_MAXZ;

/** Create a communication model for MxN repartitioning using \p scheme */
void communicationModel (int M,				/**< [in] Initial part count */
			 int N,				/**< [in] Final part count */
			 int ubfactor,			/**< [in] unbalance factor */
			 Graph *quotient,		/**< [in] Quotient graph of old partition */
			 enum CommScheme scheme,	/**< [in] Communication scheme */
			 const void *options,		/**< [in] Options (may be null) (depends on scheme) */
			 int skipremap,             	/**< [in] skip remapping */
			 int * modelmat			/**< [out] Communication model (matrix of dimension M by N) */ 
			 );

/** Optimize a communication model for MxN repartitioning enabling migration among quotient graph edges (\p g). */
/* This method builds an enriched quotient graph and optimize the migration problem using LP (linear programming). */
void optimizeCommunicationModel(int M,	         /**< [in] Initial part count */
				int N,	         /**< [in] Final part count */
				int ubfactor,    /**< [in] unbalance factor */
				int ecfactor,    /**< [in] edge-connection factor (in %) */ 
				int objective, /**< [in] migration scheme */
				Graph *g,	 /**< [in] Quotient graph (with M vertices) */
				int * modelmat,  /**< [in] Communication model (matrix of dimension M by N) */ 
				int * optmat     /**< [out] Optimized migration matrix (matrix of dimension M by N) */ 
				);

/** Find a remapping \p remap according to the communication matrix \p comms */
void remap (int M,	/**< [in] Number of old parts */
	    int N,	/**< [in] Number of new parts */
	    int *comms,	/**< [in] Communications matrix (M lines, N columns) */
	    int *remap	/**< [out] Remapping of new parts */
	    );

/** Compute the score of the matching between \p h and \p g.
 * 
 *  The vertex i of \p g is matched with the vertex perm[i] of \p h.
 *  \return The score of the matching.
 */
int matchingScore (Hypergraph *h,	/**< [in] Hypergraph */
		   Graph *g,		/**< [in] Graph */
		   int *perm		/**< [in] Matching between \p g and \h */
		   );

/** Find a permutation \p perm giving a good matching between \p h and \p g using simulated annealing*/
int matching_sa (Hypergraph *h,	/**< [in] Hypergraph to match */
		 Graph *g,		/**< [in] Graph to permute */
		 int *perm,		/**< [out] Permutation of \p g. p[i] is the new position of part i */
		 double Tinit,		/**< [in] Initial temperature */
		 double Tend,		/**< [in] Temperature threshold to end the algorithm */
		 double cooling	/**< [in] Cooling factor, each iteration the temperature is multiplied by this */
		 );

//@}

#endif
