/* Co-Partitioning of 2 Coupled Meshes. */
/* Author(s): aurelien.esnard@labri.fr */

/* Examples: 
   ./mesh-copart.exe -m SCOTCHML1_KGGGP -s PROJREPART -n USER -a 0 -z 1 -v ../data/unit-cube-hexa-10x10x10.txt  ../data/unit-cube-tetra-5638.txt 8 8 4 4
   ./mesh-copart.exe -m SCOTCHML1_HUN -n USER -s PROJREPART -v ../data/cerfacs/cylinder/cylinder.avbp.part2.txt ../data/cerfacs/cylinder/cylinder.avtp.txt 5 8 5 4
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <getopt.h>

#include "metapart.h"

#define UBFACTOR 5

/* *********************************************************** */

void usage(int argc, char* argv[]) 
{
  printf("usage: %s %s\n", argv[0],  
	 "[-d] [-v] [-w] [-p interedgefile] [-l interedgefile] [-z shiftz] [-Z vtkshiftz] [-n nprocscheme] [-s copartscheme] [-m method] [-a seed] meshA.txt meshB.txt kA kB [kcplA kcplB] ");
  printf("\t\t -s copartscheme: select the co-partitioning scheme (NAIVE, AWARE, PROJREPART, SUBPARTPROJ, REPARTPROJ, MULTICONST)\n");
  printf("\t\t -n nprocscheme: select the number of processors in cpl scheme (USER as default, GEOMETRIC, LINEAR, MULTIPLE)\n");
  printf("\t\t -m method: select the graph partitioning method with fixed vertex support (SCOTCHFM, SCOTCHML_HUN, SCOTCHML_KGGGP, ...)\n");  
  printf("\t\t -a seed: set the random generator seed\n");
  printf("\t\t -v: produce VTK output files\n");
  printf("\t\t -V: produce more VTK output files\n");
  printf("\t\t -d: debug mode\n");
  printf("\t\t -p interedgefile: save interedge file\n");
  printf("\t\t -l interedgefile: load interedge file (instead of computing it)\n");  
  printf("\t\t -w: ignore interedge weights in copartitioning\n");  
  printf("\t\t -z shiftz: shift meshB on z axis (after loading file)\n");
  printf("\t\t -Z vtkshiftz: shift meshB on z axis (for VTK output)\n");
  exit (EXIT_FAILURE);  
}

/* *********************************************************** */


int main(int argc, char * argv[]) 
{   
  
  /* 0) input args */
  
  int opt;
  int vtkopt = 0;
  int interedgeweightopt = 1;  // default
  int debugopt = 0;
  int saveopt = 0;
  int loadopt = 0;
  double shiftz = 0.0;  
  double vtkshiftz = 0.0;  
  char schemestr[256] = "PROJREPART";
  int procscplscheme = -1;
  char procscplstr[256] = "USER";
  char interedgestr[256];
  int randseed = time (NULL);
  char methodstr[256] = "SCOTCHML1_HUN";
  
  while ((opt = getopt (argc, argv, "wvVds:Z:R:z:p:l:n:a:m:")) != -1) {
    switch (opt) {
      
    case 'a':
      randseed = atoi (optarg);     
      break; 
      
    case 'd':
      debugopt = 1;
      break;
      
    case 'v':
      vtkopt = 1;
      break;
      
    case 'V':
      vtkopt = 2;
      break;
      
    case 'w':
      interedgeweightopt = 0;
      break;      
      
    case 'p':
      saveopt = 1;
      strncpy(interedgestr,optarg,256);      
      break;
      
    case 's':
      strncpy(schemestr,optarg,256);
      break;
      
    case 'l':
      loadopt = 1;
      strncpy(interedgestr,optarg,256);
      break;
      
    case 'z':
      shiftz = strtod(optarg, NULL);     
      break;
      
    case 'Z':
      vtkshiftz = strtod(optarg, NULL);     
      break;
      
    case 'n':
      strncpy(procscplstr,optarg,256);
      break;
      
    case 'm':
      strncpy(methodstr,optarg,256);
      break;	      

    default:
      usage(argc,argv);
    }
  }  

  // check
  assert(saveopt + loadopt < 2);
  
  char* meshfileA;
  char* meshfileB;
  int npartsA; 
  int npartsB;
  int procsnumbcplA = 0;
  int procsnumbcplB = 0;
  
  if (!strcmp(procscplstr,"USER")) {
    if(argc - optind != 6) usage(argc,argv);
    
    meshfileA = argv[optind];
    meshfileB = argv[optind+1];
    npartsA = atoi(argv[optind+2]); 
    npartsB = atoi(argv[optind+3]);
    procsnumbcplA =  atoi(argv[optind+4]);
    procsnumbcplB =  atoi(argv[optind+5]);
    procscplscheme = USER_ALPHA;
    assert(procsnumbcplA > 0 && procsnumbcplB > 0);
  }
  else
    {
      if(argc - optind != 4) usage(argc,argv);
      
      meshfileA = argv[optind];
      meshfileB = argv[optind+1];
      npartsA = atoi(argv[optind+2]); 
      npartsB = atoi(argv[optind+3]);
    }
  
  /* initialize random seed */
  srand(randseed);
  printf("Random Seed = %d\n", randseed);
  
  printf("npartsA = %d, npartsB = %d\n", npartsA, npartsB);
  assert(npartsA > 0 && npartsB > 0);
  
  
  // check scheme
  int scheme = -1; 
  printf("COPARTITIONING SCHEME: %s\n", schemestr);
  if(strcmp(schemestr,"NAIVE") == 0) scheme = COPART_NAIVE;
  else if(strcmp(schemestr,"AWARE") == 0) scheme = COPART_AWARE_NAIVE;
  else if(strcmp(schemestr,"PROJREPART") == 0) scheme = COPART_AWARE_PROJREPART;
  else if(strcmp(schemestr,"REPARTPROJ") == 0) scheme = COPART_AWARE_REPARTPROJ;
  else if(strcmp(schemestr,"SUBPARTPROJ") == 0) scheme = COPART_AWARE_SUBPARTPROJ;
  else if(strcmp(schemestr,"PROJSUBPART") == 0) scheme = COPART_AWARE_PROJSUBPART;    
  else if(strcmp(schemestr,"MULTICONST") == 0) scheme = COPART_MULTICONST;
  else { usage(argc,argv); exit(EXIT_FAILURE); }
  
  printf("GENERATING PROCESSOR CPL SCHEME: %s \n",procscplstr);
  if ( strcmp(procscplstr,"LINEAR") == 0) procscplscheme = LINEAR_ALPHA;
  else if ( strcmp(procscplstr,"GEOMETRIC") == 0) procscplscheme = GEOMETRIC_ALPHA;
  else if ( strcmp(procscplstr,"MULTIPLE") == 0) procscplscheme = MULTIPLE_ALPHA;
  else if ( strcmp(procscplstr,"USER") == 0) procscplscheme = USER_ALPHA;  
  else { usage(argc,argv); exit(EXIT_FAILURE); }  
  
  /* 1) load mesh A & B */ 
  
  printf("\n*************** LOADING MESHES ***************\n\n");
  
  Mesh meshA, meshB;
  
  printf("Loading mesh A: %s\n", meshfileA);  
  if(!loadMesh(meshfileA, &meshA)) exit(EXIT_FAILURE);
  
  printf("Loading mesh B: %s\n", meshfileB);  
  if(!loadMesh(meshfileB, &meshB)) exit(EXIT_FAILURE);
  
  if(shiftz != 0.0) {
    printf("Shifting mesh B on Z: %.2f\n", shiftz);  
    shiftMesh(&meshB, 0.0, 0.0, shiftz);
  }
  
  if(debugopt) {
    printf("********************* MESH A *********************\n");
    printMesh(&meshA);
    printf("\n********************* MESH B *********************\n");
    printMesh(&meshB);
  }
  
  printf("Mesh A: nb cells = %d, nb nodes = %d\n", meshA.nb_cells, meshA.nb_nodes);  
  printf("Mesh B: nb cells = %d, nb nodes = %d\n", meshB.nb_cells, meshB.nb_nodes);  
  
  /* 2) generate graphs from meshes */
  
  Graph gA, gB;
  mesh2Graph(&meshA, &gA);
  mesh2Graph(&meshB, &gB);
  
  if(debugopt) {
    printf("\n********************* GRAPH A *********************\n");
    printGraph(&gA);
    printf("\n********************* GRAPH B *********************\n");
    printGraph(&gB);
  }
  
  printf("Graph A: nb vertices = %d, nb edges = %d\n", gA.nvtxs, gA.nedges);  
  printf("Graph B: nb vertices = %d, nb edges = %d\n", gB.nvtxs, gB.nedges);  
  
  /* 3) compute/load/save inter-edges */ 
  
  int nbinteredges = 0;
  int * interedges = NULL;
  int * weights = NULL;     
  int inverse = 0;
  //int inverse = 1;
  if(loadopt) {

    printf("\n*************** LOAD INTER-EDGES ***************\n\n");

    if(inverse) printf("# INVERSE MODE ACTIVATED\n");
    printf("Loading interedges: %s\n", interedgestr);
    FILE* finteredges = fopen(interedgestr, "r");
    assert(finteredges);
    int a, b, w;

    while(fscanf(finteredges,"%d %d %d\n", &a, &b, &w) > 0) nbinteredges++;
    printf("nb interedges = %d\n",  nbinteredges);
    interedges = malloc(2*nbinteredges*sizeof(int));
    weights = malloc(nbinteredges*sizeof(int));    
    rewind(finteredges);
    for(int k = 0 ; k < nbinteredges ; k++) {
      int r;
      if(inverse)
	r = fscanf(finteredges,"%d %d %d\n", interedges + 2*k+1, interedges + 2*k, weights + k);
      else
	r = fscanf(finteredges,"%d %d %d\n", interedges + 2*k, interedges + 2*k+1, weights + k);
      //if(k == 0) printf("warning: no weight in interedge file!\n");
      //weights[k] = 1;
      assert(r == 3);
      //if(r == 2) weights[k] = 1; // weight can be ignored
      //if(k == 0 && r== 2) printf("warning: no weight in interedge file!\n");
    }
    fclose(finteredges);        
  }
  
  else {
    
    printf("\n*************** COMPUTE INTER-EDGES ***************\n\n");

    weights = NULL;
    if(interedgeweightopt) {
      double * volumes = NULL;    
      intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, &volumes);
      const int minwgt = 0, maxwgt = 10;
      computeInteregdeWeights(&nbinteredges, &interedges, &volumes, &weights, minwgt, maxwgt);
      free(volumes);
    }
    else 
      intersectMesh(&meshA, &meshB, &nbinteredges, &interedges, NULL);
    
    // save interedges
    if(saveopt) {
      FILE* finteredges = fopen(interedgestr, "w");
      for(int k = 0 ; k < nbinteredges ; k++)
	fprintf(finteredges,"%d %d %d\n",interedges[2*k], interedges[2*k+1], weights?weights[k]:1);
      fclose(finteredges);
      printf("Saving interedges: %s\n", interedgestr);      
    }    
  }
  
  printf("nb interedges found = %d\n", nbinteredges);  
  
  if(debugopt) {
    for(int i = 0 ; i < nbinteredges ; i++)
      printf("interedge %d: A%d <---> B%d (%d)\n", i, interedges[2*i], interedges[2*i+1], weights?weights[i]:1);
  }
  
  /* 4) generate the number of processors in coupling phase */ 
  
  int npartscplA, npartscplB;
  if (procscplscheme != USER_ALPHA)
    generateProcsCpl(&gA, &gB, npartsA, npartsB, nbinteredges, interedges, weights, procscplscheme, &npartscplA, &npartscplB);
  else if (procscplscheme == USER_ALPHA) {
    npartscplA = procsnumbcplA;
    npartscplB = procsnumbcplB;
  }
  
  printf("- procscplscheme = %s\n", procscplstr);
  printf("- npartscplA = %d\n", npartscplA);
  printf("- npartscplB = %d\n", npartscplB);
  
  /* 5) copart */
  
  printf("\n*************** CO-PARTITIONING SCHEME: %s ***************\n\n", schemestr);
  
  int* partA = malloc(gA.nvtxs*sizeof(int));
  int* partB = malloc(gB.nvtxs*sizeof(int));  
  int edgecutA = 0, edgecutB = 0;
  int ubfactor = UBFACTOR;
  
  copart(&gA, &gB, 
	 npartsA, npartsB, 
	 ubfactor,
	 nbinteredges, 
	 interedges,
	 weights,
	 scheme,
	 npartscplA, npartscplB,
	 randseed,
	 partA, partB,
	 &edgecutA, &edgecutB, 
	 NULL, &meshA,
	 NULL, &meshB,
	 methodstr); 
  
  printf("\n*************** DIAGNOSTIC ***************\n\n");
  
  printCopartDiagnostic(&gA, &gB, npartsA, npartsB, npartscplA, npartscplB, partA, partB, nbinteredges, interedges, weights);
  
  /* 6) saving partition */
  
  if(debugopt) {
    
    printf("\n*************** SAVE PARTITION ***************\n\n");
    
    /* mesh A */
    char partfileA[255];
    sprintf(partfileA,"%s.part%d", meshfileA, npartsA);
    printf("Saving mesh partition file: %s\n", partfileA);
    savePartition(meshA.nb_cells, partA, partfileA);
    
    /* mesh B */
    char partfileB[255];
    sprintf(partfileB,"%s.part%d", meshfileB, npartsB);
    printf("Saving mesh partition file: %s\n", partfileB);
    savePartition(meshB.nb_cells, partB, partfileB);
  }
  
  /* 7) VTK output*/
  
  if(vtkopt) {
    
    printf("\n*************** VTK OUTPUT FILES ***************\n\n");
    
    addMeshVariable(&meshA, "partA", CELL_INTEGER, 1, partA);
    addMeshVariable(&meshB, "partB", CELL_INTEGER, 1, partB);
    
    if(vtkshiftz != 0.0) {
      printf("Shifting mesh B on Z for VTK output: %.2f\n", vtkshiftz);  
      shiftMesh(&meshB, 0.0, 0.0, vtkshiftz);
      // for HIPC14 output
      // printf("Rotating mesh B around Y axis (180 deg) for VTK output\n");  
      // rotateMesh(&meshB, 0.0, 180.0, 0.0);
      // shiftMesh(&meshB, 1.0, 0.0, 1.0);
    }
  
    // interedges
    char * outfileCPL = "cpl.vtk";    
    Mesh cpl;  
    generateCouplingMesh(&meshA, &meshB, nbinteredges, interedges, 0.0, &cpl); 
    printf("Saving VTK mesh: %s\n", outfileCPL);
    saveVTKMesh(outfileCPL, &cpl);
    freeMesh(&cpl);
    
    // mesh
    char * outfileA = "meshA.vtk";
    printf("Saving VTK mesh: %s\n", outfileA);
    saveVTKMesh(outfileA, &meshA);
    
    char * outfileB = "meshB.vtk";
    printf("Saving VTK mesh: %s\n", outfileB);
    saveVTKMesh(outfileB, &meshB);
    
    if(vtkopt > 1) {
      // graph
      char * outfileGA = "graphA.vtk";
      printf("Saving VTK mesh: %s\n", outfileGA);
      double * centersA = malloc(meshA.nb_cells*3*sizeof(double));
      cellCenters(&meshA, centersA);
      Mesh GA; 
      graph2LineMesh(&gA, centersA, &GA);
      saveVTKMesh(outfileGA, &GA);
      
      char * outfileGB = "graphB.vtk";
      printf("Saving VTK mesh: %s\n", outfileGB);
      double * centersB = malloc(meshB.nb_cells*3*sizeof(double));
      cellCenters(&meshB, centersB);
      Mesh GB; 
      graph2LineMesh(&gB, centersB, &GB);
      saveVTKMesh(outfileGB, &GB);
      
      free(centersA);
      free(centersB);          
    }
    
    // quotient
    /* char * outfileQA = "quotientA.vtk"; */
    /* printf("Saving VTK mesh: %s\n", outfileQA); */
    /* Graph qA; */
    /* quotientGraph(&gA, npartsA, partA, &qA); */
    /* double * centersQA = calloc(npartsA*3, sizeof(double)); */
    /* for(int i = 0 ; i < gA.ntvxs ; i++) { */
    /*   int pa = partA[i]; */
    /*   centersQA[pa*3] += centersA[i*3]; */
    /*   centersQA[pa*3+1] += 0; */
    /*   centersQA[pa*3+2] += 0; */
    /* } */
    /* Mesh QA;  */
    /* graph2Mesh(&qA, centersQA, &QA); */
    /* saveVTKMesh(outfileGA, &QA); */    
    // free(centersQA);
  }
  
  
  /* 8) free */
  freeGraph(&gA);
  freeGraph(&gB);
  freeMesh(&meshA);
  freeMesh(&meshB);
  free(partA);
  free(partB);
  free(interedges);
  if(weights) free(weights);  
  
  return EXIT_SUCCESS;
  
}
