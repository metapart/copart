/* BENCHMARK for MxN REPART */
/* Author(s): clement.vuchener@inria.fr ; aurelien.esnard@labri.fr */

#include "metapart.h"
#include "ext.h"
#include "parallelpart.h"

#ifdef PARALLEL
#include <mpi.h>
#endif

// #define _GNU_SOURCE
#include <sys/types.h>
#include <signal.h>
#include <setjmp.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <getopt.h>
#include <assert.h>
#include <time.h>
#include <limits.h>
#include <stdlib.h>

#define MIGCOST 10
#define REPARTMULT 1
#define UBFACTOR 1

/* *********************************************************** */

struct stats_t {
  int niter;
  int *g_cut,
      *hg_cut;
  double *imbalance;
  int *mig,
      *max_send,
      *max_recv,
      *max_v,
      *ncomms,
      *max_z;
  unsigned int *seeds;
  double *time;
  int *failure;
};
  
void init_stats (struct stats_t *s, int niter);
void free_stats (struct stats_t *s);
void print_stats (struct stats_t *s, const char *name, int printminmax);

struct method {
  const char *name;
  void (*func) (Graph *, Hypergraph *, int *, int, int *, int, int, int, const void *);
  const void *options;
};

static void repartitioning (Mesh *m,
			    Graph *g,
			    Hypergraph *hg,
			    int *interedges,
			    int *partA,
			    int M, 
			    int N,
			    struct method *method,
			    int repartmult,
			    int migcost,
			    struct stats_t *s,
			    int iter,
			    int printcommmatrix,
			    void (*addMeshPartition)(Mesh *mesh, const char *id, int nb_components, int *data)
			    );

static void repart_Zoltan (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);
static void repart_ParMetis (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);
static void repart_ScratchG (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);
static void repart_ScratchHG (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);
static void repart_LBC2 (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);
static void repart_Scotch (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options);

void diagRepartitioning (Graph *g, Hypergraph *hg, int M, int *partA, int N, int *partB, int *placB, int *comms,
			 struct stats_t *s, int iter);
static void _printCommMatrix (int M, int N, int *intercomms, int *placB);
void remap_part (int M, int N, int *plac, int n, int *part);
void saveMatrix (int *placB, int *comms, int M, int N, const char *prefix, const char *name, int iter);

void logResults (FILE *output, struct stats_t *s, const char *title);

void catch_abort (int signum);
jmp_buf try_buf;


/* *********************************************************** */

#define XSTR(s) STR(s)
#define STR(s) #s

const char *usage =
  "Usage: %s [-w] [-n N] [-c] [-v] [-i niter] [-a seed] [-o outfile.vtk] mesh.txt partition.txt M\n"  
  "\n"
  " -v\tVerbose mode.\n"
  " -m\tPrint standard deviation, minium and maximum in results\n"
  " -c\tPrint communication matrices.\n"
  " -n N\tChange the number of processors of the target partition.\n"
  " -p\tPartition input mesh and save it in partition.txt (instead of loading initial mesh partition).\n"
  " -w [file]\tUnbalance initial partition by chnaging vertex weights before repartitioning.\n"
  " -i niter\tDo niter partitioning/repartitioning then print the average results.\n"
  " -r repartmult\tSet repartition multiplier (default is " XSTR(REPARTMULT) ").\n"
  " -g migcost\tSet migration cost (default is " XSTR(MIGCOST) ").\n"
  " -o outfile.vtk\tSave mesh partitions for VTK in outfile.vtk\n"
  " -a seed\tSet the random generator seed\n"
  " -s\tInput mesh is a Scotch graph basename (.grf and .xyz), instead of internal mesh format (mesh.txt).\n"
  " -h\tInput is a Rutherford-Boeing graph\n"
  " -b\tInput is a Matrix Market graph\n"
  "\n\n"
  "Nota Bene: This program requires to be launched with 'mpirun' on at least 2 nodes.\n\n"
  ;


/* *********************************************************** */
/*                       Method list                           */
/* *********************************************************** */

struct lbc2repart_options {
  enum RepartScheme repart_scheme;
  enum CommScheme comm_scheme;
  const void *options;
} repart_1d = { FIXEDVTXS, B1D, NULL },
  repart_ub1d = { FIXEDVTXS, UB1D, NULL },
  repart_HM = { FIXEDVTXS, HM, NULL },
  repart_HB = { FIXEDVTXS, GREEDYHB2, &GHB_NO_DIAG },
  repart_HBD = { FIXEDVTXS, GREEDYHB2, &GHB_OPT_DIAG },
  repart_difftv = { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_TOTALV },
  repart_diffmv = { DIFFUSION, OPTIMIZE, &GREEDYDEGREE_MAXV },
  repart_diffmm = { DIFFUSION_MM, OPTIMIZE, &GREEDYDEGREE_TOTALV };
  

struct method methods[] = {
  { "Scratch_G", repart_ScratchG, NULL },
  // { "HMK", repart_HMK },
  // { "Scratch_HG", repart_ScratchHG, NULL },
  { "Zoltan", repart_Zoltan, NULL },
  { "ParMetis", repart_ParMetis, NULL },
  { "Scotch", repart_Scotch, NULL },
  // { "1D", repart_LBC2, &repart_1d,
  { "UB1D", repart_LBC2, &repart_ub1d },
  // { "HM", repart_HM },
  { "HB", repart_LBC2, &repart_HB },
  { "HBD", repart_LBC2, &repart_HBD },
  { "DIFFTV", repart_LBC2, &repart_difftv },
  { "DIFFMV", repart_LBC2, &repart_diffmv },
  { NULL, NULL, NULL }
};

/* *********************************************************** */

int main (int argc, char *argv[]) {
  int rank, size;
  MPI_Init (NULL, NULL);
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);
  
  int do_initial_partition = 0;
  int verbose = 0;
  int M, N = -1;
  int unbalance = -1;
  int printcommmatrix = 0;
  int niter = 1;
  int migcost = MIGCOST;
  int repartmult = REPARTMULT;
  char outfile[64] = "out.vtk";
  enum InputFormat {
    MESH,
    SCOTCH,
    RB,
    MM
  } input_format = MESH;
  int printminmax = 0;
  
  int seed = 0; //time (NULL);
  
  int opt;
  while ((opt = getopt (argc, argv, "shbcmpvw:n:i:r:g:o:a:")) != -1) {
    switch (opt) {
    case 'a':
      seed = atoi (optarg);
      break;

    case 'p':
      do_initial_partition = 1;
      break;

    case 'v':
      verbose = 1;
      break;

    case 'm':
      printminmax = 1;
      break;
    
    case 'w':
      unbalance = strtol (optarg, NULL, 10);
      break;
    
    case 'c':
      printcommmatrix = 1;
      break;
    
    case 'n':
      N = atoi (optarg);
      break;
      
    case 'i':
      niter = atoi (optarg);
      break;
      
    case 'o':
      strncpy (outfile, optarg, sizeof(outfile));
      break;
    
    case 'r':
      repartmult = atoi (optarg);
      break;
    
    case 'g':
      migcost = atoi (optarg);
      break;
    
    case 's':
      input_format = SCOTCH;
      break;
    
    case 'h':
      input_format = RB;
      break;
    
    case 'b':
      input_format = MM;
      break;
    
    default:
      if (rank == 0) {
	fprintf (stderr, usage, argv[0]);
      }
      MPI_Finalize ();
      exit (EXIT_FAILURE);
    }
  }
  
  if(argc - optind < 3) {
    if (rank == 0) fprintf (stderr, usage, argv[0]);
    MPI_Finalize ();
    exit (EXIT_FAILURE);
  }
  
  srand (seed);
  if (verbose && rank == 0) printf ("### root seed: %d\n", seed);
  
  char *input = argv[optind];
  const char *partitionfile = argv[optind+1];
  M = atoi (argv[optind+2]);
  if (N == -1) N = M;
  
  Mesh m;
  Graph g;
  Hypergraph hg;
  void (*addMeshPartition)(Mesh *mesh, const char *id, int nb_components, int *data);
  int *interedges;
  if (rank == 0) {
    if (verbose)
      printf("### loading mesh input: %s\n", input);
    if (input_format == SCOTCH) {
      char filename[256];
      snprintf (filename, sizeof(filename), "%s.grf", input);
      loadScotchGraph (filename, &g);
      double *coords = malloc (3*g.nvtxs * sizeof(double));
      snprintf (filename, sizeof(filename), "%s.xyz", input);
      loadScotchCoordinates (filename, g.nvtxs, coords);
      graph2Mesh (&g, coords, &m);
      free (coords);
      /*graph2HypergraphV (&g, &hg);*/
      graph2HypergraphE (&g, &hg);
      addMeshPartition = addMeshNodeVariableI;
    }
    else if (input_format == RB) {
      loadRBGraph (input, &g);
      graph2HypergraphE (&g, &hg);
      addMeshPartition = NULL;
    }
    else if (input_format == MM) {
      loadMMGraph (input, &g);
      graph2HypergraphE (&g, &hg);
      addMeshPartition = NULL;
    }
    else {
      loadMesh (input, &m);
      mesh2Graph (&m, &g);
      /*mesh2Hypergraph (&m, &hg);*/
      graph2HypergraphE (&g, &hg);
      addMeshPartition = addMeshCellVariableI;
    }
    g.vwgts = malloc (g.nvtxs * sizeof(int));
    hg.vwgts = malloc (hg.nvtxs * sizeof(int));
    for (int i = 0; i < g.nvtxs; i++)
      g.vwgts[i] = hg.vwgts[i] = 1;
    
    interedges = malloc (2 * hg.nvtxs * sizeof(int));
    for (int i = 0; i < g.nvtxs; i++)
      interedges[2*i] = interedges[2*i+1] = i;
  }
  
  int *partA, *partB;
  
  int method_count = 0;
  while (methods[method_count].name != NULL)
    method_count++;
  
  struct stats_t *stats = malloc (method_count * sizeof (struct stats_t));
  unsigned int *seeds = malloc (niter * sizeof (unsigned int));
  if (rank == 0) {
    for (int i = 0; i < method_count; i++) {
      init_stats (&stats[i], niter);
      stats[i].seeds = seeds;
    }
    for (int i = 0; i < niter; i++)
      seeds[i] = rand ();
  }
  MPI_Bcast (seeds, niter, MPI_INT, 0, MPI_COMM_WORLD);
  
  
  if (rank == 0) {
    partA = malloc (g.nvtxs * sizeof (int));
    partB = malloc (g.nvtxs * sizeof (int));

    /* do initial partition */
    if(do_initial_partition) {
      if (verbose)
        printf("### partitioning in %d parts...\n", M);
      srand(0);
      int initial_edgecut = 0;
      partitionGraph(&g, M, UBFACTOR, partA, &initial_edgecut, NULL);    
      if (verbose)
	printf("### saving initial partition: %s\n", partitionfile);
      savePartition (g.nvtxs, partA, partitionfile);      
    } 
    /* load initial partition */
    else {
      if (verbose)
	printf("### loading initial partition: %s\n", partitionfile);
      loadPartition (partitionfile, g.nvtxs, partA);
    }
    if (verbose)
      printGraphDiagnostic(&g, M, partA);      
  }

  for (int iter = 0; iter < niter; iter++) {

    if (rank == 0) {
      srand (seeds[iter]);

      if(iter == 0)
	if (addMeshPartition != NULL)
	  addMeshPartition (&m, "partA", 1, partA);
      
      if (unbalance >= 0) {
	if (verbose)
	  printf("### unbalancing initial partition\n");
	if (!g.vwgts)
	  g.vwgts = malloc (g.nvtxs * sizeof (int));
	unbalancePartitionLinear (g.vwgts, g.nvtxs, M, partA, unbalance);
	if (!hg.vwgts)
	  hg.vwgts = malloc (hg.nvtxs * sizeof (int));
	memcpy (hg.vwgts, g.vwgts, g.nvtxs * sizeof (int));
	if (verbose)
	  printGraphDiagnostic (&g, M, partA);
	if(iter == 0)	
	  if (addMeshPartition != NULL)
	    addMeshPartition (&m, "vwgts", 1, hg.vwgts);
      }
      
    }

    /* repartitioning... */      
    for (int i = 0; i < method_count; i++) {

      if (rank == 0 && verbose)
	printf("### repartitioning from %d to %d parts with method %s (%d/%d)...\n", M, N, methods[i].name, iter, niter-1);
      
      srand (seeds[iter]); // to enforce random seed in case of some partitionner change it...
      
      struct sigaction sa, old_sa;
      sa.sa_handler = catch_abort;
      sigemptyset (&sa.sa_mask);
      sa.sa_flags = 0;
      sigaction (SIGABRT, &sa, &old_sa);
      if (setjmp (try_buf) == 1) {
	sigaction (SIGABRT, &old_sa, NULL);
	fprintf (stderr, "%s failed at iter %d with seed %d\n", methods[i].name, iter, seeds[iter]);
	stats[i].failure[iter] = 1;
	continue;
      }
      
      repartitioning (&m, &g, &hg, interedges, partA, M, N, &methods[i], repartmult, migcost, &stats[i], iter, printcommmatrix, addMeshPartition);
      
      sigaction (SIGABRT, &old_sa, NULL);
    }
          
  }  

  if (rank == 0 && verbose) printf("### end of repart exp.\n");

  if (rank == 0 && addMeshPartition != NULL) {
    if (verbose)
      printf("### saving VTK output mesh %s (first iteration only)...\n", outfile);
    saveVTKMesh (outfile, &m);
  }

  if (rank == 0) {
    printf ("%12s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", "Method", "CutG", "CutHG", "Imbal.", "Mig", "MaxSend", "MaxRecv", "MaxV", "Comms", "MaxZ", "Time");
    for (int i = 0; i < method_count; i++)
      print_stats (&stats[i], methods[i].name, printminmax);
    
    FILE *logfile = fopen ("comp-repart.log", "w");
    for (int i = 0; i < method_count; i++)
      logResults (logfile, &stats[i],  methods[i].name);
    fclose (logfile);
    
    
    free (partA);
    free (partB);
    freeGraph (&g);
    freeHypergraph (&hg);
    if (addMeshPartition)
      freeMesh (&m);
    free (interedges);
    for (int i = 0; i < method_count; i++)
      free_stats (&stats[i]);
    free (stats);
  }
  
  MPI_Finalize ();
  return EXIT_SUCCESS;
}
  
void init_stats (struct stats_t *s, int niter) {
  s->niter = niter;
  s->g_cut = calloc (niter, sizeof (int));
  s->hg_cut = calloc (niter, sizeof (int));
  s->imbalance = calloc (niter, sizeof (double));
  s->mig = calloc (niter, sizeof (int));
  s->max_send = calloc (niter, sizeof (int));
  s->max_recv = calloc (niter, sizeof (int));
  s->max_v = calloc (niter, sizeof (int));
  s->ncomms = calloc (niter, sizeof (int));
  s->max_z = calloc (niter, sizeof (int));
  s->time = calloc (niter, sizeof (double));
  s->failure = calloc (niter, sizeof (int));
}

void free_stats (struct stats_t *s) {
  free (s->g_cut);
  free (s->hg_cut);
  free (s->imbalance);
  free (s->mig);
  free (s->max_send);
  free (s->max_recv);
  free (s->max_v);
  free (s->ncomms);
  free (s->max_z);
  free (s->time);
  free (s->failure);
}

void catch_abort (int signum) {
  longjmp (try_buf, 1);
}

static void repartitioning (Mesh *m,
			    Graph *g,
			    Hypergraph *hg,
			    int *interedges,
			    int *partA,
			    int M, 
			    int N,
			    struct method *method,
			    int repartmult,
			    int migcost,
			    struct stats_t *s,
			    int iter,
			    int printcommmatrix,
			    void (*addMeshPartition)(Mesh *mesh, const char *id, int nb_components, int *data)
			    ) 
{
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  int *comms;
  int *perm = malloc (N * sizeof (int));
  int *partB = malloc (g->nvtxs * sizeof(int));
  
  double start = MPI_Wtime ();
  method->func (g, hg, partA, M, partB, N, repartmult, migcost, method->options);
  double end = MPI_Wtime ();
  
  if (rank == 0) {
    s->time[iter] = end - start;
    comms = malloc (M * N * sizeof (int));
    computeIntercomm (g->nvtxs, g->vwgts, M, partA, N, partB, g->nvtxs, interedges, 0, comms);
    remap (M, N, comms, perm);
    
    diagRepartitioning (g, hg, M, partA, N, partB, perm, comms,
			s, iter);
    /*saveMatrix (perm, comms, M, N, "./comms/", name, iter);*/
  }
  
  if (iter == 0 && rank == 0) {
    if (printcommmatrix) {
      printf ("%8s\n", method->name);
      _printCommMatrix (M, N, comms, perm);
    }
    remap_part (M, N, perm, hg->nvtxs, partB);
    if (addMeshPartition != NULL)
      addMeshPartition (m, method->name, 1, partB);
    free (comms);
  }
}

static void repart_Zoltan (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    memcpy (partB, partA, hg->nvtxs * sizeof (int));
    partitionHypergraphParZoltan (hg, N, 1, 1, 1, partB, &cut, MPI_COMM_SELF);
  }
}

static void repart_ParMetis (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0)
    memcpy (partB, partA, g->nvtxs * sizeof (int));
  partitionGraphParMetis (g, N, UBFACTOR, 1.0, partB, &cut, MPI_COMM_WORLD);
  /*partitionGraphParMetis (g, N, UBFACTOR, 1000000.0, partB, &cut, MPI_COMM_WORLD); /* Cut */
  /*partitionGraphParMetis (g, N, UBFACTOR, 0.000001, partB, &cut, MPI_COMM_WORLD); /* Migration */
}

static void repart_ScratchHG (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    partitionHypergraph (hg, N, 1, partB, &cut, NULL);
  }
}

static void repart_ScratchG (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    partitionGraph (g, N, UBFACTOR, partB, &cut, NULL);
  }
}

static void repart_LBC2 (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  
  const struct lbc2repart_options *opt = options;
  if (rank == 0) {

    /* char *oldgpartenv = NULL; */
    /* if (opt->repart_scheme == FIXEDVTXS) { */
    /*   oldgpartenv = getenv("GPARTITIONER"); */
    /*   setenv ("GPARTITIONER", "SCOTCHK", 1); */
    /* } */
    repartitionGraph (g, M, partA, UBFACTOR, repartmult, migcost, N, opt->repart_scheme, opt->comm_scheme, opt->options, partB, &cut, NULL, NULL, 0, NULL, "SCOTCHK");

    /* if (opt->repart_scheme == FIXEDVTXS) { */
    /*   if (oldgpartenv) */
    /* 	setenv ("GPARTITIONER", oldgpartenv, 1); */
    /*   else */
    /* 	unsetenv ("GPARTITIONER"); */
    /* } */
  }
}

static void repart_HMK (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int cut;
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    // char * oldgpartenv = getenv("GPARTITIONER");
    // setenv("GPARTITIONER", "KGGGPFM", 1);

    repartitionGraph (g, M, partA, UBFACTOR, repartmult, migcost, N, FIXEDVTXS, HM, NULL, partB, &cut, NULL, NULL, 0, NULL, "KGGGPML");

    // if(oldgpartenv) setenv("GPARTITIONER", oldgpartenv, 1); else unsetenv("GPARTITIONER"); // BUG ?????
  }
}

static void repart_Scotch (Graph *g, Hypergraph *hg, int *partA, int M, int *partB, int N, int repartmult, int migcost, const void *options) {
  int rank;
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  if (rank == 0) {
    SCOTCH_Graph sg;
    SCOTCH_graphInit (&sg);
    SCOTCH_graphBuild (&sg, 0, g->nvtxs, g->xadj, g->xadj+1, g->vwgts, NULL, 2*g->nedges, g->adjncy, g->ewgts);
    SCOTCH_graphCheck (&sg);  
    
    SCOTCH_Strat strat;
    SCOTCH_stratInit (&strat);
    SCOTCH_stratGraphMapBuild (&strat, SCOTCH_STRATBALANCE | SCOTCH_STRATQUALITY | SCOTCH_STRATREMAP, N, 0.01);
    int *oldpart = partA;
    if (M > N) {
      oldpart = malloc (g->nvtxs * sizeof (int));
      memcpy (oldpart, partA, g->nvtxs * sizeof (int));
      for (int i = 0; i < g->nvtxs; i++)
	if (oldpart[i] > N)
	  oldpart[i] = -1;
    }
    SCOTCH_graphRepart (&sg, N, oldpart, 1.0, NULL, &strat, partB);
    if(M > N)
      free (oldpart);
    
    SCOTCH_stratExit (&strat);
    SCOTCH_graphExit (&sg);
  }
}

static void print_mean_sd_i (int *data, int *failure, int count, int prec, int printminmax) {
  double mean = 0.0;
  double min = INFINITY;
  double max = -INFINITY;
  int nsamples = 0;
  for (int i = 0; i < count; i++) {
    if (!failure[i]) {
      mean += data[i];
      if (data[i] < min)
	min = data[i];
      if (data[i] > max)
	max = data[i];
      nsamples++;
    }
  }
  mean /= nsamples;
  
  double sd = 0.0;
  for (int i = 0; i < count; i++)
    if (!failure[i])
      sd += (mean - data[i])*(mean - data[i]);
  sd /= nsamples;
  sd = sqrt (sd);

  if(printminmax)  
    printf ("\t%1$.*5$f(%2$.*5$f;%3$.*5$f;%4$.*5$f)", mean, sd, min, max, prec);
  else
    printf ("\t%.*f", prec, mean);
}

static void print_mean_sd_d (double *data, int *failure, int count, int prec, int printminmax) {
  double mean = 0.0;
  double min = INFINITY;
  double max = -INFINITY;
  int nsamples = 0;
  for (int i = 0; i < count; i++) {
    if (!failure[i]) {
      mean += data[i];
      if (data[i] < min)
	min = data[i];
      if (data[i] > max)
	max = data[i];
      nsamples++;
    }
  }
  mean /= nsamples;
  
  double sd = 0.0;
  for (int i = 0; i < count; i++)
    if (!failure[i])
      sd += (mean - data[i])*(mean - data[i]);
  sd /= nsamples;
  sd = sqrt (sd);
  
  if(printminmax)
    printf ("\t%1$.*5$g(%2$.*5$g;%3$.*5$g;%4$.*5$g)", mean, sd, min, max, prec);
  else
    printf ("\t%.*g", prec, mean);
}

void print_stats (struct stats_t *s, const char *name, int printminmax) {
  printf ("%12s", name);
  
  print_mean_sd_i (s->g_cut, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->hg_cut, s->failure, s->niter, 1, printminmax);
  print_mean_sd_d (s->imbalance, s->failure, s->niter, 3, printminmax);
  print_mean_sd_i (s->mig, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->max_send, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->max_recv, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->max_v, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->ncomms, s->failure, s->niter, 1, printminmax);
  print_mean_sd_i (s->max_z, s->failure, s->niter, 1, printminmax);
  print_mean_sd_d (s->time, s->failure, s->niter, 3, printminmax);
  
  int nfailure = 0;
  for (int i = 0; i < s->niter; i++)
    if (s->failure[i])
      nfailure ++;
  if (nfailure > 0)
    printf ("\t%d/%d failed!", nfailure, s->niter);
  
  printf ("\n");
}

void diagRepartitioning (Graph *g, Hypergraph *hg, int M, int *partA, int N, int *partB, int *placB, int *comms,
			 struct stats_t *s, int iter) {
  s->g_cut[iter] = computeGraphEdgeCut (g, N, partB);
  s->hg_cut[iter] = computeHypergraphEdgeCut (hg, N, partB);
  s->mig[iter] = 0;
  int part_send[M];
  int part_recv[N];
  int part_size[N];
  int hg_size = 0;
  memset (part_send, 0, M * sizeof(int));
  memset (part_recv, 0, N * sizeof(int));
  memset (part_size, 0, N * sizeof(int));
  for (int i = 0; i < hg->nvtxs; i++) {
    if (partA[i] != placB[partB[i]]) {
      s->mig[iter]++;
      part_send[partA[i]]++;
      part_recv[partB[i]]++;
    }
    part_size[partB[i]] += hg->vwgts[i];
    hg_size += hg->vwgts[i];
  }
  
  s->max_send[iter] = 0;
  for (int i = 0; i < M; i++) {
    if (part_send[i] > s->max_send[iter])
      s->max_send[iter] = part_send[i];
  }
  s->max_recv[iter] = 0;
  int max_part = 0;
  for (int i = 0; i < N; i++) {
    if (part_recv[i] > s->max_recv[iter])
      s->max_recv[iter] = part_recv[i];
    if (part_size[i] > max_part)
      max_part = part_size[i];
  }  
  
  s->ncomms[iter] = 0;
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++) {
      if (comms[i*N+j] != 0) {
	s->ncomms[iter]++;
      }
    }
  }
  
  s->max_v[iter] = INT_MIN;
  s->max_z[iter] = INT_MIN;
  for (int p = 0; p < M || p < N; p++) {
    int z = 0, v = 0;
    if (p < N)
      for (int i = 0; i < M; i++)
	if (i != p && comms[i*N+p] > 0) {
	  v += comms[i*N+p];
	  z ++;
	}
    if (p < M)
      for (int j = 0; j < N; j++)
	if (j != p && comms[p*N+j] > 0) {
	  v += comms[p*N+j];
	  z ++;
	}
    if (v > s->max_v[iter])
      s->max_v[iter] = v;
    if (z > s->max_z[iter])
      s->max_z[iter] = z;
  }
  
  s->imbalance[iter] = ((double)max_part)/((double)hg_size/N);
  /*printf ("\t%d\t%2f\t%d\t%d\t%d", cut, ((double)max_part)/((double)hg_size/N), mig, max_send, max_recv);*/
}

static void _printCommMatrix (int M, int N, int *intercomms, int *placB) {
  
  for (int i = 0; i < N; i++)
    printf ("%8d", placB[i]);
  printf ("\n");
  
  printf ("\n");
     
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++)
      if (i == placB[j])
	printf ("\x1b[31m%8d\x1b[0m", intercomms[i*N + j]);
      else
	printf ("%8d", intercomms[i*N + j]);
    printf ("\n");
  }
    printf ("\n");
}

void remap_part (int M, int N, int *plac, int n, int *part) {
  int p[N];
  memcpy (p, plac, N*sizeof(int));
  int k = M;
  for (int i = 0; i < N; i++)
    if (p[i] == -1)
      p[i] = k++;
  for (int i = 0; i < n; i++)
    part[i] = p[part[i]];
}

void saveMatrix (int *placB, int *comms, int M, int N, const char *prefix, const char *name, int iter) {
  char filename[256];
  snprintf (filename, 256, "%scomms_%s_%d.txt", prefix, name, iter);
  FILE *f = fopen (filename, "w");
  for (int i = 0; i < N; i++)
    fprintf (f, "%8d", placB[i]);
  fprintf (f, "\n");
  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; j++)
      fprintf (f, "%8d", comms[i*N + j]);
    fprintf (f, "\n");
  }
  fclose (f);
}

void logResults (FILE *output, struct stats_t *s, const char *title) {
  fprintf (output, "*** %s ***\n", title);
  fprintf (output, "%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s%12s\n",
	    "Seed",
	    "Cut (G)",
	    "Cut (HG)",
	    "Imbalance",
	    "Migration",
	    "Max send",
	    "Max recv",
	    "MaxV",
	    "Comms",
	    "MaxZ",
	    "Time");
  for (int i = 0; i < s->niter; i++) {
    fprintf (output, "%12d", s->seeds[i]);
    fprintf (output, "%12d", s->g_cut[i]);
    fprintf (output, "%12d", s->hg_cut[i]);
    fprintf (output, "%12.3lf", s->imbalance[i]);
    fprintf (output, "%12d", s->mig[i]);
    fprintf (output, "%12d", s->max_send[i]);
    fprintf (output, "%12d", s->max_recv[i]);
    fprintf (output, "%12d", s->max_v[i]);
    fprintf (output, "%12d", s->ncomms[i]);
    fprintf (output, "%12d", s->max_z[i]);
    fprintf (output, "%12.1lg", s->time[i]);
    fprintf (output, "\n");
  }
}
