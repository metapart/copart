/* Communication Model for MxN repart */
/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <limits.h>

// #define DEBUG
#include "modelcomm.h"
#include "matrixcomm.h"
#include "misc.h"
#include "libgraph.h"

/* #include "sort.h" */
/* #include "io.h" */
/* #include "debug.h" */
/* #include "hypergraph.h" */
/* #include "graph.h" */

/* *********************************************************** */

#define MIN2(a, b)	((a) < (b) ? (a) : (b))
#define MIN3(a, b, c)	(MIN2(a, b) < (c) ? MIN2(a, b) : (c))

// #define DRAND() (drand48())   // dont use it: different seed as rand()
#define DRAND() (rand()*1.0/RAND_MAX)


const struct ghb_options
  GHB_NO_DIAG = { 0 },
  GHB_OPT_DIAG = { 1 };

const struct opt_options
  GREEDYDEGREE_TOTALV = { MIG_TOTAL_V, GREEDYDEGREE, NULL },
  GREEDYDEGREE_MAXV = { MIG_MAX_V, GREEDYDEGREE, NULL },
  GREEDYDEGREE_TOTALZ = { MIG_TOTAL_Z, GREEDYDEGREE, NULL },
  GREEDYDEGREE_MAXZ = { MIG_MAX_Z, GREEDYDEGREE, NULL };

/* *********************************************************** */

static void comm_Zoltan (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_1D (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_UB1D (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_HM (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_UBHM (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_GREEDYHB(int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_GREEDYHB2(int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_GREEDYHBP(int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_GREEDYDEGREE(int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_OPTIMIZE(int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);
static void comm_BF (int M, int N, int ubfactor, Graph *quotient, const void *options, int *modelmat);

/* *********************************************************** */

/* weights: array of size M */
void saveHypergraphModel(char * filename, const int * m, int M, int N, int * weights) 
{
  Hypergraph modelhg;
  dense2sparse (m, M, N, &modelhg); // dual hg
  assert(modelhg.nhedges == M);
  if(weights) {
    modelhg.hewgts = malloc(M*sizeof(int));
    memcpy(modelhg.hewgts, weights, M*sizeof(int));
  }
  saveHypergraph (filename, &modelhg);  
  freeHypergraph(&modelhg);  
}

/* *********************************************************** */

void communicationModel (int M,	        
			 int N,	
			 int ubfactor, 
			 Graph *quotient,
			 enum CommScheme scheme,
			 const void *options,
			 int skipremap,
			 int *modelmat) 
{ 
  assert(modelmat);
  memset(modelmat, 0, M*N*sizeof(int));
  
  if (scheme == ZOLTAN) {
    comm_Zoltan ( M, N, ubfactor, quotient, options, modelmat);
  }
  /* Initial partition partition is balanced, optimize communication count */
  else if (scheme == B1D) {
    comm_1D ( M, N, ubfactor, quotient, options, modelmat);
  }
  /* Use 1d scheme on unbalanced partition */
  else if (scheme == UB1D) {
    comm_UB1D ( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == HM) {
    comm_HM ( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == UBHM) {
    comm_UBHM ( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == GREEDYHB) {
    comm_GREEDYHB ( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == GREEDYHB2) {
    comm_GREEDYHB2 ( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == GREEDYHBP) {
    comm_GREEDYHBP( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == GREEDYDEGREE) {
    comm_GREEDYDEGREE( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == OPTIMIZE) {
    comm_OPTIMIZE( M, N, ubfactor, quotient, options, modelmat);
  }
  else if (scheme == BRUTEFORCE) {
    comm_BF ( M, N, ubfactor, quotient, options, modelmat);
  }
  else {
    fprintf (stderr, "communicationModel: invalid scheme.\n");
    exit(EXIT_FAILURE);
  }
  
  /* Skip remapping for some schemes */
  if (skipremap || scheme == GREEDYDEGREE || scheme == OPTIMIZE) return;

  /* remapping is more tricky... skip it */
  if (M > N) return; 

  /* Remapping of model matrix */  
  int *perm = malloc (N * sizeof (int));
  remap (M, N, modelmat, perm);
  if (M < N) { /* for new parts */
    int p = M;
    for (int i = 0; i < N; i++)
      if (perm[i] == -1)
	perm[i] = p++;
  }
  
  /* apply remapping to model matrix */
  int * _modelmat = malloc(M*N*sizeof(int));
  memcpy(_modelmat, modelmat, M*N*sizeof(int));
  for (int i = 0; i < M; i++)
    for (int j = 0; j < N; j++)
      modelmat[i*N + perm[j]] = _modelmat[i*N + j];
  
  free(_modelmat);
  free(perm);
}

/* *********************************************************** */

int matching_sa (Hypergraph *h, Graph *g, int *perm, double Tinit, double Tend, double cooling) {
  int npass = 1;
  /*printf ("niter = %d\n", (int)ceil(log(Tend/Tinit)/log(cooling))*npass);*/
  
  assert (h->nvtxs == g->nvtxs);
  
  int M = h->nvtxs;
  // warning int N = h->nhedges;
  
  for (int i = 0; i < M; i++)
    perm[i] = i;
  
  int *p1 = calloc(M, sizeof(int));
  int *p2 = calloc(M, sizeof(int));
  
  int scoremax, score1, score2;
  
  for (int i = 0; i < npass; i++) {
    memcpy (p1, perm, M*sizeof(int));
    scoremax = score1 = matchingScore(h, g, perm);
    
    double T = Tinit;
    while (T > Tend) {
      memcpy (p2, p1, M*sizeof(int));
      int k = 0;
      while (DRAND() < pow((T/Tinit)/1.01, k/2)) {
	/* Permute random i and j */
	int i = rand()%M;
	int j = rand()%M;
	int tmp = p2[i];
	p2[i] = p2[j];
	p2[j] = tmp;
	
	k++;
      }
      
      score2 = matchingScore(h, g, p2);
      if (score2 > score1 || DRAND() < exp((score2-score1)/T)) {
	memcpy (p1, p2, M*sizeof(int));
	score1 = score2;
	if (score1 > scoremax) {
	  memcpy (perm, p1, M*sizeof(int));
	  scoremax = score1;
	}
      }
      T *= cooling;
    }
  }
  
  free (p1);
  free (p2);
  
  return scoremax;
}

/* *********************************************************** */

static void comm_Zoltan (int M,	int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  Hypergraph modelhg;
  int min = (M < N ? M : N);
  createIdentityMatrix (min, NULL, &modelhg); // todo: should return a matrix ?
  modelhg.nvtxs = N;  
  
  // convert model hypergraph in model matrix
  sparse2dense(&modelhg, modelmat); 
  computeComms(modelmat, M, N, quotient->vwgts);    
  freeHypergraph (&modelhg);    
}

/* *********************************************************** */

static void comm_1D (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  Hypergraph modelhg;
  int *neworder = malloc(M*sizeof(int));
  reorderPartitionQuotientGraph (quotient, neworder); 
  create1dRepartMatrix (M, N, neworder, &modelhg);    
  free (neworder);
  
  // convert model hypergraph in model matrix
  sparse2dense(&modelhg, modelmat); 
  computeComms(modelmat, M, N, quotient->vwgts);    
  freeHypergraph (&modelhg);    
}

/* *********************************************************** */

static void comm_UB1D (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  Hypergraph modelhg;
  modelhg.nvtxs = N;
  modelhg.nhedges = M;
  modelhg.vwgts = modelhg.hewgts = NULL;
  
  int *neworder = malloc(M*sizeof(int));
  reorderPartitionQuotientGraph (quotient, neworder);    
  
  int *neworder_inv = malloc (M*sizeof(int));
  for (int i = 0; i < M; i++)
    neworder_inv[neworder[i]] = i;
  
  int s[M+1]; /* s[i] weight of all parts before i */
  s[0] = 0;
  for (int i = 1; i < M+1; i++) {
    s[i] = s[i-1] + quotient->vwgts[neworder_inv[i-1]];
  }
  
  modelhg.eptr = malloc ((M+1) * sizeof (int));
  modelhg.eptr[0] = 0;
  for (int i = 0; i < M; i++) {
    modelhg.eptr[i+1] = modelhg.eptr[i] +
      ceil (s[neworder[i]+1] * (double)N/s[M]) -
      floor (s[neworder[i]] * (double)N/s[M]);
  }
  
  modelhg.eind = malloc (modelhg.eptr[M] * sizeof (int));
  
  int k = 0;
  for (int i = 0; i < M; i++) {
    for (int j = floor (s[neworder[i]] * (double)N/s[M]);
	 j < ceil (s[neworder[i]+1] * (double)N/s[M]);
	 j++) {
      modelhg.eind[k++] = j;
    }
  }
  
  free (neworder);
  
  // convert model hypergraph in model matrix
  sparse2dense(&modelhg, modelmat); 
  computeComms(modelmat, M, N, quotient->vwgts);    
  freeHypergraph (&modelhg);    
}

/* *********************************************************** */

static void comm_HM (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  Hypergraph modelhg;
  
  /* Non permuted hypergraph */
  createDiag1dMatrix (M, N, NULL, &modelhg); // todo: should return a matrix ?
  
  /* Matching */
  Hypergraph dual;
  dualHypergraph (&modelhg, &dual);
  int *p = malloc(M*sizeof(int));
  matching_sa (&dual, quotient, p, 1.0, 0.05, 0.99);
  freeHypergraph (&dual);
  freeHypergraph (&modelhg);
  
  /* Permuted hypergraph */
  createDiag1dMatrix (M, N, p, &modelhg);  // todo: should return a matrix ?
  
  free (p);
  
  // convert model hypergraph in model matrix
  sparse2dense(&modelhg, modelmat); 
  computeComms(modelmat, M, N, quotient->vwgts);    
  freeHypergraph (&modelhg);    
}

/* *********************************************************** */

static void comm_UBHM (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  Hypergraph modelhg;
  
  // savePartition ( "HM_partition");
  
  /* Non permuted hypergraph */
  createDiag1dMatrix (M, N, NULL, &modelhg);   // todo: should return a matrix ?
  
  /* Matching */
  Hypergraph dual;
  dualHypergraph (&modelhg, &dual);
  int *p = malloc(M*sizeof(int));
  matching_sa (&dual, quotient, p, 1.0, 0.05, 0.99);
  freeHypergraph (&dual);
  
  /* Permuted hypergraph */
  createDiag1dMatrix (M, N, p, &modelhg);
  free (p);
  
  int *m = malloc (M * N * sizeof (int));
  sparse2dense (&modelhg, m);
  computeComms (m, M, N, quotient->vwgts);
  
  int counter = 0;
  char filename[256];
  snprintf (filename, 256, "HM_hg%03d.txt", counter);
  saveHypergraph (filename, &modelhg);
  
  int *coord = malloc (M*N * 4 * sizeof (int)); /* TODO: smaller than M*N ? */
  int *score = malloc (M*N * sizeof (int));
  int *id = malloc (M * sizeof (int));
  for (int i = 0; i < M; i++)
    id[i] = i;
  
  dualHypergraph (&modelhg, &dual);
  fprintf (stderr, "Initial score = %d\n", matchingScore (&dual, quotient, id));
  /* Use simple transformations to remove negative values */
  while (1) {
    int n = 0;
    for (int i = 0; i < M; i++)
      for (int j = 0; j < N; j++)
	if (m[i*N + j] < 0)
	  for (int i2 = 0; i2 < M; i2++)
	    if (i2 != i && m[i2*N + j] > 0)
	      for (int j2 = 0; j2 < N; j2++)
		if (j2 != j && m[i*N + j2] > 0) {
		  coord[4*n] = i;
		  coord[4*n+1] = j;
		  coord[4*n+2] = i2;
		  coord[4*n+3] = j2;
		  int d = MIN3(-m[i*N + j], m[i2*N + j], m[i*N + j2]);
		  t1 (m, N, i, j, i2, j2, d);
		  dense2sparse (m, M, N, &modelhg);
		  dualHypergraph (&modelhg, &dual);
		  score[n] = matchingScore (&dual, quotient, id);
		  if (j2 < M)
		    score[n] -= 0;
		  t1 (m, N, i, j, i2, j2, -d);
		  fprintf (stderr, "score (%d, %d) (%d, %d) = %d\n", i, j, i2, j2, score[n]);
		  n++;
		}
    if (n == 0)
      break;
    
    int min = 0;
    for (int k = 1; k < n; k++)
      if (score[k] > score[min])
	min = k;
    int i = coord[4*min];
    int j = coord[4*min+1];
    int i2 = coord[4*min+2];
    int j2 = coord[4*min+3];
    printCommMatrix (m, M, N);
    fprintf (stderr, "t1 (%d, %d) (%d, %d)\n", i, j, i2, j2);
    t1 (m, N, i, j, i2, j2, MIN3(-m[i*N + j], m[i2*N + j], m[i*N + j2]));
    
    dense2sparse (m, M, N, &modelhg);
    fprintf (stderr, "%d comms\n", modelhg.eptr[modelhg.nhedges]);
    char filename[256];
    counter++;
    snprintf (filename, 256, "HM_hg%03d.txt", counter);
    saveHypergraph (filename, &modelhg);
  }
  free (score);
  free (coord);
  free (id);
  
  printCommMatrix (m, M, N);
  dense2sparse (m, M, N, &modelhg);
  saveHypergraph ("HM_hg_after.txt", &modelhg);
  freeHypergraph (&modelhg);    
  
  // fprintf (stderr, "nb comms in model = %d\n", modelhg.eptr[modelhg.nhedges]);
  
  memcpy(modelmat, m, M*N*sizeof(int));
  free (m);
}

/* *********************************************************** */

static void updateDegrees(Graph* quotient, int M, int* weights, int * degrees) 
{  
  memset(degrees, 0, M*sizeof(int));
  for (int i = 0; i < M; i++) 
    for (int k = quotient->xadj[i]; k < quotient->xadj[i+1]; k++) {
      int ii = quotient->adjncy[k]; /* i neighbor of ii */	    
      if(weights[i] > 0 && weights[ii] > 0) degrees[i]++;
    }
}

/* *********************************************************** */

/* static void diagHB(Graph* quotient, const int *m, int M, int N) { */
/*   assert(quotient->vwgts && m); */
  
/*   int * weights = calloc (M, sizeof (int)); */
/*   for (int i = 0; i < M; i++) */
/*     for (int j = 0; j < N; j++)  */
/*       weights[i] += m[i*N + j]; */
/*   int * degrees = calloc (M, sizeof (int)); */
/*   updateDegrees(quotient, M, weights, degrees); */
  
/*   for (int i = 0; i < M; i++) { // M lines */
/*     for (int j = 0; j < N; j++)  // N columns */
/*       printf ("%8d", m[i*N + j]); */
/*     printf (RED "%8d (%d)" BLACK "\n", quotient->vwgts[i]-weights[i], degrees[i]);  */
/*   } */
  
/*   // check sum */
/*   for (int j = 0; j < N; j++) { */
/*     int sum = 0; */
/*     for (int i = 0; i < M; i++) sum += m[i*N + j]; */
/*     printf (RED "%8d" BLACK, sum); */
/*   } */
  
/*   printf ("\n"); */
  
/*   free(weights); */
/*   free(degrees); */
/* } */


/* *********************************************************** */

static void comm_GREEDYHBP(int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {  
  comm_GREEDYHB (M, N, 0, quotient, options, modelmat); /* no tolerance, perfect */
}

/* *********************************************************** */

// #define SCORE2 1.0

static void comm_GREEDYHB (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) 
{
  // check
  assert(quotient->vwgts && quotient->ewgts);
  
  int total_weight = 0;
  for (int i = 0; i < M; i++)
    total_weight += quotient->vwgts[i];
  int tw = total_weight; /* remaining total weight */
  int tolerance = total_weight/N*ubfactor/100.0;  
  
  int *weights = calloc (M, sizeof (int));
  memcpy(weights, quotient->vwgts, M * sizeof (int));  
  int *m = calloc (M*N, sizeof (int));
  int *degrees = calloc (M, sizeof (int));
  updateDegrees(quotient, M, weights, degrees);
  
  // debug
  /* printf("=> initial weights {"); */
  /* for(int r = 0 ; r < M ; r++) printf(" %d ", weights[r]); */
  /* printf(" }\n"); */
  /* printf("=> initial degrees {"); */
  /* for(int r = 0 ; r < M ; r++) printf(" %d ", degrees[r]); */
  /* printf(" }\n"); */
  
  int e = 0; /* hyperedge index */  
  
#ifdef DEBUG    
  /* save hypergraph model */
  int counter = 0;
  char filename[256];
  snprintf (filename, 256, "HB_hg%03d.txt", counter);
  fprintf (stderr, "writing %s\n", filename);
  saveHypergraphModel(filename, m, M, N, weights);
  
  printf("-> communication model (initial)\n");
  // maria diagHB(quotient, m, M, N);   
#endif
  
  /* 1) Build trivial hyperedges to optimize migration (hyperedge of size 1) */
  
  for (int v = 0; v < M; v++) {
    int w0 = total_weight/N + ((total_weight%N > e)?1:0); 
    int w = w0;
    if (weights[v] >= (w-tolerance)) { 
      int _w = 0;
      _w = MIN2(w+tolerance, weights[v]);
      m[v*N + e] = _w;
      // printf("-> building hyperedge %d: vertex %d, weight %d\n", e, v, _w);
      // diagHB(quotient, m, M, N); 
      
      weights[v] -= _w;
      tw -=  _w;
      e++;
      
      /* update degrees */
      updateDegrees(quotient, M, weights, degrees);
      
      // debug
      /* printf("=> remaining weights {"); */
      /* for(int r = 0 ; r < M ; r++) printf(" %d ", weights[r]); */
      /* printf(" }\n"); */
      /* printf("=> remaining degrees {"); */
      /* for(int r = 0 ; r < M ; r++) printf(" %d ", degrees[r]); */
      /* printf(" }\n"); */      
      
    }
    
  }
  
#ifdef DEBUG  
  printf("-> communication model (trivial steps)\n");
  // maria diagHB(quotient, m, M, N); 
  
  /* save hypergraph model */
  counter++;
  snprintf (filename, 256, "HB_hg%03d.txt", counter);
  fprintf (stderr, "writing %s\n", filename);
  saveHypergraphModel(filename, m, M, N, weights);
  
  // printf("=> perfect weight: %d (+/- %d)\n", total_weight/N, tolerance);
  // printf("=> add trivial hyperedeges: %d\n", e);
  // printf("=> remaining total weight: %d\n", tw);
  
#endif
  
  /* 2) Build remaining hyperedges with a greedy algorithm based on quotient graph (hyperedge of size >= 1) */
  
  for (; e < N; e++) {
    
    int k = 0;        /* step index */
    int heset[M];     /* hyperedge set */
    int hesize = 0;   /* hyperedge size */
    int heweight = 0; /* hyperedge weight */
    
    int w0 = total_weight/N + ((total_weight%N > e)?1:0); /* perfect balanced weight (TODO: perfect weight may be different due to the tolerance in step 1) */
    int w = w0; /* remaining weight to find for current hyperedge */
    
    /* 2.1) select first vertex with minimum degree & weight for a new hyperedge e */
    int v = -1;
    int minw = INT_MAX;
    int mind = INT_MAX;
    
    for (int i = 0; i < M; i++) {
      
      if (weights[i] > 0 && degrees[i] <= mind) {
	
	if (degrees[i] > 0 && degrees[i] < mind) { 
	  v = i;
	  mind = degrees[i];
	  minw = weights[i];
	}
	else if(weights[i] < minw) { // all degrees can be 0 at end...
	  v = i;
	  mind = degrees[i]; 
	  minw = weights[i];
	}
      }
      
    }
    
    if(v == -1) break; // no more weights due to tolerance (next hyperedges will be empty !!!)
    // assert(v >= 0); 
    
    assert(minw != INT_MAX && minw > 0);
    assert(mind != INT_MAX && mind >= 0);
    // int _w = MIN2(w, weights[v]);  
    int _w = MIN2(w+tolerance, weights[v]); 
    heset[hesize++] = v; 
    heweight += _w;      
    // printf("-> hyperedge %d at step %d: weight %d / %d (+/- %d), select vertex %d with weight %d / %d\n", e, k, heweight, w0, tolerance, v, _w, weights[v]);    
    m[v*N + e] = _w;
    
#ifdef DEBUG  
    printf("-> building hyperedge %d: vertex %d, weight %d\n", e, v, _w);
    // maria diagHB(quotient, m, M, N); 
#endif
    
    w -= _w;
    weights[v] -= _w;
    tw -=  _w;
    // assert(w >= 0 && tw >= 0);    
    
#ifdef DEBUG  
    /* save hypergraph model */
    counter++;
    snprintf (filename, 256, "HB_hg%03d.txt", counter);
    fprintf (stderr, "writing %s\n", filename);
    saveHypergraphModel(filename, m, M, N, weights);
#endif
    
    
    /* 2.2) select next vertices to complete hyperedge e with weight w0  */
    while (w > 0 && tw > 0) {  
      
      if(heweight >= (w0-tolerance)) { 
	printf("WARNING: skip to next hyperedge!\n");
	break; // dont enlarge current hyperedge if not absolutly required!
      }
      
      /* w: target weight of current hyperedge */ 
      /* tw: total weight */       
      v = -1;
      
      /* Choose another vertex */
      double score = -INFINITY;
      for (int v2 = 0; v2 < M; v2++) { /* v2 is the next candidate for hyperedge e */
	if (weights[v2] > 0) {
	  double s = 0.0; /* The score is the sum of edges between v2 and vertices (v1) in e */
	  int n = 0;
	  
	  for (int i = quotient->xadj[v2]; i < quotient->xadj[v2+1]; i++) {
	    int v1 = quotient->adjncy[i]; /* v1 neighbor of v2 */	    
	    if (m[v1*N + e] > 0) {  /* v1 in e */
	      s += quotient->ewgts[i];
	      n ++;
	    } 
	  }
	  s = 2*s*n/((hesize+1)*hesize);
	  
	  
#ifdef SCORE2
	  /* additional score that tries to keep the remaining graph as connected as possible... */
	  if (weights[v2] <= w) {
	    double s2min = INFINITY;
	    for (int i = quotient->xadj[v2]; i < quotient->xadj[v2+1]; i++) {
	      int v1 = quotient->adjncy[i]; /* v1 neighbor of v2 */
	      double s2 = 0.0;
	      for (int ii = quotient->xadj[v1]; ii < quotient->xadj[v1+1]; ii++) {
		int v3 = quotient->adjncy[ii]; /* v3 neighbor of v1 */
		if (v3 != v2 && weights[v3] > 0) 
		  s2 += quotient->ewgts[ii];		
	      }
	      if(s2 < s2min) s2min = s2;	      
	    }
	    assert(s2min != INFINITY);
	    s2min *= SCORE2;
	    // printf("hyperedge %d:  [step %d] candidate %d => score %.2f, additional score %.2f \n", e, k, v2, s, s2min);
	    s += s2min; 
	  }
#endif
	  
	  /* update best score */
	  if (s > score) {
	    score = s;
	    v = v2;
	  }
	  
	}
      }
      
      /* select vertex v if better score */
      assert(v >= 0);
      // int _w = MIN2(w, weights[v]);
      int _w = MIN2(w+tolerance, weights[v]);
      heset[hesize++] = v;
      heweight += _w;
      // printf("-> hyperedge %d at step %d: weight %d / %d (+/- %d), select vertex %d with weight %d / %d\n", e, k, heweight, w0, tolerance, v, _w, weights[v]);    
      m[v*N + e] = _w;
      
#ifdef DEBUG  
      printf("-> building hyperedge %d: vertex %d, weight %d\n", e, v, _w);
      // maria diagHB(quotient, m, M, N); 
#endif
      
      weights[v] -= _w;
      w -= _w;
      tw -=  _w;
      // assert(w >= 0 && tw >= 0);
      
      k++; // next candidate
      
      /* update degrees */
      updateDegrees(quotient, M, weights, degrees);            
      
#ifdef DEBUG  
      /* save hypergraph model */
      counter++;
      snprintf (filename, 256, "HB_hg%03d.txt", counter);
      fprintf (stderr, "writing %s\n", filename);
      saveHypergraphModel(filename, m, M, N, weights);
#endif
      
    } // end of main while loop
    
    // debug
    /* printf("-> hyperedge %d: { ", e); */
    /* for(int r = 0 ; r < hesize ; r++) printf("%d ",heset[r]); */
    /* printf("}, size %d, weight %d / %d (+/- %d)\n", hesize,  heweight, w0, tolerance); */
    /* printf("=> remaining weights {"); */
    /* for(int r = 0 ; r < M ; r++) printf(" %d ", weights[r]); */
    /* printf(" }\n"); */
    /* printf("=> remaining degrees {"); */
    /* for(int r = 0 ; r < M ; r++) printf(" %d ", degrees[r]); */
    /* printf(" }\n"); */
    
  }
  
  // debug
#ifdef DEBUG  
  printf("=> final communication model... \n");
  // maria diagHB(quotient, m, M, N); 
#endif
  
  // check
  for(int r = 0 ; r < M ; r++) assert(weights[r] == 0);
  assert(checkCommMatrix(m, M, N));    
  
  // score
  // Hypergraph modelhg, dual;
  // dense2sparse (m, M, N, &modelhg);
  // dualHypergraph (&modelhg, &dual);
  // int score = matchingScore (&dual, quotient, NULL);
  // fprintf (stderr, "nb comms in model = %d\n", modelhg.eptr[modelhg.nhedges]);  
  // fprintf (stderr, "Best matrix, score = %d\n", score);
  // freeHypergraph(&modelhg);    
  // freeHypergraph(&dual);    
  
  // return
  memcpy(modelmat, m, M*N*sizeof(int));
  
  free(m);
  free(weights);
}

/* *********************************************************** */

/** Test if subset weight is near perfect weight
 * \return Number of final part corresponding to the weight, 0 if no solution is found. */
static int testSubsetWeight (int subset_wgt, int subset_size, int M, int N, int wfinal, double ubfactor) {
  int tol = wfinal * ubfactor;
  
  PRINT ("Subset size = %d, wgt = %d\n", subset_size, subset_wgt);
  
  int i, imin, imax;
  if (M < N)
    imin = subset_size, imax = subset_size + N - M;
  else
    imin = subset_size + N - M, imax = subset_size;
  for (i = imin; i <= imax; i++) {
    PRINT ("Compared with %d (%d, %d)\n", i*wfinal, i*wfinal - tol, i*wfinal + tol);
    if (subset_wgt < i*wfinal - tol)
      return 0;
    if (subset_wgt < i*wfinal + tol)
      return i;
  }
  return 0;
}

/** Compute the score for adding \p vtx in \p he in the subgraph \p quotient restricted to \p subset.
 * \return Score */
static int computeNewVertexScore (Graph *quotient, int *subset, int *he, int vtx) {
  int new_wdegree = 0;
  int new_con = 0;
  int new_ndegree = 0;
  int new_nedges = 0;
  for (int e = quotient->xadj[vtx]; e < quotient->xadj[vtx+1]; e++) {
    int v2 = quotient->adjncy[e];
    if (subset[v2] || he[v2]) {
      new_wdegree += quotient->ewgts[e];
      new_ndegree ++;
    }
    if (he[v2]) {
      new_con += quotient->ewgts[e];
      new_nedges ++;
    }
  }  
  
  int con = 0;
  int nedges = 0;
  for (int v = 0; v < quotient->nvtxs; v++) {
    if (he[v] || v == vtx) {
      for (int e = quotient->xadj[v]; e < quotient->xadj[v+1]; e++) {
	int v2 = quotient->adjncy[e];
	if (he[v2] || v2 == vtx) {
	  con += quotient->ewgts[e];
	  nedges++;
	}
      }
    }
  }
  
  PRINT ("new_degree = %d, new_con = %d, new_ndegree = %d, new_nedges = %d, con = %d, nedges = %d\n", new_wdegree, new_con, new_ndegree, new_nedges, con, nedges);
  
  if (new_con * new_nedges == 0)
    return 0;
  return (new_con * new_nedges * nedges * con) / (new_wdegree * new_ndegree);
}

/** Find a pseudo peripheral vertex with low degree in the subgraph of \p quotient restricted to \p subset
 * \return A vertex */
static int pseudoPeripheralVertex (Graph *quotient, int *subset) {
  int vtx = -1;
  int best_dist = 0, best_degree = INT_MAX;
  int current_ecc, new_ecc = 0;
  
  /* Select a vertex */
  for (int i = 0; i < quotient->nvtxs; i++)
    if (subset[i]) {
      vtx = i;
      break;
    }
  
  assert (vtx != -1);
  
  int *tab = malloc (quotient->nvtxs * sizeof (int));
  int *disttab = malloc (quotient->nvtxs * sizeof (int));
  int iter = 0;
  do {
    iter ++;
    current_ecc = new_ecc;    
    
    /* Breadth first search from current vertex
     * Select a new vertex with max distance and min degree */;
    best_dist = 0;
    int start = 0, end = 1; /* Start and end index in tab */
    memset (disttab, 0, quotient->nvtxs * sizeof (int));
    tab[0] = vtx;
    disttab[vtx] = 1;
    
    int u;
    while (start != end) {
      u = tab[start++];
      
      int degree = 0;
      
      for (int e = quotient->xadj[u]; e < quotient->xadj[u+1]; e++) {
	int v = quotient->adjncy[e];
	if (subset[v]) {
	  /*degree++;*/
	  degree += quotient->ewgts[e];
	  if (!disttab[v]) {
	    disttab[v] = disttab[u] + 1;
	    tab[end++] = v;
	  }
	}
      }
      
      if (disttab[u] > best_dist || degree < best_degree) {
	vtx = u;
	best_dist = disttab[u];
	best_degree = degree;
      }   
    }
    new_ecc = disttab[u];
  } while (new_ecc > current_ecc);
  
  free (tab);
  free (disttab);
  PRINT ("pseudoPeripheralVertex: %d iters\n", iter);
  
  return vtx;
}

#ifdef DEBUG
static int hb2_subset_num;
#endif

/** Build a communication sub-hypergraph from former parts in \p subset to \p n new parts (starting with \p first). */
static void buildHypergraphSubset (int M, int N, int *subset, int first, int n, int ubfactor, Graph *quotient, int opt_diag, int* modelmat) {
  int i, he;
  int total_wgt = 0;
  int subset_size = 0;
  int *oldpart = malloc (M * sizeof (int));
  int *oldpart_wgt = calloc (M, sizeof (int));
  int *hetab = malloc (M * sizeof (int));
  for (i = 0; i < M; i++)
    if (subset[i]) {
      oldpart[subset_size] = i;
      subset_size++;
      oldpart_wgt[i] = quotient->vwgts[i];
      total_wgt += quotient->vwgts[i];
    }
  
  he = 0;
  
#ifdef DEBUG
  int counter = 0;
  char filename[256];
#endif
  
  if (opt_diag) {
    /* Build 1-hyperedges in order to optimize migration */
    for (i = 0; i < subset_size && he < n; i++) {
      int wfinal = total_wgt/n + (total_wgt%n > he ? 1 : 0);
      int vtx = oldpart[i];
      if (oldpart_wgt[vtx] >= wfinal) {
	modelmat[vtx*N + first+he] = wfinal;
	oldpart_wgt[vtx] -= wfinal;
	he++;
      }      
      
#ifdef DEBUG  
      /* save hypergraph model */
      counter++;
      snprintf (filename, 256, "HB2_hg%03d-%03d.txt", hb2_subset_num, counter);
      fprintf (stderr, "writing %s\n", filename);
      saveHypergraphModel(filename, modelmat, M, N, oldpart_wgt);
#endif
    }
  }
  
  
  /* Build remaining hyperedges */
  for (; he < n; he++) {
    int wfinal = total_wgt/n + (total_wgt%n > he ? 1 : 0);
    
    memset (hetab, 0, M * sizeof (int));
    int wpart = 0;
    /* Search first vertex */
    int vtx = pseudoPeripheralVertex (quotient, oldpart_wgt);
    hetab[vtx] = 1;
    int w = MIN2 (oldpart_wgt[vtx], wfinal);
    wpart += w;
    modelmat[vtx*N + first+he] = w;
    oldpart_wgt[vtx] -= w;
    
#ifdef DEBUG  
    /* save hypergraph model */
    counter++;
    snprintf (filename, 256, "HB2_hg%03d-%03d.txt", hb2_subset_num, counter);
    fprintf (stderr, "writing %s\n", filename);
    saveHypergraphModel(filename, modelmat, M, N, oldpart_wgt);
#endif
    
    //int he_con = 0; /* Sum of weights of edges in hyperedge */
    //int he_nedges = 0; /* Number of edges in hyperedge */
    
    while (wpart < wfinal) {
      int best_vtx = -1, best_score = INT_MIN;
      //int best_con, new_con; /* Sum of weight of new edges in he */
      //int best_nedges, new_nedges; /* Number of new edges in he */
      
      for (i = 0; i < subset_size; i++) {
	vtx = oldpart[i];
	if (oldpart_wgt[vtx] == 0)
	  continue;
	//new_con = 0;
	//new_nedges = 0;
	//for (int e = quotient->xadj[vtx]; e < quotient->xadj[vtx+1]; e++)
	//  if (hetab[quotient->adjncy[e]]) {
	//    new_con += quotient->ewgts[e];
	//    new_nedges++;
	//  }
	/* Matching score
	 * no need to divide by max number of edges as it is the same in every case */
	//int score = (he_nedges + new_nedges) * (he_con + new_con);
	
	int score = computeNewVertexScore (quotient, subset, hetab, vtx);
	PRINT ("Score (%d) = %d\n", vtx, score);
	if (score > best_score) {
	  best_score = score;
	  best_vtx = vtx;
	  //best_con = new_con;
	  //best_nedges = new_nedges;
	}
      }
      //he_con += best_con;
      //he_nedges += best_nedges;
      
      hetab[best_vtx] = 1;
      int w = MIN2 (oldpart_wgt[best_vtx], wfinal-wpart);
      wpart += w;
      modelmat[best_vtx*N + first+he] = w;
      oldpart_wgt[best_vtx] -= w;
      
#ifdef DEBUG  
      /* save hypergraph model */
      counter++;
      snprintf (filename, 256, "HB2_hg%03d-%03d.txt", hb2_subset_num, counter);
      fprintf (stderr, "writing %s\n", filename);
      saveHypergraphModel(filename, modelmat, M, N, oldpart_wgt);
#endif
    }
  }
  
  for (int i = 0; i < M; i++)
    if (subset[i])
      assert (oldpart_wgt[i] == 0);
  
  free (oldpart);
  free (oldpart_wgt);
  free (hetab);
}

static void comm_GREEDYHB2 (int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  int opt_diag = (options ? ((struct ghb_options *)options)->opt_diag : 0); /* Default value is 0 */
  /*
   * Decompose former parts in subsets before building communication hypergraph
   * for each subset in order to minimize the number of communications.
   */
  memset (modelmat, 0, M * N * sizeof (int));
  int current_he = 0;
  
  int *is_free, *subset;
  int free_vtxs, subset_size;
  is_free = calloc (M, sizeof (int));
  subset = calloc (M, sizeof (int));
  
#ifdef DEBUG
  hb2_subset_num = 0;
  int counter = 0;
  char filename[256];
  Hypergraph subsets;
  subsets.nvtxs = M;
  subsets.nhedges = 0;
  subsets.eptr = malloc ((M+1) * sizeof (int));
  subsets.eptr[0] = 0;
  subsets.eind = malloc (M * sizeof (int));
  subsets.vwgts = subsets.hewgts = NULL;
#endif
  
#define SELECT_VTX(v) is_free[v] = 0, subset[v] = 1, free_vtxs--, subset_size++, subset_wgt += quotient->vwgts[v]
  
  int total_wgt = 0;
  for (int i = 0; i < M; i++)
    total_wgt += quotient->vwgts[i];
  
  for (int i = 0; i < M; i++)
    is_free[i] = 1;
  free_vtxs = M;
  
  while (free_vtxs > 0) {
    /* Build a subset */
    int subset_wgt = 0;
    memset (subset, 0, M * sizeof (int));
    subset_size = 0;
    int n;
    
#ifdef DEBUG
    hb2_subset_num ++;
    counter = 0;
#endif
    
    int vtx = pseudoPeripheralVertex (quotient, is_free);
    SELECT_VTX(vtx);
    
#ifdef DEBUG  
    /* save subset */
    counter++;
    snprintf (filename, 256, "HB2_subset%03d-%03d.txt", hb2_subset_num, counter);
    fprintf (stderr, "writing %s\n", filename);
    subsets.nhedges = hb2_subset_num;
    {
      int i, j;
      for (i = 0, j = 0; i < M; i++)
	if (subset[i])
	  subsets.eind[subsets.eptr[hb2_subset_num-1]+(j++)] = i;
    }
    subsets.eptr[hb2_subset_num] = subsets.eptr[hb2_subset_num-1] + subset_size;
    saveHypergraph (filename, &subsets);
#endif
    
    //int ss_con = 0; /* Sum of weights of edges in subset */
    //int ss_nedges = 0; /* Number of edges in subset */
    
    while (!(n = testSubsetWeight (subset_wgt, subset_size, M, N, total_wgt/N, ubfactor/100.0)) && free_vtxs > 0) {
      /* Test each neighbor vertex. If no good subset is found, add the most connected vertex in the current subset */
      int best_vtx = -1, best_score = INT_MIN, best_eq = 0;
      //int best_con, best_nedges;
      for (vtx = 0; vtx < M; vtx++) {
	if (!is_free[vtx])
	  continue;
	/* Compute connectivity */
	//int new_con = 0; /* Sum of weight of new edges in he */
	int new_nedges = 0; /* Number of new edges in he */
	for (int e = quotient->xadj[vtx]; e < quotient->xadj[vtx+1]; e++)
	  if (subset[quotient->adjncy[e]]) {
	    //new_con += quotient->ewgts[e];
	    new_nedges++;
	  }
	
	
	/* Test subset */
	int eq = 0;
	if (new_nedges > 0 && testSubsetWeight (subset_wgt+quotient->vwgts[vtx], subset_size+1, M, N, total_wgt/N, ubfactor/100.0)) {
	  eq = 1;
	}
	
	//int score = (ss_nedges + new_nedges) * (ss_con + new_con);
	int score = computeNewVertexScore (quotient, is_free, subset, vtx);
	PRINT ("Score (%d) = %d\n", vtx, score);
	if (eq > best_eq || (eq == best_eq && score > best_score)) {
	  best_score = score;
	  best_vtx = vtx;
	  best_eq = eq;
	  //best_con = new_con;
	  //best_nedges = new_nedges;
	}
      }
      if (vtx < M) /* A good subset was found */
	break;
      
      /* Select best connectivity */
      SELECT_VTX(best_vtx);
#ifdef DEBUG  
      /* save subset */
      counter++;
      snprintf (filename, 256, "HB2_subset%03d-%03d.txt", hb2_subset_num, counter);
      fprintf (stderr, "writing %s\n", filename);
      subsets.nhedges = hb2_subset_num;
      {
	int i, j;
	for (i = 0, j = 0; i < M; i++)
	  if (subset[i])
	    subsets.eind[subsets.eptr[hb2_subset_num-1]+(j++)] = i;
      }
      subsets.eptr[hb2_subset_num] = subsets.eptr[hb2_subset_num-1] + subset_size;
      saveHypergraph (filename, &subsets);
#endif
      //ss_con += best_con;
      //ss_nedges += best_nedges;
    }
    if (n == 0) {
      PRINT ("Warning: wrong subset weight\n");
      n = N - current_he;
    }
    
#ifdef DEBUG
    fprintf (stderr, "Subset (size = %d, wgt = %d):", subset_size, subset_wgt);
    for (int i = 0; i < M; i++)
      fprintf (stderr, " %d", subset[i]);
    fprintf (stderr, "\n");
#endif
    buildHypergraphSubset (M, N, subset, current_he, n, ubfactor, quotient, opt_diag, modelmat);
    current_he += n;
  }
  
  free (is_free);
  free (subset);
#ifdef DEBUG
  freeHypergraph (&subsets);
#endif
}

/* *********************************************************** */

static int ntests;
static void comm_BF_rec (int M, int N, Graph *quotient, int *weights, int total_weight, int e, int w, int *m, int *best_matrix, int *best_score) 
{
  if (w == 0) {
    e++;
    w = total_weight/N;
    if (total_weight%N > e)
      w++;
  }
  
  if (e == N) {
    Hypergraph comms, dual;
    dense2sparse (m, M, N, &comms);
    dualHypergraph (&comms, &dual);
    int score = matchingScore (&dual, quotient, NULL);
    ntests ++;
    freeHypergraph (&comms);
    freeHypergraph (&dual);
    
    if (score > *best_score) {
      *best_score = score;
      memcpy (best_matrix, m, M*N*sizeof (int));
    }
  }
  else {
    /*fprintf (stderr, "%d %d\n", e, w);
      printCommMatrix (m, M, N);*/
    for (int v = 0; v < M; v++)
      if (weights[v] > 0 && m[v*N + e] == 0) {
	if (weights[v] < w)
	  m[v*N + e] = weights[v];
	else
	  m[v*N + e] = w;
	
	w -= m[v*N + e];
	weights[v] -= m[v*N + e];
	
	comm_BF_rec (M, N, quotient, weights, total_weight, e, w, m, best_matrix, best_score);
	
	w += m[v*N + e];
	weights[v] += m[v*N + e];
	m[v*N + e] = 0;
      }
  }
}

/* *********************************************************** */

static void comm_BF ( int M, int N, int ubfactor, Graph *quotient, const void *options, int* modelmat) {
  int total_weight = 0;
  for (int i = 0; i < M; i++)
    total_weight += quotient->vwgts[i];
  
  int *weights = malloc (M * sizeof (int));
  memcpy (weights, quotient->vwgts, M * sizeof (int));
  
  int *m = calloc (M*N, sizeof (int));
  int *best_matrix = calloc (M*N, sizeof (int));
  int best_score = INT_MIN;
  
  int e = 0;
  for (int v = 0; v < M; v++) {
    int w = total_weight/N + ((total_weight%N > e)?1:0);
    if (weights[v] >= w) {
      m[v*N + e] = w;
      weights[v] -= w;
      e++;
    }
  }
  
  ntests = 0;
  comm_BF_rec (M, N, quotient, weights, total_weight, e, total_weight/N, m, best_matrix, &best_score);
  
  // debug
  fprintf (stderr, "%d hypergraphs tested\n", ntests);  
  fprintf (stderr, "Best matrix, score = %d\n", best_score);
  // printCommMatrix (best_matrix, M, N);
  // dense2sparse (best_matrix, M, N, comms);
  
  memcpy(modelmat, best_matrix, M*N*sizeof(int));
  
  free (m);
  free (best_matrix);
  free (weights);
}


/* *********************************************************** */
/*               GREEDY DEGREE HEURISTIC                       */
/* *********************************************************** */


#define MAXCYCLESIZE 3
#define MAXVTXS MAXCYCLESIZE
#define MAXEDGES (MAXCYCLESIZE*(MAXCYCLESIZE-1)/2)
#define ECFACTOR 35    /* edge-connection factor (%)*/

typedef struct {
  int nvtxs;           /* nb vertices in cycle */
  int vtxs[MAXVTXS];   /* vertices in cycles, else -1 */
  int nedges;          /* nb edges in cycles */
  int edges[MAXEDGES]; /* edges in cyles, else -1 */
  int vw;              /* weight of vertices in cycle */
  int ew;              /* weight of edges in cycle */
} Cycle;

/* *********************************************************** */

int updateCycleScore(Graph * g, int ncycles, Cycle * cycles, int * score) 
{
  int bestscore = -1;
  int c = -1;
  
  for(int i = 0 ; i < ncycles ; i++) {
    
    cycles[i].vw = cycles[i].ew = 0;
    
    for(int k = 0; k < cycles[i].nvtxs; k++) 
      cycles[i].vw += g->vwgts[cycles[i].vtxs[k]];
    
    for(int k = 0; k < cycles[i].nedges; k++)     
      cycles[i].ew += g->ewgts[cycles[i].edges[k]];
    
    // compute score as sum of weights
    score[i] = cycles[i].vw;
    
    if(score[i] > bestscore) {bestscore = score[i]; c = i; } 
  }
  
  assert(bestscore >= 0 && c >= 0);
  return c; /* cycle index with best score */
}

/* *********************************************************** */

/* find all cycles of size 2 and 3 */
Cycle * findCycles(Graph * g, 
		   int ewlo,       /* [in] ewlo: edge weight lower bound (for edges of graph g) */
		   int * ncycles)  /* [out] nb of cycles */
{
  
  int maxcycles = g->nedges; /* nb of cycles of size 2 */
  Cycle * cycles = calloc(maxcycles, sizeof(Cycle));  
  
  int z = 0, ntests = 0;
  
  // find all cycles
  for(int i = 0 ; i < g->nvtxs ; i++) 
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {      
      int ii = g->adjncy[k];  /* edge (i,ii) */
      
      if(i < ii) {
	
	if(g->ewgts[k] < ewlo) continue;
	
	/* cycle of size 2 (edge) */
    	PRINT("cycle of size 2 found (%d,%d)\n", i, ii);
	cycles[z].nvtxs = 2;
	cycles[z].vtxs[0] = i;
	cycles[z].vtxs[1] = ii;
	cycles[z].vtxs[2] = -1;
	cycles[z].nedges = 1;
	cycles[z].edges[0] = k;
	cycles[z].edges[1] = -1;
	cycles[z].edges[2] = -1;
    	z++;
	ntests++;	      
	
	// realloc
	if(z >= maxcycles)  { maxcycles *= 2; cycles = realloc(cycles, maxcycles*sizeof(Cycle)); }
	
	for(int kk = g->xadj[ii] ; kk < g->xadj[ii+1] ; kk++) {     
	  int iii = g->adjncy[kk];  /* edge (ii,iii) */	  
	  
	  if(ii < iii) {
	    
	    if(g->ewgts[kk] < ewlo) continue;
	    
	    /* test if edge (i,iii) exists... */
	    for(int kkk = g->xadj[i] ; kkk < g->xadj[i+1] ; kkk++) {
	      int iiii = g->adjncy[kkk];  /* edge (i,iiii) */
	      
	      if(iii == iiii) {
		
		if(g->ewgts[kkk] < ewlo) break;
		
		/* cycle of size 3 */
	    	PRINT("cycle of size 3 found (%d,%d,%d)\n", i, ii, iii);
		cycles[z].nvtxs = 3;
		cycles[z].vtxs[0] = i;
		cycles[z].vtxs[1] = ii;
		cycles[z].vtxs[2] = iii;
		cycles[z].nedges = 3;
		cycles[z].edges[0] = k;
		cycles[z].edges[1] = kk;
		cycles[z].edges[2] = kkk;
	    	z++;
		
		// realloc
		if(z >= maxcycles)  { maxcycles *= 2; cycles = realloc(cycles, maxcycles*sizeof(Cycle)); }
		
		break;
	      }	      
	      ntests++;	      
	    }
	  }
	  
	}
      }
    }  
  
  PRINT("=> %d cycles found (%d tests)!\n", z, ntests); 
  
  // free memory
  cycles = realloc(cycles, z*sizeof(Cycle)); 
  
  *ncycles = z;
  return cycles;
}

/* *********************************************************** */

void comm_GREEDYDEGREE(int M, int N, int ubfactor, Graph* g, const void *options, int* model)
{
  assert(M <= N);
  assert(g && model);
  assert(g->nvtxs == M);
  
  // reset model matrix
  memset(model, 0, M*N*sizeof(int));
  
  // statistic
  int meanW = 0, loW = 0, upW = 0;
  for(int i = 0 ; i < M ; i++) meanW += g->vwgts[i];
  meanW /= N;
  loW = meanW*(1.0-ubfactor/100.0);
  upW = meanW*(1.0+ubfactor/100.0);
  PRINT("mean wgt = %d, ubfactor = %d, lb = %d, up =%d\n", meanW, ubfactor, loW, upW);
  
  // compute edge-weight lower bound for edge-connection
  int ewtot = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) 
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {      
      int ii = g->adjncy[k];  /* edge (i,ii) */
      if(i < ii) ewtot += g->ewgts[k];
    }
  int ecfactor = ECFACTOR;
  int ewmean = ewtot / g->nedges;
  int ewlo = ewmean*ecfactor/100; /* lower bound for edge connection */      
  
  fprintf(stderr, "WARNING: comm_GREEDYDEGREE() used ECFACTOR=%d%%. (edge weight > %d)\n", ecfactor, ewlo);
  
  PRINT ("nedges = %d, mean ew = %d,  ecfactor = %d, lower bound = %d\n", g->nedges, ewmean, ecfactor, ewlo);  
  
  
  Graph gg; 
  dupGraph(g, &gg);
  
  /* ========== degree 0 (in-place) ========== */
  
  int j = 0;
  for(int i = 0 ; i < M ; i++) {
    if(gg.vwgts[i] < loW) { // not enough data
      // warning int w = gg.vwgts[i];
      model[i*N+i] = 1; // gg.vwgts[i]; 
      gg.vwgts[i] = 0; 
      PRINT("new part %d (degree 0) built from old part %d (remaining weight %d)\n", j, i, gg.vwgts[i]);     
      PRINT("  -> Warning: not enough load (-%d%%)!!!\n", ((loW-w)*100/loW));
      j++;
    }
    else if(gg.vwgts[i] >= upW) { 
      model[i*N+i] = 1; // upW; 
      gg.vwgts[i] -= upW; 
      PRINT("new part %d (degree 0) built from old part %d (remaining weight %d)\n", j, i, gg.vwgts[i]);
      j++;
    }
    else if(gg.vwgts[i] < upW) { 
      model[i*N+i] = 1; // gg.vwgts[i]; 
      gg.vwgts[i] = 0; 
      PRINT("new part %d (degree 0) built from old part %d (remaining weight %d)\n", j, i, gg.vwgts[i]);
      j++;
    }
  }
  
  if(j == N) return; // STOP
  
  /* ========== degree 1 (graph vertex) ========== */
  
  int i = 0;
  while(i < M && j < N) {
    if(gg.vwgts[i] < loW) {i++; continue; } // not enough data
    else if(gg.vwgts[i] >= upW) { 
      model[i*N+j] = 1; // upW; 
      gg.vwgts[i] -= upW; 
      PRINT("new part %d (degree 1) built from old part %d (remaining weight %d)\n", j, i, gg.vwgts[i]);
      j++; 
    }
    else if(gg.vwgts[i] < upW) { 
      model[i*N+j] = 1; // gg.vwgts[i]; 
      gg.vwgts[i] = 0;       
      PRINT("new part %d (degree 1) built from old part %d (remaining weight %d)\n", j, i, gg.vwgts[i]);
      j++;
    }    
  }
  
  if(j == N) return; // STOP
  
  /* ========== cycles of size 2 and 3  ========== */
  
  /* now, we need to take weight from several former parts (2 or 3) to
     build a new part */
  
  // warning int nedges = gg.nedges;
  int ncycles = 0;
  Cycle * cycles = findCycles(&gg, ewlo, &ncycles);
  assert(cycles && ncycles > 0);
  
  /* initialize score */
  int * score = calloc(ncycles, sizeof(int));    /* score */
  
  while(j < N) { 
    int k = updateCycleScore(&gg, ncycles, cycles, score); // find best score !!!
    int i = cycles[k].vtxs[0], ii = cycles[k].vtxs[1], iii = cycles[k].vtxs[2];
    PRINT("cycle (%d,%d,%d) with best score %d (vw = %d, ew = %d)\n", i, ii, iii, score[k], cycles[k].vw, cycles[k].ew);
    
    /* we empty the 2 or 3 former parts, even if cycles[k].vw > upW */
    if(cycles[k].vw > upW) PRINT("  -> Warning: too much load (+%d%%)!!!\n", (cycles[k].vw-upW)*100/upW);
    if(cycles[k].vw < loW) PRINT("  -> Warning: not enough load (-%d%%)!!!\n", ((loW-cycles[k].vw)*100/loW));    
    
    if(cycles[k].nvtxs == 2) {
      model[i*N+j] = 1; // gg.vwgts[i];    /* i -> j */
      model[ii*N+j] = 1; // gg.vwgts[ii];  /* ii -> j */
      // gg.vwgts[i] = gg.vwgts[ii] = 0;
      gg.vwgts[i] -= (cycles[k].vw/2); if(gg.vwgts[i] < 0) gg.vwgts[i] = 0;
      gg.vwgts[ii] -= (cycles[k].vw/2); if(gg.vwgts[ii] < 0) gg.vwgts[ii] = 0;     
      PRINT("new part %d (degree 2) built from old parts (%d,%d)\n", j, i, ii);    
      j++;
    }
    else if(cycles[k].nvtxs == 3) {
      model[i*N+j] = 1; // gg.vwgts[i];    /* i -> j */
      model[ii*N+j] = 1; // gg.vwgts[ii];  /* ii -> j */
      model[iii*N+j] = 1; // gg.vwgts[iii];  /* iii -> j */
      // gg.vwgts[i] = gg.vwgts[ii] = gg.vwgts[iii] = 0;
      gg.vwgts[i] -= (cycles[k].vw/3); if(gg.vwgts[i] < 0) gg.vwgts[i] = 0;
      gg.vwgts[ii] -= (cycles[k].vw/3); if(gg.vwgts[ii] < 0) gg.vwgts[ii] = 0;     
      gg.vwgts[iii] -= (cycles[k].vw/3); if(gg.vwgts[iii] < 0) gg.vwgts[iii] = 0;           
      PRINT("new part %d (degree 3) built from old parts (%d,%d,%d)\n", j, i, ii, iii);    
      j++;
    }
    
  } 
  
  
  /* free */
  free(cycles);
  free(score);
  freeGraph(&gg);
  
}

/* *********************************************************** */

void comm_OPTIMIZE(int M, int N, int ubfactor, Graph *quotient, const void *options, int *model)
{
  int *model0 = calloc(M*N,sizeof(int));
  communicationModel (M, N, ubfactor, quotient,
		      (options ? ((struct opt_options *)options)->base_scheme : GREEDYDEGREE),
		      (options ? ((struct opt_options *)options)->base_options : NULL),
		      0,
		      model0);
#ifdef DEBUG
  printf("MxN Communication Matrix before optimisation\n");
  printCommMatrix(model0, M, N);
#endif
  optimizeCommunicationModel(M, N, ubfactor, ECFACTOR,
			     (options ? ((struct opt_options *)options)->objective : MIG_TOTAL_V),
			     quotient, model0, model);
  
#ifdef DEBUG
  printf("MxN Communication Matrix after optimisation\n");
  printCommMatrix(model, M, N);
  printf("\n");
#endif
  free(model0);
}

/* *********************************************************** */

void optimizeCommunicationModel(int M, int N, int ubfactor, int ecfactor, 
				int objective, Graph *g, 
				int * modelmat, int * optmat)
{
  assert(M <= N);
  assert(g && modelmat && optmat);
  
  // compute lower bound for edge-connection in quotient graph
  int ewtot = 0;
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {
      int ii = g->adjncy[k];  /* edge (i,ii) */
      if(i < ii) ewtot += g->ewgts[k];
    }
  int ewmean = ewtot / g->nedges;
  int ewlo = ewmean*ecfactor/100; /* lower bound for edge connection */
  PRINT("nedges = %d, mean ew = %d,  ecfactor = %d, lower bound = %d\n", g->nedges, ewmean, ecfactor, ewlo);
  
  // create the new enriched graph gg (with N vertices)
  Graph gg;
  assert(M <= N);
  int gmat[M*M];
  graph2MatW(g, gmat); /* edge weigted */
  int ggmat[N*N];
  memset(ggmat, 0, sizeof(ggmat));
  for(int i = 0 ; i < N ; i++)
    for(int j = 0 ; j < N ; j++) {
      /* keep former edges of g (up to an ecfactor) */
      if(i < j && i < M && j < M && gmat[i*M+j] > ewlo) 
	ggmat[i*N+j] = ggmat[j*N+i] = 1;
      /* add new edges from model */
      if(i < j && i < M && j < N && modelmat[i*N+j] > 0) 
	ggmat[i*N+j] = ggmat[j*N+i] = 1;
    }
  
  mat2Graph(N, ggmat, &gg);
  
  /* fill weight for vertices & edges */
  gg.vwgts = malloc(gg.nvtxs*sizeof(int));
  memset(gg.vwgts, 0, gg.nvtxs*sizeof(int)); // N vertices
  memcpy(gg.vwgts, g->vwgts, g->nvtxs*sizeof(int)); // copy M first weight
  gg.ewgts = malloc(2*gg.nedges*sizeof(int));
  memset(gg.ewgts, 0, 2*gg.nedges*sizeof(int));
  
  // optimize migration volume subject to migration model 
  
  int * migmat = calloc(N*N, sizeof(int));  
  /*fprintf(stderr, "WARNING: migration scheme used is MIG_VTOT\n");
  scheme = MIG_VTOT;
  computeMigrationMatrix(&gg, ubfactor, scheme, migmat);*/
  computeMigrationMatrixLP (&gg, ubfactor, objective, migmat);
  
#ifdef DEBUG
  printf("NxN Migration Matrix\n");
  printCommMatrix(migmat, N, N);
#endif
  
  /* compute MxN comm matrix */
  // int * optmat = calloc(M*N, sizeof(int));
  assert(optmat);
  for(int i = 0 ; i < M ; i++) {
    optmat[i*N+i] = g->vwgts[i]; // set initial weight
    for(int j = 0 ; j < N ; j++) {
      int w = migmat[i*N+j];      
      /* send data from former proc. i (i<M) to newer proc. j (j<N) */
      if(w > 0) { optmat[i*N+j] = w; optmat[i*N+i] -= w; }      
    }
  }
  
#ifdef DEBUG    
  PRINT("MxN Communication Matrix\n");
  printCommMatrix(optmat, M, N);
#endif  
  
  
  /* // compute final graph migg (with N vertices) according to migration matrix...    */
  // Graph * migg = NULL;
  /* if(migg) {     */
  /*   dupGraph(&gg, migg); */
  /*   for(int i = 0 ; i < N ; i++) { */
  /*     for(int j = 0 ; j < N ; j++) { */
  /* 	migg->vwgts[i] -= migmat[i*N+j]; // apply migration (remove data sent and add data receive) !  */
  /*     } */
  /*   } */
  /*   for(int i = 0 ; i < migg->nvtxs ; i++)  */
  /*     for(int k = migg->xadj[i] ; k < migg->xadj[i+1] ; k++) { */
  /* 	int j = migg->adjncy[k];  /\* edge (i,j) *\/ */
  /* 	migg->ewgts[k] = migmat[i*N+j];  */
  /*     }     */
  /* } */
  
  // free
  free(migmat);  
  freeGraph(&gg);  
}


/* *********************************************************** */

int matchingScore (Hypergraph *h, Graph *g, int *perm) {
  int score = 0;
  for (int he = 0; he < h->nhedges; he++) {
    
    int s = 0; /* score of current hyperedge */
    int n = 0; /* nb of quotient edges in current hyperedge */ 
    for (int i1 = h->eptr[he]; i1 < h->eptr[he+1]; i1++) {
      int v1 = h->eind[i1];
      for (int i2 = h->eptr[he]; i2 < h->eptr[he+1]; i2++) {
	int v2 = h->eind[i2];
	for (int i = 0; i < g->nvtxs; i++) { /* TODO: useless loop */
	  for (int e = g->xadj[i]; e < g->xadj[i+1]; e++) {
	    int j = g->adjncy[e];
	    if ((perm && perm[i] == v1 && perm[j] == v2) ||
		(!perm && i == v1 && j == v2)) {
	      s += g->ewgts[e]; /* add edge weight */
	      n++;
	    }
	  }
	}
      }
    }
    int d = h->eptr[he+1] - h->eptr[he];
    if (d > 1) {
      score += 2*s*n/(d*(d-1)); /* sum of edge weigths * nb edges in hyperedge / max possible edges in hyperedge */
    }
    /*score += s;*/
  }
  
  return score;
}

/* *********************************************************** */

void remap (int M, int N, int *comms, int *remap) {
  int* perm = calloc(M*N, sizeof(int));
  sortI (M*N, comms, perm);  // look for largest values in comms[M*N]
  
  for (int i = 0; i < N; i++)
    remap[i] = -1;
  
  int *A = calloc (M, sizeof(int)); /* A[i]: old part i is associated with a new one */
  int *B = calloc (N, sizeof(int)); /* B[i]: new part i is associated with a new one */
  int mapped_part = 0;
  
  for (int i = M*N-1; i >= 0 && mapped_part < M && mapped_part < N; i--) {
    int a = perm[i]/N, b = perm[i]%N; // (a,b) mat coord of largest comm value
    if (!(A[a] || B[b])) { // don't remap 
      remap[b] = a;
      A[a] = B[b] = 1;
      mapped_part++;
    }
  }
  
  free (A);
  free (B);
  free (perm);
}


/* *********************************************************** */
