/**
 *   @file misc.h
 *
 *   @author     Aurelien Esnard
 *   @author     Clement Vuchener
 *
 *   @defgroup misc
 *   @brief misc for mesh, graph and hypergraph.
 *
 */

#ifndef __MISC_H__
#define __MISC_H__

#include "libgraph.h"


//@{


typedef struct PartitionTree_ {
  int nparts;       /**< nb of parts in the tree */
  int height;       /**< nb of levels of the underlying perfect binary tree */
  int realheight;   /**< real height of the tree could be height+1 if the tree is not binary perfect */
  int length;       /**< array length  */
  int * array;      /**< array that stores the size of each merged part at each level */
} PartitionTree;


/** Create coupling graph from two meshes */
void createCouplingGraph (Mesh *meshA,	/**< [in] Mesh for code A */
			  int vwgtA,	/**< [in] Vertex weight for code A */
			  int ewgtA,	/**< [in] Edge weight for code A */
			  int cvwgtA,	/**< [in] Vertex weight in coupling zone for code A */
			  int cewgtA,	/**< [in] Coupling edge weight for code A */
			  Mesh *meshB,	/**< [in] Mesh for code B */
			  int vwgtB,	/**< [in] Vertex weight for code B */
			  int ewgtB,	/**< [in] Edge weight for code B */
			  int cvwgtB,	/**< [in] Vertex weight in coupling zone for code B */
			  int cewgtB,	/**< [in] Coupling edge weight for code B */
			  int nb_interedges, int * interedges,
			  Graph *g	/**< [out] Coupling graph */
			  );


/** Create coupling hypergraph from two meshes */
void createCouplingHypergraph (Mesh *meshA,	/**< [in] Mesh for code A */
			       int vwgtA,	/**< [in] Vertex weight for code A */
			       int ewgtA,	/**< [in] Edge weight for code A */
			       int cvwgtA,	/**< [in] Vertex weight in coupling zone for code A */
			       int cewgtA,	/**< [in] Coupling edge weight for code A */
			       Mesh *meshB,	/**< [in] Mesh for code B */
			       int vwgtB,	/**< [in] Vertex weight for code B */
			       int ewgtB,	/**< [in] Edge weight for code B */
			       int cvwgtB,	/**< [in] Vertex weight in coupling zone for code B */
			       int cewgtB,	/**< [in] Coupling edge weight for code B */
			       int nb_interedges, int * interedges,
			       Hypergraph *g,	/**< [out] Coupling graph */
			       int *coupledvtxs	/**< [out] */
			       );

/* ******* MISC ******* */

/** Try to guess the partition tree according to recursive bisection
    used in Metis/HMetis/PaTOH. Return the number of levels in the
    partition tree (i.e. ptree->height).
*/
int guessPartitionTree(int nparts,             /**< [in] nb of partitions */
		       PartitionTree * tree    /**< [out] array of size nparts */
		       );

/** Return the mapping from a part rank (in the initial partition) to
    its rank in the merged parts at a given level of the partition
    tree. At level 0, there is no partition; level 1 is first the
    bi-partition; etc. Return the number of effective parts at the
    given level.
*/
int mapPartitionTree(PartitionTree * tree,    /**< [in] partition tree */
		     int level,               /**< [in] level in the partition tree (<= ceil(ln(nparts))) */
		     int * map                /**< [out] array of size tree->nparts with values in [0,2^level[ */
		     );

/** 
 * Try to compute an new order of parts such that consecutive parts are connected
 * \return 0 if the path is not connected
 */
int reorderPartition (int *connection,	/**< [in] part connection matrix (adjacency matrix of quotient graph) */
		      int nparts,	/**< [in] number of parts */
		      int *neworder	/**< [out] new part order */
		      );

/** 
 * Try to compute an new order of parts such that consecutive parts are connected
 * \return 0 if the path is not connected
 */
int reorderPartitionHypergraph (Hypergraph *hg,	/**< [in] hypergraph */
				int *part,	/**< [in] partition of the hypergraph */
				int nparts,	/**< [in] number of parts */
				int *neworder	/**< [out] new part order */
				);

/** 
 * Try to compute an new order of parts such that consecutive parts are connected
 * \return 0 if the path is not connected
 */
int reorderPartitionGraph (Graph *g,		/**< [in] graph */
			   int *part,		/**< [in] partition of the hypergraph */
			   int nparts,		/**< [in] number of parts */
			   int *neworder	/**< [out] new part order */
			   );

/** 
 * Try to compute an new order of parts such that consecutive parts are connected
 * \return 0 if the path is not connected
 */
int reorderPartitionQuotientGraph (Graph *g,		/**< [in] quotient graph */
				   int *neworder	/**< [out] new part order */
				   );

/**
 * Unbalance a partition by changing the weight of vertices
 */
void unbalancePartition(int *vwgts,		/**< [in,out] Weight array */
			int nvtxs,		/**< [in] number of vertices */
			int nparts,		/**< [in] number of parts */
			int *part,		/**< [in] partition */
			double *part_ub		/**< [in] part unbalance, may be NULL for random unbalance */
			);

/**
 * Unbalance a partition with a fixed unbalance factor and linear weight progression between parts by changing the weight of vertices
 */
void unbalancePartitionLinear (int *vwgts,		/**< [in,out] Weight array */
			       int nvtxs,		/**< [in] number of vertices */
			       int nparts,		/**< [in] number of parts */
			       int *part,		/**< [in] partition */
			       int ubfactor		/**< [in] unbalance factor */
			       );

/* random permute */
void randomPermute(int size, int * perm);

/** greatest common divisor */
int gcd (int a, int b);

/** least common multipe */
int lcm(int a, int b);

/** Generate a random order*/
void generateRandomOrder (int *order,	/** [out] random order */
			  int size	/** [in] Order size */
			  );
/*-----------------------------*/

//@}

#endif
