/**
 *   @file repart.h
 *
 *   @author     Aurelien Esnard
 *   @author     Clement Vuchener
 *
 *   @addtogroup Repart Repartitioning & migration routines.
 *
 */

#ifndef __REPART_H__
#define __REPART_H__


#include "libgraph.h"
#include "modelcomm.h"

//@{

/** MxN repartitioning schemes */
enum RepartScheme {
  SCRATCHREMAP,			/**< Partitioning from scratch with remapping */
  DIFFUSION,			/**< Diffusion scheme (with multimove = 0) */
  DIFFUSION_MM,			/**< Diffusion scheme (with multimove = 1) */
  FIXEDVTXS,		        /**< Using fixed vertices and migration edges for skewed partitioning */
  COUPLING_HYPEREDGES,	        /**< Using one large hyperedge for each old part */
  COUPLING_HYPEREDGES_FIXEDVTXS,/**< Using one large hyperedge for each old part (optimize messages) and fixed vertices (1 per old part) (optimize migration) */
  COUPLING_HYPEREDGES_PERFECT	/**< Same as \ref COUPLING_HYPEREDGES_PERFECT, but with a better and very slow partionner */
};

/* *********************************************************** */
/*               REPARTITIONING ROUTINES                       */
/* *********************************************************** */

/* /\** Graph repartitioning routine with different strategies. *\/ */
/* void repartitionGraph(Graph * g, */
/* 		      int oldnparts,			/\**< [in] old nb of partitions *\/ */
/* 		      int * oldpart,			/\**< [in] old array of partitions (of size g->nvtxs) *\/  */
/* 		      int ubfactor,			/\**< [in] unbalanced factor *\/ */
/* 		      int repartmult,			/\**< [in] how much to favorize edgecut over migration (objective function is repartmult * edgecut + migration) *\/ */
/* 		      int migcost,			/\**< [in] migration cost from one old partition to a new one *\/ */
/* 		      int newnparts,			/\**< [in] new nb of partitions *\/ */
/* 		      enum RepartScheme rscheme,        /\**< [in] repartition scheme *\/ */
/* 		      enum CommScheme cscheme,	        /\**< [in] communication scheme *\/ */
/* 		      const void *comm_options,		/\**< [in] Communication scheme options (may be null) (depends on scheme) *\/ */
/* 		      int * newpart,			/\**< [out] new array of partitions (of size hg->nvtxs) *\/  */
/* 		      int * edgecut,			/\**< [out] cut size *\/ */
/* 		      int * modelmat,			/\**< [out] Communication model used (if any) (may be NULL) *\/ */
/* 		      int * realmat,			/\**< [out] Real communication matrix (may be NULL) *\/ */
/* 		      int doremap,                      /\**< [in] remapping as a post-processing step (1:true, 0: false) *\/ */
/* 		      Mesh * mesh,                      /\**< [in] debug mesh (or NULL if useless) *\/ */
/* 		      char * env                        /\**< [in] set partitioner method (or NULL for default) *\/ */
/* 		      ); */


/* /\** Hypergraph repartitioning routine with different strategies. *\/ */
/* void repartitionHypergraph(Hypergraph * hg,             /\**< [in] hypergraph *\/ */
/* 			   int oldnparts,		/\**< [in] old nb of partitions *\/ */
/* 			   int * oldpart,		/\**< [in] old array of partitions (of size hg->nvtxs) *\/ */
/* 			   int ubfactor,		/\**< [in] unbalanced factor *\/ */
/* 			   int repartmult,		/\**< [in] how much to favorize edgecut over migration (objective function is repartmult * edgecut + migration) *\/ */
/* 			   int migcost,			/\**< [in] migration cost from one old partition to a new one *\/ */
/* 			   int newnparts,		/\**< [in] new nb of partitions *\/ */
/* 			   enum RepartScheme rscheme,	/\**< [in] repartition scheme *\/ */
/* 			   enum CommScheme cscheme,	/\**< [in] communication scheme *\/ */
/* 			   const void *comm_options,		/\**< [in] Communication scheme options (may be null) (depends on scheme) *\/ */
/* 			   int * newpart,		/\**< [out] new array of partitions (of size hg->nvtxs) *\/ */
/* 			   int * edgecut,		/\**< [out] cut size *\/ */
/* 			   int * modelmat,		/\**< [out] Communication model used (if any) (maybe NULL) *\/ */
/* 			   int * realmat,		/\**< [out] Real communication matrix (may be NULL) *\/ */
/* 			   Mesh * mesh                  /\**< [in] debug mesh (or NULL if useless) *\/ */
/* 			   ); */


/* *********************************************************** */
/*                 AUXILIARY ROUTINES                          */
/* *********************************************************** */

/** Build the mesh \p out according to coordinates in \p mesh showing communication in \p comms. */
void fixedVerticesMesh (Mesh *mesh,		/**< [in] Original mesh */
			int *oldpart,		/**< [in] Old partition */
			Hypergraph *comms,	/**< [in] Communication hypergraph (vertex = new part, hyperedge = old part) */
			Mesh *out);		/**< [out] Output mesh */


/** Add \p newnparts fixed vertices and edges according the bipartite cmmunication graph */
int addFixedVerticesG (Graph *old,		/**< [in] Old graph */
		       int *oldpart, 		/**< [in] Old partition */
		       Hypergraph *comms, 	/**< [in] Communication hypergraph. Vertices are new parts, hyperedges are old parts. */
		       int repartmult,		/**< [in] Repartition multiplier */
		       int migcost, 		/**< [in] New edges weight */
		       Graph *new 		/**< [out] New graph */
		       );

/** Add coupling hyperedges in \p old hypergrah according to \p oldpart */
void addCouplingHyperedges (Hypergraph *old,	/**< [in] Old hypergraph */
			    int *oldpart, 	/**< [in] Old partition */
			    int oldnparts, 	/**< [in] Old number of parts */
			    int migcost, 	/**< [in] coupling hyperedges weight */
			    Hypergraph *new	/**< [out] New hypergraph */
			    );

/** Add \p newnparts fixed vertices and edges according the bipartite cmmunication graph */
int addFixedVerticesHG (Hypergraph *old,	/**< [in] Old hypergraph */
			int *oldpart, 		/**< [in] Old partition */
			Hypergraph *comms, 	/**< [in] Communication hypergraph. Vertices are new parts, hyperedges are old parts. */
			int repartmult,		/**< [in] Repartition multiplier */
			int migcost, 		/**< [in] New edges weight */
			Hypergraph *new 	/**< [out] New hypergraph */
			);


//@}

#endif
