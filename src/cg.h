/**
 *   @file cg.h
 *
 *   @author     Vasia Kalavri
 *
 *   @defgroup CG Conjugate Gradient Solver.
 *   @brief Conjugate Gradient Solver.
 *
 */

#ifndef __CG_H__
#define __CG_H__

//@{

/* *********************************************************** */
/*                      CONJUGATE GRADIENT                     */
/* *********************************************************** */


/** Solve Ax=b with conjugate gradient method. */
int cg(int n,       /**< [in] dimension of matrix A (symetric definite-positive) */
       double *A,   /**< [in] data of matrix A (array of size n*n) */
       double *b,   /**< [in] right hand-side (array of size n) */
       double *x    /**< [out] solution vector (array of size n) */
       ); 

//@}

#endif                        
