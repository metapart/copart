/**
 *   @file migration.h
 *
 *   @author     Aurelien Esnard
 *   @author     Clement Vuchener
 *
 *   @addtogroup Repart Repartitioning & migration routines.
 *
 */

#ifndef __MIGRATION_H__
#define __MIGRATION_H__

#include "libgraph.h"

//@{

/** Migration schemes */
enum MigrationScheme {
  MIG_VTOT,	        /**< Scheme to optimize total load volume (simplex). */
  MIG_VMAX,	        /**< Scheme to optimize max load volume per processor (simplex). \bug */
  MIG_VTOT_ABS,	        /**< Scheme to optimize total load volume (simplex). \bug */
  MIG_VMAX_ABS,	        /**< Scheme to optimize max load volume per processor (simplex). \bug */
  MIG_VTOT2 	        /**< Scheme to optimize total load volume (Euclidean norm, conjugate gradient). */
};

/** Compute a migration matrix to equilibrate an unbalanced graph for \p scheme (using linear programming optimization) */
void computeMigrationMatrix(Graph * g, 		                /**< [in] graph (vertices = processor, vertex weights = proc. load) */
			    int ubfactor,		        /**< [in] unbalance factor */		     
			    enum MigrationScheme scheme,	/**< [in] migration scheme */
			    int * migmat			/**< [out] migration matrix (of dimension g->nvtxs*g->nvtxs, such as migmat[i][j] = -migmat[j][i]) */ 
			    );


#define MIG_TOTAL	1
#define MIG_MAX		2
#define MIG_VOLUME	4
#define MIG_MESSAGES	8

#define MIG_TOTAL_V	(MIG_TOTAL | MIG_VOLUME)	/**< TotalV objective for computeMigrationMatrixLP */
#define MIG_MAX_V	(MIG_MAX | MIG_VOLUME)		/**< MaxV objective for computeMigrationMatrixLP */
#define MIG_TOTAL_Z	(MIG_TOTAL | MIG_MESSAGES)	/**< TotalZ objective for computeMigrationMatrixLP */
#define MIG_MAX_Z	(MIG_MAX | MIG_MESSAGES)	/**< MaxZ objective for computeMigrationMatrixLP */

/** Same as computeMigrationMatrix but with new objectives */
void computeMigrationMatrixLP (Graph *g, int ubfactor, int mig_objective, int *migmat);

//@}

#endif
