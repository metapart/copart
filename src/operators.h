#ifndef __OPERATORS_H__
#define __OPERATORS_H__

#include <stdbool.h>
#include "libgraph.h"
#include "repart.h"

#define VALUES_IN_LINE 3

void extension(Graph * g, 
	       int * part, 
	       int * fixed
	       );

void projection(Graph * g_a, 
		Graph * g_b, 
		int *part_a, 
		int nparts, 
		int nbinteredges, 
		int * interedges,
		int ** part, //out 
		int ** fixed, //out 
		Graph ** g, //out
		int inverse // a-->b or b-->a ?
		); 

void repartition(Graph * g,
		 int oldnparts,
		 int * oldpart,
		 int ubfactor,
		 int repartmult,
		 int migcost,
		 int newnparts,
		 enum RepartScheme rscheme,
		 enum CommScheme cscheme,
		 const void *comm_options,
		 int * modelmat,
		 Graph ** eg //out
		 );

void restriction(Graph * g, 
		 int * array, 
		 Graph ** rg, //out  
		 int ** rmap //out
		 );


void processInteredges(int * p_interedges, 
		       int length, 
		       int nbclms, 
		       int * array1, 
		       int * array2, 
		       int * interedges);

/* when loading cerfacs hybrid meshes the coupling part should be load first. */
/* Is it always true??*/ 

void processCerfacsHybridMesh(HybridMesh * hm, 
			      int * interedges, 
			      int nb_interedges,
			      int nb_columns);

int getNbColumnsInFile(char * filename);

void locate(Graph * g, 
	    int nbinteredges, 
	    int * interedges, 
	    int side, // as inverse
	    int maxlevel, 
	    int * used);

#endif

