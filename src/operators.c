/* Co-partitioning Module */
/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <math.h>
#include <time.h>


// #define DEBUG
#define TOLERANCE 1.001
#define UBFACTOR 5


#include "libgraph.h"
#include "operators.h"
#include "repart.h"
#include "modelcomm.h"
#include "matrixcomm.h"

/* *********************************************************** */ 

void extension(Graph * g, int * part, int * fixed) { 
  assert(part);
  assert(fixed);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    if(part[i] != -1)
      fixed[i] = 1; 
  }
}

/* *********************************************************** */ 

/* ATTENTION IN THE WAY INTEREDGES ARE MANAGED !! MAYBE NOT ALLIGNED WITH STARPART INTARRAY*/
void projection(Graph * g_a, Graph * g_b, int *part_a, 
		int nparts, int nbinteredges, int * interedges, // in
		int ** part, int ** fixed,  Graph ** g, int inverse) { // in-out

  int _nbinteredges = 0;
  Graph g_ab;
  assert(g_a && part_a);
  assert(interedges);

  int * coords = malloc(nbinteredges*4*sizeof(int));
  assert(coords);
  int * ewgts = malloc(nbinteredges*2*sizeof(int));
  assert(ewgts);
  int k = 0;
  int maxew = 0;

  for(int i = 0 ; i < nbinteredges ; i++) {
    int v_a = interedges[i*VALUES_IN_LINE + inverse];
    assert(v_a >= 0 && v_a < g_a->nvtxs);
    int v_b = interedges[i*VALUES_IN_LINE + (inverse ^ 1)]; 
    assert(v_b >= 0 && v_b < g_b->nvtxs);
    int p_a = part_a[v_a];
    // vertices not in restricted graph are not yet partitioned -> allow -1 in p_a
    assert(p_a >= -1 && p_a < nparts); 
    // if((pA+1) > npartsA) npartsA = pA+1; // guess npartsA
    int w = interedges[i*VALUES_IN_LINE + VALUES_IN_LINE-1];
    if(w == 0) continue; // skip interedge of weight 0
    if(w > maxew) maxew = w;
    coords[k*2] = g_b->nvtxs + p_a; // merge all vertices in same part of gA
    coords[k*2+1] = v_b;
    ewgts[k] = w; assert(ewgts[k] > 0);
    _nbinteredges++;
    k++;
  }
  // add symmetric interedges
  for(int i = 0 ; i < _nbinteredges ; i++) {
    coords[(_nbinteredges+i)*2] = coords[i*2+1];
    coords[(_nbinteredges+i)*2+1] = coords[i*2];
    ewgts[_nbinteredges+i] = ewgts[i];
  }
  
  assert(_nbinteredges <= nbinteredges);
 
  if(_nbinteredges != nbinteredges) printf("WARNING: %d interedges of null weight ignored!\n", nbinteredges - _nbinteredges);
  
  int ncoords = 2*_nbinteredges;
  int * perm = malloc(ncoords*sizeof(int));
  sortP(ncoords, coords, perm);
  
  // apply permutation
  permuteP(ncoords, coords, perm);
  permuteI(ncoords, ewgts, perm);
  
  stripCoords(&ncoords, &coords, &ewgts);
  assert(ewgts);
  _nbinteredges = ncoords / 2;
  
  coords2Graph(g_b->nvtxs+nparts, _nbinteredges, coords, ewgts, NULL, &g_ab);
  assert(g_ab.nvtxs <= g_b->nvtxs + nparts);
  assert(g_ab.nedges <= _nbinteredges);
  

  free(coords);
  free(ewgts);
  free(perm);
  
  // merge graphs to create the enriched g_b (_g)
  Graph * _g = allocGraph();
  int ewf = 1; // interedge weight factor
  assert(g_b);
  assert(&g_ab);
  assert(_g);
  mergeGraphs(g_b, &g_ab, 1, ewf, 1, 1, _g); // update ew of gB according to gAB (interedges)
  assert(_g->nvtxs == g_b->nvtxs + nparts);
  
  int * _part_b = calloc(_g->nvtxs,sizeof(int));
  assert(_part_b);
  for(int i = 0 ; i < _g->nvtxs ; i++) 
    _part_b[i] = -1; /* free all vtxs */
  // important
  for(int i = 0 ; i < nparts ; i++) 
    _part_b[g_b->nvtxs+i] = i; /* fixed last vtxs */
  // fix the vertices belonging to the "g_a"
  int * _fixed = calloc(_g->nvtxs,sizeof(int));
  assert(_fixed);
  for(int i = 0 ; i < _g->nvtxs ; i++)
    if(_part_b[i] != -1)
      _fixed[i] = 1;
  *part = _part_b;
  *fixed = _fixed;
  *g = _g;

}

/* *********************************************************** */ 

/* returns the restricted graph rg out of g based on the boolean array. */
/* returns the mapping array of size rg */
void restriction(Graph * g, int * array, Graph ** rg, int ** rmap) {
  assert(g);
  assert(array);
  /* check array is boolean */
  for( int i = 0 ; i < g->nvtxs ; i++ )
    assert( array[i] == 0 || array[i] == 1 );

  Graph subg;
  subGraphFromArray(g, array, &subg);
  assert(subg.nvtxs <= g->nvtxs);
  assert(subg.nedges <= g->nedges);
  assert(subg.nvtxs > 0);
  Graph * _rg = allocGraph();
  dupGraph(&subg,_rg);
  assert(_rg && (_rg->nvtxs == subg.nvtxs));
 
  // remapping
  int * _rmap = malloc( sizeof(int) * subg.nvtxs );
  int k = 0;
  for(int i = 0 ; i < g->nvtxs ; i++)
    if(array[i])
      _rmap[k++] = i;
  /* check */
  assert(k == subg.nvtxs);

  /* output */
  if(rg) *rg = _rg;
  if(!rg && _rg) { freeGraph(_rg); free(_rg); }
  if(rmap) *rmap = _rmap;
  if(!rmap && _rmap) { free(_rmap); }
}

/* *********************************************************** */ 

void repartition(Graph * g,
		      int oldnparts,
		      int * oldpart,
		      int ubfactor,
		      int repartmult,
		      int migcost,
		      int newnparts,
		      enum RepartScheme rscheme,
		      enum CommScheme cscheme,
		      const void *comm_options,
		      int * modelmat,
		      Graph ** eg
)
{
    
  if (modelmat) memset (modelmat, 0, oldnparts * newnparts * sizeof (int));
  
  int *_modelmat = NULL;
  
  assert(rscheme == FIXEDVTXS); // for now
  assert(cscheme != UNUSED);
    
  //  if(rscheme == FIXEDVTXS) {

  Graph quotient;
  quotientGraph (g, oldnparts, oldpart, &quotient);
  //printGraph(&quotient);
    
  /* Communication model */
  _modelmat = calloc (oldnparts * newnparts, sizeof (int));
  const int skipremap = 0;
  communicationModel(oldnparts, newnparts, ubfactor, &quotient, cscheme, 
		     comm_options, skipremap, (int*)_modelmat);
  
  /* printf("Repart Model:\n"); */
  /* printCommMatrix ((int*)_modelmat, oldnparts, newnparts); */

  Hypergraph modelhg;
  dense2sparse ((int*)_modelmat, oldnparts, newnparts, &modelhg);
  
  /* printf("Repart Hypergraph Model:\n");   */
  /* printHypergraph(&modelhg); */
  
  /* create enriched graph for skewed partitioning */
  Graph * g2 = allocGraph();
  g2->nvtxs = g->nvtxs + newnparts;
  addFixedVerticesG (g, oldpart, &modelhg, repartmult, migcost, g2);
  assert(g->nvtxs + newnparts == g2->nvtxs);

  int *p = malloc (g2->nvtxs * sizeof(int));
  for (int i = 0; i < g->nvtxs; i++) p[i] = -1;
  for (int i = 0; i < newnparts; i++) p[g->nvtxs + i] = i;

  *eg = g2;

  /* free mem */
  free (p);
  freeGraph (&quotient);
  freeHypergraph (&modelhg);

/* } */
/* /\* error *\/ */
/*  else { */
/*   fprintf (stderr, "ERROR: Unknown Repartitioning Scheme!!!\n"); */
/*   exit(EXIT_FAILURE); */
/* } */
  
}

/* *********************************************************** */ 

void processInteredges(int * p_interedges, int length, int nbclms, int * array1, int * array2, int * interedges) {
  assert(p_interedges);
  assert(array1);
  assert(array2);
  assert(interedges);
  int nbinteredges = length/nbclms;
  switch( nbclms ) {
  case 2:
    for(int i = 0 ; i < length; i = i + nbclms) {
      if(p_interedges[i] > -1)
      array1[p_interedges[i]] = 1;
      if(p_interedges[i+1] > -1)
      array2[p_interedges[i+1]] = 1;
    }
    for(int i = 0 ; i < nbinteredges; i++) {
      interedges[i*VALUES_IN_LINE] = p_interedges[i*nbclms];
      interedges[i*VALUES_IN_LINE + 1] = p_interedges[i*nbclms + 1];
      interedges[i*VALUES_IN_LINE + 2] = 1;
      //*(interedges + i + VALUES_IN_LINE-1) = 1;
    }
    break;
  case 3:
    for(int i = 0 ; i < length; i=i+3) {
      if(p_interedges[i])
  	array1[p_interedges[i]] = 1;
      if(p_interedges[i+1])
  	array2[p_interedges[i+1]] = 1;
    }
    for(int i = 0 ; i <length; i++) {
      interedges[i] = p_interedges[i];
    }
    break;
  case 1:
    printf("WARNING: Interedges are not properly given!\n");
    abort();
    break;
  }
}

/* *********************************************************** */

int getNbColumnsInFile(char * filename) {
  char line[1024];
  char * result = NULL;
  int nb_columns = 0;
  assert(filename);
  FILE * file = fopen(filename, "r");
  if(!file) printf("Error: fail to load \"%s\"!\n", filename);
  fgets(line, sizeof(line), file);  
  result = strtok( line, " " );
  while( result != NULL ) {
    nb_columns++;
    result = strtok( NULL, " " );
  }    
  fclose(file);
  return nb_columns;
}

/* *********************************************************** */

void processCerfacsHybridMesh(HybridMesh * hm, int * interedges, int nb_interedges, int nb_columns) {
  assert(interedges);
  assert(hm);
  Mesh * m = NULL;
  int nb_cells = getHybridMeshNbCells(hm);
  m = getHybridMeshComponent(hm,0);
  for(int i = 0 ; i < nb_interedges; i++) {
    //printf("%d --> ",interedges[i*nb_columns]);
    interedges[i*nb_columns] =  interedges[i*nb_columns] + m->nb_cells; 
    //printf("%d\n",interedges[i*nb_columns]);
    assert(interedges[i*nb_columns] < nb_cells && interedges[i*nb_columns] >= m->nb_cells);
    }
}

/* *********************************************************** */

void locate(Graph * g, int nbinteredges, int * interedges, int side, int maxlevel, int * used)
{
  assert(g);
  assert(used);
  assert(maxlevel >= -1);
  assert(side == 0 || side == 1);
  
  /* reset */
  int nused = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) used[i] = 0;
  
  /* locate vertices connected to interedges */
  for(int i = 0 ; i < nbinteredges ; i++) {
    int v = interedges[i*VALUES_IN_LINE+side];
    used[v] = 1; // used
    nused++;
    int w = interedges[i*VALUES_IN_LINE + (VALUES_IN_LINE-1)]; 
    assert(w > 0);
  }
  

  if(isConnectedSubgraph(g,used)) {
    printf("[LOCATE] * at level 0 (nvtxs used = %d)\n", nused);
    return;
  }
  
  for(int i = 0; i < g->nvtxs; i++)
    assert(used[i] == 1 || used[i] == 0);
  
  printf("[LOCATE] * Should consider neighbors! \n");
  printf("[LOCATE] * FORCED RETURN! \n");
  return;
   /* consider also neighbors as used */
  for(int l = 1; l <= maxlevel || maxlevel == -1 ; l++) {
    for(int i = 0; i < g->nvtxs; i++) {
      for(int j = g->xadj[i]; j < g->xadj[i+1]; j++) {
	int ii = g->adjncy[j]; /* edge j = (i,ii) */
	/* following line -- error (used --> boolean)*/
	/* if(used[i] == l && used[ii] == 0) { used[ii] = l+1; nused++; } */
	/* correction ? */
	if(used[i] == l && used[ii] == 0) { used[ii] = 1; nused++; }
      }
    }
    
    for(int i = 0; i < g->nvtxs; i++)
      assert(used[i] == 1 || used[i] == 0);

    if(isConnectedSubgraph(g,used)) {
      printf("[LOCATE] * at level %d (nvtxs used = %d)\n",l, nused);
      return;
    }
  }
}

/* *********************************************************** */
