/* Misc. Tools */
/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <limits.h>


#include "misc.h"
#include "libgraph.h"

/* #include "struct.h" */
/* #include "geom.h" */
/* #include "sort.h" */
/* #include "debug.h" */
/* #include "graph.h" */
/* #include "hypergraph.h" */


/* *********************************************************** */

#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))

/* *********************************************************** */

#define MAXNBCELLSPERNODE 60 /* max nb of cells shared by a node */

/* *********************************************************** */

int gcd (int a, int b) {
  while (b > 0) {
    int r = a % b;
    a = b;
    b = r;
  }
  return a;
}

/* *********************************************************** */

int lcm(int a, int b) {
  return a*b / gcd(a,b);
}

/* *********************************************************** */

void randomPermute(int size, int * perm) {
  
  // int * perm = malloc(size*sizeof(int));
  for(int i = 0 ; i < size ; i++) perm[i] = i;
  for(int i = 0 ; i < size ; i++) {
    int k = rand()*1.0*size/RAND_MAX;
    int tmp = perm[i];
    perm[i] = perm[k];
    perm[k] = tmp;
  }  
  
}

/* *********************************************************** */

void createCouplingGraph (Mesh *meshA, int vwgtA, int ewgtA, int cvwgtA, int cewgtA,
			  Mesh *meshB, int vwgtB, int ewgtB, int cvwgtB, int cewgtB,
			  int nb_interedges, int * interedges,
			  Graph *g) {
  Graph a, b;
  mesh2Graph (meshA, &a);
  mesh2Graph (meshB, &b);
  
  // int *interedges, nb_interedges;
  // intersectMesh (meshA, meshB, &nb_interedges, &interedges);
  
  g->nvtxs = a.nvtxs + b.nvtxs;
  g->nedges = a.nedges + b.nedges + nb_interedges;
  g->xadj = malloc ((g->nvtxs+1) * sizeof(int));
  g->adjncy = malloc (2*g->nedges * sizeof(int));
  g->vwgts = malloc (g->nvtxs * sizeof(int));
  g->ewgts = malloc (2*g->nedges * sizeof(int));
  
  int k = 0;
  g->xadj[0] = 0;
  /* Edges from mesh A */
  for (int i = 0; i < a.nvtxs; i++) {
    /* Regular edges */
    for (int j = a.xadj[i]; j < a.xadj[i+1]; j++) {
      g->adjncy[k] = a.adjncy[j];
      g->ewgts[k] = ewgtA;
      k++;
    }
    /* Coupling edges */
    int coupled = 0;
    for (int j = 0; j < nb_interedges; j++)
      if (interedges[2*j] == i) {
	coupled = 1;
	g->adjncy[k] = a.nvtxs + interedges[2*j+1];
	g->ewgts[k] = cewgtA;
	k++;
      }
    /* Vertex weight */
    g->vwgts[i] = vwgtA;
    if (coupled)
      g->vwgts[i] += cvwgtA;
    
    g->xadj[i+1] = k;
  }
  /* Edges from mesh B */
  for (int i = 0; i < b.nvtxs; i++) {
    /* Regular edges */
    for (int j = b.xadj[i]; j < b.xadj[i+1]; j++) {
      g->adjncy[k] = a.nvtxs + b.adjncy[j];
      g->ewgts[k] = ewgtB;
      k++;
    }
    /* Coupling edges */
    int coupled = 0;
    for (int j = 0; j < nb_interedges; j++)
      if (interedges[2*j+1] == i) {
	coupled = 1;
	g->adjncy[k] = interedges[2*j];
	g->ewgts[k] = cewgtB;
	k++;
      }
    /* Vertex weight */
    g->vwgts[a.nvtxs + i] = vwgtA;
    if (coupled)
      g->vwgts[a.nvtxs + i] += cvwgtA;
    
    g->xadj[a.nvtxs + i + 1] = k;
  }
  assert (k == 2*g->nedges);
  
  free (interedges);
}

/* *********************************************************** */

void createCouplingHypergraph (Mesh *meshA, int vwgtA, int ewgtA, int cvwgtA, int cewgtA,
			       Mesh *meshB, int vwgtB, int ewgtB, int cvwgtB, int cewgtB,
			       int nb_interedges, int * interedges,
			       Hypergraph *hg, int *coupledvtxs) {
  Hypergraph a, b;
  mesh2Hypergraph (meshA, &a);
  mesh2Hypergraph (meshB, &b);
  
  // int *interedges, nb_interedges;
  // intersectMesh (meshA, meshB, &nb_interedges, &interedges);
  
  int *coupledA = calloc (a.nvtxs, sizeof(int));
  int *coupledB = calloc (b.nvtxs, sizeof(int));
  for (int i = 0; i < nb_interedges; i++) {
    coupledA[interedges[2*i]]++;
    coupledB[interedges[2*i+1]]++;
  }
  int ncoupledA = 0, ncoupledB = 0;
  for (int i = 0; i < a.nvtxs; i++)
    if (coupledA[i] > 0)
      ncoupledA++;
  for (int i = 0; i < b.nvtxs; i++)
    if (coupledB[i] > 0)
      ncoupledB++;
  
  hg->nvtxs = a.nvtxs + b.nvtxs;
  hg->nhedges = a.nhedges + b.nhedges + ncoupledA + ncoupledB;
  hg->eptr = malloc ((hg->nhedges + 1) * sizeof(int));
  hg->eind = malloc ((a.eptr[a.nhedges] + b.eptr[b.nhedges] + ncoupledA + ncoupledB + 2*nb_interedges) * sizeof(int));
  hg->vwgts = malloc (hg->nvtxs * sizeof(int));
  hg->hewgts = malloc (hg->nhedges * sizeof(int));
  
  /* Edges from A */
  memcpy (hg->eptr, a.eptr, (a.nhedges+1) * sizeof (int));
  memcpy (hg->eind, a.eind, a.eptr[a.nhedges] * sizeof (int));
  for (int i = 0; i < a.nvtxs; i++)
    hg->vwgts[i] = vwgtA + (coupledA[i] ? cvwgtA : 0);
  for (int i = 0; i < a.nhedges; i++)
    hg->hewgts[i] = ewgtA;
  /* Edges from B */
  for (int i = 1; i <= b.nvtxs; i++)
    hg->eptr[a.nvtxs+i] = a.eptr[a.nhedges] + b.eptr[i];
  for (int i = 0; i < b.eptr[b.nvtxs]; i++)
    hg->eind[hg->eptr[a.nvtxs] + i] = a.nvtxs + b.eind[i];
  for (int i = 0; i < b.nvtxs; i++)
    hg->vwgts[a.nvtxs + i] = vwgtB + (coupledB[i] ? cvwgtB : 0);
  for (int i = 0; i < b.nhedges; i++)
    hg->hewgts[a.nhedges + i] = ewgtB;
  
  /* Coupling edges */
  int k = a.nhedges + b.nhedges;
  /* Coupling hyperedge index for each coupled vertex */
  int *heA = calloc (a.nvtxs, sizeof(int)); 
  int *heB = calloc (b.nvtxs, sizeof(int));
  /* fill eptr */
  for (int i = 0; i < a.nvtxs; i++)
    if (coupledA[i]) {
      hg->eind[hg->eptr[k]] = i;
      hg->eptr[k+1] = hg->eptr[k] + 1 + coupledA[i];
      hg->hewgts[k] = cewgtA;
      heA[i] = k;
      k++;
    }
  for (int i = 0; i < b.nvtxs; i++)
    if (coupledB[i]) {
      hg->eind[hg->eptr[k]] = a.nvtxs + i;
      hg->eptr[k+1] = hg->eptr[k] + 1 + coupledB[i];
      hg->hewgts[k] = cewgtB;
      heB[i] = k;
      k++;
    }
  /* number of vertices currently added to each coupling hyperedge */
  int *ptr = calloc (ncoupledA + ncoupledB, sizeof(int));
  /* fill eind */
  for (int i = 0; i < nb_interedges; i++) {
    int va = interedges[2*i];
    int vb = interedges[2*i+1];
    hg->eind[hg->eptr[heA[va]] + (++ptr[heA[va] - (a.nhedges+b.nhedges)])] = a.nvtxs + vb;
    hg->eind[hg->eptr[heB[vb]] + (++ptr[heB[vb] - (a.nhedges+b.nhedges)])] = va;
  }
  free (heA);
  free (heB);
  free (ptr);
  
  if (coupledvtxs != NULL) {
    memcpy (coupledvtxs, coupledA, a.nvtxs * sizeof (int));
    memcpy (coupledvtxs+a.nvtxs, coupledB, b.nvtxs * sizeof(int));
  }
  
  freeHypergraph (&a);
  freeHypergraph (&b);
  free (coupledA);
  free (coupledB);
  free (interedges);
}

/* *********************************************************** */

int mapPartitionTree(PartitionTree * tree, 
		     int level,            
		     int * map             
		     )
{  
  /* number of effective parts at the given level */
  int nparts = 1 << level;
  
  /* init map */  
  for(int i = 0 ; i < tree->nparts ; i++)
    map[i] = i;
  
  if(level < 0 || level > tree->height) return tree->nparts;
  
  /* compute map at a given level */
  int width = 1 << level;
  int start = (1 << level) - 1;
  int k = 0;
  for(int i = 0 ; i < width ; i++) {
    for(int j = 0 ; j < tree->array[start+i] ; j++, k++) {
      assert(i >= 0 && i < nparts);
      map[k] = i;      
    }
  }
  assert(k == tree->nparts);  
  
  return nparts;
}

/* *********************************************************** */

int guessPartitionTree(int nparts, PartitionTree * tree)
{
  tree->nparts = nparts;
  tree->height = floor(log(nparts)/log(2)); 
  tree->realheight = ceil(log(nparts)/log(2.0));
  assert(tree->height <= tree->realheight);
  tree->length = (1 << (tree->height+1)) - 1;  
  tree->array = malloc(tree->length*sizeof(int));
  assert(tree->array != NULL);
  
  /* debug */
  /*printf("nparts = %d, height = %d, real height = %d, length = %d\n", 
    tree->nparts, tree->height, tree->realheight, tree->length);*/
  
  /* compute partition tree as a vector */
  
  tree->array[0] = nparts; /* level 0 */
  for(int i = 1 ; i < tree->length ; i++) tree->array[i] = 0; /* no partition (all merged in part 0) */
  
  for(int l = 1 ; l <= tree->height ; l++) { /* l: current level */
    int i = (1 << l) - 1; /* i: first index in tree vector at level l */
    int w = 1 << l;       /* w: width of level l */
    // printf("\t l = %d, i = %d, w = %d\n",l,i,w);
    
    for(int j = 0 ; j < w ; j++) {
      int jj = i + j;
      int f = (jj - 1) / 2; /* f: father index of jj */
      int left = jj % 2; /* true or false */
      if(left) tree->array[jj] = tree->array[f] / 2; /* left */
      else tree->array[jj] = tree->array[f] - tree->array[jj-1];
    }    
  }
  
  /*   printf("["); */
  /*   for(int i = 0 ; i < length ; i++) printf("%d ", tree->array[i]); */
  /*   printf("]\n"); */
  
  return tree->height;
  
}

/* *********************************************************** */

/**
 * \brief compute the connection between two part in the bissection tree
 * \return connection between part \arg n1 at depth \arg h1 and part \arg n2 at depth \arg h2
 */
static int partUnionConnection (int *connection,	/**< [in] part connection matrix */
				int *map,		/**< [in] bissection tree map */
				int h,			/**< [in] bissection tree height */
				int nparts,		/**< [in] number of parts */
				int h1,			/**< [in] depth of the first part in the bissection tree */
				int n1,			/**< [in] first part index */
				int h2,			/**< [in] depth of the second part in the bissection tree */
				int n2			/**< [in] second part index */
				) {
  int i, j;
  int c = 0;
  for (i = 0; i < nparts; i++) /* TODO: Better loop ? (n1 are consecutive in map) */
    if (map[h1*nparts + i] == n1) {
      for (j = 0; j < nparts; j++) /* same */
	if (map[h2*nparts + j] == n2) {
	  /* Connection is the maximum connection between two subparts */
	  if (connection[i*nparts + j] > c)
	    c = connection[i*nparts + j];
	}
    }
  return c;
}

/* *********************************************************** */

struct list {
  struct list* next;
  int v;
};

/* *********************************************************** */

static void reorder_rec (const int *adj, int nparts,
			 int *path, int length, int value,
			 int *best_path, int final_length, int *best_value,
			 struct list *freevtxs) {
  int i = (length > 0 ? path[length-1] : -1);
  struct list **p, *e;
  for (p = &freevtxs, e = freevtxs;
       e != NULL;
       p = &e->next, e = e->next) {
    int j = e->v;
    int v = (i != -1 ? adj[i*(nparts+1) + j] : INT_MAX);
    if (v > 0) {      
      if (value < v)
	v = value;
      path[length] = j;
      
      if (length < final_length-1) {
	*p = e->next; /* Remove j from list */
	reorder_rec (adj, nparts, path, length+1, v, best_path, final_length, best_value, freevtxs);
	*p = e; /* Restore j in list */
      }
      else {
	if (adj[nparts*(nparts+1) + j] < v)
	  v = adj[nparts*(nparts+1) + j];
	if (v > *best_value) {
	  memcpy (best_path, path, final_length*sizeof(int));
	  *best_value = v;
	}
      }
    }
  }
}

/* *********************************************************** */

#define SET_BIT(bf, b) (bf[b/(8*sizeof(unsigned int))] |= (unsigned int)1 << b%(8*sizeof(unsigned int)))
#define RESET_BIT(bf, b) (bf[b/(8*sizeof(unsigned int))] &= ~((unsigned int)1 << b%(8*sizeof(unsigned int))))
#define TEST_BIT(bf, b) (bf[b/(8*sizeof(unsigned int))] & ((unsigned int)1 << b%(8*sizeof(unsigned int))))

/* *********************************************************** */

int reorderPartitionHypergraph (Hypergraph *hg, int *part, int nparts, int *neworder) {
  int *connection = calloc (nparts * nparts, sizeof(int));
  
  unsigned int s = (nparts-1)/(8*sizeof(unsigned int)) + 1;
  unsigned int *hedge = malloc (s * sizeof(unsigned int));
  for (int e = 0; e < hg->nhedges; e++) {
    memset (hedge, 0, s*sizeof(unsigned int));
    for (int i = hg->eptr[e]; i < hg->eptr[e+1]; i++) {
      SET_BIT (hedge, part[hg->eind[i]]);
    }
    
    for (int i = 0; i < nparts-1; i++) {
      if (TEST_BIT (hedge, i)) {
	for (int j = i+1; j < nparts; j++) {
	  if (TEST_BIT (hedge, j)) {
	    connection[i*nparts + j] += (hg->hewgts ? hg->hewgts[e] : 1);
	    connection[j*nparts + i] += (hg->hewgts ? hg->hewgts[e] : 1);
	  }
	}
      }
    }
  }
  
  int r = reorderPartition (connection, nparts, neworder);
  free (connection);
  free (hedge);
  return r;
}

/* *********************************************************** */

int reorderPartitionGraph (Graph *g, int *part, int nparts, int *neworder) {
  int *connection = calloc (nparts * nparts, sizeof(int));
  
  for (int v1 = 0; v1 < g->nvtxs; v1++) {
    for (int i = g->xadj[v1]; i < g->xadj[v1+1]; i++) {
      int v2 = g->adjncy[i];
      if (part[v1] != part[v2])
	connection[part[v1]*nparts + part[v2]] += (g->ewgts ? g->ewgts[i] : 1);
    }
  }
  
  int r = reorderPartition (connection, nparts, neworder);
  free (connection);
  return r;
}

/* *********************************************************** */

int reorderPartitionQuotientGraph (Graph *g, int *neworder) {
  int *connection = calloc (g->nvtxs * g->nvtxs, sizeof(int));
  
  for (int v = 0; v < g->nvtxs; v++) {
    for (int e = g->xadj[v]; e < g->xadj[v+1]; e++) {
      connection[v*g->nvtxs + g->adjncy[e]] += (g->ewgts ? g->ewgts[e] : 1);
    }
  }
  
  int r = reorderPartition (connection, g->nvtxs, neworder);
  free (connection);
  return r;
}

/* *********************************************************** */

int reorderPartition (int *connection, int nparts, int *neworder) {
  int h, n, i, j, k;
  int goodpath = 1;
  
  int H = ceil(log(nparts)/log(2))+1;
  PartitionTree tree;
  guessPartitionTree (nparts, &tree);
  int map[H * nparts];
  for(h = 0; h < H; h++) {
    mapPartitionTree (&tree, h, &map[h*nparts]);
  }
  int map_reord[H * nparts];
  memcpy (map_reord, map, H * nparts * sizeof(int));
  
#define NODE(t, h, n) (t).array[(1 << (h)) - 1 + (n)]
  
  int threshold = 3;
  
  /* Swap children in partition tree */
  int buffer[nparts];
  for (h = 0; h < H-1 - threshold; h++) { /* Foreach level */
    i = 0;
    for (n = 0; n < (1<<h); n++) { /* Foreach node in the level */
      if (h < H-2 || NODE(tree, h, n) == 2) {
	int lfirst = 0, rfirst = 0, lsecond = 0, rsecond = 0;
	/* Children indices */
	int l;
	int r;
	l = map_reord[(h+1)*nparts + i];
	if (h < H-2)
	  r = map_reord[(h+1)*nparts + i + NODE(tree, h+1, 2*n)];
	else
	  r = map_reord[(h+1)*nparts + i + 1];
	/* Common hyperedges with neightbours */
	if (n > 0) {
	  int prev = map_reord[(h+1)*nparts + i - 1];
	  lfirst = partUnionConnection (connection, map, H, nparts, h+1, l, h+1, prev);
	  rfirst = partUnionConnection (connection, map, H, nparts, h+1, r, h+1, prev);
	}
	if (n < (1<<h)-1) { /* There is a part on the right */
	  int next = map_reord[h*nparts + i + NODE(tree, h, n)];
	  lsecond = partUnionConnection (connection, map, H, nparts, h+1, l, h, next);
	  rsecond = partUnionConnection (connection, map, H, nparts, h+1, r, h, next);
	}
	
	if ((n > 0 && lfirst == 0 && rfirst == 0) || (n < (1<<h)-1 && lsecond == 0 && rsecond == 0)) {
	  fprintf (stderr, "\033[1;31mNo good order for part %d and %d at level %d\n\033[0m", l, r, h+1);
	  goodpath = 0;
	}
	
	/* Test swap */
	if ((n > 0 && lfirst == 0) ||		/* The left child is not connected to the part on the left */
	    (n < (1<<h)-1 && rsecond == 0) ||	/* The right child is not connected to the part on the right */
	    lfirst - lsecond < rfirst - rsecond) {
	  if (h < H-2) {
	    /* Swap map */
	    for (k = h+1; k < H; k++) {
	      memcpy (&buffer[i + NODE(tree, h+1, 2*n+1)], &map_reord[k*nparts + i], NODE(tree, h+1, 2*n) * sizeof(int));
	      memcpy (&buffer[i], &map_reord[k*nparts + i + NODE(tree, h+1, 2*n)], NODE(tree, h+1, 2*n+1) * sizeof(int));
	      memcpy (&map_reord[k*nparts + i], &buffer[i], (NODE(tree, h+1, 2*n) + NODE(tree, h+1, 2*n+1)) * sizeof(int));
	    }
	    /* Swap tree */
	    for (k = h+1; k < H-1; k++) {
	      int a = n * (1 << (k-h));
	      int s = 1 << (k-h-1);
	      memcpy (&buffer[a], &NODE(tree, k, a + s), s * sizeof(int));
	      memcpy (&buffer[a + s], &NODE(tree, k, a), s * sizeof(int));
	      memcpy (&NODE(tree, k, a), &buffer[a], 2*s * sizeof(int));
	    }
	  }
	  else {
	    int tmp;
	    /* Swap map */
	    tmp = map_reord[(h+1)*nparts + i];
	    map_reord[(h+1)*nparts + i] = map_reord[(h+1)*nparts + i+1];
	    map_reord[(h+1)*nparts + i+1] = tmp;
	  }
	}
      }
      i += NODE(tree, h, n);
    }
  }
  
  /*for (i = 0; i < H; i++) {
    for (j = 0; j < nparts; j++)
    printf ("%2d ", map_reord[i*nparts + j]);
    printf ("\n");
    }
    printf ("%d\n", h);*/
  
  /* Test all paths for leaves */
  if (threshold > 0) {
    
    struct list *vtxs = malloc(nparts * sizeof(struct list));
    for (int j = 0; j < nparts; j++)
      vtxs[j].v = j;
    int *adj = calloc ((nparts+1)*(nparts+1), sizeof(int));
    
    i = 0;
    int path[nparts];
    for (n = 0; n < (1<<h); n++) { /* Foreach node in the level */
      int s = NODE(tree, h, n);
      
      /* Compute part list and adj matrix */
      struct list *l = NULL;
      for (j = i; j < i + s; j++) {
	int v = map_reord[(H-1)*nparts + j];
	vtxs[v].next = l;
	l = &vtxs[v];
	
	for (k = j+1; k < i + s; k++) {
	  int v2 = map_reord[(H-1)*nparts + k];
	  adj[v*(nparts+1) + v2] = adj[v2*(nparts+1) + v] = connection[v*nparts + v2];
	}
	if (n > 0) { /* There is a part on the left */
	  int v2 = map_reord[(H-1)*nparts + i - 1];
	  adj[v*(nparts+1) + v2] = adj[v2*(nparts+1) + v] = connection[v*nparts + v2];
	}
	if (n < (1<<h)-1) /* There is a part on the right */
	  adj[v*(nparts+1) + nparts] = adj[nparts*(nparts+1) + v] = partUnionConnection (connection, map, 1, nparts, H-1, v, h, map_reord[h*nparts + i + s]);
	else
	  adj[v*(nparts+1) + nparts] = adj[nparts*(nparts+1) + v] = INT_MAX;
      }
      
      /* Find path */
      int best_value = 0;
      if (n > 0) {
	path[0] = map_reord[(H-1)*nparts + i - 1];
	reorder_rec (adj, nparts, path, 1, INT_MAX, &map_reord[(H-1)*nparts + i - 1], s+1, &best_value, l);
      }
      else
	reorder_rec (adj, nparts, path, 0, INT_MAX, &map_reord[(H-1)*nparts + i], s, &best_value, l);
      
      /*for (k = i; k < i+s; k++)
	printf ("%2d ", map_reord[(H-1)*nparts + k]);
	printf ("\n");*/
      if (best_value == 0) {
	fprintf (stderr, "\033[1;31mNo good path for");
	for (k = i; k < i+s; k++)
	  fprintf (stderr, " %d", map_reord[(H-1)*nparts + k]);
	fprintf (stderr, "\n\033[0m");
	goodpath = 0;
      }
      /*else {
	printf ("\e[1;32m%d\n\e[0m", best_value);
	}*/
      
      /*for (j = 0; j < H; j++) {
	for (k = 0; k < nparts; k++)
	printf ("%2d ", map_reord[j*nparts + k]);
	printf ("\n");
	}
	printf ("%d - %d\n", i, s);*/
      
      i += s;      
    }
    free (adj);
    free (vtxs);
  }
  
  for (i = 0; i < nparts; i++)
    neworder[map_reord[(H-1)*nparts + i]] = i;
  /*memcpy (neworder, &map_reord[(H-1)*nparts], nparts * sizeof(int));*/
  
  free (tree.array);
  
  return goodpath;
}

/* *********************************************************** */
#if 0
void reorderPartition (Hypergraph *hg, int *part, int nparts, int *neworder) {
  int map[nparts];
  struct list *vtxs = malloc(nparts * sizeof(struct list));
  for (int i = 0; i < nparts; i++) {
    map[i] = i;
    vtxs[i].v = i;
    vtxs[i].next = (i == nparts-1 ? NULL : &vtxs[i+1]);
  }
  
  int adj[nparts*nparts];
  for (int i = 0; i < nparts; i++) {
    adj[i*nparts + i] = 0;
    for (int j = i; j < nparts; j++) {
      adj[i*nparts + j] = adj[j*nparts + i] = common_hedges (hg, map, 1, part, nparts, 0, i, 0, j);
    }
  }
  
  int path[nparts];
  int best_path[nparts];
  int best_value = 0;
  reorder_rec (adj, nparts, path, 0, INT_MAX, best_path, nparts, &best_value, vtxs);
  
  for (int i = 0; i < nparts; i++)
    neworder[best_path[i]] = i;
}
#endif

/* *********************************************************** */

void generateRandomOrder (int *order, int size) {
  order[0] = 0;
  for (int i = 1; i < size; i++) {
    int j = rand () % (i+1);
    order[i] = order[j];
    order[j] = i;
  }
}

/* *********************************************************** */

/* Change vertex weights */
/*
  int part_size[M];
  double xmin = +INFINITY, xmax = -INFINITY,
  ymin = +INFINITY, ymax = -INFINITY,
  zmin = +INFINITY, zmax = -INFINITY;
  for (int i = 0; i < m.nb_cells; i++) {
  double center[3];
  cellCenter (&m, i, center);
  if (center[0] < xmin)
  xmin = center[0];
  if (center[0] > xmax)
  xmax = center[0];
  if (center[1] < ymin)
  ymin = center[1];
  if (center[1] > ymax)
  ymax = center[1];
  if (center[2] < zmin)
  zmin = center[2];
  if (center[2] > zmax)
  zmax = center[2];
  }
  for (int i = 0; i < m.nb_cells; i++) {
  double center[3];
  cellCenter (&m, i, center);
  const double a = 0.3;
  if (center[0] > ((1+a)*xmin+(1-a)*xmax)/2.0 && center[0] < ((1-a)*xmin+(1+a)*xmax)/2.0 &&
  center[1] > ((1+a)*ymin+(1-a)*ymax)/2.0 && center[1] < ((1-a)*ymin+(1+a)*ymax)/2.0 /\*&&
  center[2] > ((1+a)*zmin+(1-a)*zmax)/2.0 && center[2] < ((1-a)*zmin+(1+a)*zmax)/2.0*\/)
  g.vwgts[i] = hg.vwgts[i] = 3;
  }
  
  memset (part_size, 0, M * sizeof(int));
  int hg_size = 0;
  for (int i = 0; i < hg.nvtxs; i++) {
  part_size[partA[i]] += hg.vwgts[i];
  hg_size += hg.vwgts[i];
  }
  int max = 0;
  for (int i = 0; i < M; i++)
  if (part_size[i] > max)
  max = part_size[i];
  printf ("Unbalance: %f\n", ((double)max) / (((double)hg_size)/M));
  
*/

/* *********************************************************** */

void unbalancePartition(int *vwgts, int nvtxs, int nparts, int *part, double *part_ub) {
  for (int i = 0; i < nvtxs; i++)
    vwgts[i] = 1;
  int *part_size = calloc (nparts, sizeof (int));
  for (int i = 0; i < nvtxs; i++)
    part_size[part[i]]++;
  
  /* find weight to add per vertex */
  int min_part = -1;
  if (part_ub) {
    for (min_part = 0; min_part < nparts; min_part++) {
      int p;
      for (p = 0; p < nparts; p++)
	if (part_ub[p]*part_size[min_part] - part_ub[min_part]*part_size[p] < 0)
	  break;
      if (p == nparts)
	break;
    }
  }
  
  int added_weight;
  if (part_ub) {
    added_weight= 1;
    while (1) {
      int p;
      for (p = 0; p < nparts; p++)
	if (part_ub[p]/part_ub[min_part]*part_size[min_part] > (1.0+added_weight)*part_size[p])
	  break;
      if (p == nparts)
	break;
      added_weight++;
    }
  }
  else
    added_weight = 2;      
  
  int *order = malloc (nvtxs * sizeof (int));
  generateRandomOrder (order, nvtxs);
  for (int p = 0; p < nparts; p++) {
    int n;
    if (part_ub) {
      n = (part_ub[p]/part_ub[min_part]*part_size[min_part] - part_size[p])/added_weight;
    }
    else {
      n = rand() % part_size[p];
    }
    
    for (int i = 0; i < nvtxs && n > 0; i++) {
      int v = order[i];
      if (part[v] == p) {
	vwgts[order[i]] = 1+added_weight;
	n--;
      }
    }
  }
  free (part_size);
  
}

/* *********************************************************** */

void unbalancePartitionLinear (int *vwgts, int nvtxs, int nparts, int *part, int ubfactor) {
  for (int i = 0; i < nvtxs; i++)
    vwgts[i] = 1;
  int *part_size = calloc (nparts, sizeof (int));
  for (int i = 0; i < nvtxs; i++)
    part_size[part[i]]++;
  
  int max_part_size = INT_MIN;
  for (int i = 0; i < nparts; i++)
    if (part_size[i] > max_part_size)
      max_part_size = part_size[i];
  
  assert (ubfactor > 0 && ubfactor < 100);
  double ub = 1.00 + ubfactor/100.0;
  double step = 2*max_part_size*(ub - 1)/((nparts - 1)*(2 - ub));
  
  int *vtx_order = malloc (nvtxs * sizeof (int));
  generateRandomOrder (vtx_order, nvtxs);
  int *part_order = malloc (nparts * sizeof (int));
  generateRandomOrder (part_order, nparts);
  for (int p = 0; p < nparts; p++) {
    double final_wgt = max_part_size + part_order[p] * step;
    int new_vtx_wgt = ceil (final_wgt/part_size[p]);
    if (new_vtx_wgt <= 1)
      continue;
    int n = (final_wgt-part_size[p])/(new_vtx_wgt-1);
    assert (n <= part_size[p]);
    
    for (int i = 0; i < nvtxs && n > 0; i++) {
      int v = vtx_order[i];
      if (part[v] == p) {
	vwgts[v] = new_vtx_wgt;
	n--;
      }
    }
  }
  free (part_size);
  
}

/* *********************************************************** */

