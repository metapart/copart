/* Co-partitioning Module */


#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <math.h>
#include <time.h>


#define TOLERANCE 1.001
#define UBFACTOR 5
#define BIPART 2

#include "repart.h"
#include "all.h"
#include "starpart.h"
#include "operators.h"
#include "schemes.h"
#include "copart.h"
#include "codiag.h"
#include "genmesh.h"

#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))

/* *********************************************************** */
/*                     COPART METHODS                        */
/* *********************************************************** */

int CoPart_RegisterMethods(StarPart_Context * ctx) {
assert(ctx);
StarPart_registerAllMethods(ctx);
printf("Registering CoPart methods...\n");
 StarPart_registerNewMethod(ctx,"BASIC/EXTEND", CoPart_EXTEND_call, CoPart_EXTEND_init, CoPart_EXTEND_desc);
 StarPart_registerNewMethod(ctx,"BASIC/REPART", CoPart_REPART_call, CoPart_REPART_init, CoPart_REPART_desc);
 StarPart_registerNewMethod(ctx,"BASIC/PROJECT", CoPart_PROJECT_call, CoPart_PROJECT_init, CoPart_PROJECT_desc);
 StarPart_registerNewMethod(ctx,"BASIC/RESTRICT", CoPart_RESTRICT_call, CoPart_RESTRICT_init, CoPart_RESTRICT_desc);
 StarPart_registerNewMethod(ctx,"BASIC/COPART", CoPart_COPART_call, CoPart_COPART_init, CoPart_COPART_desc);
 StarPart_registerNewMethod(ctx,"BASIC/BNDARRAY", CoPart_BNDARRAY_call, CoPart_BNDARRAY_init, CoPart_BNDARRAY_desc);
 StarPart_registerNewMethod(ctx,"BASIC/CODIAG", CoPart_CODIAG_call, CoPart_CODIAG_init, CoPart_CODIAG_desc);
 StarPart_registerNewMethod(ctx,"BASIC/INTEREDGES", CoPart_INTEREDGES_call, CoPart_INTEREDGES_init, CoPart_INTEREDGES_desc);
 StarPart_registerNewMethod(ctx,"BASIC/CUBE", CoPart_CUBE_call, CoPart_CUBE_init, CoPart_CUBE_desc);
  return EXIT_SUCCESS;
}

/* *********************************************************** */
/* Create a 3D cube mesh of dimension (dim x dim x dim). 
 * The default element type of the cube is hexahedron.
 * TODO: include other types. */
void CoPart_CUBE_init(StarPart_Context * ctx, char* method)  {

  StarPart_Data * data = StarPart_addMethodArgument(ctx, method, "data", STARPART_INOUT);
  StarPart_addNewItem(data, "dim", STARPART_IN, STARPART_INT, NEW_INT(1), STARPART_LOCAL, true);
  StarPart_addNewItem(data, "type", STARPART_IN, STARPART_STR, NEW_STR("hexa"), STARPART_LOCAL, true);
  StarPart_addNewItem(data, "graph", STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "mesh", STARPART_OUT, STARPART_MESH, NULL, STARPART_GLOBAL, true);

}

/* *********************************************************** */

void CoPart_CUBE_call(StarPart_Context * ctx, char* method)  {
  assert(ctx);  
  StarPart_Data * data = StarPart_getCurrentMethodArgument(ctx, "data");
  int dim = StarPart_getInt(data, "dim"); assert(dim);
  char * type = StarPart_getStr(data, "type");
  int elem_type = -1;
  if(!strcmp(type,"hexa")) elem_type = HEXAHEDRON;
  else fprintf (stderr, "ERROR: Wrong element type!\n");
  StarPart_Graph * g = StarPart_getGraph(data, "graph");
  StarPart_Mesh * m = StarPart_getMesh(data, "mesh");  
  
  if(!g) { g = allocGraph(); StarPart_setGraph(data, "graph", g, true); }
  if(!m) { m = allocMesh(); StarPart_setMesh(data, "mesh", m, true); }
  generateCubeMesh(dim, elem_type, 1.0, 1.0, 1.0, m);
  mesh2Graph(m, g);

}

/* *********************************************************** */
/* Create random edges between two input graphs (interedges). 
 * If not indicated, the size of the two subgraphs induced 
 * to the interedges corresponds to 5% of the smallest input graph (ifactor). 
 * The output is an intarray of interedges of size nbinteredges*VALUES_IN_LINE
 * and two boolean intarrays for each graph that indicates if a vertex 
 * is connected with an interedge or not (size of g1->nvtxs and g2->nvtxs respectively). */
void CoPart_INTEREDGES_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data1 = StarPart_addMethodArgument(ctx, method, "data1", STARPART_IN | STARPART_OUT);
  StarPart_Data * data2 = StarPart_addMethodArgument(ctx, method, "data2", STARPART_NO);

  StarPart_addNewItem(data1, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "array", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "ifactor", STARPART_IN, STARPART_INT, NEW_INT(5), STARPART_LOCAL, true); 
  StarPart_addNewItem(data1, "nbinteredges", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "array", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "interedges", STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);

}

/* *********************************************************** */

void CoPart_INTEREDGES_call(StarPart_Context * ctx, char * method)
{
  assert(ctx);
  StarPart_Data * data1 = StarPart_getCurrentMethodArgument(ctx, "data1");
  StarPart_Data * data2 = StarPart_getCurrentMethodArgument(ctx, "data2");
  Graph * g1 = StarPart_getGraph(data1, "graph"); 
  Graph * g2 = StarPart_getGraph(data2, "graph");
  assert(g1 && g2);
  int * interedges = StarPart_getIntArray(data1, "interedges");
  int nbinteredges = StarPart_getInt(data1, "nbinteredges");

  int * array1 = StarPart_getIntArray(data1, "array");
  int * array2 = StarPart_getIntArray(data2, "array");
  if(array1)  {
    StarPart_checkIntArray(data1, "array", g1->nvtxs);
    for(int i = 0 ; i < g1->nvtxs ; i++)
      array1[i] = -1; // re-initialise if already used
  }
  else array1 = StarPart_allocIntArray(data1, "array", g1->nvtxs, -1);  
  if(array2) {
    StarPart_checkIntArray(data2, "array", g2->nvtxs);
    for(int i = 0 ; i < g2->nvtxs ; i++)
      array2[i] = -1;
  }
  else array2 = StarPart_allocIntArray(data2, "array", g2->nvtxs, -1);
  assert( nbinteredges <= g1->nvtxs || nbinteredges <= g2->nvtxs );
  int ifactor = StarPart_getInt(data1, "ifactor");

  /* if nbinteredges is not given, use ifactor */
  /* TODO: use only ifactor, nbinteredges only as an output variable */
  if(!nbinteredges) {
    nbinteredges  = (int) round(MIN(g1->nvtxs,g2->nvtxs) / (100.0/ifactor));
    nbinteredges = MAX(nbinteredges, 1);
    StarPart_setInt(data1, "nbinteredges", nbinteredges);
  }
  if(!interedges)
    interedges = StarPart_allocIntArray(data1, "interedges", nbinteredges * VALUES_IN_LINE, -1);
    
  /* find subgraphs induced to random interedges */
  computeGraphSeeds(g1, BIPART, 0, array1);
  levelset1(g1, BIPART, nbinteredges-1, array1);
  for(int i = 0 ; i < g1->nvtxs ; i++)
    if(array1[i] == -1) array1[i] = 0;
  computeGraphSeeds(g2, BIPART, 0, array2);
  levelset1(g2, BIPART, nbinteredges-1, array2);
  for(int i = 0 ; i < g2->nvtxs ; i++)
    if(array2[i] == -1) array2[i] = 0;

  /* seting interedges (random connections) unitairy weight */
  int k = 0;
  for(int i = 0 ; i < g1->nvtxs ; i++) {
    if(array1[i]) {
      interedges[(k*VALUES_IN_LINE) + 0] = i; 
      k++;
    }
  }
  k = 0;
  for(int i = 0 ; i < g2->nvtxs ; i++) {
    if(array2[i]) {
      interedges[(k*VALUES_IN_LINE) + 1] = i; 
      k++;
    }
  }
  for(int i = 0 ; i < nbinteredges; i++) 
    interedges[(i*VALUES_IN_LINE) + 2] = 1;

}

/* *********************************************************** */
/* Load and store the boundary array, necessary for the t72. */
/* Currently not used. TODO: replace it with INTARRAY. */
void CoPart_BNDARRAY_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data = StarPart_addMethodArgument(ctx, method, "data", STARPART_INOUT);    
  StarPart_addNewItem(data, "filename", STARPART_IN, STARPART_STR, NULL, STARPART_LOCAL, true);
  StarPart_addNewItem(data, "first", STARPART_IN, STARPART_INT, NEW_INT(0), STARPART_LOCAL, true);
  StarPart_addNewItem(data, "last", STARPART_IN, STARPART_INT, NULL, STARPART_LOCAL, true);     
  StarPart_addNewItem(data, "boundary", STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
}

/* *********************************************************** */

void CoPart_BNDARRAY_call(StarPart_Context * ctx, char * method)
{
  assert(ctx);
  StarPart_Data * data = StarPart_getCurrentMethodArgument(ctx, "data");    
  char * filename = StarPart_getStr(data, "filename");
  int length = 0;
  int * array = NULL;
  
  if(filename) {
    FILE * file = fopen(filename, "r");
    if(!file) StarPart_error("fail to load \"%s\"!\n", filename);
    int tmp;
    while(fscanf(file, "%d", &tmp) > 0) length++;
    array = calloc(length, sizeof(int));
    assert(array);
    rewind(file);
    for(int i = 0 ; i < length; i++) fscanf(file, "%d",array +i);
    fclose(file);
  }
  else {
    int first = StarPart_getInt(data, "first");
    int last = StarPart_getInt(data, "last");
    assert(last > 0 && last >= first);
    length = last - first + 1;
    array = calloc(length, sizeof(int));
    assert(array);
    for(int i = first, k = 0 ; i <= last ; i++) array[k++] = i;
  }      
  StarPart_setIntArray(data, "boundary", length, array, true);
}

/*  ***********************************************************  */

/* Fix an input partition of a subgraph before extending 
 *  (partitioning) it to the entire graph. */
void CoPart_EXTEND_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data = StarPart_addMethodArgument(ctx, method, "data", STARPART_IN | STARPART_OUT);
  StarPart_addNewItem(data, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true); 
  StarPart_addNewItem(data, "fixed", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NULL, STARPART_GLOBAL, true);
}

/*  ***********************************************************  */

void CoPart_EXTEND_call(StarPart_Context * ctx, char * method)
{
  assert(ctx);    
  StarPart_Data * data = StarPart_getCurrentMethodArgument(ctx, "data");
  int * part = StarPart_getIntArray(data, "part");
  int * fixed = StarPart_getIntArray(data, "fixed");  
  Graph * g = StarPart_getGraph(data, "graph");  
  int nparts = StarPart_getInt(data, "nparts"); 

  assert(g);
  assert(nparts > 0);
  assert(part);
  StarPart_checkIntArray(data, "part", g->nvtxs);
  for(int i = 0; i < g->nvtxs; i++ )
    assert(part[i] >= -1 || part[i] < nparts); // allow vtxs set to -1.
  if(fixed) StarPart_checkIntArray(data, "fixed", g->nvtxs);
  else fixed = StarPart_allocIntArray(data, "fixed", g->nvtxs, 0);
  extension(g, part, fixed);
  /* check */
  assert(fixed);
  for(int i = 0; i < g->nvtxs; i++ )
    if( part[i] != -1) assert(fixed[i] == 1);
    else assert(fixed[i] == 0);
}

/*  ***********************************************************  */
/* Find a subgraph induced to a given boolean array information 
 * on the vertices of the graph. Return the restricted graph and 
 * the mapping information (intarray of size of the restricted graph). 
 * Variables bnd and bndmap are auxiliary arrays containing extra information 
 * on the boundary of the input graph (used when input is a cerfacs mesh 
 * and the restricted graph is not connected ). */
void CoPart_RESTRICT_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * in = StarPart_addMethodArgument(ctx, method, "in", STARPART_IN);
  StarPart_Data * out = StarPart_addMethodArgument(ctx, method, "out", STARPART_OUT);    
  StarPart_addNewItem(in, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(in, "array", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(out, "map",STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(out, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(in, "bnd", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(in, "bndmap", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_LOCAL, true); // out??
  StarPart_addNewItem(in, "isconnected", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(1), STARPART_GLOBAL, true); 
}

/*  ***********************************************************  */

void CoPart_RESTRICT_call(StarPart_Context * ctx, char * method) {
  
  assert(ctx);
  StarPart_Data * in = StarPart_getCurrentMethodArgument(ctx, "in");
  StarPart_Data * out = StarPart_getCurrentMethodArgument(ctx, "out"); 
  
  Graph * g = StarPart_getGraph(in, "graph");
  int * array = StarPart_getIntArray(in, "array");
  Graph * rg = StarPart_getGraph(out, "graph");
  int * rmap = StarPart_getIntArray(out, "map");
  int isconnected = StarPart_getInt(in, "isconnected");

  int * bnd = StarPart_getIntArray(in, "bnd");
  int * bndmap = StarPart_getIntArray(in, "bndmap");
  int flen = 0;
  int * fmap = NULL;

  assert(g);
  assert(array);
  assert(StarPart_getIntArrayLength(in, "array") == g->nvtxs); 
  /* check  */
  int array_set = 0;
  for( int i = 0 ; i <  g->nvtxs; i++ )
    if (array[i])
      array_set++;
  restriction(g, array, &rg, &rmap); // rmap of size rg
  assert(rg && rmap);
  assert( array_set == rg->nvtxs );
  
  flen = rg->nvtxs;
  fmap = rmap;
  /* check if restricted graph is connected. */
  /* if not try to rectify with boundary info. */
  if(isconnected) {
    if(!isConnectedSubgraph(g,array)) {
    //if(!isConnectedGraph(rg)) {
      if(!bnd)
	fprintf (stderr, "WARNING: restricted graph is not connected but boundary information is not available.\n");
      else { 
	Graph * bg;
	Graph _rg;
	restriction(g, bnd, &bg, &bndmap); 
	assert(bg); assert(bndmap);
	/* if(!isConnectedGraph(bg))  */
	/*   fprintf (stderr, "WARNING: boundary graph is not connected.\n"); */
	mergeGraphs(rg, bg, 1, 1, 1, 1,  &_rg);
	if(isConnectedGraph(&_rg)) {
	  fprintf (stderr, "DEBUG: restricted graph is connected with the help of boundary info!\n");
	  rg = &_rg;
	  flen = StarPart_getIntArrayLength(in, "bndmap"); 
	  // TODO : need the mapping of merged graph! Update mapping
	  fmap = bndmap;
	}
	else 
	  fprintf (stderr, "WARNING: boundary information is available but restricted graph is not connected.\n");
      }	   
    }
  }
  
  StarPart_setGraph(out, "graph", rg, true);
  StarPart_setIntArray(out, "map", flen, fmap, true);
}

/*  *************************************************************  */

/*-------------CONVENTION ABOUT INTEREDGES FILE ----------*/
/* interedges file (used as input in many co-partitioning functions) should have 2 or 3 columns */
/* The first column should index vertices in g1 graph that correspond to component 1  */
/* The first column should index vertices in g2 graph that correspond to component 2 */
/* The third column should provide the weight of each interedge. */
/* If only two columns are given, we consider that all weights are equal to 1.*/
/* so the array that stores the interedges it also includes the weight and is */
/* of size (3 x the number of lines in the interedges file). */
void CoPart_PROJECT_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * left = StarPart_addMethodArgument(ctx, method, "left", STARPART_IN);
  StarPart_Data * right = StarPart_addMethodArgument(ctx, method, "right", STARPART_OUT);
  StarPart_addNewItem(left, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(left, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(right, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(right, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(right, "fixed", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true); 
  StarPart_addNewItem(left, "pstrat", STARPART_IN, STARPART_STR, NULL, STARPART_LOCAL, true); 
  StarPart_addNewItem(left, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(left, "ubfactor", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(STARPART_UBFACTOR), STARPART_GLOBAL, true); 
  StarPart_addNewItem(left, "interedges", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(left, "finteredges", STARPART_IN , STARPART_STR, NULL, STARPART_GLOBAL, true); 
  StarPart_addNewItem(left, "inverse", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
}

/*  *************************************************************  */

void CoPart_PROJECT_call(StarPart_Context * ctx, char * method)
{
  assert(ctx);
  StarPart_Data * left = StarPart_getCurrentMethodArgument(ctx, "left");
  StarPart_Data * right = StarPart_getCurrentMethodArgument(ctx, "right");
  Graph * ga;
  Graph * gb;
  ga = StarPart_getGraph(left, "graph");
  gb = StarPart_getGraph(right,"graph");
  assert(ga && gb);

  int ubfactor = StarPart_getInt(left, "ubfactor");
  int * parta = StarPart_getIntArray(left, "part");
  char * pstrat = StarPart_getStr(left, "pstrat");
  int nparts = StarPart_getInt(left, "nparts");
  int inverse = StarPart_getInt(left, "inverse");
  assert(parta);
  StarPart_checkIntArray(left, "part", ga->nvtxs);
  for (int i = 0; i< ga->nvtxs; i++) {
    assert(parta[i] >= -1 && parta[i] < nparts);
  }
  int * partb = StarPart_getIntArray(right, "part");
  int * fixedb = StarPart_getIntArray(right, "fixed");
  int * interedges = StarPart_getIntArray(left, "interedges");
  int nbinteredges = 0; 
  int linteredges = 0;
  assert(interedges);
  linteredges = StarPart_getIntArrayLength(left, "interedges");
  nbinteredges = linteredges/VALUES_IN_LINE;
  if(partb) StarPart_checkIntArray(right, "part", gb->nvtxs);  
  else partb = StarPart_allocIntArray(right, "part", gb->nvtxs, -1);
  int * fixed = NULL;
  int * part = NULL;
  Graph * egraph = NULL;
  projection(ga, gb, parta, nparts,
	     nbinteredges, interedges,
  	     &part, &fixed, &egraph,
	     inverse);
  assert(part);
  assert(fixed);
  assert(egraph);

  /* process data in nested context */
  StarPart_Data * _data = StarPart_newData("data");
  StarPart_addNewItem(_data, "graph", STARPART_NO, STARPART_GRAPH, egraph, STARPART_GLOBAL, false);
  StarPart_addNewItem(_data, "part", STARPART_NO, STARPART_INTARRAY, NEW_INTARRAY(egraph->nvtxs, part), STARPART_GLOBAL, false);
  StarPart_addNewItem(_data, "nparts", STARPART_NO, STARPART_INT, NEW_INT(nparts), STARPART_GLOBAL, true);
  StarPart_addNewItem(_data, "ubfactor", STARPART_NO, STARPART_INT, NEW_INT(ubfactor), STARPART_GLOBAL, true);
  StarPart_addNewItem(_data, "fixed", STARPART_NO, STARPART_INTARRAY, NEW_INTARRAY(egraph->nvtxs, fixed),STARPART_GLOBAL, false);
  StarPart_Context * _ctx = StarPart_newNestedContext(ctx,_data);
  assert(pstrat);
  StarPart_processStrat(_ctx, pstrat);
  StarPart_freeNestedContext(_ctx);

  /* partb should be of size gb NOT of size egraph */
  /* ignore last nparts vertices used for the enriched graph */  
  for( int i = 0 ; i <  gb->nvtxs; i++ )
    partb[i] = part[i];
  
  if(fixedb) StarPart_checkIntArray(right, "fixed", gb->nvtxs);
  else fixedb = StarPart_allocIntArray(right, "fixed", gb->nvtxs, 0);
  for( int i = 0 ; i < gb->nvtxs; i++ )
    if(!partb[i])
      fixedb[i] = 1;
  
}

/*  *************************************************************  */
/* Repartition in a new nparts > older nparts. Only one scheme is currently used. */
/* TODO: include other schemes */
void CoPart_REPART_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data = StarPart_addMethodArgument(ctx, method, "data", STARPART_IN | STARPART_OUT);
  StarPart_addNewItem(data, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "fixed", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "ubfactor", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(STARPART_UBFACTOR), STARPART_GLOBAL, true); 
  StarPart_addNewItem(data, "pstrat", STARPART_IN, STARPART_STR, NULL, STARPART_LOCAL, true); 
  StarPart_addNewItem(data, "rscheme", STARPART_IN, STARPART_STR, NEW_STR("FIXEDVTXS"), STARPART_GLOBAL, true);
  StarPart_addNewItem(data, "cscheme", STARPART_IN, STARPART_STR, NEW_STR("GREEDYHB2"), STARPART_GLOBAL, true);
}

/*  *************************************************************  */

void CoPart_REPART_call(StarPart_Context * ctx, char * method)
{
  const int repartmult = 1;
  const int migcost = 10;
  // int doremap = 0; // we don't care about remapping in this case!
  assert(ctx);
  StarPart_Data * data = StarPart_getCurrentMethodArgument(ctx, "data");
  Graph * g = StarPart_getGraph(data, "graph");
  char * pstrat = StarPart_getStr(data, "pstrat");
  int nparts = StarPart_getInt(data, "nparts");
  int ubfactor = StarPart_getInt(data, "ubfactor");
  int * part = StarPart_getIntArray(data, "part");
  //char * rscheme = StarPart_getStr(data, "rscheme");
  //char * cscheme = StarPart_getStr(data, "cscheme");
  int * fixed = StarPart_getIntArray(data, "fixed");
  
  enum RepartScheme rscheme = FIXEDVTXS;
  enum CommScheme cscheme = GREEDYHB2;
  const void *comm_options = &GHB_NO_DIAG; // dont optimize migration volume strongly. opposite: &GHB_OPT_DIAG;
  assert(nparts > 0);  
  assert(g);
  assert(part);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    assert(part[i] >= 0 && part[i] < nparts);
  }
  if (fixed) {
    for(int i = 0 ; i < g->nvtxs ; i++)
      fixed[i] = 0;
  }
  Graph * egraph = NULL;
  repartition(g, nparts, part,
	      ubfactor, repartmult, migcost,
	      nparts,
	      rscheme, cscheme, comm_options,
	      NULL,
	      &egraph);
  assert(egraph);

  int * part_r = calloc(egraph->nvtxs,sizeof(int));
  for(int i = 0 ; i < g->nvtxs ; i++)
    part_r[i] = part[i];
  /* fix only the auxilairy vertices of egraph (the last nparts) */
  for(int i = 0 ; i < nparts ; i++)
    part_r[g->nvtxs+i] = i;
  /* un-fix regular vertices */
  int * fixed_r = calloc(egraph->nvtxs,sizeof(int));
  for(int i = 0 ; i < nparts ; i++)
    fixed_r[g->nvtxs+i] = 1;
  
  /* process data in nested context */
  StarPart_Data * _data = StarPart_newData("data");
  StarPart_addNewItem(_data, "graph", STARPART_NO, STARPART_GRAPH, egraph, STARPART_GLOBAL, false);
  StarPart_addNewItem(_data, "part", STARPART_NO, STARPART_INTARRAY, NEW_INTARRAY(egraph->nvtxs, part_r), STARPART_GLOBAL, false);
  StarPart_addNewItem(_data, "nparts", STARPART_NO, STARPART_INT, NEW_INT(nparts), STARPART_GLOBAL, true);
  StarPart_addNewItem(_data, "fixed", STARPART_NO, STARPART_INTARRAY,NEW_INTARRAY(egraph->nvtxs, fixed_r),STARPART_GLOBAL, false);
  StarPart_addNewItem(_data, "ubfactor", STARPART_NO, STARPART_INT, NEW_INT(ubfactor), STARPART_GLOBAL, true);
  StarPart_Context * _ctx = StarPart_newNestedContext(ctx, _data);
  assert(pstrat);
  StarPart_processStrat(_ctx, pstrat);
  StarPart_freeNestedContext(_ctx);  

  /* update part and fixed - ignore last vertices */
  for( int i = 0 ; i <  g->nvtxs; i++ )
    part[i] = part_r[i];
  /* un-fix regular vertices if fixed was given ?? */
  if(fixed)
    for( int i = 0 ; i < g->nvtxs; i++ )
      fixed[i] = fixed_r[i];
}

/*  *************************************************************  */
/* Copartition two input graphs based on a given scheme and the interedges. */
void CoPart_COPART_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data1 = StarPart_addMethodArgument(ctx, method, "data1", STARPART_IN | STARPART_OUT);
  StarPart_Data * data2 = StarPart_addMethodArgument(ctx, method, "data2", STARPART_NO);

  StarPart_addNewItem(data1, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "array", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "hmesh", STARPART_IN | STARPART_OUT, STARPART_HYBRIDMESH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "npartscpl", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "array", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "npartscpl", STARPART_IN | STARPART_OUT, STARPART_INT,NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "pstrat", STARPART_IN, STARPART_STR, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "ubfactor", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(STARPART_UBFACTOR), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "finteredges", STARPART_IN | STARPART_OUT, STARPART_STR, NULL, STARPART_GLOBAL, true); 
  StarPart_addNewItem(data1, "interedges", STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "nbinteredges", STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  /* inverse = 0 (A -> B) */ 
  StarPart_addNewItem(data1, "inverse", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "repart", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "scheme", STARPART_IN, STARPART_STRSEQ, NEW_STRSEQ("aware","aware|projrepart|naive"), STARPART_GLOBAL, true);
}

/* *********************************************************** */

void CoPart_COPART_call(StarPart_Context * ctx, char * method) {
  
  assert(ctx);
  StarPart_Data * data1 = StarPart_getCurrentMethodArgument(ctx, "data1");
  StarPart_Data * data2 = StarPart_getCurrentMethodArgument(ctx, "data2");

  Graph * g1 = StarPart_getGraph(data1, "graph");
  HybridMesh * hm = StarPart_getHybridMesh(data1, "hmesh");
  Graph * g2 = StarPart_getGraph(data2, "graph");
  char * finteredges = StarPart_getStr(data1, "finteredges");

  int * interedges = StarPart_getIntArray(data1, "interedges");
  int nbinteredges = StarPart_getInt(data1, "nbinteredges");
  int * array1 = StarPart_getIntArray(data1, "array");
  int * array2 = StarPart_getIntArray(data2, "array");


  int  npts1 = StarPart_getInt(data1, "nparts");
  int  nptscpl1 = StarPart_getInt(data1, "npartscpl");
  int  * part1 = StarPart_getIntArray(data1, "part");
  int  npts2 = StarPart_getInt(data2, "nparts");
  int  nptscpl2 = StarPart_getInt(data2, "npartscpl");
  int  * part2 = StarPart_getIntArray(data2, "part");
  char * pstrat = StarPart_getStr(data1, "pstrat");
  char * scheme = StarPart_getStrSeq(data1, "scheme");
  int  inverse = StarPart_getInt(data1, "inverse"); 
  int  repart = StarPart_getInt(data1, "repart");
  char * storeVal = malloc(STRATSIZE*sizeof(char));
  char * debugVal = malloc(STRATSIZE*sizeof(char));
  GHashTable * user = StarPart_getCurrentUserArguments(ctx);
  if(StarPart_isKey(user, "#debug")) strcpy(debugVal,",#debug");
  else strcpy(debugVal,"");
  char * store = (char*)StarPart_getValue(user, "#store");
  if(!StarPart_isKey(user, "#store")) strcpy(storeVal,"");
  else 
    if(store) sprintf(storeVal,", #store=%s",store);
    else strcpy(storeVal,",#store");
  if(!pstrat) {
    pstrat = "SCOTCH_MLKW";
    fprintf (stderr, "WARNING: SCOTCH_MLKW is used as a default pstrat!\n");
  }
  char * strategy= malloc(STRATSIZE*sizeof(char));
  assert(g1 && g2);
  if(part1) StarPart_checkIntArray(data1, "part", g1->nvtxs);
  else part1 = StarPart_allocIntArray(data1, "part", g1->nvtxs, -1);
  if(part2) StarPart_checkIntArray(data2, "part", g2->nvtxs);
  else part2 = StarPart_allocIntArray(data2, "part", g2->nvtxs, -1);

  assert(finteredges || (interedges && array1 && array2));

  if(array1) StarPart_checkIntArray(data1, "array", g1->nvtxs);
  else array1 = StarPart_allocIntArray(data1, "array", g1->nvtxs, 0);
  if(array2) StarPart_checkIntArray(data2, "array", g2->nvtxs);
  else array2 = StarPart_allocIntArray(data2, "array", g2->nvtxs, 0);
  /* --------------- */
  /* LOAD INTEREDGES */
  /* --------------- */
  // if finteredges is given as an input I assume that this is the file where I should find the interedges
  /* TODO: seperate the loading of interedges and the co-partitioning. */
  if(finteredges) {
    sprintf(strategy,"INTARRAY[filename=%s];",finteredges);
    assert(strategy);
    StarPart_Data * _data = StarPart_dupData(data1,1);
    StarPart_Context * _ctx = StarPart_newNestedContext(ctx, _data);
    StarPart_processStrat(_ctx, strategy);
    int * p_interedges = StarPart_getIntArray(_data, "array");
    assert(p_interedges);
    int  lp_interedges = StarPart_getIntArrayLength(_data, "array");
    int nb_columns = getNbColumnsInFile(finteredges);
    assert(nb_columns == 2 || nb_columns == 3);
    if(!nbinteredges) {
      nbinteredges = lp_interedges/nb_columns;
      StarPart_setInt(data1, "nbinteredges", nbinteredges);
    }
    else assert(nbinteredges == lp_interedges/nb_columns);
    if(!interedges)
      interedges = StarPart_allocIntArray(data1, "interedges", (nbinteredges * VALUES_IN_LINE), 0);
    else 
      StarPart_checkIntArray(data1, "interedges", nbinteredges * VALUES_IN_LINE);
    // TODO: ONLY FOR CERFACS HYBRID MESH FILES 
    if(hm)
      processCerfacsHybridMesh(hm,p_interedges,nbinteredges,nb_columns);
    processInteredges(p_interedges, lp_interedges, 
		      nb_columns, array1, 
		      array2, interedges);
    assert(array1 && array2 && interedges);
    StarPart_freeNestedContext(_ctx);
  }
  else  StarPart_checkIntArray(data1, "interedges", nbinteredges * VALUES_IN_LINE);
 
  int check1 = 0;
  for(int i = 0 ; i < g1->nvtxs; i++)  {
    if(array1[i]) { check1 = 1; break;}
  }
  if(!check1) printf("WARNING: empty subgraph from given interedges!\n");
  int check2 = 0;
  for(int i = 0 ; i < g2->nvtxs; i++)
    if(array2[i]) { check2 = 1; break;}
  if(!check2) printf("WARNING: empty subgraph from given interedges!\n");
  /* --------------- */

  int * useda = NULL;
  int * usedb = NULL;
  int len_ga = 0;
  int len_gb = 0;
  int nptsa = 0;
  int nptsb = 0;
  int nptscpla = 0;
  int nptscplb = 0;
  int len_rga = 0;
  int len_rgb = 0;
  StarPart_Data * dt_a = NULL;
  StarPart_Data * dt_b = NULL;
  int * parta = NULL;
  int * partb = NULL;
  Graph * ga = NULL;
  Graph * gb = NULL;

  if( !inverse ) { // A --> B
    useda = array1;
    usedb = array2;
    len_ga = g1->nvtxs;
    len_gb = g2->nvtxs;
    ga = g1;
    gb = g2;
    nptsa = npts1;
    nptsb = npts2;
    nptscpla = nptscpl1;
    nptscplb = nptscpl2;
    dt_a = data1;
    dt_b = data2;
    parta = part1;
    partb = part2;
  }
  else {  // B --> A
    useda = array2;
    usedb = array1;
    len_ga = g2->nvtxs;
    len_gb = g1->nvtxs;
    ga = g2;
    gb = g1;
    nptsa = npts2;
    nptsb = npts1;
    nptscpla = nptscpl2;
    nptscplb = nptscpl1;
    dt_a = data2;
    dt_b = data1;
    parta = part2;
    partb = part1;
  }
  assert(useda && usedb);
  for(int i = 0 ; i < len_ga; i++)
    if( useda[i] == 1 ) 
      len_rga++;
  for(int i = 0 ; i < len_gb; i++)
    if( usedb[i] == 1 ) 
      len_rgb++;
  if(repart)
    assert(nptscplb > 0 && nptscplb >= nptscpla && nptscplb <= nptsb);
  assert(nptscpla <= nptsa);

  // TODO: parta partb are obsolute
  // TODO: maybe also interedges for projrepart
  if (!strcmp(scheme,"aware"))
    aware(dt_a, dt_b, ctx, useda, usedb, parta, partb, storeVal, debugVal);
  else if(!strcmp(scheme,"projrepart"))
    projrepart(dt_a, dt_b, ctx , data1, useda, usedb, parta, partb, interedges, (nbinteredges * VALUES_IN_LINE), storeVal, debugVal);
  else if(!strcmp(scheme,"naive"))
    naive(dt_a, dt_b, ctx, parta, partb, storeVal, debugVal);
  else
    fprintf (stderr, "ERROR: Copart scheme is invalid!\n");
  free(strategy);
}

/* *********************************************************** */
/* Printing diagnostic for the copartitioning. */
void CoPart_CODIAG_init(StarPart_Context * ctx, char * method)
{
  StarPart_Data * data1 = StarPart_addMethodArgument(ctx, method, "data1", STARPART_IN | STARPART_OUT);
  StarPart_Data * data2 = StarPart_addMethodArgument(ctx, method, "data2", STARPART_NO);

  StarPart_addNewItem(data1, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "hmesh", STARPART_IN | STARPART_OUT, STARPART_HYBRIDMESH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "mesh", STARPART_IN | STARPART_OUT, STARPART_MESH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "npartscpl", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true); 
  StarPart_addNewItem(data2, "graph", STARPART_IN | STARPART_OUT, STARPART_GRAPH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "hmesh", STARPART_IN | STARPART_OUT, STARPART_HYBRIDMESH, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "mesh", STARPART_IN | STARPART_OUT, STARPART_MESH, NULL, STARPART_GLOBAL, true);

  StarPart_addNewItem(data2, "nparts", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "npartscpl", STARPART_IN | STARPART_OUT, STARPART_INT,NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data2, "part", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "pstrat", STARPART_IN, STARPART_STR, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "nbinteredges", STARPART_IN | STARPART_OUT, STARPART_INT,NEW_INT(0), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "interedges", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NULL, STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "scheme", STARPART_IN, STARPART_STRSEQ, NEW_STRSEQ("aware","aware|projrepart|naive"), STARPART_GLOBAL, true);
  StarPart_addNewItem(data1, "inverse", STARPART_IN | STARPART_OUT, STARPART_INT, NEW_INT(0), STARPART_GLOBAL, true);

}

/* *********************************************************** */

void CoPart_CODIAG_call(StarPart_Context * ctx, char * method)
{ 
  assert(ctx);
  StarPart_Data * data1 = StarPart_getCurrentMethodArgument(ctx, "data1");
  StarPart_Data * data2 = StarPart_getCurrentMethodArgument(ctx, "data2");

  int  inverse = StarPart_getInt(data1, "inverse");
  Graph * g_b = NULL;
  Graph * g_a = NULL;
  HybridMesh * hm_a = NULL; 
  HybridMesh * hm_b = NULL;
  Mesh * m_a = NULL; 
  Mesh * m_b = NULL;
  int  npts_b, nptscpl_b;
  int  * part_b = NULL;
  int  npts_a, nptscpl_a;
  int  * part_a = NULL;
  char * name_a = malloc(STRATSIZE*sizeof(char));
  char * name_b = malloc(STRATSIZE*sizeof(char));
     
  assert(inverse == 1 || !inverse);
  if(inverse) {
    g_b = StarPart_getGraph(data1, "graph");
    hm_b = StarPart_getHybridMesh(data1, "hmesh");
    m_b = StarPart_getMesh(data1, "mesh");
    g_a = StarPart_getGraph(data2, "graph");
    hm_a = StarPart_getHybridMesh(data2, "hmesh");
    m_a = StarPart_getMesh(data2, "mesh");
    npts_b = StarPart_getInt(data1, "nparts");
    nptscpl_b = StarPart_getInt(data1, "npartscpl");
    part_b = StarPart_getIntArray(data1, "part");
    npts_a = StarPart_getInt(data2, "nparts");
    nptscpl_a = StarPart_getInt(data2, "npartscpl");
    part_a = StarPart_getIntArray(data2, "part");
  
  }
  else {
    g_a = StarPart_getGraph(data1, "graph");
    hm_a = StarPart_getHybridMesh(data1, "hmesh");
    m_a = StarPart_getMesh(data1, "mesh");
    g_b = StarPart_getGraph(data2, "graph");
    hm_b = StarPart_getHybridMesh(data2, "hmesh");
    m_b = StarPart_getMesh(data2, "mesh");
    npts_a = StarPart_getInt(data1, "nparts");
    nptscpl_a = StarPart_getInt(data1, "npartscpl");
    part_a = StarPart_getIntArray(data1, "part");
    npts_b = StarPart_getInt(data2, "nparts");
    nptscpl_b = StarPart_getInt(data2, "npartscpl");
    part_b = StarPart_getIntArray(data2, "part");
  }
  char * pstrat = StarPart_getStr(data1, "pstrat");

  int nbinteredges = StarPart_getInt(data1, "nbinteredges");
  int  * interedges = StarPart_getIntArray(data1, "interedges");
  assert(interedges);
  char * scheme = StarPart_getStrSeq(data1, "scheme");
  if(hm_a)  sprintf(name_a,"hmesh_a_%d",getHybridMeshNbCells(hm_a)); // && !m_a)
  else if(m_a)  sprintf(name_a,"mesh_a_%d",m_a->nb_cells);
  if(hm_b)  sprintf(name_b,"hmesh_b_%d",getHybridMeshNbCells(hm_b)); 
  else if(m_b) sprintf(name_b,"mesh_b_%d",m_b->nb_cells);
  assert(name_a && name_b);

  CopartResults copartinfo;

  /* printf("[CODIAG] * Parta \n["); */
  /* for(int i = 0; i < g_a->nvtxs; i++ ) */
  /*   if(i < 20 || i > g_a->nvtxs -20) */
  /*   printf("%d ",part_a[i]); */
  /* printf("]\n"); */
  /* printf("[CODIAG] * Partb \n["); */
  /* for(int i = 0; i < g_b->nvtxs; i++ ) */
  /*   if(i < 20 || i > g_b->nvtxs -20) */
  /*   printf("%d ",part_b[i]); */
  /* printf("]\n"); */
  /* printf("[CODIAG] * Interedges \n"); */
  /* for(int i = 0 ; i < nbinteredges; i++) */
  /*   if(i < 10 || i > nbinteredges-10) */
  /*     printf("%d %d %d\n",interedges[(i*VALUES_IN_LINE) + 0],interedges[(i*VALUES_IN_LINE) + 1],interedges[(i*VALUES_IN_LINE) + 2]); */
  /* printf("\n"); */


  int intercomm[npts_a][npts_b];
  computeCopartDiagnostic(g_a, g_b,
  			  npts_a,
  			  npts_b,
  			  nptscpl_a, nptscpl_b,
  			  part_a, part_b,
  			  nbinteredges, interedges,
  			  &copartinfo,
  			  (int*)intercomm,
			  inverse);

  printCopartDiagnostic(g_a, g_b,
		       npts_a,
		       npts_b,
		       nptscpl_a, nptscpl_b,
		       part_a, part_b,
		       nbinteredges, interedges,
		       inverse);

  printf("# MESHA MESHB METHOD SCHEME NPARTSA NPARTSB NPARTSACPL NPARTSBCPL NPARTSACPLR NPARTSBCPLR UBA UBB UBACPL UBBCPL CUTA CUTB CUTACPL CUTBCPL MAXV MAXVOPT TOTV TOTVOPT TOTZ TOTZM TOTZOP\n");

  printf("%s %s %s %s %d %d %d %d %d %d %.2f %.2f %.2f %.2f %d %d %d %d %d %d %d %d %d %d %d\n",
  	 name_a,name_b,
  	 pstrat,
  	 scheme,
  	 copartinfo.k_a, copartinfo.k_b,
  	 copartinfo.kcpl_a, copartinfo.kcpl_b,
  	 copartinfo.kcplreal_a, copartinfo.kcplreal_b,
  	 copartinfo.maxub_a, copartinfo.maxub_b,
  	 copartinfo.maxubcpl_a, copartinfo.maxubcpl_b,
  	 copartinfo.edgecut_a, copartinfo.edgecut_b,
  	 copartinfo.edgecutcpl_a, copartinfo.edgecutcpl_b,
  	 copartinfo.maxV, copartinfo.maxVopt,
  	 copartinfo.totV, copartinfo.totVopt,
  	 copartinfo.totZ, copartinfo.totZM, copartinfo.totZopt
  	 );

  free(name_a);
  free(name_b);
}

/* *********************************************************** */
