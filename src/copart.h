#ifndef __COPART_H__
#define __COPART_H__



#include <stdbool.h>

#include "starpart.h"

#define STRATSIZE 400
#define CoPart_PROJECT_desc "projection of an initial partition to a surface connected with interedges"
#define CoPart_COPART_desc "general copart function with some default options. A specific scheme should still be selected. "
#define CoPart_REPART_desc "repartition an initial partition into the same or greater number of parts"
#define CoPart_EXTEND_desc "fix vertices that belong to an initial partition. Similar to StarPart_FIXED_call"
#define CoPart_RESTRICT_desc "return a subgraph of an initial graph based on a input information"
/* CoPart_BNDARRAY_call is used for problematic mesh intersection as in the 
 * case of T72. It should not be a generic function. 
 * TODO: replace it with an intarray_call that will create a 
 * new item, name it and add it in the current data.  */
#define CoPart_BNDARRAY_desc "load boundary information for a mesh (additional array for mesh intersection)"
#define CoPart_CODIAG_desc "print basic diagnostic for the copartitioned graphs"
#define CoPart_INTEREDGES_desc "create random interedges between two disjoint graphs"
#define CoPart_CUBE_desc "create a 3D hexahedron mesh cube"
/* *********************************************************** */
/*                    ARGS MANAGEMENT                          */
/* *********************************************************** */
int CoPart_RegisterMethods(StarPart_Context * ctx);

void CoPart_PROJECT_call(StarPart_Context * ctx, char * method);
void CoPart_COPART_call(StarPart_Context * ctx, char * method);
void CoPart_REPART_call(StarPart_Context * ctx, char * method);
void CoPart_EXTEND_call(StarPart_Context * ctx, char * method);
void CoPart_RESTRICT_call(StarPart_Context * ctx, char * method);
void CoPart_BNDARRAY_call(StarPart_Context * ctx, char * method);
void CoPart_CODIAG_call(StarPart_Context * ctx, char * method);
void CoPart_INTEREDGES_call(StarPart_Context * ctx, char * method);
void CoPart_CUBE_call(StarPart_Context * ctx, char * method);

void CoPart_PROJECT_init(StarPart_Context * ctx, char * method);
void CoPart_COPART_init(StarPart_Context * ctx, char * method);
void CoPart_REPART_init(StarPart_Context * ctx, char * method);
void CoPart_EXTEND_init(StarPart_Context * ctx, char * method);
void CoPart_RESTRICT_init(StarPart_Context * ctx, char * method);
void CoPart_BNDARRAY_init(StarPart_Context * ctx, char * method);
void CoPart_CODIAG_init(StarPart_Context * ctx, char * method);
void CoPart_INTEREDGES_init(StarPart_Context * ctx, char * method);
void CoPart_CUBE_init(StarPart_Context * ctx, char * method);

#endif
