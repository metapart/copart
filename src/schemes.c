/* Co-partitioning Module */
/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <math.h>
#include <time.h>


#include <ctype.h>


#include "all.h"
#include "starpart.h"
#include "operators.h"
#include "schemes.h"

#define STRATSIZE 400

// TODO: I do not have to pass part1 nor part2
int aware(StarPart_Data * data1, StarPart_Data * data2, StarPart_Context * ctx, int * array1, int * array2, int * part1, int * part2, char * storeStr,char * debugStr) {

  assert(ctx);
  int  npts1 = StarPart_getInt(data1, "nparts");
  int  nptscpl1 = StarPart_getInt(data1, "npartscpl");
  char * pstrat = StarPart_getStr(data1, "pstrat");  
  int  npts2 = StarPart_getInt(data2, "nparts");
  int  nptscpl2 = StarPart_getInt(data2, "npartscpl");
  int  ubfactor = StarPart_getInt(data1, "ubfactor");
  Graph * g1 = StarPart_getGraph(data1, "graph");
  Graph * g2 = StarPart_getGraph(data2, "graph");
  char * strategy= malloc(STRATSIZE*sizeof(char));
  assert(storeStr && debugStr);
  if(!strcmp(storeStr,""))  printf("Tag #store: off\n");
  else printf("Tag #store: on\n");
  if(!strcmp(debugStr,""))  printf("Tag #debug: off\n");
  else printf("Tag #debug: on\n");

  /* -------------------------------------- */
  /*  RESTRICT/PART/REMAP/EXTEND COMPONENT1 */
  /* -------------------------------------- */
  char * data1ID = StarPart_getDataID(data1);
  // enforce KMETIS partitioner for the first coupling partitioning
  sprintf(strategy,"RESTRICT[@in=%s %s %s];KMETIS[nparts=%d,ubfactor=%d %s %s];",data1ID,storeStr,debugStr,nptscpl1,ubfactor,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  
  /* process data in nested context */
  StarPart_Data * _data1 = StarPart_dupData(data1,1);
  StarPart_addNewItem(_data1, "array", STARPART_IN | STARPART_OUT,STARPART_INTARRAY, NEW_INTARRAY(g1->nvtxs, array1), STARPART_GLOBAL, false);
  StarPart_Context * ctx1 = StarPart_newNestedContext(ctx, _data1);
  StarPart_processStrat(ctx1, strategy);
  /* acquire the output data */
  StarPart_Data * rdata1 = StarPart_getCurrentData(ctx1);
  Graph * rg1 = StarPart_getGraph(rdata1, "graph");
  int * rpart1 = StarPart_getIntArray(rdata1, "part");
  int * rmap1 = StarPart_getIntArray(rdata1, "map");
  assert( rg1->nvtxs == StarPart_getIntArrayLength(rdata1, "part"));
  assert(rg1);
  assert(rpart1);
  assert(rmap1);
  /* check */
  for(int i = 0; i < rg1->nvtxs; i++ )
    assert(rpart1[i]!=-1);
  
  printf("[AWARE] * Printing Diagnostics of data \"%s\":\n",data1ID);
  // storing the arrays for extend is redundant
  sprintf(strategy,"REMAP[@out=%s %s %s];EXTEND[nparts=%d %s];%s[nparts=%d %s %s];DIAG;",data1ID,storeStr,debugStr,npts1,debugStr,pstrat,npts1,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  StarPart_processStrat(ctx1, strategy);
  //part1 = StarPart_getIntArray(_data1, "part");
  assert( g1->nvtxs == StarPart_getIntArrayLength(_data1, "part"));
  //assert(part1);
  StarPart_freeNestedContext(ctx1);
  printf("[AWARE] * End of Diagnostics for data \"%s\":\n",data1ID);
  /* -------------------------------------- */

  /* -------------------------------------- */
  /*  RESTRICT/PART/REMAP/EXTEND COMPONENT1 */
  /* -------------------------------------- */
  char * data2ID = StarPart_getDataID(data2);
  sprintf(strategy,"RESTRICT[@in=%s %s %s];KMETIS[nparts=%d,ubfactor=%d %s %s];",data2ID,debugStr,storeStr,nptscpl2,ubfactor,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  /* process data in another nested context */
  StarPart_Data * _data2 = StarPart_dupData(data2,1);
  StarPart_Context * ctx2 = StarPart_newNestedContext(ctx, _data2);
  StarPart_addNewItem(_data2, "array", STARPART_IN | STARPART_OUT,STARPART_INTARRAY, NEW_INTARRAY(g2->nvtxs, array2), STARPART_GLOBAL, false);
  StarPart_processStrat(ctx2, strategy);
  /* acquire the output data */
  StarPart_Data * rdata2 = StarPart_getCurrentData(ctx2);
  Graph * rg2 = StarPart_getGraph(rdata2, "graph");
  int * rpart2 = StarPart_getIntArray(rdata2, "part");
  int * rmap2 = StarPart_getIntArray(rdata2, "map");
  assert( rg2->nvtxs == StarPart_getIntArrayLength(rdata2, "part"));
  assert(rg2);
  assert(rpart2);
  assert(rmap2);
  printf("[AWARE] * Printing Diagnostics of data \"%s\":\n",data2ID);
  sprintf(strategy,"REMAP[@out=%s %s %s];EXTEND[nparts=%d %s];%s[nparts=%d %s %s];DIAG;",data2ID,debugStr,storeStr,npts2,debugStr,pstrat,npts2,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  StarPart_processStrat(ctx2, strategy);
  //part2 = StarPart_getIntArray(_data2, "part");
  assert( g2->nvtxs == StarPart_getIntArrayLength(_data2, "part"));
  //assert(part2);
  StarPart_freeNestedContext(ctx2);
  /* -------------------------------------- */
 return 1;
}



int projrepart(StarPart_Data * dt_a, StarPart_Data * dt_b, StarPart_Context * ctx, 
	       StarPart_Data * data, int * useda, int * usedb, int * parta, int * partb,
	       int * interedges, int linteredges, char * storeStr, char * debugStr) {

  assert(ctx);
  int  nptsa = StarPart_getInt(dt_a, "nparts");
  int  nptscpla = StarPart_getInt(dt_a, "npartscpl");
  int  nptsb = StarPart_getInt(dt_b, "nparts");
  int  nptscplb = StarPart_getInt(dt_b, "npartscpl");
  Graph * ga = StarPart_getGraph(dt_a, "graph");
  Graph * gb = StarPart_getGraph(dt_b, "graph");
  // common data
  char * pstrat = StarPart_getStr(data, "pstrat");  
  int  ubfactor = StarPart_getInt(data, "ubfactor");
  int  inverse = StarPart_getInt(data, "inverse"); // inverse = 0 corresponds to A -> B 
  int  repart = StarPart_getInt(data, "repart");
  char * strategy= malloc(STRATSIZE*sizeof(char));
  assert(storeStr && debugStr);
  if(!strcmp(storeStr,""))  printf("Tag #store: off\n");
  else printf("Tag #store: on\n");
  if(!strcmp(debugStr,""))  printf("Tag #debug: off\n");
  else printf("Tag #debug: on\n");
  int * _interedges = malloc( linteredges * sizeof(int));
  for ( int i = 0 ; i < linteredges; i++ )
    _interedges[i] = interedges[i]; 

  /* -------------------------------------- */
  /*  RESTRICT/PART/REMAP/EXTEND COMPONENT_A --> PROJECT COMPONENT_B/EXTEND */
  /* -------------------------------------- */
  char * dt_aID = StarPart_getDataID(dt_a);
  printf("[PROJR] * Processing data: \"%s\" (as ga)\n",dt_aID);
  // enforce KMETIS partitioner for the first cpl partitioning
  sprintf(strategy,"RESTRICT[@in=%s %s %s];KMETIS[nparts=%d,ubfactor=%d %s %s];", 
	  dt_aID, debugStr, storeStr, nptscpla, ubfactor, storeStr, debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  StarPart_Data * _dt_a = StarPart_dupData(dt_a,1);
  StarPart_addNewItem(_dt_a, "array", STARPART_IN | STARPART_OUT,STARPART_INTARRAY, NEW_INTARRAY(ga->nvtxs, useda), STARPART_LOCAL, false); // TODO: global or local? Currently global but should go back to local
  /* process data in nested context */
  StarPart_Context * ctx_ab = StarPart_newNestedContext(ctx, _dt_a);
  StarPart_processStrat(ctx_ab, strategy);
  // need to acquire the output rdata
  StarPart_Data * rdt_a = StarPart_getCurrentData(ctx_ab);
  Graph * rga = StarPart_getGraph(rdt_a, "graph");
  int * rparta = StarPart_getIntArray(rdt_a, "part");
  int * rmapa = StarPart_getIntArray(rdt_a, "map");
  assert(rga);
  assert(rparta);
  assert(rmapa);
  
  char * rdt_aID = StarPart_getDataID(rdt_a);
  sprintf(strategy,"REMAP[@in=%s, @out=%s %s %s];EXTEND[nparts=%d %s];%s[nparts=%d,ubfactor=%d %s %s]", rdt_aID, dt_aID, debugStr, storeStr, nptsa, debugStr, pstrat, nptsa, ubfactor, storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  StarPart_processStrat(ctx_ab, strategy);
  parta = StarPart_getIntArray(_dt_a, "part");
  assert( ga->nvtxs == StarPart_getIntArrayLength(_dt_a, "part"));
  assert(parta);
  int * mapa = StarPart_getIntArray(_dt_a, "map");
  assert( ga->nvtxs == StarPart_getIntArrayLength(_dt_a, "map"));
  assert(mapa);
  
  char * dt_bID = StarPart_getDataID(dt_b);
  StarPart_Data * _dt_b = StarPart_dupData(dt_b,1);
  StarPart_registerData(ctx_ab, _dt_b); 
  /*----------------------------------------------*/

  sprintf(strategy,"RESTRICT[@in=%s %s %s];",dt_bID,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  StarPart_addNewItem(_dt_b, "array", STARPART_IN | STARPART_OUT,STARPART_INTARRAY, NEW_INTARRAY(gb->nvtxs, usedb), STARPART_LOCAL, false);
  StarPart_processStrat(ctx_ab, strategy);
  /* acquire the output data */
  StarPart_Data * rdt_b = StarPart_getCurrentData(ctx_ab);
  Graph * rgb = StarPart_getGraph(rdt_b, "graph");
  int * rmapb = StarPart_getIntArray(rdt_b, "map");
  assert(rgb);
  assert(rmapb);
  int * mapb = NULL;
  if(rmapb && !mapb) {
    mapb = calloc(gb->nvtxs,sizeof(int));
    for(int i = 0 ; i < rgb->nvtxs ; i++)
      mapb[rmapb[i]] = i;    
  }
  char * rdt_bID = StarPart_getDataID(rdt_b);
  /*----------------------------------------------*/
  printf("[PROJR] * Processing data: \"%s\" (as gb)\n",dt_bID);
  if(repart)
    sprintf(strategy," PROJECT[@left=%s,@right=%s,pstrat=%s,inverse=%d %s %s];REPART[nparts=%d,pstrat=%s %s %s];REMAP[@in=%s, @out=%s %s %s];EXTEND[@data=%s,nparts=%d %s];%s[nparts=%d,ubfactor=%d %s %s]",rdt_aID,rdt_bID,pstrat,inverse,storeStr,debugStr,nptscplb,pstrat,storeStr,debugStr,rdt_bID,dt_bID,debugStr,storeStr,dt_bID,nptsb,debugStr,pstrat,nptsb,ubfactor,debugStr,storeStr);
  else
    sprintf(strategy," PROJECT[@left=%s,@right=%s,pstrat=%s,inverse=%d,#debug %s %s];REMAP[@in=%s, @out=%s %s %s];EXTEND[@data=%s,nparts=%d %s];%s[nparts=%d,ubfactor=%d %s %s]",rdt_aID,rdt_bID,pstrat,inverse,debugStr,storeStr,rdt_bID,dt_bID,storeStr,debugStr,dt_bID,nptsb,debugStr,pstrat,nptsb,ubfactor,storeStr,debugStr);

  assert(strategy);
  printf("%s\n",strategy);
  if(StarPart_getGraph(rdt_a, "graph")->nvtxs == rga->nvtxs) {
    /* update interedges array to index rga and not ga (mapping from ga to rga)*/
    assert(mapa);
    for ( int i = 0 ; i < linteredges; i = i + 3 ) {
      _interedges[i + inverse] = mapa[_interedges[i + inverse]];
    }
  }
  if(StarPart_getGraph(rdt_b, "graph")->nvtxs == rgb->nvtxs) {
    /* update interedges array to index rgb and not gb (mapping from gb to rgb)*/
    assert(mapb);
    for ( int i = 0 ; i < linteredges; i = i + 3 ) {
      _interedges[i + (inverse ^ 1)] = mapb[_interedges[i + (inverse ^ 1)]];
    }
  }

  /* printf("After processing .. \n"); */
  /* printf("[PROJREPART] * Interedges\n"); */
  /* for(int i = 0 ; i < linteredges; i++) { */
  /*   printf("%d ",_interedges[i]); */
  /*   if((i%VALUES_IN_LINE == 2)) */
  /*     printf("\n"); */
  /*   if(i == 29) break; */
  /* } */
  /* printf("[PROJREPART] * End Interedges \n"); */


  StarPart_addNewItem(rdt_a, "interedges", STARPART_IN | STARPART_OUT, STARPART_INTARRAY, NEW_INTARRAY(linteredges,_interedges), STARPART_LOCAL, false);
  StarPart_processStrat(ctx_ab, strategy);
  // the "output" data here is data2 (size:gb->nvts) - not the size of a reduced graph
  partb = StarPart_getIntArray(_dt_b, "part");
  assert( gb->nvtxs == StarPart_getIntArrayLength(_dt_b, "part"));
  assert(partb);

 
  StarPart_freeNestedContext(ctx_ab);
  free(_interedges);
  return 1;
}


int naive(StarPart_Data * data1, StarPart_Data * data2, StarPart_Context * ctx, int * part1, int * part2, char * storeStr, char * debugStr) {

  assert(ctx);
  int  npts1 = StarPart_getInt(data1, "nparts");
  char * pstrat = StarPart_getStr(data1, "pstrat");  
  int  npts2 = StarPart_getInt(data2, "nparts");
  int  ubfactor = StarPart_getInt(data1, "ubfactor");
  //Graph * g1 = StarPart_getGraph(data1, "graph");
  //Graph * g2 = StarPart_getGraph(data2, "graph");

  char * strategy= malloc(STRATSIZE*sizeof(char));
  assert(storeStr && debugStr);
  if(!strcmp(storeStr,""))  printf("Tag #store: off\n");
  else printf("Tag #store: on\n");
  if(!strcmp(debugStr,""))  printf("Tag #debug: off\n");
  else printf("Tag #debug: on\n");

  char * data1ID = StarPart_getDataID(data1);
  printf("[NAIVE] * Printing Diagnostics of data \"%s\":\n",data1ID);
  sprintf(strategy,"%s[@data=%s,nparts=%d,ubfactor=%d %s %s];DIAG",pstrat,data1ID,npts1,ubfactor,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  /* process data in nested context */
  StarPart_Data * _data1 = StarPart_dupData(data1,1);
  StarPart_Context * ctx1 = StarPart_newNestedContext(ctx, _data1);
  StarPart_processStrat(ctx1, strategy);
  StarPart_freeNestedContext(ctx1);
  printf("[NAIVE] * End of Diagnostics for data \"%s\":\n",data1ID);
  /* -------------------------------------- */

  char * data2ID = StarPart_getDataID(data2);
  printf("[AWARE] * Printing Diagnostics of data \"%s\":\n",data2ID);
  sprintf(strategy,"%s[@data=%s,nparts=%d,ubfactor=%d %s %s];DIAG",pstrat,data2ID,npts2,ubfactor,storeStr,debugStr);
  assert(strategy);
  printf("%s\n",strategy);
  /* process data in another nested context */
  StarPart_Data * _data2 = StarPart_dupData(data2,1);
  StarPart_Context * ctx2 = StarPart_newNestedContext(ctx, _data2);
  StarPart_processStrat(ctx2, strategy);
  
  // part2 = StarPart_getIntArray(_data2, "part");
  StarPart_freeNestedContext(ctx2);
  /* -------------------------------------- */
 return 1;
}
