/* Migration Routine */
/* Author(s): aurelien.esnard@labri.fr */

#include "migration.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#ifdef USEGLPK
#include <glpk.h>
#endif

#include "misc.h"
#include "cg.h"

//#define DEBUG
#include "debug.h"

#define TOL 1e-6

/* *********************************************************** */

static void opt_vtot_norm1(Graph * g, int ubfactor, int * migmat);       // using algebraic edge weight
static void opt_vmax_norm1(Graph * g, int ubfactor, int * migmat);       // using algebraic edge weight
static void opt_vtot_norm1_abs(Graph * g, int ubfactor, int * migmat);   
static void opt_vmax_norm1_abs(Graph * g, int ubfactor, int * migmat);   
static void opt_vtot_norm2(Graph * g, int ubfactor, int * migmat);       // conjugate gradient


/* *********************************************************** */

void computeMigrationMatrix(Graph * g, int ubfactor, enum MigrationScheme scheme, int * migmat)
{
  assert(migmat);
  memset(migmat, 0, g->nvtxs*g->nvtxs*sizeof(int));
  
  if (scheme == MIG_VTOT)
    opt_vtot_norm1(g, ubfactor, migmat);
  else if (scheme == MIG_VMAX)
    opt_vmax_norm1(g, ubfactor, migmat);
  else if (scheme == MIG_VTOT_ABS)
    opt_vtot_norm1_abs(g, ubfactor, migmat);
  else if (scheme == MIG_VTOT2)
    opt_vtot_norm2(g, 0, migmat); /* ubfactor not used! */
  else if (scheme == MIG_VMAX_ABS) 
    opt_vmax_norm1_abs(g, ubfactor, migmat);
  else {
    fprintf(stderr, "migrationModel: invalid scheme.\n");
    exit(EXIT_FAILURE);
  }
  
}

/* *********************************************************** */

#define PRINTF(x,n,s) 							\
  do { printf("%s = {",(s));						\
    for(int i = 0 ; i < (n) ; i++) printf("%f ", (x)[i]);		\
    printf("}\n"); } while(0)						

#define PRINTI(x,n,s) 							\
  do { printf("%s = {",(s));						\
    for(int i = 0 ; i < (n) ; i++) printf("%d ", (x)[i]);		\
    printf("}\n"); } while(0)						

#define PRINT2I(x,n,m,s)						\
  do { printf("%s = {\n",(s));						\
    for(int i = 0 ; i < (n) ; i++) {					\
      for(int j = 0 ; j < (m) ; j++) printf("%d ", (x)[i*m+j]);		\
      printf("\n");							\
    }									\
    printf("}\n"); } while(0)						

#define PRINT2F(x,n,m,s)						\
  do { printf("%s = {\n",(s));						\
    for(int i = 0 ; i < (n) ; i++) {					\
      for(int j = 0 ; j < (m) ; j++) printf("%e ", (x)[i*m+j]);		\
      printf("\n");							\
    }									\
    printf("}\n"); } while(0)						



/* *********************************************************** */

// simplex solver (using glpk)
void opt_vtot_norm1(Graph * g, int ubfactor, int * migmat)
{
#ifndef USEGLPK
  fprintf(stderr, "ERROR: The GLPK library is not available (USEGLPK not defined).\n");
  exit(EXIT_FAILURE);
#else
  
  // compute load unbalance...
  double * b = malloc(g->nvtxs*sizeof(double));
  int totload = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) totload += g->vwgts[i];
  int avgload = totload / g->nvtxs;
  int remainload = totload % g->nvtxs;
  PRINT("total load = %d, average load = %d (remain %d)\n", totload, avgload, remainload);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    // PRINT("%d\n", g->vwgts[i]);
    b[i] =  g->vwgts[i] - ((i < remainload)?1:0) - avgload;  
  }
  //PRINTF(b, g->nvtxs, "b");
  
  int check = 0;
  for(int i = 0; i < g->nvtxs ; i++) check += b[i];
  PRINT("check sum of b_i = %d\n", check);    
  assert(check == 0);
  
  // create glpk problem
  glp_prob *lp;
  lp = glp_create_prob();
  glp_term_out(0); /* enable/disable terminal output */
  glp_set_prob_name(lp, "VTOT");
  glp_set_obj_dir(lp, GLP_MIN);
  
  // add |V| constraints (as rows)
  glp_add_rows(lp, g->nvtxs);  
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v%d", i);
    glp_set_row_name(lp, i+1, name);
    int lb = b[i]-ubfactor*avgload/100;
    int ub = b[i]+ubfactor*avgload/100;
    glp_set_row_bnds(lp, i+1, GLP_DB, lb, ub); /* balancing up to an unbalanced factor */
    PRINT("[row %d] declare constraint %d <= %s <= %d\n", i+1, lb, name,  ub);
  }
  
  // add |E| variables (as cols)  
  glp_add_cols(lp, 2*g->nedges);
  int gmat[g->nvtxs][g->nvtxs];
  int z = 1;
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      gmat[i][j] = z;
      char name[10]; sprintf(name, "e(%d,%d)", i, j);
      PRINT("[col %d] declare variable %s >= 0\n", z, name);
      glp_set_col_kind(lp, z, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
      glp_set_col_name(lp, z, name);
      glp_set_col_bnds(lp, z, GLP_LO, 0.0, 0.0); /* 0.0 <= e(i,j)  */	
      glp_set_obj_coef(lp, z, 1.0); /* minimize sum of 1.0 * e(i,j) */
      z++;
    }
  
  // g->nvtxs equations, g->nedges*2 data
  int ia[1+1000];
  int ja[1+1000];
  double ar[1+1000];  
  int zz=1;
  int row = 1;
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; 
      PRINT(" + e(%d,%d)", i, j);
      zz++;
      ia[zz] = row, ja[zz] = gmat[j][i], ar[zz] = -1.0; 
      PRINT(" - e(%d,%d)", j, i);
      zz++;
    }
    PRINT("\n");
    row++;
  }
  assert(4*g->nedges == zz-1); // check
  glp_load_matrix(lp, 4*g->nedges, ia, ja, ar);
  
  // solve problem 
  PRINT("minimize z = sum of all e(i,j)\n"); /* assuming e(i,j) >= 0 */
  glp_simplex(lp, NULL);
  double vtot1 = glp_get_obj_val(lp);
  PRINT("vtot1 = %g\n", vtot1);
  
  // compute migration matrix (output)
  
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {
      int j = g->adjncy[k];  /* edge (i,j) */
      int kk1 = gmat[i][j];
      int kk2 = gmat[j][i];
      double w1 = glp_get_col_prim(lp, kk1);
      double w2 = glp_get_col_prim(lp, kk2);
      assert(w1 == 0 || w2 == 0);
      // update edge weight as output
      // if(w1 > 0) g->ewgts[k] = w1; else g->ewgts[k] = -w2;
      if(w1 > 0) migmat[i*g->nvtxs+j] = w1, migmat[j*g->nvtxs+i] = -w1; // round !!!!???
      if(w1 > 0) PRINT("[col %d] e(%d,%d) = %g\n", kk1, i, j, w1);
    }
  
  // compute new weight
  for(int i = 0 ; i < g->nvtxs ; i++) {
    double delta = glp_get_row_prim(lp,i+1);
    double newwgt = g->vwgts[i] - delta;
    PRINT("new weight %d: %g (%g%%)\n", i, newwgt, (newwgt-avgload)/avgload*100.0);
  }
  
  // free
  glp_delete_prob(lp);
  glp_free_env();
  free(b);

#endif
  
}

/* *********************************************************** */

// simplex solver (using glpk)
void opt_vmax_norm1(Graph * g, int ubfactor, int * migmat)  
{
#ifndef USEGLPK
  fprintf(stderr, "ERROR: The GLPK library is not available (USEGLPK not defined).\n");
  exit(EXIT_FAILURE);
#else

  // compute load unbalance...
  double * b = malloc(g->nvtxs*sizeof(double));
  int totload = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) totload += g->vwgts[i];
  int avgload = totload / g->nvtxs;
  int remainload = totload % g->nvtxs;
  PRINT("total load = %d, average load = %d (remain %d)\n", totload, avgload, remainload);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    // PRINT("%d\n", g->vwgts[i]);
    b[i] =  g->vwgts[i] - ((i < remainload)?1:0) - avgload;  
  }
  PRINTF(b, g->nvtxs, "b");
  
  int check = 0;
  for(int i = 0; i < g->nvtxs ; i++) check += b[i];
  PRINT("check sum of b_i = %d\n", check);    
  assert(check == 0);
  
  // create glpk problem
  glp_prob *lp;
  lp = glp_create_prob();
  glp_set_prob_name(lp, "VMAX");
  glp_set_obj_dir(lp, GLP_MIN);
  
  // add |V| constraints (as rows)
  int row = 1;
  glp_add_rows(lp, 2*g->nvtxs);  
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v%d", i);
    glp_set_row_name(lp, i+1, name);
    int lb = b[i]-ubfactor*avgload/100;
    int ub = b[i]+ubfactor*avgload/100;
    glp_set_row_bnds(lp, i+1, GLP_DB, lb, ub); /* balancing up to an unbalanced factor */
    PRINT("[row %d] declare constraint %d <= %s <= %d\n", i+1, lb, name,  ub);
    row++;
  }
  
  /* constraints for max (|V|) */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v'%d", i);
    glp_set_row_name(lp, row, name);
    glp_set_row_bnds(lp, row, GLP_UP, 0.0, 0.0); /* v'i=(vi-z) <= 0 */
    PRINT("[row %d] declare constraint %s=(v%d-z) <= 0\n", row, name,  i);
    row++;
  }
  int rowmax = row-1;
  
  // add 2*|E| variables (as cols)  
  glp_add_cols(lp, 2*g->nedges+1);
  int gmat[g->nvtxs][g->nvtxs];
  int col = 1;
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      gmat[i][j] = col;
      char name[10]; sprintf(name, "e(%d,%d)", i, j);
      PRINT("[col %d] declare variable %s >= 0\n", col, name);
      glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
      glp_set_col_name(lp, col, name);
      glp_set_col_bnds(lp, col, GLP_LO, 0.0, 0.0); /* 0.0 <= e(i,j)  */	
      glp_set_obj_coef(lp, col, 0.0); /* not used for minimization... */
      col++;
    }
  
  /* 1 variable z for max problem (as col) */  
  PRINT("[col %d] declare variable z >= 0\n", col);
  glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
  glp_set_col_name(lp, col, "z");
  glp_set_col_bnds(lp, col, GLP_LO, 0.0, 0.0); /* 0.0 <= z  */
  glp_set_obj_coef(lp, col, 1.0); /* minimize 1.0 * z */
  int zcol = col;
  
  int ia[1+1000];
  int ja[1+1000];
  double ar[1+1000];  
  
  /* |V| regular equations (4*|E| data) */
  int zz=1;
  row = 1;
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; 
      PRINT(" + e(%d,%d)", i, j);
      zz++;
      ia[zz] = row, ja[zz] = gmat[j][i], ar[zz] = -1.0; 
      PRINT(" - e(%d,%d)", j, i);
      zz++;
    }
    PRINT("\n");
    row++;
  }
  assert(4*g->nedges == zz-1); // check
  
  /* |V| max equations (4*|E|+|V| data) */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v'%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; /* + 1.0 * e(i,j) */  
      PRINT(" + e(%d,%d)", i, j);
      zz++;
      ia[zz] = row, ja[zz] = gmat[j][i], ar[zz] = 1.0; /* + 1.0 * e(j,i) */  
      PRINT(" + e(%d,%d)", j, i);
      zz++;
    }
    ia[zz] = row, ja[zz] = zcol, ar[zz] = -1.0; /* -1 * z */
    PRINT(" - z\n");
    zz++;
    row++;
  }
  
  assert(zz-1 == (8*g->nedges+g->nvtxs));  // check  
  assert(rowmax == row-1); //check
  
  glp_load_matrix(lp, zz-1, ia, ja, ar);
  PRINT("minimize z = max(v'0, v'1, v'2, ...)\n");
  
  // solve problem 
  glp_simplex(lp, NULL);
  double vmax = glp_get_obj_val(lp);
  PRINT("vmax = %g\n", vmax);
  
  
  // compute migration matrix (output)
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {
      int j = g->adjncy[k];  /* edge (i,j) */
      int kk1 = gmat[i][j];
      int kk2 = gmat[j][i];
      double w1 = glp_get_col_prim(lp, kk1);
      double w2 = glp_get_col_prim(lp, kk2);
      if(w1 > 0) migmat[i*g->nvtxs+j] = w1, migmat[j*g->nvtxs+i] = -w1; // round !!!!???
      if(w1 > 0) PRINT("[col %d] e(%d,%d) = %g\n", kk1, i, j, w1);
      assert(w1 == 0 || w2 ==0);
    }
  
  // check z col
  double c = glp_get_col_prim(lp, zcol);
  PRINT("z variable: %g \n", c);
  
  // check row constraints
  for(int i = 0 ; i < g->nvtxs ; i++) {
    int row = g->nvtxs+i+1;
    double c = glp_get_row_prim(lp, row);
    PRINT("constraint for row %d (v'%d): %g \n", row, i, c);
  }
  
  // free
  glp_delete_prob(lp);
  glp_free_env();
  free(b);

#endif
  
}


/* *********************************************************** */

// simplex solver (using glpk)
void opt_vtot_norm1_abs(Graph * g, int ubfactor, int * migmat) 
{
#ifndef USEGLPK
  fprintf(stderr, "ERROR: The GLPK library is not available (USEGLPK not defined).\n");
  exit(EXIT_FAILURE);
#else

  // compute load unbalance...
  double * b = malloc(g->nvtxs*sizeof(double));
  int totload = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) totload += g->vwgts[i];
  int avgload = totload / g->nvtxs;
  int remainload = totload % g->nvtxs;
  PRINT("total load = %d, average load = %d (remain %d)\n", totload, avgload, remainload);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    // PRINT("%d\n", g->vwgts[i]);
    b[i] =  g->vwgts[i] - ((i < remainload)?1:0) - avgload;  
  }
  PRINTF(b, g->nvtxs, "b");
  
  int check = 0;
  for(int i = 0; i < g->nvtxs ; i++) check += b[i];
  PRINT("check sum of b_i = %d\n", check);    
  assert(check == 0);
  
  // create glpk problem
  glp_prob *lp;
  lp = glp_create_prob();
  glp_set_prob_name(lp, "VTOT_ABS");
  glp_set_obj_dir(lp, GLP_MIN);
  
  // add |V|+2*|E| constraints (as rows)
  glp_add_rows(lp, g->nvtxs + 2*g->nedges);  
  int row = 1;
  /* vertex constraints */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v%d", i);
    glp_set_row_name(lp, row, name);
    // glp_set_row_bnds(lp, row, GLP_FX, b[i], b[i]);  /* perfect balancing */
    int lb = b[i]-ubfactor*avgload/100;
    int ub = b[i]+ubfactor*avgload/100;
    glp_set_row_bnds(lp, row, GLP_DB, lb, ub); /* balancing up to an unbalanced factor */
    PRINT("[row %d] declare vertex constraint %d <= %s <= %d\n", row, lb, name,  ub);
    row++;
  }
  
  /* constraints for absolute variable (trick) */
  for(int i = 0 ; i < 2*g->nedges ; i++) {
    char name[10]; sprintf(name, "row%d", row);
    glp_set_row_name(lp, row, name);
    // PRINT("[row %d] declare edge constraint for abs <= %s\n", row, name);
    glp_set_row_bnds(lp, row, GLP_LO, 0.0, 0.0); /* >= 0 */
    row++;
  }
  
  // add |E| variables (as cols)  
  glp_add_cols(lp, 2*g->nedges); // for each variable e(i,j) with i < j, we declare abs(e(i,j)) as well  
  int col = 1;
  int gmat[g->nvtxs][g->nvtxs]; /* gmat[i][j] = col number in glpk for variable e(i,j) */
  memset(gmat, 0, sizeof(gmat));
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	gmat[i][j] = gmat[j][i] = col;
	char name[10]; sprintf(name, "e(%d,%d)", i, j);
	PRINT("[col %d] declare variable %s free\n", col, name);
	glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
	glp_set_col_name(lp, col, name);
	glp_set_col_bnds(lp, col, GLP_FR, 0.0, 0.0); /* e(i,j) is unbounded */
	glp_set_obj_coef(lp, col, 0.0); /* unused in objective function to minimize */
	col++;
      }
    }
  
  /* absolute variables... */
  int gmatabs[g->nvtxs][g->nvtxs]; /* gmatabs[i][j] = col number in glpk for variable abs(e(i,j)) */
  memset(gmatabs, 0, sizeof(gmatabs));
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	gmatabs[i][j] = gmatabs[j][i] = col;
	char name[10]; sprintf(name, "abs(e(%d,%d))", i, j);
	PRINT("[col %d] declare variable %s >= 0\n", col, name);
	glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
	glp_set_col_name(lp, col, name);
	glp_set_col_bnds(lp, col, GLP_LO, 0.0, 0.0); /* 0.0 <= abs(e(i,j))  */
	glp_set_obj_coef(lp, col, 1.0); /* minimize sum of 1.0 * abs(e(i,j)) */
	col++;
      }
    } 
  
  // data 
  int ia[1+1000];
  int ja[1+1000];
  double ar[1+1000];  
  int zz=1;
  row = 1;
  /* linear equation for balancing */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; /* + 1.0 * e(i,j) with i < j */
	PRINT(" + e(%d,%d)", i, j);
      }
      else {
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = -1.0; /* - 1.0 * e(j,i) with j < i */
	PRINT(" - e(%d,%d)", j, i);
      }
      zz++;
    }
    PRINT("\n");
    row++;
  }
  assert(zz-1 == g->nedges*2);
  
  /* linear equation for absolute variables */
  /* nota bene: abs(x) >= x and abs(x) >= -x, with abs(x) >= 0 and x free */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	/* abs(e(i,j)) >= e(i,j) */
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = 1.0; /* + 1.0 * abs(e(i,j)) */
	zz++;
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = -1.0; /* - 1.0 * e(i,j) */
	zz++;	
	row++;
	/* abs(e(i,j)) >= -e(i,j) */
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = 1.0; /* + 1.0 * abs(e(i,j)) */
	zz++;
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; /* -+1.0 * e(i,j) */
	zz++;	
	row++;
      }
    }
  }
  assert(zz-1 == g->nedges*2+g->nedges*4);
  glp_load_matrix(lp, g->nedges*2+g->nedges*4, ia, ja, ar);
  
  // solve problem 
  PRINT("minimize z = sum of all abs(e(i,j))\n");
  glp_simplex(lp, NULL);
  double vtot1 = glp_get_obj_val(lp);
  PRINT("vtot1 = %g\n", vtot1);
  
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {
      int j = g->adjncy[k];  /* edge (i,j) */
      int kk = gmat[i][j];
      double w = glp_get_col_prim(lp, kk);
      // g->ewgts[k] = (i < j)?w:-w; // update edge weight as output
      if(w > 0) migmat[i*g->nvtxs+j] = w, migmat[j*g->nvtxs+i] = -w; 
    }
  
  // compute new weight
  for(int i = 0 ; i < g->nvtxs ; i++) {
    double delta = glp_get_row_prim(lp,i+1);
    double newwgt = g->vwgts[i] - delta;
    PRINT("new weight %d: %g (%g%%)\n", i, newwgt, (newwgt-avgload)/avgload*100.0);
    // g->vwgts[i] = newwgt; // update vertex weight as output
    // migmat[i*g->nvtxs+i] = newwgt;
  }
  
  // free
  glp_delete_prob(lp);
  glp_free_env();
  free(b);

  #endif
}

/* *********************************************************** */

// conjugate gradient solver (ubfactor = 0)
void opt_vtot_norm2(Graph * g, int ubfactor, int * migmat)	       
{
  // allocate matrices and vectors
  int * L = calloc(g->nvtxs*g->nvtxs,sizeof(int));
  double * A = malloc(g->nvtxs*g->nvtxs*sizeof(double));
  double * b = malloc(g->nvtxs*sizeof(double));
  double * x = malloc(g->nvtxs*sizeof(double));
  double * bb = malloc(g->nvtxs*sizeof(double));
  
  PRINT("build laplacian matrix of graph\n");
  laplacian(g, L);
  PRINT2I(L, g->nvtxs, g->nvtxs, "A");  
  for(int i = 0 ; i < g->nvtxs*g->nvtxs ; i++) A[i] = L[i];  
  
  // compute load unbalance...
  int totload = 0;
  for(int i = 0 ; i < g->nvtxs ; i++) totload += g->vwgts[i];
  int avgload = totload / g->nvtxs;
  int remainload = totload % g->nvtxs;
  PRINT("total load = %d, average load = %d (remain %d)\n", totload, avgload, remainload);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    // PRINT("%d\n", g->vwgts[i]);
    b[i] =  g->vwgts[i] - ((i < remainload)?1:0) - avgload;  
  }
  PRINTF(b, g->nvtxs, "b");
  
  int check = 0;
  for(int i = 0; i < g->nvtxs ; i++) check += b[i];
  PRINT("check sum of b_i = %d\n", check);    
  assert(check == 0);
  
  // conjugate grandient solver
  PRINT("solve Ax=b\n");
  cg(g->nvtxs, A, b, x);
  PRINTF(x, g->nvtxs, "x");
  
  // check A.x-b=0
  PRINT("Check Ax-b = 0\n");
  for(int i = 0 ; i < g->nvtxs ; i++) {
    bb[i] = -b[i];
    for(int j = 0 ; j < g->nvtxs ; j++)
      bb[i] += L[i*g->nvtxs+j]*x[j];
  }  
  PRINTF(bb, g->nvtxs, "Ax-b");
  for(int i = 0 ; i < g->nvtxs ; i++) assert(fabs(bb[i]) < TOL);
  
  /* for all edges */
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int j = g->xadj[i] ; j < g->xadj[i+1] ; j++) {
      int k = g->adjncy[j];  /* edge (i,k) */
      int w = round(x[i]-x[k]);
      // g->ewgts[j] = w; // for svg output
      if(w > 0) migmat[i*g->nvtxs+j] = w, migmat[j*g->nvtxs+i] = -w;
      // if(i < k) PRINT("edge (%d,%d) = %f\n", i, k, x[i]-x[k]);
    }  
  
  // migmat[i*g->nvtxs+i] = ???;
  
  /* free memory */
  free(A); 
  free(L);
  free(b);
  free(bb);
  free(x);
}

/* *********************************************************** */

// simplex solver (using glpk)
void opt_vmax_norm1_abs(Graph * g, int ubfactor, int * migmat)	   
{
#ifndef USEGLPK
  fprintf(stderr, "ERROR: The GLPK library is not available (USEGLPK not defined).\n");
  exit(EXIT_FAILURE);
#else

  // compute load unbalance...
  double * b = malloc(g->nvtxs*sizeof(double));
  int totload = 0;
  for(int i = 0 ; i <  g->nvtxs ; i++) totload += g->vwgts[i];
  int avgload = totload / g->nvtxs;
  int remainload = totload % g->nvtxs;
  PRINT("total load = %d, average load = %d (remain %d)\n", totload, avgload, remainload);
  for(int i = 0 ; i < g->nvtxs ; i++) {
    // PRINT("%d\n", g->vwgts[i]);
    b[i] =  g->vwgts[i] - ((i < remainload)?1:0) - avgload;  
  }
  PRINTF(b, g->nvtxs, "b");
  
  int check = 0;
  for(int i = 0; i < g->nvtxs ; i++) check += b[i];
  PRINT("check sum of b_i = %d\n", check);    
  assert(check == 0);
  
  // create glpk problem
  glp_prob *lp;
  lp = glp_create_prob();
  glp_set_prob_name(lp, "VMAX_ABS");
  glp_set_obj_dir(lp, GLP_MIN);
  
  
  
  // add all constraints (as rows)
  glp_add_rows(lp, 2*g->nvtxs+2*g->nedges);  
  int row = 1;
  /* regular constraints (|V|) */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v%d", i);
    glp_set_row_name(lp, row, name);
    // glp_set_row_bnds(lp, i+1, GLP_FX, b[i], b[i]);  /* perfect balancing */
    int lb = b[i]-ubfactor*avgload/100;
    int ub = b[i]+ubfactor*avgload/100;
    glp_set_row_bnds(lp, row, GLP_DB, lb, ub); /* balancing up to an unbalanced factor */
    PRINT("[row %d] declare constraint %d <= %s <= %d\n", row, lb, name,  ub);
    row++;
  }
  /* constraints for max (|V|) */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    char name[10]; sprintf(name, "v'%d", i);
    glp_set_row_name(lp, row, name);
    glp_set_row_bnds(lp, row, GLP_UP, 0.0, 0.0); /* v'i=(vi-z) <= 0 */
    PRINT("[row %d] declare constraint %s=(v%d-z) <= 0\n", row, name,  i);
    row++;
  }
  /* constraints for abs (2*|E|) */
  for(int i = 0 ; i < 2*g->nedges ; i++) {
    char name[10]; sprintf(name, "row%d", row);
    glp_set_row_name(lp, row, name);
    glp_set_row_bnds(lp, row, GLP_LO, 0.0, 0.0); /* >= 0 */
    row++;
  }
  int rowmax = row-1;
  
  // add variables (as cols)  
  glp_add_cols(lp, 2*g->nedges+1);
  
  /* |E| regular variables e(i,j) */
  int col = 1;
  int gmat[g->nvtxs][g->nvtxs];
  memset(gmat, 0, sizeof(gmat));
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	gmat[i][j] = gmat[j][i] = col; /* gmat[i][j] = col number in glpk for variable e(i,j) */
	char name[10]; sprintf(name, "e(%d,%d)", i, j);
	PRINT("[col %d] declare variable %s free\n", col, name);
	glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
	glp_set_col_name(lp, col, name);
	glp_set_col_bnds(lp, col, GLP_FR, 0.0, 0.0); /* e(i,j) free  */
	glp_set_obj_coef(lp, col, 0.0); 
	col++;
      }
    }
  
  /* |E| absolute variables abs(e(i,j)) */
  int gmatabs[g->nvtxs][g->nvtxs]; /* gmatabs[i][j] = col number in glpk for variable abs(e(i,j)) */
  memset(gmatabs, 0, sizeof(gmatabs));
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	gmatabs[i][j] = gmatabs[j][i] = col;
	char name[10]; sprintf(name, "abs(e(%d,%d))", i, j);
	PRINT("[col %d] declare variable %s >= 0\n", col, name);
	glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
	glp_set_col_name(lp, col, name);
	glp_set_col_bnds(lp, col, GLP_LO, 0.0, 0.0); /* 0.0 <= abs(e(i,j))  */
	glp_set_obj_coef(lp, col, 0.0); 
	col++;
      }
    }   
  
  /* 1 variable z for max problem */  
  PRINT("[col %d] declare variable z >= 0\n", col);
  glp_set_col_kind(lp, col, GLP_CV); /* CV: continous variable, IV: integer variable, BV: binary variable */
  glp_set_col_name(lp, col, "z");
  glp_set_col_bnds(lp, col, GLP_LO, 0.0, 0.0); /* 0.0 <= z  */
  glp_set_obj_coef(lp, col, 1.0); /* minimize 1.0 * z */
  int zcol = col;
  
  // data 
  int ia[1+1000];
  int ja[1+1000];
  double ar[1+1000];  
  int zz=1;
  /* |V| regular equations (2*|E| data) */
  row = 1;
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; /* + 1.0 * e(i,j) */
	PRINT(" + e(%d,%d)", i, j);
      }
      else {
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = -1.0;  /* - 1.0 * e(j,i) */
	PRINT(" - e(%d,%d)", j, i);
      }
      zz++;
    }
    row++;
    PRINT("\n");
  }
  assert(zz-1 == (2*g->nedges));  // check  
  /* |V| max equations (2*|E|+|V| data) */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    PRINT("[row %d] v'%d =", row, i);
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = 1.0; /* + 1.0 * abs(e(i,j)) */
	PRINT(" + |e(%d,%d)|", i, j);
      }
      else {
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = +1.0; /* + 1.0 * abs(e(j,i)) */
	PRINT(" + |e(%d,%d)|", j, i);
      }
      zz++;
    }
    ia[zz] = row, ja[zz] = zcol, ar[zz] = -1.0; /* -1 * z */
    PRINT(" - z\n");
    zz++;
    row++;
  }
  assert(zz-1 == (4*g->nedges+g->nvtxs));  // check  
  /* 2*|E| abs equations (4*|E| data) */
  /* nota bene: abs(x) >= x and abs(x) >= -x, with abs(x) >= 0 and x free */
  for(int i = 0 ; i < g->nvtxs ; i++) {
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++)  {	
      int j = g->adjncy[k];  /* edge (i,j) */
      if(i < j) {
	/* abs(e(i,j)) >= e(i,j) */
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = 1.0; /* + 1.0 * abs(e(i,j)) */
	zz++;
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = -1.0; /* - 1.0 * e(i,j) */
	zz++;	
	row++;
	/* abs(e(i,j)) >= -e(i,j) */
	ia[zz] = row, ja[zz] = gmatabs[i][j], ar[zz] = 1.0; /* + 1.0 * abs(e(i,j)) */
	zz++;
	ia[zz] = row, ja[zz] = gmat[i][j], ar[zz] = 1.0; /* -+1.0 * e(i,j) */
	zz++;	
	row++;
      }
    }
  }
  assert(zz-1 == (8*g->nedges+g->nvtxs));  // check
  assert(rowmax == row-1); //check
  glp_load_matrix(lp, zz-1, ia, ja, ar);
  PRINT("minimize z = max(v'0, v'1, v'2, ...)\n");
  
  // solve problem 
  glp_simplex(lp, NULL);
  double vmax = glp_get_obj_val(lp);
  PRINT("vmax (send+recv) = z = %g\n", vmax);
  
  for(int i = 0 ; i < g->nvtxs ; i++)
    for(int k = g->xadj[i] ; k < g->xadj[i+1] ; k++) {
      int j = g->adjncy[k];  /* edge (i,j) */
      int kk = gmat[i][j];
      double w = glp_get_col_prim(lp, kk);
      // g->ewgts[k] = (i < j)?w:-w; // update edge weight as output
      if(w > 0) migmat[i*g->nvtxs+j] = w, migmat[j*g->nvtxs+i] = -w;
    }
  
  // compute new weight
  for(int i = 0 ; i < g->nvtxs ; i++) {
    double delta = glp_get_row_prim(lp,i+1);
    double newwgt = g->vwgts[i] - delta;
    PRINT("new weight %d: %g (%g%%)\n", i, newwgt, (newwgt-avgload)/avgload*100.0);
    // g->vwgts[i] = newwgt; // update vertex weight as output
    // migmat[i*g->nvtxs+i] = newwgt;
  }
  
  // check row constraints
  for(int i = 0 ; i < g->nvtxs ; i++) {
    int row = g->nvtxs+i+1;
    double c = glp_get_row_prim(lp, row);
    PRINT("constraint for row %d (v'%d): %g \n", row, i, c);
  }
  
  // check z col
  double c = glp_get_col_prim(lp, zcol);
  PRINT("z variable: %g \n", c);
  
  // free
  glp_delete_prob(lp);
  glp_free_env();
  free(b);

#endif
  
}

/* *********************************************************** */

void printVarName (FILE *file, Graph *g, int i, int bCol, int mig_objective) {
  i--;
  if (bCol) {
    if (i < 2*g->nedges) {
      fprintf (file, "e_%d", i);
      return;
    }
    i -= 2*g->nedges;
    if (mig_objective & MIG_MAX) {
      if (i == 0) {
	fprintf (file, "%c", (mig_objective & MIG_VOLUME ? 'v' : 'z'));
	return;
      }
      i--;
    }
    if (mig_objective & MIG_MESSAGES) {
      if (i < 2*g->nedges) {
	fprintf (file, "x_%d", i);
	return;
      }
      i -= 2*g->nedges;
    }
  }
  else {
    if (i < g->nvtxs) {
      fprintf (file, "v_%d", i);
      return;
    }
    i -= g->nvtxs;
    if (mig_objective & MIG_MAX) {
      if (i < g->nvtxs) {
	fprintf (file, "%s_%d", (mig_objective & MIG_VOLUME ? "v'" : "z"), i);
	return;
      }
      i -= g->nvtxs;
    }
    if (mig_objective & MIG_MESSAGES) {
      if (i < 4*g->nedges) {
	fprintf (file, "%c_%d", (i%2 == 0 ? 'a' : 'b'), i/2);
	return;
      }
      i -= 4*g->nedges;
    }
  }
  fprintf (file, "error");
}
  

/* *********************************************************** */

void computeMigrationMatrixLP (Graph *g, int ubfactor, int mig_objective, int *migmat)
{

#ifndef USEGLPK
  fprintf(stderr, "ERROR: The GLPK library is not available (USEGLPK not defined).\n");
  exit(EXIT_FAILURE);
#else

  int total_wgt = 0;
  for (int i = 0; i < g->nvtxs; i++)
    total_wgt += g->vwgts[i];
  double avg_wgt = (double)total_wgt/g->nvtxs;
  
  glp_term_out(0); /* enable/disable terminal output */
  glp_prob *prob = glp_create_prob ();
  glp_set_obj_dir (prob, GLP_MIN);
  
  int nrows = g->nvtxs;		/* load variation for each part (v_i) */
  int ncols = 2*g->nedges;	/* edge migration volume (e_ij) */
  if (mig_objective & MIG_MAX) {
    nrows += g->nvtxs;	/* volume or messages per part minus max (v'_i or z_i) */
    ncols++;		/* max for all parts (v or z) */
  }
  if (mig_objective & MIG_MESSAGES) {
    nrows += 4*g->nedges;	/* used for binary vars constraints (a_ij, b_ij)*/
    ncols += 2*g->nedges;		/* binary vars for each message (x_ij) */
  }
  
  glp_add_rows (prob, nrows);
  glp_add_cols (prob, ncols);
  
  // Rows
  int row = 1;
  for (int i = 0; i < g->nvtxs; i++) {
#ifdef DEBUG
    fprintf (stderr, "%lg < v_%d < %lg\n", avg_wgt*(1-ubfactor/100.0) - g->vwgts[i], i, avg_wgt*(1+ubfactor/100.0) - g->vwgts[i]); /* v_i */
#endif
    glp_set_row_bnds (prob, row+i, GLP_DB, avg_wgt*(1-ubfactor/100.0) - g->vwgts[i], avg_wgt*(1+ubfactor/100.0) - g->vwgts[i]); /* v_i */
  }
  row += g->nvtxs;
  if (mig_objective & MIG_MAX) {
    for (int i = 0; i < g->nvtxs; i++) {
      glp_set_row_bnds (prob, row+i, GLP_UP, 0, 0); /* v'_i or z_i */
    }
    row += g->nvtxs;
  }
  if (mig_objective & MIG_MESSAGES) {
    for (int i = 0; i < 2*g->nedges; i++) {
      glp_set_row_bnds (prob, row+2*i, GLP_LO, 0, 0); /* a_ij */
      glp_set_row_bnds (prob, row+2*i+1, GLP_UP, 0, 0); /* b_ij */
    }
    row += 4*g->nedges;
  }
  
  // Cols
  int col = 1;
  for (int i = 0; i < 2*g->nedges; i++) { /* e_ij */
    glp_set_col_kind (prob, col+i, GLP_IV);
    glp_set_col_bnds (prob, col+i, GLP_LO, 0.0, 0.0);
    if (mig_objective == MIG_TOTAL_V)
      glp_set_obj_coef (prob, col+i, 1.0);
    else
      glp_set_obj_coef (prob, col+i, 0.0);
  }
  col += 2*g->nedges;
  if (mig_objective & MIG_MAX) { /* v or z */
    glp_set_col_kind (prob, col, GLP_CV);
    glp_set_col_bnds (prob, col, GLP_LO, 0.0, 0.0);
    if (mig_objective & MIG_MAX)
      glp_set_obj_coef (prob, col, 1.0);
    else
      glp_set_obj_coef (prob, col, 0.0);
    col ++;
  }
  if (mig_objective & MIG_MESSAGES) {
    for (int i = 0; i < 2*g->nedges; i++) { /* x_ij */
      glp_set_col_kind (prob, col+i, GLP_BV);
      glp_set_col_bnds (prob, col+i, GLP_FR, 0.0, 0.0);
      if (mig_objective == MIG_TOTAL_Z)
	glp_set_obj_coef (prob, col+i, 1.0);
      else
	glp_set_obj_coef (prob, col+i, 0.0);
    }
    col += 2*g->nedges;
  }
  
  /* Build symmetric edge index */
  int *sym_edge = malloc (2*g->nedges * sizeof (int));
  for (int v1 = 0; v1 < g->nvtxs; v1++) {
    for (int e1 = g->xadj[v1]; e1 < g->xadj[v1+1]; e1++) {
      int v2 = g->adjncy[e1];
      for (int e2 = g->xadj[v2]; e2 < g->xadj[v2+1]; e2++) 
	if (g->adjncy[e2] == v1) {
	  sym_edge[e1] = e2;
	  break;
	}
    }
  }
  
  /* Constraints */
  int ne = 4 * g->nedges;
  if (mig_objective & MIG_MAX)
    ne += 4 * g->nedges + g->nvtxs;
  if (mig_objective & MIG_MESSAGES)
    ne += 8 * g->nedges;
  /* IJV or coord sparse matrix format */
  int *itab = malloc (ne * sizeof (int)),
      *jtab = malloc (ne * sizeof (int));
  double *vtab = malloc (ne * sizeof (double));
  int n = 0;
  for (int v = 0; v < g->nvtxs; v++) {
    for (int e = g->xadj[v]; e < g->xadj[v+1]; e++) {
      itab[n] = itab[n+1] = 1 + v;
      jtab[n] = 1 + e;
      jtab[n+1] = 1 + sym_edge[e];
      vtab[n] = 1.0;
      vtab[n+1] = -1.0;
      n += 2;
    }
  }
  if (mig_objective & MIG_MAX) {
    for (int v = 0; v < g->nvtxs; v++) {
      for (int e = g->xadj[v]; e < g->xadj[v+1]; e++) {
	itab[n] = itab[n+1] = 1 + g->nvtxs + v;
	if (mig_objective & MIG_VOLUME) {
	  jtab[n] = 1 + e;		/* edge volume */
	  jtab[n+1] = 1 + sym_edge[e];
	}
	else {
	  jtab[n] = 1 + 2*g->nedges + 1 + e;	/* edge message (binary var) */
	  jtab[n+1] = 1 + 2*g->nedges + 1 + sym_edge[e];
	}
	vtab[n] = 1.0;
	vtab[n+1] = 1.0;
	n += 2;
      }
      itab[n] = 1 + g->nvtxs + v;
      jtab[n] = 1 + 2*g->nedges; /* max */
      vtab[n] = -1.0;
      n++;
    }
  }
  if (mig_objective & MIG_MESSAGES) {
    int row_offset = 0, col_offset = 0; /* offset due to max vars (if they exist) */
    if (mig_objective & MIG_MAX) {
      row_offset = g->nvtxs;
      col_offset = 1;
    }
    for (int v = 0; v < g->nvtxs; v++)
      for (int e = g->xadj[v]; e < g->xadj[v+1]; e++) {
	/* a_ij = (0) e_ij - (1) x_ij
	 * b_ij = (2) e_ij - (3) W * x_ij */
	itab[n] = itab[n+1] = 1 + g->nvtxs + row_offset + 2*e; /* a_ij */
	itab[n+2] = itab[n+3] = 1 + g->nvtxs + row_offset + 2*e + 1; /* b_ij */
	jtab[n] = jtab[n+2] = 1 + e; /* e_ij */
	jtab[n+1] = jtab[n+3] = 1 + 2*g->nedges + col_offset + e; /* x_ij */
	vtab[n] = vtab[n+2] = 1.0;
	vtab[n+1] = -1.0;
	vtab[n+3] = -total_wgt;
	n += 4;
      }
  }
  assert (n == ne);

#ifdef DEBUG /* print constraints */
  int current = 0;
  for (int i = 0; i < ne; i++) {
    if (itab[i] != current) {
      current = itab[i];
      fprintf (stderr, "\n");
      printVarName (stderr, g, itab[i], 0, mig_objective);
      fprintf (stderr, " =");
    }
    fprintf (stderr, " + %.0lf ", vtab[i]);
    printVarName (stderr, g, jtab[i], 1, mig_objective);
  }
  fprintf (stderr, "\n");
#endif
  
  /* Solve */
  glp_load_matrix (prob, ne, itab-1, jtab-1, vtab-1);
  glp_simplex (prob, NULL);
  
  /* Fill mig matrix */
  for (int i = 0; i < g->nvtxs; i++)
    for (int j = 0; j < g->nvtxs; j++)
      migmat[i*g->nvtxs + j] = 0;
  for (int v1 = 0; v1 < g->nvtxs; v1++)
    for (int e = g->xadj[v1]; e < g->xadj[v1+1]; e++) {
      int v2 = g->adjncy[e];
      int w = glp_get_col_prim (prob, 1+e);
      if (w > 0) {
	migmat[v1*g->nvtxs + v2] = -w;
	migmat[v2*g->nvtxs + v1] = w;
      }
    }

#ifdef DEBUG /* Print matrix */
  for (int i = 0; i < g->nvtxs; i++) {
    for (int j = 0; j < g->nvtxs; j++)
      fprintf (stderr, "% 8d", migmat[i*g->nvtxs + j]);
    fprintf (stderr, "\n");
  }
#endif

  /* Free mem */
  free (itab);
  free (jtab);
  free (vtab);
  free (sym_edge);
  glp_delete_prob(prob);
  glp_free_env();

#endif

}


/* *********************************************************** */
