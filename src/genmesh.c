#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <math.h>
#include <time.h>

#include "libgraph.h"
#include "genmesh.h"

/* *********************************************************** */

#define INDEX2D(I,J,DX,DY) ((J)*(DX)+(I))
#define INDEX3D(I,J,K,DX,DY,DZ) ((K)*(DX)*(DY)+(J)*(DX)+(I))

/* *********************************************************** */

void generateCubeMesh(int dim,
		      int elem_type,
		      double cellsizeX,
		      double cellsizeY,
		      double cellsizeZ,		      
		      Mesh * m)
{

  int elmsize = ELEMENT_SIZE[elem_type];
  
  /* initialization */
  m->elm_type = elem_type;
  m->nb_nodes = (dim+1)*(dim+1)*(dim+1);
  m->nb_cells = dim*dim*dim;
  m->cells = malloc(m->nb_cells*elmsize*sizeof(int));
  m->nodes = malloc(m->nb_nodes*3*sizeof(double));
  m->nb_node_vars = 0;
  m->nb_cell_vars = 0;
  m->node_vars = NULL;
  m->cell_vars = NULL;  
  /*
   * The k-th cell, that connects nodes {a,b,c,d,e,f,g,h}   
   *
   *    e---f
   *  a---b
   *  | h | g
   *  d---c
   *
   *  lower corner : a
   *  upper corner : g
   *
   *    -y
   *    ^ 
   *    |
   *    ---> +x
   *   /
   *  -z
   */
  
  int idx = 0;
  
  /* for all cells (x,y,z) */
  for(int z = 0 ; z < dim ; z++) 
    for(int y = 0 ; y < dim ; y++) 
      for(int x = 0 ; x < dim ; x++) 
	{	
	  /* cell index */
	  int k = INDEX3D(x,y,z,dim,dim,dim); 
	  assert(k == idx);
	  /* node indices */
	  int a = INDEX3D(x,y,z,dim+1,dim+1,dim+1);
	  int b = INDEX3D(x+1,y,z,dim+1,dim+1,dim+1); 
	  int c = INDEX3D(x+1,y+1,z,dim+1,dim+1,dim+1); 
	  int d = INDEX3D(x,y+1,z,dim+1,dim+1,dim+1); 
	  int e = INDEX3D(x,y,z+1,dim+1,dim+1,dim+1);
	  int f = INDEX3D(x+1,y,z+1,dim+1,dim+1,dim+1); 
	  int g = INDEX3D(x+1,y+1,z+1,dim+1,dim+1,dim+1); 
	  int h = INDEX3D(x,y+1,z+1,dim+1,dim+1,dim+1); 
	  m->cells[elmsize*k+0] = a;
	  m->cells[elmsize*k+1] = b;
	  m->cells[elmsize*k+2] = c;
	  m->cells[elmsize*k+3] = d;
	  m->cells[elmsize*k+4] = e;
	  m->cells[elmsize*k+5] = f;
	  m->cells[elmsize*k+6] = g;
	  m->cells[elmsize*k+7] = h;
	  idx++;
	}
  
  
  cellsizeX = cellsizeX/dim;
  cellsizeY = cellsizeY/dim;
  cellsizeZ = cellsizeZ/dim;
  /* for all nodes of code  */
  for(int z = 0 ; z < dim+1 ; z++) 
    for(int y = 0 ; y < dim+1 ; y++) 
      for(int x = 0 ; x < dim+1 ; x++) {
	int k = INDEX3D(x,y,z,dim+1,dim+1,dim+1); 
	m->nodes[3*k+0] = x*cellsizeX; /* x */
	m->nodes[3*k+1] = y*cellsizeY; /* y */
	m->nodes[3*k+2] = z*cellsizeZ; /* z */
      }    
  
}
/* *********************************************************** */
