/* MxN Repart algorithm */
/* Author(s): aurelien.esnard@labri.fr */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include <limits.h>

//#define DEBUG

#include "libgraph.h"
#include "matrixcomm.h"
#include "repart.h"

// #define DRAND() (drand48())   // dont use it: different seed as rand()
#define DRAND() (rand()*1.0/RAND_MAX)


/* *********************************************************** */
/*                 AUXILIARY ROUTINES                          */
/* *********************************************************** */

/* Hypergraph repartitioning routine with fixed vertices. */
/* void repartitionHypergraphFixed(Hypergraph * hg, /\**< [in] hypergraph *\/ */
/* 				Hypergraph *comms, 	/\**< [in] Communication hypergraph. Vertices are new parts, hyperedges are old parts. *\/ */
/* 				int * oldpart,   /\**< [in] old array of partitions (of size hg->nvtxs) *\/  */
/* 				int ubfactor,    /\**< [in] unbalanced factor *\/ */
/* 				int repartmult,  /\**< [in] how much to favorize edgecut over migration (objective function is repartmult * edgecut + migration) *\/ */
/* 				int migcost,     /\**< [in] migration cost from one old partition to a new one *\/ */
/* 				int * newpart,   /\**< [out] new array of partitions (of size hg->nvtxs) *\/  */
/* 				int * edgecut    /\**< [out] cut size *\/ */
/* 				); */


/* *********************************************************** */
/* *********************************************************** */
/* *********************************************************** */
/* *********************************************************** */

void addCouplingHyperedges (Hypergraph *old,
			    int *oldpart, 	/**< [in] Old partition */
			    int oldnparts, 	/**< [in] Old number of parts */
			    int migcost, 	/**< [in] coupling hyperedges weight */
			    Hypergraph *new) {
  new->nvtxs = old->nvtxs;
  new->nhedges = old->nhedges + oldnparts;
  new->eptr = malloc ((new->nhedges + 1) * sizeof(int));
  int eindsize = old->eptr[old->nhedges] + old->nvtxs;
  new->eind = malloc (eindsize * sizeof(int));
  new->vwgts = malloc (new->nvtxs * sizeof(int));
  new->hewgts = malloc (new->nhedges * sizeof(int));
  
  /* Copy old hyperedges */
  for (int i = 0; i <= old->nhedges; i++)
    new->eptr[i] = old->eptr[i];
  for (int i = 0; i < old->eptr[old->nhedges]; i++)
    new->eind[i] = old->eind[i];
  
  /* Add coupling hyperedges */
  int k = old->eptr[old->nhedges];
  for (int i = 0; i < oldnparts; i++) {
    for (int j = 0; j < old->nvtxs; j++) {
      if (oldpart[j] == i)
	new->eind[k++] = j;
    }
    new->eptr[old->nhedges + i + 1] = k;
  }
  
  /* Vertices weights */
  if (old->vwgts != NULL) {
    for (int i = 0; i < old->nvtxs; i++)
      new->vwgts[i] = old->vwgts[i];
  }
  else {
    for (int i = 0; i < old->nvtxs; i++)
      new->vwgts[i] = 1;
  }
  
  /* Old hyperedges weights */
  if (old->hewgts != NULL) {
    for (int i = 0; i < old->nhedges; i++)
      new->hewgts[i] = old->hewgts[i];
  }
  else {
    for (int i = 0; i < old->nhedges; i++)
      new->hewgts[i] = 1;
  }
  /* Coupling hyperedges weights */
  for (int i = 0; i < oldnparts; i++)
    new->hewgts[old->nhedges + i] = migcost;
}

/* *********************************************************** */

int addFixedVerticesHG (Hypergraph *old,	
			int *oldpart, 	
			Hypergraph* comms,
			int repartmult,
			int migcost, 	
			Hypergraph *new) 
{
  // int M = comms->nhedges;
  int N = comms->nvtxs;
  /* Compute hyperedge count */
  int nbaddedhedges = 0;
  for(int v = 0; v < old->nvtxs; v++)  {
    nbaddedhedges += comms->eptr[oldpart[v]+1] - comms->eptr[oldpart[v]];
  }
  
  /* Create new hypergrah */
  new->nvtxs = old->nvtxs + N;
  new->nhedges = old->nhedges + nbaddedhedges;
  new->eptr = malloc((new->nhedges+1)*sizeof(int));
  int oldeindsize = old->eptr[old->nhedges];
  int neweindsize = oldeindsize + nbaddedhedges*2;
  new->eind = malloc(neweindsize *sizeof(int));
  new->vwgts  = malloc(new->nvtxs*sizeof(int));
  new->hewgts = malloc(new->nhedges*sizeof(int));
  
  /* eptr: copy old hyperedges */
  for(int i = 0 ; i < old->nhedges ; i++)
    new->eptr[i] = old->eptr[i];
  
  /* eptr: add new hyperedges */
  int kk = oldeindsize;
  for(int i = old->nhedges ; i < new->nhedges ; i++) {
    new->eptr[i] = kk;
    kk += 2;
  }
  new->eptr[new->nhedges] = kk; /* end */
  assert(kk == neweindsize);
  
  /* eind: copy old hyperedges */
  for(int i = 0 ; i < oldeindsize ; i++)
    new->eind[i] = old->eind[i];
  
  /* eind: add new hyperedges */
  int k = oldeindsize;
  for(int v = 0; v < old->nvtxs; v++) {
    for (int i = comms->eptr[oldpart[v]]; i < comms->eptr[oldpart[v]+1]; i++) {
      assert (old->nvtxs + comms->eind[i] < new->nvtxs);
      assert (old->nvtxs + comms->eind[i] >= 0);
      new->eind[k++] = old->nvtxs + comms->eind[i];
      new->eind[k++] = v;
    }
  }
  assert (k == neweindsize);
  
  /* vertex weights */
  
  if(old->vwgts == NULL)
    for(int i = 0 ; i < old->nvtxs ; i++)
      new->vwgts[i] = 1;
  else
    for(int i = 0 ; i < old->nvtxs ; i++)
      new->vwgts[i] =  old->vwgts[i];
  
  for(int i = 0 ; i < N ; i++)
    new->vwgts[old->nvtxs+i] = 0;
  
  /* hyperedge weights */
  
  if(old->hewgts == NULL)
    for(int i = 0 ; i < old->nhedges ; i++)
      new->hewgts[i] = repartmult;
  else
    for(int i = 0 ; i < old->nhedges ; i++)
      new->hewgts[i] =  old->hewgts[i] * repartmult;
  
  for(int i = old->nhedges; i < new->nhedges; i++)
    new->hewgts[i] = migcost;
  
  return N;
}

/* *********************************************************** */

/* void repartitionHypergraphFixed(Hypergraph * hg, */
/* 				Hypergraph * modelhg, */
/* 				int *oldpart, */
/* 				int ubfactor, */
/* 				int repartmult, */
/* 				int migcost, */
/* 				int *newpart, */
/* 				int *edgecut)  */
/* { */
  
/*   Hypergraph shg; */
/*   int nbfixedvtxs = addFixedVerticesHG (hg, oldpart, modelhg, repartmult, migcost, &shg); */
  
/*   // debug */
/*   //printHypergraph(&shg); */
  
/*   /\* re-partition it with fixed vertices... *\/ */
/*   int * snewpart =  malloc(shg.nvtxs * sizeof(int)); */
/*   for(int i = 0 ; i < hg->nvtxs; i++) */
/*     snewpart[i] = -1; /\* free vertices *\/ */
/*   for(int i = 0 ; i < nbfixedvtxs; i++) */
/*     snewpart[hg->nvtxs + i] = /\*i*\/ (int)(((double)i*(modelhg->nvtxs-1))/(nbfixedvtxs-1)+0.5); /\* special fixed vertices *\/ */
/*   partitionHypergraphFixed(&shg, modelhg->nvtxs, ubfactor, snewpart, edgecut, NULL); */
  
  
/*   /\* results *\/ */
/*   memcpy(newpart, snewpart, hg->nvtxs*sizeof(int)); */
  
/*   /\* free *\/ */
/*   free(snewpart); */
/*   freeHypergraph (&shg); */
/* } */

/* *********************************************************** */

void fixedVerticesMesh (Mesh *mesh,
			int *oldpart,
			Hypergraph *modelhg,
			Mesh *out)
{
  int i, j;
  
  int M = modelhg->nhedges;
  int N = modelhg->nvtxs;
  
  out->elm_type = LINE; /* lines */
  out->nb_nodes = M + N;
  out->nodes = calloc(3*out->nb_nodes, sizeof(double));
  out->nb_node_vars = 0;
  out->nb_cell_vars = 0;
  out->node_vars = NULL;
  out->cell_vars = NULL;
  
  int *ncells = calloc (out->nb_nodes, sizeof(int));
  
  for (i = 0; i < mesh->nb_cells; i++) {
    double G[3];
    cellCenter (mesh, i, G);
    out->nodes[3*oldpart[i]+0] += G[0];
    out->nodes[3*oldpart[i]+1] += G[1];
    ncells[oldpart[i]]++;
  }
  for (i = 0; i < M; i++) {
    if (ncells[i] != 0) {
      out->nodes[3*i+0] /= ncells[i];
      out->nodes[3*i+1] /= ncells[i];
    }
    else {
      out->nodes[3*i+0] = 0.0;
      out->nodes[3*i+1] = 0.0;
    }
    out->nodes[3*i+2] = 0.0;
  }
  
  Hypergraph dual;
  dualHypergraph (modelhg, &dual);
  
  for (j = 0; j < N; j++) {
    for (i = dual.eptr[j]; i < dual.eptr[j+1]; i++) {
      out->nodes[3*(M+j)+0] += out->nodes[3*dual.eind[i]+0];
      out->nodes[3*(M+j)+1] += out->nodes[3*dual.eind[i]+1];
    }
    out->nodes[3*(M+j)+0] /= dual.eptr[j+1] - dual.eptr[j];
    out->nodes[3*(M+j)+1] /= dual.eptr[j+1] - dual.eptr[j];
    out->nodes[3*(M+j)+2] = 1.0;
  }
  
  freeHypergraph (&dual);
  out->nb_cells = modelhg->eptr[modelhg->nhedges];
  out->cells = malloc(2*modelhg->eptr[modelhg->nhedges]*sizeof(int));
  int k = 0;
  for (i = 0; i < modelhg->nhedges; i++) {
    for (j = modelhg->eptr[i]; j < modelhg->eptr[i+1]; j++) {
      out->cells[2*k+0] = i;
      out->cells[2*k+1] = M + modelhg->eind[j];
      k++;
    }
  }
  
  int part[M + N];
  for(i = 0 ; i < M; i++)
    part[i] = i;
  for(i = 0 ; i < N; i++)
    part[M + i] = i;
  addMeshVariable (out, "part", NODE_INTEGER, 1, part);
  
  free (ncells);
}

/* *********************************************************** */

int addFixedVerticesG (Graph *old,	/**< [in] Old hypergraph */
		       int *oldpart, 	/**< [in] Old partition */
		       Hypergraph *modelhg,
		       int repartmult,
		       int migcost, 	/**< [in] New edges weight */
		       Graph *new) 	/**< [out] New hypergraph */
{
  int M = modelhg->nhedges;
  int N = modelhg->nvtxs;
  
  int nbaddededges = 0;
  for(int v = 0; v < old->nvtxs; v++)  {
    nbaddededges += modelhg->eptr[oldpart[v]+1] - modelhg->eptr[oldpart[v]];
  }
  new->nvtxs = old->nvtxs + N;
  new->nedges = old->nedges + nbaddededges;
  new->xadj = malloc ((new->nvtxs+1) * sizeof(int));
  assert(new->xadj);
  new->adjncy = malloc (2 * new->nedges * sizeof(int));
  assert(new->adjncy);
  new->vwgts = malloc (new->nvtxs * sizeof(int));
  assert(new->vwgts);
  new->ewgts = malloc (2 * new->nedges * sizeof(int));
  assert(new->ewgts);
  
  /* Add edges from regular vertices */
  int k = 0;
  for (int v = 0; v < old->nvtxs; v++) {
    new->xadj[v] = k;
    /* Copy adjncy */
    for (int i = old->xadj[v]; i < old->xadj[v+1]; i++) {
      new->adjncy[k] = old->adjncy[i];
      new->ewgts[k++] = repartmult * (old->ewgts ? old->ewgts[i] : 1);
    }
    
    /* Add new edges with new vertices */
    for (int i = modelhg->eptr[oldpart[v]]; i < modelhg->eptr[oldpart[v]+1]; i++) {
      new->adjncy[k] = old->nvtxs + modelhg->eind[i];
      new->ewgts[k++] = migcost;
    }
  }
  
  /* Add new edges from new vertices */
  int *pvtxs = calloc (M, sizeof (int));
  for (int v = 0; v < old->nvtxs; v++)
    pvtxs[oldpart[v]]++;
  new->xadj[old->nvtxs] = k;
  for (int i = 0; i < N; i++) {
    new->xadj[old->nvtxs+i+1] = new->xadj[old->nvtxs+i];
    for (int j = 0; j < M; j++)
      for (int l = modelhg->eptr[j]; l < modelhg->eptr[j+1]; l++)
	if (modelhg->eind[l] == i) {
	  new->xadj[old->nvtxs+i+1] += pvtxs[j];
	  break;
	}
    /*for (int j = floor (i * (double)oldnparts/newnparts);
      j < ceil ((i+1) * (double)oldnparts/newnparts);
      j++) {
      new->xadj[old->nvtxs+i+1] += pvtxs[j];
      }*/
  }
  int *ind = calloc (N, sizeof (int));
  for (int v = 0; v < old->nvtxs; v++) {
    for (int i = modelhg->eptr[oldpart[v]]; i < modelhg->eptr[oldpart[v]+1]; i++) {
      new->adjncy[new->xadj[old->nvtxs+modelhg->eind[i]] + ind[modelhg->eind[i]]] = v;
      new->ewgts[new->xadj[old->nvtxs+modelhg->eind[i]] + ind[modelhg->eind[i]]++] = migcost;
    }
  }
  free (pvtxs);
  free (ind);
  
  /* Weights */
  for (int v = 0; v < old->nvtxs; v++)
    new->vwgts[v] = (old->vwgts ? old->vwgts[v] : 1);
  for (int i = 0; i < N; i++)
    new->vwgts[old->nvtxs+i] = 1;
  
  return N;
}

/* *********************************************************** */

/* void projectionRepartitionHypergraph(Hypergraph * hg, */
/* 				     int nb_interedges, */
/* 				     int *interedges, */
/* 				     int oldnparts, */
/* 				     int * oldpart, */
/* 				     int ubfactor, */
/* 				     int migcost, */
/* 				     int newnparts, */
/* 				     int * newpart, */
/* 				     int * edgecut) */
/* { */
/*   int *parts = malloc (hg->nvtxs * oldnparts * sizeof(int)); */
/*   int *nparts = calloc (hg->nvtxs, sizeof(int)); */
/*   int *part_size = calloc (oldnparts, sizeof(int)); */
/*   int extra_eindsize = 0; */

/*   for (int i = 0; i < nb_interedges; i++) { */
/*     int p = oldpart[interedges[2*i]]; */
/*     int v = interedges[2*i+1]; */
/*     int j; */
/*     for (j = 0; j < nparts[v]; j++) */
/*       if (parts[v * oldnparts + j] == p) */
/* 	break; */
/*     if (j == nparts[v]) { */
/*       parts[v * oldnparts + nparts[v]++] = p; */
/*       part_size[p]++; */
/*       extra_eindsize++; */
/*     } */
/*   } */

/*   /\******************************************* */
/*    *        Using Coupling Hyperedges        * */
/*    *******************************************\/ */
/*   Hypergraph shg; */
/*   shg.nvtxs = hg->nvtxs; */
/*   shg.nhedges = hg->nhedges + oldnparts; */
/*   shg.eptr = malloc ((shg.nhedges + 1) * sizeof(int)); */
/*   int eindsize = hg->eptr[hg->nhedges] + extra_eindsize; */
/*   shg.eind = malloc (eindsize * sizeof(int)); */
/*   shg.vwgts = malloc (shg.nvtxs * sizeof(int)); */
/*   shg.hewgts = malloc (shg.nhedges * sizeof(int)); */

/*   /\* Copy old hyperedges *\/ */
/*   for (int i = 0; i <= hg->nhedges; i++) */
/*     shg.eptr[i] = hg->eptr[i]; */
/*   for (int i = 0; i < hg->eptr[hg->nhedges]; i++) */
/*     shg.eind[i] = hg->eind[i]; */

/*   /\* Add coupling hyperedges *\/ */
/*   for (int i = 0; i < oldnparts; i++) */
/*     shg.eptr[hg->nhedges+i+1] = shg.eptr[hg->nhedges+i] + part_size[i]; */
/*   int *k = calloc (oldnparts, sizeof(int)); */
/*   for (int v = 0; v < hg->nvtxs; v++) */
/*     for (int i = 0; i < nparts[v]; i++) { */
/*       int p = parts[v * oldnparts + i]; */
/*       shg.eind[shg.eptr[hg->nhedges+p] + k[p]++] = v; */
/*     } */
/*   free(k); */

/*   free (parts); */
/*   free (nparts); */
/*   free (part_size); */

/*   /\* Vertices weights *\/ */
/*   if (hg->vwgts != NULL) { */
/*     for (int i = 0; i < hg->nvtxs; i++) */
/*       shg.vwgts[i] = hg->vwgts[i]; */
/*   } */
/*   else { */
/*     for (int i = 0; i < hg->nvtxs; i++) */
/*       shg.vwgts[i] = 1; */
/*   } */

/*   /\* Old hyperedges weights *\/ */
/*   if (hg->hewgts != NULL) { */
/*     for (int i = 0; i < hg->nhedges; i++) */
/*       shg.hewgts[i] = hg->hewgts[i]; */
/*   } */
/*   else { */
/*     for (int i = 0; i < hg->nhedges; i++) */
/*       shg.hewgts[i] = 1; */
/*   } */
/*   /\* Coupling hyperedges weights *\/ */
/*   for (int i = 0; i < oldnparts; i++) */
/*     shg.hewgts[hg->nhedges + i] = migcost; */

/*   /\* debug *\/ */
/*   //printHypergraph (&shg); */
/*   partitionHypergraphR (&shg, newnparts, ubfactor, newpart, edgecut); */

/*   free (shg.eptr); */
/*   free (shg.eind); */
/*   free (shg.vwgts); */
/*   free (shg.hewgts); */
/* } */

/* *********************************************************** */
/*               REPARTITIONING ROUTINES                       */
/* *********************************************************** */

/* void repartitionHypergraph(Hypergraph * hg, */
/* 			   int oldnparts, */
/* 			   int * oldpart, */
/* 			   int ubfactor, */
/* 			   int repartmult, */
/* 			   int migcost, */
/* 			   int newnparts, */
/* 			   enum RepartScheme rscheme, */
/* 			   enum CommScheme cscheme, */
/* 			   const void *comm_options, */
/* 			   int * newpart, */
/* 			   int * edgecut, */
/* 			   int * modelmat, */
/* 			   int * realmat, */
/* 			   Mesh * mesh) */
  
/* { */
  
/*   /\******************************************* */
/*    *          Using Scratch-Remap            * */
/*    *******************************************\/ */
  
/*   if (rscheme == SCRATCHREMAP) { */
/*     assert(cscheme == UNUSED); */
/*     partitionHypergraph(hg, newnparts, ubfactor, newpart, edgecut, NULL); */
/*     if (modelmat) */
/*       memset (modelmat, 0, oldnparts * newnparts * sizeof (int)); */
/*   } */
  
/*   /\******************************************* */
/*    *        Using Coupling Hyperedges        * */
/*    *******************************************\/ */
  
/*   else if (rscheme == COUPLING_HYPEREDGES || rscheme == COUPLING_HYPEREDGES_PERFECT) { */
/*     assert(cscheme == UNUSED); */
    
/*     Hypergraph shg; */
/*     addCouplingHyperedges (hg, oldpart, oldnparts, migcost, &shg); */
    
/*     /\* debug *\/ */
/*     //printHypergraph (&shg); */
/*     if (rscheme == COUPLING_HYPEREDGES_PERFECT) */
/*       partitionSmallHypergraph (&shg, newnparts, newpart, edgecut); */
/*     else */
/*       partitionHypergraph(&shg, newnparts, ubfactor, newpart, edgecut, NULL); */
    
/*     freeHypergraph (&shg); */
/*     if (modelmat) */
/*       memset (modelmat, 0, oldnparts * newnparts * sizeof (int)); */
/*   } */
  
/*   else if (rscheme == COUPLING_HYPEREDGES_FIXEDVTXS) { /\* Hybrid COUPLING_HYPEREDGES/ZOLTAN *\/ */
/*     Hypergraph shg; */
/*     addCouplingHyperedges (hg, oldpart, oldnparts, 10000*migcost, &shg); */
    
/*     Hypergraph qhg; */
/*     quotientHypergraph (hg, oldnparts, oldpart, &qhg); */
/*     Graph quotient; */
/*     hypergraph2Graph (&qhg, &quotient); */
/*     freeHypergraph (&qhg); */
    
/*     /\* Communication Model *\/ */
/*     int _modelmat[oldnparts][newnparts]; */
/*     const int skipremap = 0; */
/*     communicationModel(oldnparts, newnparts, ubfactor, &quotient, ZOLTAN, NULL, skipremap, (int*)_modelmat); */
/*     freeGraph (&quotient); */
/*     Hypergraph modelhg; */
/*     dense2sparse((int*)_modelmat, oldnparts, newnparts, &modelhg); */
/*     repartitionHypergraphFixed (&shg, &modelhg, oldpart, ubfactor, repartmult, migcost, newpart, edgecut); */
/*     freeHypergraph (&modelhg);         */
/*     if (modelmat) memcpy(modelmat, _modelmat, oldnparts*newnparts*sizeof(int));     */
/*   } */
  
/*   /\************************************** */
/*    *        Using Fixed Vertices        * */
/*    **************************************\/ */
  
/*   else if (rscheme == FIXEDVTXS) { */
/*     assert(cscheme != UNUSED); */
/*     Hypergraph qhg; */
/*     quotientHypergraph (hg, oldnparts, oldpart, &qhg); */
/*     Graph quotient; */
/*     hypergraph2Graph (&qhg, &quotient); */
/*     freeHypergraph (&qhg); */
    
/*     /\* Communication Model *\/ */
/*     int _modelmat[oldnparts][newnparts]; */
/*     const int skipremap = 0; */
/*     communicationModel(oldnparts, newnparts, ubfactor, &quotient, cscheme, comm_options, skipremap, (int*)_modelmat); */
/*     freeGraph (&quotient); */
/*     Hypergraph modelhg; */
/*     dense2sparse((int*)_modelmat, oldnparts, newnparts, &modelhg); */
/*     repartitionHypergraphFixed (hg, &modelhg, oldpart, ubfactor, repartmult, migcost, newpart, edgecut); */
/*     freeHypergraph (&modelhg);         */
/*     if (modelmat) memcpy(modelmat, _modelmat, oldnparts*newnparts*sizeof(int));     */
/*   } */
  
/*   /\* Remapping *\/ */
/*   int *comms = calloc (oldnparts * newnparts, sizeof (int)); */
/*   for (int i = 0; i < hg->nvtxs; i++) */
/*     comms[oldpart[i]*newnparts + newpart[i]] += hg->vwgts?hg->vwgts[i]:1; */
  
/*   int *perm = malloc (newnparts * sizeof (int)); */
/*   remap (oldnparts, newnparts, comms, perm); */
/*   if (oldnparts < newnparts) { */
/*     int p = oldnparts; */
/*     for (int i = 0; i < newnparts; i++) */
/*       if (perm[i] == -1) */
/* 	perm[i] = p++; */
/*   } */
  
/*   for (int v = 0; v < hg->nvtxs; v++) */
/*     newpart[v] = perm[newpart[v]]; */
  
/*   free (perm); */
/*   free (comms); */
  
/*   /\* Real communication matrix *\/ */
/*   if (realmat) { */
/*     memset (realmat, 0, oldnparts * newnparts * sizeof (int)); */
/*     for (int i = 0; i < hg->nvtxs; i++) */
/*       realmat[oldpart[i]*newnparts + newpart[i]] += hg->vwgts?hg->vwgts[i]:1; */
/*   } */
  
/*   /\* error *\/ */
/*   else { */
/*     fprintf (stderr, "ERROR: Unknown Repartitioning Scheme!!!\n");  */
/*     exit(EXIT_FAILURE); */
/*   } */
  
  
/* } */

/* /\* *********************************************************** *\/ */

/* void repartitionGraph(Graph * g, */
/* 		      int oldnparts, */
/* 		      int * oldpart, */
/* 		      int ubfactor,    */
/* 		      int repartmult, */
/* 		      int migcost, */
/* 		      int newnparts, */
/* 		      enum RepartScheme rscheme, */
/* 		      enum CommScheme cscheme, */
/* 		      const void *comm_options, */
/* 		      int * newpart, */
/* 		      int * edgecut, */
/* 		      int * modelmat, */
/* 		      int * realmat, */
/* 		      int doremap, */
/* 		      Mesh * mesh, */
/* 		      char* env)  */
/* { */
  
/*   assert(rscheme != COUPLING_HYPEREDGES && rscheme != COUPLING_HYPEREDGES_PERFECT); */
  
/*   if (modelmat) memset (modelmat, 0, oldnparts * newnparts * sizeof (int)); */
  
/*   int *_modelmat = NULL; */
  
/*   /\******************************************* */
/*    *          Using Scratch-Remap            * */
/*    *******************************************\/ */
  
/*   if (rscheme == SCRATCHREMAP) { */
/*     assert(cscheme == UNUSED); */
/*     partitionGraph (g, newnparts, ubfactor, 0, newpart, edgecut, NULL, env); */
/*   } */
  
/*   /\******************************************* */
/*    *          Using Diffusion                * */
/*    *******************************************\/ */
  
/*   else if (rscheme == DIFFUSION || rscheme == DIFFUSION_MM) { */
/*     assert(cscheme != UNUSED); */
    
/*     Graph quotient; */
/*     quotientGraph (g, oldnparts, oldpart, &quotient); */
    
/*     /\* Communication model *\/ */
/*     _modelmat = calloc (oldnparts * newnparts, sizeof (int)); */
/*     const int skipremap = 0; */
/*     communicationModel(oldnparts, newnparts, ubfactor, &quotient, cscheme, comm_options, skipremap, _modelmat); */
/*     freeGraph (&quotient); */
    
/*     /\* diffusion *\/ */
/*     int multimove = 0; // if 0, move vertices only one time during diffusion */
/*     if (rscheme == DIFFUSION_MM)  multimove = 1; */
/*     memcpy(newpart, oldpart, g->nvtxs*sizeof(int)); */
/*     if(oldnparts ==  newnparts) diffusion(g, oldnparts, ubfactor, multimove, _modelmat, newpart, edgecut, mesh); */
/*     // else diffusionMxN(g, oldnparts, newnparts, ubfactor, multimove, NO_SEEDS, _modelmat, newpart, edgecut, mesh);     */
/*     // else diffusionMxN(g, oldnparts, newnparts, ubfactor, multimove, STATIC_SEEDS, _modelmat, newpart, edgecut, mesh);     */
/*     // maria else diffusionMxN(g, oldnparts, newnparts, ubfactor, multimove, DYNAMIC_SEEDS, _modelmat, newpart, edgecut, mesh);     */
/*   }   */
  
/*   /\************************************** */
/*    *        Using Fixed Vertices        * */
/*    **************************************\/ */
  
/*   else if (rscheme == FIXEDVTXS) { */
/*     assert(cscheme != UNUSED); */
    
/*     Graph quotient; */
/*     quotientGraph (g, oldnparts, oldpart, &quotient); */

/* #ifdef DEBUG */
/*     printGraph(&quotient); */
/* #endif */
    
/*     /\* Communication model *\/ */
/*     _modelmat = calloc (oldnparts * newnparts, sizeof (int)); */
/*     const int skipremap = 0; */
/*     communicationModel(oldnparts, newnparts, ubfactor, &quotient, cscheme, comm_options, skipremap, (int*)_modelmat);  */

/*     /\* check modelmat *\/ */
/* #ifdef DEBUG */
/*     printf("Repart Model:\n"); */
/*     printCommMatrix ((int*)_modelmat, oldnparts, newnparts); */
/* #endif     */

/*     Hypergraph modelhg; */
/*     dense2sparse ((int*)_modelmat, oldnparts, newnparts, &modelhg); */
/* #ifdef DEBUG */
/*     printHypergraph(&modelhg); */
/* #endif */
    
/*     /\* create enriched graph for skewed partitioning *\/ */
/*     Graph g2; */
/*     g2.nvtxs = g->nvtxs + newnparts;     */
/*     addFixedVerticesG (g, oldpart, &modelhg, repartmult, migcost, &g2); */
/*     assert(g->nvtxs + newnparts == g2.nvtxs); */

/*     int *p = malloc (g2.nvtxs * sizeof(int)); */
/*     for (int i = 0; i < g->nvtxs; i++) p[i] = -1; */
/*     for (int i = 0; i < newnparts; i++) p[g->nvtxs + i] = i; */
/*     partitionGraph(&g2, newnparts, ubfactor, 1, p, edgecut, NULL, env); */
/*     memcpy (newpart, p, g->nvtxs * sizeof(int)); */
    
/*     /\* free mem *\/ */
/*     free (p); */
/*     freeGraph (&quotient); */
/*     freeGraph (&g2);     */
/*     freeHypergraph (&modelhg); */
/*   } */
  
/*   /\* error *\/ */
/*   else { */
/*     fprintf (stderr, "ERROR: Unknown Repartitioning Scheme!!!\n");  */
/*     exit(EXIT_FAILURE);     */
/*   }   */
  
/*   // check new partition */
/*   for (int i = 0; i < g->nvtxs; i++) */
/*     assert(newpart[i] >= 0 && newpart[i] < newnparts); */
  
/*   /\******************************************* */
/*    *             Remapping                   * */
/*    *******************************************\/ */
  
/*   /\* Remapping: second remapping on the real matrix... *\/ */
/*   if((rscheme == SCRATCHREMAP) || doremap) { */
/*     int *comms = calloc (oldnparts * newnparts, sizeof (int)); */
/*     for (int i = 0; i < g->nvtxs; i++) */
/*       comms[oldpart[i]*newnparts + newpart[i]] += g->vwgts?g->vwgts[i]:1; */
    
/*     int *perm = malloc (newnparts * sizeof (int)); */
/*     remap (oldnparts, newnparts, comms, perm); */
/*     if (oldnparts < newnparts) { */
/*       int p = oldnparts; */
/*       for (int i = 0; i < newnparts; i++) */
/* 	if (perm[i] == -1) perm[i] = p++; */
/*     } */
    
/*     /\* remap partition *\/ */
/*     for (int v = 0; v < g->nvtxs; v++) newpart[v] = perm[newpart[v]]; */
    
/*     /\* remap model matrix *\/ */
/*     if (modelmat && _modelmat) { */
/*       for (int i = 0; i < oldnparts; i++) */
/* 	for (int j = 0; j < newnparts; j++) */
/* 	  modelmat[i*newnparts + perm[j]] = _modelmat[i*newnparts + j]; */
/*     } */
/*     free (perm); */
/*     free (comms); */
/*   } */
/*   /\* no remapping *\/ */
/*   else {  */
/*     if(_modelmat && modelmat) memcpy(modelmat, _modelmat, oldnparts*newnparts*sizeof(int)); */
/*   } */
  
/*   /\* compute real matrix *\/ */
/*   if (realmat) { */
/*     memset (realmat, 0, oldnparts * newnparts * sizeof (int)); */
/*     for (int i = 0; i < g->nvtxs; i++) */
/*       realmat[oldpart[i]*newnparts + newpart[i]] += g->vwgts?g->vwgts[i]:1; */
/*   }   */
 
/*   // free */
/*   free (_modelmat);  */

/*   // compute edgecut */
/*   *edgecut = computeGraphEdgeCut(g, newnparts, newpart); */
/* } */


/* *********************************************************** */


