#ifndef __GENMESH_H__
#define __GENMESH_H__

#include "libgraph.h"

void generateCubeMesh(int dim, 
		      int elem_type,
		      double cellsizeX,
		      double cellsizeY,
		      double cellsizeZ,
		      Mesh * m);

#endif
