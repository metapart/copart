#ifndef __SCHEMES_H__
#define __SCHEMES_H__



#include <stdbool.h>

#include "starpart.h"

/* temp */
int aware(StarPart_Data * data1, 
	  StarPart_Data * data2, 
	  StarPart_Context * ctx, 
	  int * array1, int * array2, 
	  int * part1, int * part2, 
	  char * storeStr, char * debugStr);

int projrepart(StarPart_Data * dt_a, 
	       StarPart_Data * dt_b, 
	       StarPart_Context * ctx, 
	       StarPart_Data * data, 
	       int * useda, int * usedb, 
	       int * parta, int * partb,
	       int * interedges, int interlength,
	       char * storeStr, char * debugStr);

int naive(StarPart_Data * data1, 
	  StarPart_Data * data2, 
	  StarPart_Context * ctx, 
	  int * part1, int * part2, 
	  char * storeStr, char * debugStr);
#endif
