/* Diagnostic module */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>

#include "codiag.h"
#include "libgraph.h"
#include "operators.h"
/* #include "bfs.h" */
/* #include "tools.h" */
/* #include "matrixcomm.h" */

#define MIN(a,b) (((a)<=(b))?(a):(b))
#define MAX(a,b) (((a)>=(b))?(a):(b))

/* *********************************************************** */
/*                      DIAGNOSTIC ROUTINE                     */
/* *********************************************************** */


void printCopartDiagnostic(Graph * g_a,       /* [in] graph A */
			   Graph * g_b,       /* [in] graph B */
			   int k_a,      
			   int k_b,
			   int kcpl_a,      
			   int kcpl_b, 			   
			   int * part_a,
			   int * part_b,
			   int nbinteredges,
			   int * interedges,
			   int inverse
			   )			   
{
  assert(g_a && g_b);
  assert(k_a > 0 && k_b > 0);
  assert(part_a && part_b);

  CopartResults copartinfo;
  int intercomm[k_a][k_b];  
  computeCopartDiagnostic(g_a, g_b, k_a, k_b, kcpl_a, kcpl_b, part_a, part_b, 
			  nbinteredges, interedges, &copartinfo,
			  (int*)intercomm,inverse);

  printf("  - k A (coupled real/asked) = %d (%d/%d)\n", copartinfo.k_a, copartinfo.kcplreal_a, copartinfo.kcpl_a);
  printf("  - k B (coupled real/asked) = %d (%d/%d)\n", copartinfo.k_b, copartinfo.kcplreal_b, copartinfo.kcpl_b);
  printf("  - nvtxs A (coupled) = %d (%d)\n", copartinfo.nvtxs_a, copartinfo.nvtxscpl_a);
  printf("  - nvtxs B (coupled) = %d (%d)\n", copartinfo.nvtxs_b, copartinfo.nvtxscpl_b);
  printf("  - max unbalance A (coupled) = %.2f%% (%.2f%%)\n", copartinfo.maxub_a, copartinfo.maxubcpl_a);
  printf("  - max unbalance B (coupled) = %.2f%% (%.2f%%)\n", copartinfo.maxub_b, copartinfo.maxubcpl_b);
  printf("  - edgecutA (coupled) = %d (%d)\n", copartinfo.edgecut_a, copartinfo.edgecutcpl_a);
  printf("  - edgecutB (coupled) = %d (%d)\n", copartinfo.edgecut_b, copartinfo.edgecutcpl_b);    

  printf("  - nb interedges = %d\n", copartinfo.nbinteredges);
  // printf("  - interedge degree A = %.2f\n", interdegreeA);
  // printf("  - interedge degree B = %.2f\n", interdegreeB);

  printf("  - totV (opt) = %d (%d)\n", copartinfo.totV, copartinfo.totVopt);  
  printf("  - maxM  = %d\n", copartinfo.maxM);
  printf("  - maxV (opt) = %d (%d)\n", copartinfo.maxV, copartinfo.maxVopt);  
  printf("  - threshold = %d\n", copartinfo.threshold);      
  printf("  - totZ (major / opt) = %d (%d / %d)\n", copartinfo.totZ, copartinfo.totZM, copartinfo.totZopt);    

  /* printf("  - max comm A / part = %d\n", maxcommA); */
  /* printf("  - max comm B / part = %d\n", maxcommB); */

  // printf ("  - intercomm table AB (m=minor, M=major):\n");
  printf ("  - intercomm table AB (%d x %d):\n", k_a, k_b);  
  for (int i = 0; i < k_a; i++) {
    // printf("\t");
    for (int j = 0; j < k_b; j++) {
      // char c = '0';
      // if(intercomm[i][j] > 0 && intercomm[i][j] <= copartinfo.threshold) c = 'm' ;
      // else if(intercomm[i][j] > copartinfo.threshold) c = 'M' ;
      // printf ("%c ", c);
      printf ("%d ", intercomm[i][j]);      
    }
    printf ("\n");
  }


  /* int kcplreal_a = copartinfo.kcplreal_a; */
  /* int kcplreal_b = copartinfo.kcplreal_b; */
  /* printf ("  - coupling comm table AB (%d x %d):\n", kcplreal_a, kcplreal_b);   */
  /* int cplcomm[kcplreal_a][kcplreal_b]; */
  /* int rows[k_a]; int cols[k_b]; */
  /* for (int i = 0; i < k_a; i++) { */
  /*   rows[i] = 0; */
  /*   for (int j = 0; j < k_b; j++) rows[i] += intercomm[i][j];       */
  /* } */
  /* for (int j = 0; j < k_b; j++) { */
  /*   cols[j] = 0; */
  /*   for (int i = 0; i < k_a; i++) cols[j] += intercomm[i][j];       */
  /* } */

  /* for (int i = 0; i < k_a; i++) { */
  /*   if(rows[i] == 0) continue; */
  /*   printf("\t"); */
  /*   for (int j = 0; j < k_b; j++) { */
  /*     if(cols[j] == 0) continue; */
  /*     printf ("%d ", intercomm[i][j]);       */
  /*   } */
  /*   printf ("\n"); */
  /* } */
  
  // printCommMatrix((int*)intercomm, k_a, k_void);
  
}

/* *********************************************************** */


int computeCopartDiagnostic(Graph * g_a,       /* [in] graph A */
			    Graph * g_b,       /* [in] graph B */
			    int k_a,      
			    int k_b,
			    int kcpl_a,      
			    int kcpl_b, 					     
			    int * part_a,
			    int * part_b,
			    int nbinteredges,
			    int * interedges,		   
			    CopartResults * copartinfo,
			    int * intercomm,
			    int inverse
			    )
{
  assert(g_a && g_b);
  assert(k_a > 0 && k_b > 0);
  assert(part_a && part_b);
  assert(copartinfo);
  assert(intercomm);  
  
  assert(interedges);
  /* *** regular phase *** */
  
  // balance constraints for A & B  
  double maxub_a = computeGraphMaxUnbalance(g_a, k_a, part_a);
  double maxub_b = computeGraphMaxUnbalance(g_b, k_b, part_b);
  
  // edgecuts A & B
  int edgecut_a = computeGraphEdgeCut(g_a, k_a, part_a);
  int edgecut_b = computeGraphEdgeCut(g_b, k_b, part_b);

  // interedges  
  int * used_a = calloc(g_a->nvtxs, sizeof(int)); assert(used_a);
  int * used_b = calloc(g_b->nvtxs, sizeof(int)); assert(used_b);

  // here I should provide the interedges that are indexed for g_a, g_b and not for rg_a, rg_b
  locate(g_a, nbinteredges, interedges, inverse, MAXLOCLEVEL, used_a);  // need to use the loclevel as copart algo
  locate(g_b, nbinteredges, interedges, (inverse ^ 1), MAXLOCLEVEL, used_b);  // need to use the loclevel as copart algo

  /* compute the restriction to "coupled graph" */
  Graph * gcpl_a, * gcpl_b;
  
  int * mapping_a = malloc(g_a->nvtxs*sizeof(int));
  int * mapping_b = malloc(g_b->nvtxs*sizeof(int));
  int * rmap_a = NULL;
  int * rmap_b = NULL;

  restriction(g_a, used_a, &gcpl_a, &rmap_a);
  restriction(g_b, used_b, &gcpl_b, &rmap_b);

  assert(gcpl_a && gcpl_b);
  assert(rmap_a && rmap_b);
  for(int i = 0; i < g_a->nvtxs ; i++) mapping_a[i] = -1;
  for(int i = 0; i < g_b->nvtxs ; i++) mapping_b[i] = -1;
  for(int i = 0 ; i < gcpl_b->nvtxs ; i++)
    mapping_b[rmap_b[i]] = i;
  for(int i = 0 ; i < gcpl_a->nvtxs ; i++)
    mapping_a[rmap_a[i]] = i; 

  int nvtxscpl_a = 0; int nvtxscpl_b = 0;    // nb vtxs in coupling subgraph (maxloclevel = MAXLOCLEVEL)
  int nvtxscpl_a0 = 0; int nvtxscpl_b0 = 0;  // nb vtxs in coupling subgraph that are incident to interedges (maxloclevel = 0)
  
  for(int i = 0 ; i < g_a->nvtxs ; i++) { if(used_a[i] > 0) nvtxscpl_a++; if(used_a[i] == 1) nvtxscpl_a0++; }
  for(int i = 0 ; i < g_b->nvtxs ; i++) { if(used_b[i] > 0) nvtxscpl_b++; if(used_b[i] == 1) nvtxscpl_b0++; }
  assert(nvtxscpl_a0 <= nvtxscpl_a); assert(nvtxscpl_b0 <= nvtxscpl_b);
  assert(gcpl_a->nvtxs == nvtxscpl_a); assert(gcpl_b->nvtxs == nvtxscpl_b);
  
  int * partcpl_a = malloc(nvtxscpl_a*sizeof(int));
  int * partcpl_b = malloc(nvtxscpl_b*sizeof(int));

  for(int i = 0 ; i < g_a->nvtxs ; i++)
    if(mapping_a[i] != -1) partcpl_a[mapping_a[i]] = part_a[i];
  for(int i = 0 ; i < g_b->nvtxs ; i++)
    if(mapping_b[i] != -1) partcpl_b[mapping_b[i]] = part_b[i];
    
  /* *** coupling phase *** */  
  
  // double interdegree_a = (double)nbinteredges / nvtxscpl_a;
  // double interdegree_b = (double)nbinteredges / nvtxscpl_b;  

  // intercomm  A->B
  assert(intercomm);
  computeIntercomm(g_a->nvtxs, k_a, part_a, k_b,  part_b, nbinteredges, interedges, 0, intercomm);
  int totZ = 0;
  for (int i = 0; i < k_a; i++)
    for (int j = 0; j < k_b; j++)
      if(intercomm[i*k_b+j] > 0) totZ++;

  int maxM = 0;
  for (int i = 0; i < k_a; i++)
    for (int j = 0; j < k_b; j++)
      if(intercomm[i*k_b+j] > maxM)
	maxM = intercomm[i*k_b+j];

  int maxS = 0; // A -> B
  for (int i = 0; i < k_a; i++) {
    int volS = 0;
    for (int j = 0; j < k_b; j++)
      volS += intercomm[i*k_b+j];
    if(volS > maxS) maxS = volS;
  }

  int maxR = 0; // A <- B
  for (int j = 0; j < k_b; j++) {
    int volR = 0;
    for (int i = 0; i < k_a; i++) 
      volR += intercomm[i*k_b+j];
    if(volR > maxR) maxR = volR;
  }  

  int totV = 0;
  for (int i = 0; i < k_a; i++)
    for (int j = 0; j < k_b; j++)
      if(intercomm[i*k_b+j] > 0)
	totV += intercomm[i*k_b+j];

  FILE * file = fopen("procsAInCpl.txt", "w");
  assert(file != NULL); 
 
  int kcplreal_a = 0, kcplreal_b = 0;
  // int maxcommA = 0;
  for (int i = 0; i < k_a; i++) {
    int x = 0;
    for (int j = 0; j < k_b; j++) if(intercomm[i*k_b+j] > 0) x++;
    // if(x > maxcommA) maxcommA = x;
    if(x > 0) { kcplreal_a++; fprintf(file, "%d\n", i + 1); } // + 1 for the fortran format
  }
  fclose(file);
  
  file = fopen("procsBInCpl.txt", "w");
  assert(file != NULL);
  // int maxcommB = 0;
  for (int j = 0; j < k_b; j++) {
    int x = 0;
    for (int i = 0; i < k_a; i++) if(intercomm[i*k_b+j] > 0) x++;
    // if(x > maxcommB) maxcommB = x;
    if(x > 0) { kcplreal_b++; fprintf(file, "%d\n", j + 1); } // + 1 for the fortran format
  }  
  fclose(file);  

  // check
  // for(int i = 0 ; i < gcpl_a->nvtxs ; i++) assert(partcpl_a[i] >= 0 && partcpl_a[i] < kcpl_a);
  // for(int i = 0 ; i < gcpl_b->nvtxs ; i++) assert(partcpl_b[i] >= 0 && partcpl_b[i] < kcpl_b);  
  
  // balance constraints for A & B  
  double maxubcpl_a = computeGraphMaxUnbalance(gcpl_a, kcpl_a, partcpl_a); // or kcplreal_a ?
  double maxubcpl_b = computeGraphMaxUnbalance(gcpl_b, kcpl_b, partcpl_b); // or kcplreal_b ?

  int edgecutcpl_a = computeGraphEdgeCut(gcpl_a, kcpl_a, partcpl_a); 
  int edgecutcpl_b = computeGraphEdgeCut(gcpl_b, kcpl_b, partcpl_b); 

  int totVopt = nvtxscpl_a0; // consider only vertices in coupling subgraph that are incident to interedge
  int maxVopt = MAX(totVopt / kcpl_a, totVopt / kcpl_b);

  /* computation of number of minor messages with given threshold */
  int THRESHOLD = 5; // in %
  // int THRESHOLD = 10; // %  
  // int threshold = (int)round(THRESHOLD/100.0*maxM); // in % of max message size
  int threshold = (int)round(THRESHOLD/100.0*maxS); // in % of max volume send 
  int totZm = 0;
  for (int i = 0; i < k_a; i++)
    for (int j = 0; j < k_b; j++)
      if( intercomm[i*k_b+j] > 0 && intercomm[i*k_b+j] <= threshold) totZm++;
  int totZM = totZ - totZm;
  int totZopt = kcpl_a + kcpl_b - gcd(kcpl_a,kcpl_b);  

   /* *************** output *************** */

  copartinfo -> k_a = k_a;
  copartinfo -> k_b = k_b;  
  copartinfo -> kcpl_a = kcpl_a;
  copartinfo -> kcpl_b = kcpl_b;

  copartinfo -> kcplreal_a = kcplreal_a;
  copartinfo -> kcplreal_b = kcplreal_b;

  copartinfo -> nvtxs_a = g_a->nvtxs;
  copartinfo -> nvtxscpl_a = nvtxscpl_a;
  copartinfo -> nvtxs_b = g_b->nvtxs;
  copartinfo -> nvtxscpl_b = nvtxscpl_b;
 
  copartinfo -> nbinteredges = nbinteredges;
  /* interdegreeA */
  /* interdegreeB */  

  
  copartinfo -> maxub_a = maxub_a*100.0; // in %
  copartinfo -> maxub_b = maxub_b*100.0; // in %
  copartinfo -> maxubcpl_a = maxubcpl_a*100.0; // in %
  copartinfo -> maxubcpl_b = maxubcpl_b*100.0; // in %
  
  copartinfo -> edgecut_a = edgecut_a;
  copartinfo -> edgecut_b = edgecut_b;
  copartinfo -> edgecutcpl_a = edgecutcpl_a;
  copartinfo -> edgecutcpl_b = edgecutcpl_b;    

  copartinfo -> threshold = threshold;
  copartinfo -> totZ = totZ;
  copartinfo -> totZopt = totZopt;  
  copartinfo -> totZM = totZM;
  copartinfo -> totZm = totZm;
  copartinfo -> totV = totV;
  copartinfo -> totVopt = totVopt;  
  copartinfo -> maxM = maxM;
  // copartinfo -> maxS = maxS;
  // copartinfo -> maxR = maxR;  
  copartinfo -> maxV = MAX(maxS,maxR);
  copartinfo -> maxVopt = maxVopt;
 
  /* free mem */
  free(partcpl_a); free(partcpl_b);
  free(used_a); free(used_b);
  free(mapping_a); free(mapping_b);
  freeGraph(gcpl_a); freeGraph(gcpl_b);
  
  return 0;
}

/* *********************************************************** */

void computeIntercomm(int nvtxsA,           
		      int npartsA,  // nb total procs in A
		      int *partA,
		      int npartsB,  // nb total procs in B
		      int *partB,
		      int nbinteredges,
		      int *interedges,   
		      int invinteredges, 
		      int *intercomm) 
{
  // assert(weights == NULL); // use interedge weight or graph weight ?

  int * parts = calloc (nvtxsA * npartsB, sizeof(int));
    
  memset (intercomm, 0, npartsA * npartsB * sizeof(int));

  for (int i = 0; i < nbinteredges; i++) {
    int va = interedges[i*VALUES_IN_LINE];
    int vb = interedges[(i*VALUES_IN_LINE)+1];
    if(invinteredges) { va = interedges[(i*VALUES_IN_LINE)+1]; vb = interedges[(i*VALUES_IN_LINE)]; }    
    //int pa = partA[va];    
    int pb = partB[vb];
    parts[va*npartsB+pb]++;
  }

  // check
  int checksum = 0;
  for (int va = 0; va < nvtxsA; va++)
    for (int pb = 0; pb < npartsB; pb++)     
      checksum += parts[va*npartsB+pb];
  assert(checksum == nbinteredges);
      
  for (int va = 0; va < nvtxsA; va++) {
    int pa = partA[va];    
    for (int pb = 0; pb < npartsB; pb++) {
      if(parts[va*npartsB+pb] > 0) // one more message from pa to pb
	intercomm[pa*npartsB+pb] += 1; // (weights ? weights[va] : 1) ????
    }
  }
  
  free (parts);
}

/* *********************************************************** */
