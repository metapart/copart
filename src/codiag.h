/**
 *   @file codiag.h
 *
 */

#ifndef CODIAG_H
#define CODIAG_H

#include "libgraph.h"
#define MAXLOCLEVEL 3

/* *********************************************************** */
/*                      DIAGNOSTIC ROUTINE                     */
/* *********************************************************** */

//@{

typedef struct CopartResults_ {
  int k_a;
  int k_b;  
  int kcpl_a;
  int kcpl_b;  

  int nvtxs_a;
  int nvtxs_b;
  int nvtxscpl_a;
  int nvtxscpl_b;    
  
  int nbinteredges;

  int kcplreal_a;
  int kcplreal_b;  
  
  double maxub_a;
  double maxub_b;
  double maxubcpl_a;
  double maxubcpl_b;

  int edgecut_a;
  int edgecut_b;
  int edgecutcpl_a;
  int edgecutcpl_b;  
  
  int threshold; // thresold for minor/major messages
  int totZ; // total nb of messages
  int totZopt; // total nb of messages (optimal)
  int totZm; // nb of minor messages
  int totZM; // nb of major messages

  int totV; // total volume of comm
  int totVopt; // total volume of comm  
  int maxM; // max message volume
  // int maxS; // max send volume / proc
  // int maxR; // max recv volume / proc
  int maxV; // max send/recv volume / proc
  int maxVopt; // max send/recv volume / proc    
  int * intercomm; 
  
} CopartResults;

//@}

void printCopartDiagnostic(Graph * g_a,       /* [in] graph A */
			     Graph * g_b,       /* [in] graph B */
			     int k_a,      
			     int k_b,
			     int kcpl_a,      
			     int kcpl_b, 			   
			     int * part_a,
			     int * part_b,
			     int nbinteredges,
			     int * interedges,
			     int inverse
			     );


int computeCopartDiagnostic(Graph * g_a,       /* [in] graph A */
			      Graph * g_b,       /* [in] graph B */
			      int k_a,      
			      int k_b,
			      int kcpl_a,      
			      int kcpl_b, 					     
			      int * part_a,
			      int * part_b,
			      int nbinteredges,
			      int * interedges,		   
			      CopartResults * copartinfo,
			      int * intercomm,
			      int inverse);


void computeIntercomm(int nvtxsA,           
		      int npartsA,  // nb total procs in A
		      int *partA,
		      int npartsB,  // nb total procs in B
		      int *partB,
		      int nbinteredges,
		      int *interedges,
		      // int * weights,		      
		      int invinteredges, 
		      int *intercomm);


#endif
