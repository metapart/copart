# Copart #

Copart is a library used on top of Starpart and performs copartitioning algorithms over
state-of-art graph/hypergraph partitioning tools

### Compilation

Before to start the compilation process, you need to fulfill some requirements:

```bash
$ apt-get install cmake git libglib2.0-dev flex bison doxygen graphviz libpng-dev
```

CoPart is made up of several internal and external libraries. To
handle them properly, StarPart will use both CMake external project
and GIT submodules.

  * CMake External Project: https://cmake.org/cmake/help/v3.0/module/ExternalProject.html
  * GIT Submodules: https://git-scm.com/book/en/v2/Git-Tools-Submodules

In the standard way, the compilation process will download
and update automatically other software dependencies:

```bash
$ git clone --recursive git@gitlab.inria.fr:metapart/copart.git      # for developpers only
$ git clone  --recursive https://gitlab.inria.fr/metapart/copart.git  # for anonymous end-users
$ cd starpart
$ cmake . ; make
```

### Test

To run all test:

```bash
$ make test  # or ctest
```

If you want to see the list of available tests:

```
$ ctest -N
```

To launch a specific test with verbose output:

```
$ ctest -V -R <testname> 
```

### Misc

### Publications

  * Comparison of initial partitioning methods for multilevel direct k-way graph partitioning with fixed vertices. Maria Predari, Aurélien Esnard, Jean Roman. Parallel Computing, Elsevier, 2017.
  * A k-way Greedy Graph Partitioning with Initial Fixed Vertices for Parallel Applications. Maria Predari, Aurélien Esnard. PDP 2016.
  * Coupling-Aware Graph Partitioning Algorithms: Preliminary Study. Maria Predari, Aurélien Esnard. HIPC 2014.
  * Graph Repartitioning with both Dynamic Load and Dynamic Processor Allocation. Clément Vuchener, Aurélien Esnard. ParCo 2013.
  * Dynamic Load-Balancing with Variable Number of Processors based on Graph Repartitioning. Clément Vuchener, Aurélien Esnard. HIPC 2012.




