#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "copart.h"
#include "starpart.h"
// #include "all.h"

int usage(int argc, char* argv[])
{
  printf("usage: %s strat\n", argv[0]);
  return EXIT_FAILURE;
}


int main(int argc, char* argv[])
{
  if(argc != 2) return usage(argc, argv);
  char * strat = argv[1];
  printf("Strat: %s\n",strat);
  StarPart_Context * ctx = StarPart_newContext();
  assert(ctx);  
  // StarPart_registerAllMethods(ctx);
  // CoPart_RegisterMethods();
  CoPart_RegisterMethods(ctx);
  assert(ctx);
  StarPart_processStrat(ctx, strat);
  StarPart_freeContext(ctx);
  return EXIT_SUCCESS;
}

